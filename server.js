var config = require('./config');
var port = config.port;

var flash = require('connect-flash');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var methodOverride = require('method-override');

var emitter = require('events');
var express = require('express');
var app = express();
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(function (req, res, next) {
    // console.log('\n> GET ----> ', req.params);
    // console.log('\n> POST ----> ', req.body);
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, HEAD, OPTIONS, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', '*');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

// app.use((req, res, next) => {
//     console.log('\n> res ----> ', req);
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requeted-With, Content-Type, Accept, Authorization, RBR");
//     if (req.headers.origin) {
//         res.header('Access-Control-Allow-Origin', req.headers.origin);
//     }
//     if (req.method === 'OPTIONS') {
//         res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE");
//         return res.status(200).json({});
//     }
//     next();
// });

// app.use(function (req, res, next) {
//     // Website you wish to allow to connect
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     // Request methods you wish to allow
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//     // Request headers you wish to allow
//     res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');
//     //  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     // Pass to next layer of middleware
//     next();
// });

//configuration
app.set('view engine', 'ejs');
app.use(express.static(__dirname));
app.use(session({ secret: 'secretcodeforpassportauthentication', resave: true, saveUninitialized: true }));
app.use(flash());
app.use(express.static(__dirname + '/public'));

function haltOnTimedout(req, res, next) {
    console.log("server timeout => ", req.timedout);
    if (req.timedout) {
        console.log('time out response sending');
        res.status(500).json({ success: false, data: 'Response Time out error' });
    }
}


app.use('/employee', require('./app/routers/employee.router'));
app.use('/target', require('./app/routers/target.router'));
app.use('/targetClientWise', require('./app/routers/targetClientWise.router'));
app.use('/client', require('./app/routers/client.router'));
app.use('/costCenter', require('./app/routers/costCenter.router'));
app.use('/clientMap', require('./app/routers/clientMap.router'));
app.use('/user', require('./app/routers/user.router'));
app.use('/hrEdit', require('./app/routers/hrEdit.router'));
app.use('/domain', require('./app/routers/domain.router'));
app.use('/endClient', require('./app/routers/endClient.router'));
app.use('/vertical', require('./app/routers/vertical.router'));
app.use('/finaceData', require('./app/routers/financeData.router'));
app.use('/migration', require('./app/routers/xlsx.router'));
app.use('/rmtSpoc', require('./app/routers/rmtSpoc.router'));
app.use('/report', require('./app/routers/report.router'));
app.use('/msp', require('./app/routers/msp.router'));
app.use('/payrollTracker', require('./app/routers/payrollTracker.router'));
app.use('/recruiter',require('./app/routers/recruiter.router'))

app.all('*', function (req, res) {
    throw new Error("Bad request")
})

app.use(function (e, req, res, next) {
    // config.log("\n > error ---> ", e.message);
    res.status(400).json({ status: 'failure', error: e.message });
});

var cronJob = require('./cron/index.cron');

app.listen(port, function () {
    config.cron ? cronJob.dailyReport.start() : '';
    emitter.EventEmitter.prototype._maxListeners = Infinity;
    console.log('listening at ---> http://localhost:' + port + '/');
});

process.on('uncaughtException', function (err) {
    console.log('uncaughtException', err);
});