var mysql = require('mysql');
var moment = require('moment');
var config = require('../../config');

// Initialize pool
var pool = mysql.createPool({
    connectionLimit: 100,
    host: config.host,
    database: config.dbName,
    user: config.userName,
    password: config.password,
    debug: false,
    multipleStatements: true,
    queueLimit: 10
});

// Attempt to catch disconnects 
pool.on('connection', function (connection) {
    console.log('DB Connection established');
    connection.on('error', function (err) {
        console.error(moment.utc().tz("Asia/Kolkata").format(), 'MySQL error', err.code);
    });
    connection.on('close', function (err) {
        console.error(moment.utc().tz("Asia/Kolkata").format(), 'MySQL close', err);
    });
    connection.on('enqueue', function () {
        //console.log('conn queued');
    });
});

module.exports = pool;