var pool = require('./connection');
exports.query = function (query, callback) {
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            throw err;
        }
        connection.query(query, function (err, rows) {
            connection.release();
            if (!err) {
                callback(null, rows);
            } else if (err && err.code === 'PROTOCOL_SEQUENCE_TIMEOUT') {
                callback('DB Timeout', null);
            } else if (err) {
                callback(err, null);
            }
        });
        connection.on('error', function (err) {
            connection.release();
            throw err;
            return;
        });
    });
}