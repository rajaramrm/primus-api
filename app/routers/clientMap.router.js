var express = require('express');
var router = express.Router();

var modal = require('../modals/clientMap.modal');

router.get('/', modal.getClientMaps);
router.get('/byStatus/:status', modal.getClientMaps);
router.get('/clientNames', modal.getClientNames);
router.get('/role/:roleType', modal.getEmployeeByRole);
router.get('/costCenter/:clientId', modal.getCostCenter);
router.get('/msp/:clientId/:costCenterId', modal.getMsp);
router.get('/rmt', modal.getRmt);
router.post('/create', modal.createClientMap);
router.post('/update', modal.updateClientMap);
router.get('/getMspNames', modal.getMspNames);
router.post('/createMsp', modal.createMsp);
router.put('/updateMsp/:id', modal.updateMsp);
router.post('/getMspForExport', modal.getMspForExport);
router.post('/getClientMapByRole', modal.getClientMapByRole);
// router.get('/plhead', modal.PLHead);
// router.get('/hrspoc', modal.HrSpoc);
// router.get('/financespoc', modal.FinanceSpoc);
// router.get('/businessmanager', modal.BusinessManager);



// router.get('/:clientId', modal.costCenter);
// router.get('/dropdowns/:type', modal.select2Dropdowns);
// router.get('/getClientMapList', modal.getClientMapList);
// router.post('/getClientMapDropDownDetails', modal.getClientMapDropDownDetails);
// router.post('/createClientMap', modal.createClientMap);

router.get('/getEmpFromClintmap/:userType', modal.getEmpFromClintmap);
router.get('/getEmpFromEmployee/:userType/:PGTCODE', modal.getEmpFromEmployee);
router.get('/getClientmapByEmployee/:userType/:PGTCODE', modal.getClientmapByEmployee);
router.post('/employeeMoveToAnotherClientMapping', modal.employeeMoveToAnotherClientMapping);

module.exports = router;