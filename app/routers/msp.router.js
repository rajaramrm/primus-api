var express = require('express');
var router = express.Router();

var modal = require('../modals/msp.modal');

router.get('/getMsp', modal.getMsp);
router.get('/mspExport', modal.mspExport);
router.get('/getClientCost', modal.getClientCost);
router.post('/createMsp', modal.createMsp);
router.put('/updateMsp/:id', modal.updateMsp);
// router.get('/userTypes', modal.userTypes);
// router.get('/getUserRolePermission/:userRole', modal.getUserRolePermission);

module.exports = router;