var express = require('express');
var router = express.Router();

var modal=require('../modals/payrollTracker.modal');

router.post('/payrollTracker',modal.payrollTracker);
router.post('/createPayroll',modal.createPayroll);
//router.delete('/deletePayroll/:id', modal.deletePayroll);
router.post('/importPayrollsheet',modal.importPayrollsheet);
router.post('/updatePayrollstatus',modal.updatePayrollStatus);
router.get('/getClientCostByFinanceSpoc/:finSpoc',modal.getClientCostByFinanceSpoc);
router.post('/getNonPgtEmployee',modal.getNonPgtEmployee);
router.post('/importPgtCheck',modal.importPgtCheck);
router.post('/importPgtCodeUpdate',modal.importPgtCodeUpdate);

module.exports=router;
