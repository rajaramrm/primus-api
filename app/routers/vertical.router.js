var express = require('express');
var router = express.Router();

var modal = require('../modals/vertical.modal');

router.get('/', modal.getAllVertical);
router.post('/create', modal.createVertical);
router.put('/update/:verticalId', modal.updateVertical);
router.post('/getVerticalForExport', modal.getVerticalForExport);

module.exports = router;