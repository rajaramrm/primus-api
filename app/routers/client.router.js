var express = require('express');
var router = express.Router();

var modal = require('../modals/client.modal');

router.get('/', modal.getAll);
router.get('/getClients/:status', modal.getClients);
router.post('/createClient', modal.createClient);
router.put('/updateClient/:id', modal.updateClient);

module.exports = router;