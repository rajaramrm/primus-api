var express = require('express');
var router = express.Router();

var modal = require('../modals/targetClientWise.modal');

router.post('/', modal.getTargetClient);
router.post('/create', modal.createTargetClientWise);
router.put('/update', modal.updateTargetClientWise);
router.put('/updateStatus', modal.updateTargetClientWiseStatus);
router.delete('/delete/:id', modal.deleteTargetClientWise);
router.get('/getEmployee', modal.getEmployee);
router.get('/getClient/:employeeId/:employeeType', modal.getClient);
router.get('/:id', modal.getTargetClientById);
router.post('/getTargetAchieved', modal.getTargetAchieved);
router.post('/getRevenueTargetAchieved', modal.getRevenueTargetAchieved);
router.post('/getGpTargetAchieved', modal.getGpTargetAchieved);
router.post('/getHeadCountTargetAchieved', modal.getHeadCountTargetAchieved);
router.post('/getTargetAchievedBM', modal.getTargetAchievedBM);
router.post('/getRevenueTargetAchievedBM', modal.getRevenueTargetAchievedBM);
router.post('/getGpTargetAchievedBM', modal.getGpTargetAchievedBM);
router.post('/getHeadCountTargetAchievedBM', modal.getHeadCountTargetAchievedBM);

module.exports = router;