var express = require('express');
var router = express.Router();

var modal = require('../modals/employee.modal');

router.get('/', modal.getEmployee);
router.get('/byStatus/:status', modal.getEmpByStatus);
router.get('/checkEmpCode/:empCode/:appEmpId', modal.checkEmpCode);
router.get('/checkEmail/:emailId/:appEmpId', modal.checkEmail);
router.post('/getEmployeeForExport', modal.getEmployeeForExport);
router.get('/getRouting', modal.getRouting);
router.post('/createRouting', modal.createRouting);
router.put('/updateRouting', modal.updateRouting);
router.get('/:empId', modal.getEmployeeById);
router.post('/create', modal.createEmployee);
router.put('/update', modal.updateEmployee);
router.put('/updateStatus', modal.updateEmployeeStatus);
router.delete('/delete/:empId', modal.deleteEmployee);
router.post('/getReportManager', modal.getReportManager);
router.post('/importInternalEmpsCheck', modal.importInternalEmpsCheck);
router.post('/importInternalEmps', modal.importInternalEmps);
router.post('/importInactiveInternalEmps', modal.importInactiveInternalEmps);
module.exports = router;