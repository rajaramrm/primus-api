var express = require('express');
var router = express.Router();

var modal = require('../modals/financeData.modal');

// var multer = require('multer');
// var upload = multer();
// var upload = multer({ storage: multer.memoryStorage() });

var multer = require('multer');
// var upload = multer({dest:'uploads/'}).single("demo_image");

router.post('/attendenceTracker', modal.attendenceTracker);
router.post('/getInvoice', modal.getInvoice);
router.post('/getInvoiceSearch', modal.getInvoiceSearch);
router.post('/searchInvoice', modal.searchInvoice);
router.post('/createTimesheet', modal.createTimesheet);
// router.get('/:id', modal.getTargetById);
// router.put('/update', modal.updateTarget);
// router.put('/updateStatus', modal.updateTargetStatus);
router.delete('/deleteTimesheet/:id', modal.deleteTimesheet);
router.post('/createInvoice', modal.createInvoice);
router.post('/createTimesheetInvoice', modal.createTimesheetInvoice);
router.get('/getInvoiceByNumber/:invoiceNumber', modal.getInvoiceByNumber);
router.post('/searchMergeInvoice', modal.searchMergeInvoice);
router.post('/createMergeInvoice', modal.createMergeInvoice);
router.get('/getMergeInvoiceByNumber/:invoiceNumber', modal.getMergeInvoiceByNumber);
// router.post('/importTimesheet', multer({ dest: 'uploads/' }).single('uploads'), modal.importTimesheet);
router.post('/importTimesheet', modal.importTimesheet);
router.post('/getPermInvoice', modal.getPermInvoice);
router.post('/createPremInvoice', modal.createPremInvoice);
router.post('/getReimburement', modal.getReimburement);
router.post('/createReimburement', modal.createReimburement);
router.post('/searchPermMergeInvoice', modal.searchPermMergeInvoice);
router.post('/createPermMergeInvoice', modal.createPermMergeInvoice);
router.get('/getPermMergeInvoiceByNumber/:invoiceNumber', modal.getPermMergeInvoiceByNumber);
router.post('/importInvoice', modal.importInvoice);
router.post('/createAssignInvoice', modal.createAssignInvoice);
router.post('/searchReimbursementAssignInvoice', modal.searchReimbursementAssignInvoice);
router.post('/createReimbursementAssignInvoice', modal.createReimbursementAssignInvoice);
router.post('/checkTimesheetDateRange', modal.checkTimesheetDateRange);
router.post('/getConversionInvoice', modal.getConversionInvoice);
router.post('/createConvInvoice', modal.createConvInvoice);
router.put('/updateContractInvoice', modal.updateContractInvoice);
router.post('/createPermAssignInvoice', modal.createPermAssignInvoice);
router.post('/importPermInvoice', modal.importPermInvoice);
router.delete('/deleteContractInvoice/:id', modal.deleteContractInvoice);
router.delete('/deletePermInvoice/:id', modal.deletePermInvoice);
router.post('/attendenceTrackerByHrMasterId', modal.attendenceTrackerByHrMasterId);

module.exports = router;

