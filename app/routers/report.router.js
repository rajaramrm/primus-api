var express = require('express');
var router = express.Router();

var modal = require('../modals/report.modal');

// var multer = require('multer');
// var upload = multer();
// var upload = multer({ storage: multer.memoryStorage() });

var multer = require('multer');
// var upload = multer({dest:'uploads/'}).single("demo_image");

router.post('/searchNotInvoicedContract', modal.searchNotInvoicedContract);
router.post('/searchNotInvoicedPerm', modal.searchNotInvoicedPerm);
router.post('/downloadNotInvoicedContract', modal.downloadNotInvoicedContract);
router.post('/downloadNotInvoicedPerm', modal.downloadNotInvoicedPerm);
router.post('/projectedSummary', modal.projectedSummary);
router.get('/getPLs', modal.getPLs);
router.post('/getBMs', modal.getBMs);
router.post('/getBMsOnly', modal.getBMsOnly);
router.post('/getSBMsOnly', modal.getSBMsOnly);
router.post('/netAddition', modal.netAdditionQ);
router.post('/searchHrReportData', modal.searchHrReportData);
router.post('/searchMonthlyInvoices', modal.searchMonthlyInvoices);
router.post('/getVerticalsReport', modal.getVerticalsReportNew);
router.post('/getAbscondTerminated', modal.getAbscondTerminated);
router.put('/updateOnboardingEmp/:hrMasterId', modal.updateOnboardingEmp);
router.post('/getMonthlyBillingReport', modal.getMonthlyBillingReport);
router.post('/getOfferPipelineReport', modal.getOfferPipelineReport);
router.post('/getSelect2ClientCost', modal.getSelect2ClientCost);
router.post('/genHrReportData', modal.genHrReportData);
// router.post('/dailyHrReportData', modal.dailyHrReportData);
router.post('/netAdditionQ', modal.netAdditionQ);
router.post('/getGMLessthanFifteen', modal.getGMLessthanFifteen);
router.post('/getNetAdditionByPractice', modal.getNetAdditionByPractice);
router.post('/getdashboardCount',modal.getdashboardCount);

module.exports = router;