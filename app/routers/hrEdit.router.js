var express = require('express');
var router = express.Router();

var modal = require('../modals/hrEdit.modal');

router.post('/getHrEditDropDownDetails/:type', modal.getHrEditDropDownDetails);
router.post('/getClientMappingId', modal.getClientMappingId);
router.post('/create', modal.createHrEdit);
router.post('/getClientMappingInfo', modal.getClientMappingInfo);
router.post('/getPersonalInfo', modal.getPersonalInfo);
router.post('/getWorkInfo', modal.getWorkInfo);
router.post('/getBillingInfo', modal.getBillingInfo);
router.put('/updateClientMappingInfo/:hrMasterId', modal.updateClientMappingInfo);
router.put('/updatePersonalInfo/:hrMasterId', modal.updatePersonalInfo);
router.put('/updateWorkInfo/:hrMasterId', modal.updateWorkInfo);
router.put('/updateBillingInfo/:hrMasterId', modal.updateBillingInfo);
router.get('/getDropdowns', modal.getDropdowns);
router.get('/select2SkillSet', modal.select2SkillSet);
router.post('/getDomainBySkill', modal.getDomainBySkill);
router.get('/select2EndClients', modal.select2EndClients);
router.post('/getHrEditForExport', modal.getHrEditForExport);
router.post('/getHrInfo', modal.getHrInfo);
router.get('/getHrPending', modal.getHrPending);
router.put('/updateHrPending', modal.updateHrPending);
router.put('/aprrovedHr/:loginUserId', modal.aprrovedHr);
router.delete('/deleteOnBoardingDetails/:hrMasterId',modal.deleteOnBoardingDetails);
router.get('/autofillBankName/:bankIfsc',modal.autofillBankName);



router.get('/getHrConversion', modal.getHrConversion);
router.put('/updateHrConversion', modal.updateHrConversion);
router.put('/aprrovedHrConversion/:loginUserId', modal.aprrovedHrConversion);
router.get('/getOnsiteEmpEndclient/:type',modal.getOnsiteEmpEndclient);
router.put('/updateEndClient',modal.updateEndClient);
router.post('/importEndclient',modal.importEndclient);

router.get('/getHrChangesApproval', modal.getHrChangesApproval);
router.put('/approvedHrChanges/:loginUserId', modal.approvedHrChanges);
router.put('/updateApprovedHrChanges/', modal.updateApprovedHrChanges);


// hrChangesApproved

module.exports = router;