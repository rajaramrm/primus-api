var express = require('express');
var router = express.Router();

var modal = require('../modals/user.modal');

router.get('/login/:email', modal.userLogin);
router.get('/userTypes', modal.userTypes);
router.get('/getUserRolePermission/:userRole', modal.getUserRolePermission);

module.exports = router;