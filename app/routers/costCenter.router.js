var express = require('express');
var router = express.Router();

var modal = require('../modals/costCenter.modal');

router.get('/getCostCenter/:status', modal.getCostCenter);
router.post('/createCostCenter', modal.createCostCenter);
router.put('/updateCostCenter/:id', modal.updateCostCenter);

router.get('/getCostCenterInv', modal.getCostCenterInv);
router.post('/createCostCenterInv', modal.createCostCenterInv);
router.put('/updateCostCenterInv/:id', modal.updateCostCenterInv);

module.exports = router;