var express = require('express');
var router = express.Router();

var modal = require('../modals/target.modal');

router.get('/', modal.getTarget);
router.get('/:id', modal.getTargetById);
router.post('/create', modal.createTarget);
router.put('/update', modal.updateTarget);
router.put('/updateStatus', modal.updateTargetStatus);
router.delete('/delete/:id', modal.deleteTarget);

module.exports = router;