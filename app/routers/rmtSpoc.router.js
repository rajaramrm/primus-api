var express = require('express');
var router = express.Router();

var modal = require('../modals/rmtSpoc.modal');

router.get('/get', modal.getRmtSpoc);
router.get('/getClient', modal.getClient);
router.get('/getCostCenter/:clientName', modal.getCostCenter);
router.post('/create', modal.createRmt);
router.put('/update/:rmtId', modal.updateRmt);
router.post('/getVerticalForExport', modal.getVerticalForExport);

module.exports = router;