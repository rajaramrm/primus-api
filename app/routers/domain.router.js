var express = require('express');
var router = express.Router();

var modal = require('../modals/domain.modal');

router.get('/', modal.getAllDomians);
router.post('/createDomain', modal.createDomain);
router.put('/updateDomain/:domainId', modal.updateDomain);
router.get('/skillSet', modal.skillSet);
router.get('/select2SkillSet', modal.select2SkillSet);
router.post('/createSkillSet', modal.createSkillSet);
router.put('/updateSkillSet/:skillSetId', modal.updateSkillSet);
router.get('/skillSetByDomain/:domainId', modal.skillSetByDomain);
router.post('/getDomainForExport', modal.getDomainForExport);
router.post('/getSkillForExport', modal.getSkillForExport);
// router.get('/skillSetByDomain/:domainId', modal.skillSetByDomain);

module.exports = router;