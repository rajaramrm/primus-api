var express = require('express');
var router = express.Router();

var modal = require('../modals/endClient.modal');

router.get('/', modal.getAllEndClient);
router.post('/create', modal.createEndClient);
router.put('/update/:endClientId', modal.updateEndClient);
router.post('/getEndClientForExport', modal.getEndClientForExport);

module.exports = router;