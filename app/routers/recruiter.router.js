var express = require('express');
var router = express.Router();

var modal = require('../modals/recruiter.modal');

router.get('/selectSourcedBy', modal.getSelectSourcedBy);
router.post('/getRecruiterPerformance',modal.getRecruiterPerformance);
router.post('/getRecruiterPDF',modal.getRecruiterPerformancePdf);

module.exports = router;