var db = require('../db/manageDB');
var query = require('../queries/client.query');
var moment = require('moment-timezone');

exports.getAll = function (req, res) {
    db.query(query.getAll, function (error, result) {
        if (error) {
            res.status(400).send({ status: "failure", message: "Something went wrong" + error });
        } else {
            var data = { status: "success", data: result };
            res.status(200).send(data);
        }
    });
};


exports.getClients = function (req, res) {
    var params = req.params;
    var SQL = query.getClients;
    SQL = SQL.replace("isActiveRe", params.status);
    db.query(SQL, function (error, result) {
        console.log("clientquery-->", query.getClients);
        if (error) {
            res.status(400).send({ status: "failure", message: "Something went wrong" + error });
        } else {
            var data = { status: "success", data: result };
            res.status(200).send(data);
        }
    });
};

exports.createClient = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientName = removeString(data.clientName);
    var accountType = removeString(data.accountType);
    var loginUser = removeString(data.loginUser);
    var isMergeInvoice = removeString(data.isMergeInvoice, 'INT');
    var isAssignInvoice = removeString(data.isAssignInvoice, 'INT');
    var isActive = removeString(data.isActive, 'INT');
    timesNow = removeString(timesNow);

    if (!clientName || !accountType || !loginUser) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else {
        var SQL = query.createClient;
        SQL = SQL.replace('clientNameRe', clientName);
        SQL = SQL.replace(/accountTypeRe/g, accountType);
        SQL = SQL.replace(/isMergeInvoiceRe/g, isMergeInvoice);
        SQL = SQL.replace(/isAssignInvoiceRe/g, isAssignInvoice);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        SQL = SQL.replace(/isActiveRe/g, isActive);

        console.log("\n > createClient SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Created' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};


exports.updateClient = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientId = removeString(data.clientId);
    var clientNameOld = removeString(data.clientNameOld);
    var clientName = removeString(data.clientName);
    var accountType = removeString(data.accountType);
    var loginUser = removeString(data.loginUser);
    var isMergeInvoice = removeString(data.isMergeInvoice, 'INT');
    var isAssignInvoice = removeString(data.isAssignInvoice, 'INT');
    var isActive = removeString(data.isActive, 'INT');
    timesNow = removeString(timesNow);

    if (!clientName || !clientId || !loginUser || isActive == "") {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else if (isActive == 1) {
        var SQL = query.updateClient;
        SQL = SQL.replace('clientNameRe', clientName);
        SQL = SQL.replace(/isMergeInvoiceRe/g, isMergeInvoice);
        SQL = SQL.replace(/isAssignInvoiceRe/g, isAssignInvoice);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/clientIdRe/g, clientId);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        SQL = SQL.replace(/isActiveRe/g, isActive);
        console.log("\n > updateClient SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                console.log('> clientNameOld ---> ', clientNameOld);
                console.log('> clientName ---> ', clientName);
                console.log('> accountType ---> ', accountType);
                if (clientNameOld !== clientName && accountType === "'CAPTIVE'") {
                    updateEndclient(clientNameOld, clientName);
                }
                res.status(200).json({ status: 'success', data: 'Updated Successfully' });
            }
        });

    }
    else {
        var CSQL = query.checkActiveEmp;
        CSQL = CSQL.replace(/clientIdRe/g, clientId);
        console.log("\nSQL QUERY client emp active check-->", CSQL);
        db.query(CSQL, function (error, results, fields) {
            if (error) {
                console.log("\n > CheckClientActiveEmp SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                if (results.length == 0) {
                    var SQL = query.updateClient;
                    SQL = SQL.replace('clientNameRe', clientName);
                    SQL = SQL.replace(/isMergeInvoiceRe/g, isMergeInvoice);
                    SQL = SQL.replace(/isAssignInvoiceRe/g, isAssignInvoice);
                    SQL = SQL.replace(/loginIdRe/g, loginUser);
                    SQL = SQL.replace(/clientIdRe/g, clientId);
                    SQL = SQL.replace(/timesNowRe/g, timesNow);
                    SQL = SQL.replace(/isActiveRe/g, isActive);
                    console.log("\n > updateClient SQL ---> ", SQL);
                    // res.status(200).json({ status: 'success', data: 'Successfully Created' });

                    db.query(SQL, function (error, results, fields) {
                        if (error) {
                            console.log("\n > updateClient SQL Err ---> ", error.code);
                            res.status(400).json({ status: 'failure', error: error.code });
                        } else {
                            console.log('> clientNameOld ---> ', clientNameOld);
                            console.log('> clientName ---> ', clientName);
                            console.log('> accountType ---> ', accountType);
                            if (clientNameOld !== clientName && accountType === "'CAPTIVE'") {
                                updateEndclient(clientNameOld, clientName);
                            }
                            res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: 'success', error: 'Please inactivate client mappings first.' });
                }

            }
        });
    }
    function updateEndclient(clientNameOld, clientName) {
        var updateClient = "UPDATE `prms_endclient` SET `endclient_name` = clientName WHERE `endclient_name` = clientNameOld;";
        updateClient = updateClient.replace('clientName', clientName);
        updateClient = updateClient.replace('clientNameOld', clientNameOld);
        console.log('\n> updateClient ---> ', updateClient);
        db.query(updateClient, function (error, results, fields) { });
    }

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};