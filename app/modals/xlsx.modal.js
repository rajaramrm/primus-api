﻿var db = require('../db/manageDB');
var xlsx = require('xlsx');
var rootPath = require("path");
var filePath = rootPath.resolve(".") + '/app/xlsx/sample.xls';
var moment = require('moment-timezone');

exports.readXlsx = function (req, res, next) {
    let params = req.params;
    let filePath = rootPath.resolve('.') + '/app/xlsx/databases/' + params.tableName + '.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, index) => {
            if (index < 100) {
                jsonData.push(res);
            }
        });
    }
    var groupBy = function (xs, key) {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };
    var groubedByTeam = groupBy(jsonData, 'INVOICE_NUMBER');
    res.status(200).json({ status: 'success', data: groubedByTeam });
};

exports.clientMapping = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/MAPPING-2021-05-16.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'MAPPING';
    mapping(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > mapping err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > mapping success ---> ", data);
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function mapping(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));
                var clientName = removeString(data.client_name);
                var costCenter = removeString(data.cost_center);
                var hrSpoc = removeString(data.hr_spoc);
                var finSpoc = removeString(data.fin_spoc);
                var plHead = removeString(data.pl_head);
                var pgtCodePandlHead = removeString(data.pgt_code_pandl_head, 'PGTCODE');
                var bsnsMgr = removeString(data.bsns_mgr);
                var pgtCodeBsnsMgr = removeString(data.pgt_code_bsns_mgr, 'PGTCODE');
                var isActive = removeString('1');
                var createdAt = removeString(timesNow);
                var rmtSpoc = removeString(data.rmt_spoc);
                var mspName = removeString(data.msp_name);

                var SQL = "INSERT INTO `prms_client_cc_mapping` (`client_name`, `cost_center`, `hr_spoc`, `fin_spoc`, `pl_head`, `pgt_code_pandl_head`, `bsns_mgr`, `pgt_code_bsns_mgr`, `is_active`, `created_at`, `rmt_spoc`, `msp_name`) VALUES (clientNameRe, costCenterRe, hrSpocRe, finSpocRe, plHeadRe, pgtCodePandlHeadRe, bsnsMgrRe, pgtCodeBsnsMgrRe, isActiveRe, createdAtRe, rmtSpocRe, mspNameRe);";

                SQL = SQL.replace('clientNameRe', clientName);
                SQL = SQL.replace('costCenterRe', costCenter);
                SQL = SQL.replace('hrSpocRe', hrSpoc);
                SQL = SQL.replace('finSpocRe', finSpoc);
                SQL = SQL.replace('plHeadRe', plHead);
                SQL = SQL.replace('pgtCodePandlHeadRe', pgtCodePandlHead);
                SQL = SQL.replace('bsnsMgrRe', bsnsMgr);
                SQL = SQL.replace('pgtCodeBsnsMgrRe', pgtCodeBsnsMgr);
                SQL = SQL.replace('isActiveRe', isActive);
                SQL = SQL.replace('createdAtRe', createdAt);
                SQL = SQL.replace('rmtSpocRe', rmtSpoc);
                SQL = SQL.replace('mspNameRe', mspName);

                console.log('\n > SQL[' + row + '] ---> ', SQL);

                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }

                db.query(SQL, function (err, results, fields) {
                    row++;
                    if (err) {
                        console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            errorLog(tableName, err.sqlMessage, row + ' - ' + clientName);
                            insertRow(json, tableName);
                        }
                    } else {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            insertRow(json, tableName);
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.hrMaster = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/final-hr-master-32.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'HR_EDIT';
    hrmaster(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > hrmaster err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > hrmaster success ---> ", data);
            // res.status(200).json({ status: 'success', data: data });

            // console.log("\n > timesheetMigrate success ---> ", data);
            const fs = require('fs')
            const content = data;
            const filePath = rootPath.resolve('.') + '/SQL/new/hr-master.sql';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function hrmaster(jsonData, tableNames, callback) {
        try {
            var row = 0;
            hrMasterSQL = "";
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                var client_name = removeString(data.client_name);
                var cost_center = removeString(data.cost_center);
                var rmt_spoc = removeString(data.rmt_spoc);
                var hr_spoc = removeString(data.hr_spoc);
                var fin_spoc = removeString(data.fin_spoc);
                var pl_head = removeString(data.pl_head);
                var pgt_code_pand_l_head = removeString(data.pgt_code_pand_l_head, 'PGTCODE');
                var bsns_mgr = removeString(data.bsns_mgr);
                var pgt_code_bsns_mgr = removeString(data.pgt_code_bsns_mgr, 'PGTCODE');

                var getMapping = "SELECT `client_cc_mapping_id` mappingId, `client_name` clientName, `cost_center` costCenter, `hr_spoc` hrSpoc, `fin_spoc` finSpoc, `pl_head` plHead, `pgt_code_pandl_head` plHeadCode, `bsns_mgr` bsnsMgr, `pgt_code_bsns_mgr` bsnsMgrCode FROM `prms_client_cc_mapping` WHERE `client_name` = client_name_re AND `cost_center` = cost_center_re AND `hr_spoc` = hr_spoc_re AND `fin_spoc` = fin_spoc_re AND `pl_head` = pl_head_re AND `pgt_code_pandl_head` = pgt_code_pand_l_head_re AND `bsns_mgr` = bsns_mgr_re AND `pgt_code_bsns_mgr` = pgt_code_bsns_mgr_re;";

                var getRmtSpoc = "SELECT rmt_spoc_id rmtSpocId, rmt_spoc_name rmtSpocName, client_name clientName, cost_center costCenter FROM `prms_rmtspoc` WHERE client_name = client_name_re AND cost_center = cost_center_re AND rmt_spoc_name = rmt_spoc_name_re;"

                var getSQL = getMapping + getRmtSpoc;

                getSQL = getSQL.replace(/client_name_re/g, client_name);
                getSQL = getSQL.replace(/cost_center_re/g, cost_center);
                getSQL = getSQL.replace('hr_spoc_re', hr_spoc);
                getSQL = getSQL.replace('rmt_spoc_name_re', rmt_spoc);
                getSQL = getSQL.replace('fin_spoc_re', fin_spoc);
                getSQL = getSQL.replace('pl_head_re', pl_head);
                getSQL = getSQL.replace('pgt_code_pand_l_head_re', pgt_code_pand_l_head);
                getSQL = getSQL.replace('bsns_mgr_re', bsns_mgr);
                getSQL = getSQL.replace('pgt_code_bsns_mgr_re', pgt_code_bsns_mgr);

                console.log('\n > getSQL[' + row + '] ---> ', getSQL);
                var mappingId = null;
                db.query(getSQL, function (err, results, fields) {
                    if (results[0].length > 0) {
                        mappingId = results[0][0].mappingId;
                        rmtSpocId = results[1].length > 0 ? results[1][0].rmtSpocId : null;
                        insertHrMaster(mappingId, rmtSpocId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, hrMasterSQL);
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    } else {
                        insertHrMaster(mappingId, rmtSpocId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, hrMasterSQL);
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    }
                });


                function insertHrMaster(mappingId, rmtSpocId, rowData, tableNames, callbackMaster) {
                    var rmt_spoc = removeString(rowData.rmt_spoc);
                    var employee_id = removeString(rowData.employee_id, 'PGTCODE');
                    var employee_name = removeString(rowData.employee_name);
                    var personal_emailid = removeString(rowData.personal_emailid);
                    var client_empid = removeString(rowData.client_empid);
                    var msp_name = removeString(rowData.msp_name);
                    var sourced_by = removeString(rowData.sourced_by);
                    var pgt_code_sourced_by = removeString(rowData.pgt_code_sourced_by, 'PGTCODE');
                    var mobile_no = removeString(rowData.mobile_no);
                    var alternate_no = removeString(rowData.alternate_no);
                    var personal_emailid = removeString(rowData.personal_emailid);
                    var official_emailid = removeString(rowData.official_emailid);
                    var dob = removeString(rowData.dob, 'DATE_FORMAT');
                    var gender = removeString(rowData.gender);
                    var marital_status = removeString(rowData.marital_status);
                    var father_husband_name = removeString(rowData.father_husband_name);
                    var emergency_contact_name = removeString(rowData.emergency_contact_name);
                    var emergency_contact_relation = removeString(rowData.emergency_contact_relation);
                    var emergency_contact_number = removeString(rowData.emergency_contact_number);
                    var permanant_address = removeString(rowData.permanant_address);
                    var present_address = removeString(rowData.present_address);
                    var pan_number = removeString(rowData.pan_number);
                    var edu_qualification = removeString(rowData.edu_qualification);
                    var aadhar_number = removeString(rowData.aadhar_number);
                    var insurance = removeString(rowData.insurance);
                    var insurance_number = removeString(rowData.insurance_number);
                    var gratuity = removeString(rowData.gratuity);
                    var name_as_per_aadhar = removeString(rowData.name_as_per_aadhar);
                    var total_exp = removeString(rowData.total_exp);
                    var relevant_exp = removeString(rowData.relevant_exp);
                    var job_title = removeString(rowData.job_title);
                    var location = removeString(rowData.location);
                    var primary_skill = removeString(rowData.primary_skill);
                    var sec_skill1 = removeString(rowData.sec_skill1);
                    var offered_dt = removeString(rowData.offered_dt, 'DATE_FORMAT');
                    var doj = removeString(rowData.doj, 'DATE_FORMAT');
                    var employee_mode = removeString(rowData.employee_mode);
                    var status = removeString(rowData.status);
                    var relieved_date = removeString(rowData.relieved_date, 'DATE_FORMAT');
                    var resigned_month = removeString(rowData.resigned_month);
                    var relieving_reason = removeString(rowData.relieving_reason);
                    var exit_typeid = removeString(rowData.exit_typeid);
                    var employee_mode = removeString(rowData.employee_mode);
                    var bill_mode = removeString(rowData.bill_mode);
                    var billing_amt = removeString(rowData.billing_amt);
                    var bill_rate = removeString(rowData.bill_rate);
                    var salary = removeString(rowData.salary);
                    var gross_margin = removeString(rowData.gross_margin);
                    var po = removeString(rowData.po);
                    var wo = removeString(rowData.wo);
                    var deployed_bldg_address = removeString(rowData.deployed_bldg_address);
                    var pf = removeString(rowData.pf);
                    var remarks = removeString(rowData.remarks);
                    var comments = removeString(rowData.comments);
                    var created_at = removeString(timesNow);

                    var SQL = "INSERT INTO `prms_pg_hr_master` (`prms_client_cc_mapping_id`, `prms_rmtspoc_id`, `rmtspoc_name`, `employee_id`, `employee_name`, `client_empid`, `sourced_by`, `pgt_code_sourced_by`, `mobile_no`, `alternate_no`, `personal_emailid`, `official_emailid`, `dob`, `gender`, `marital_status`, `father_husband_name`, `emergency_contact_name`, `emergency_contact_relation`, `emergency_contact_number`, `permanant_address`, `present_address`, `pan_number`, `edu_qualification`, `aadhar_number`, `insurance`, `insurance_number`, `gratuity`, `name_as_per_aadhar`, `total_exp`, `relevant_exp`, `job_title`, `location`, `primary_skill`, `sec_skill1`, `offered_dt`, `doj`, `status`, `relieved_date`, `resigned_month`, `relieving_reason`, `exit_typeid`, `employee_mode`, `bill_mode`, `billing_amt`, `bill_rate`, `salary`, `gross_margin`, `po`, `wo`, `deployed_bldg_address`, `pf`, `remarks`, `comments`, `created_at`) VALUES (mapping_id_re, prms_rmtspoc_id_re, rmtspoc_name_re, employee_id_re, employee_name_re, client_empid_re,  sourced_by_re, pgt_code_sourced_by_re, mobile_no_re, alternate_no_re, personal_emailid_re, official_emailid_re, dob_re, gender_re, marital_status_re, father_husband_name_re, emergency_contact_name_re, emergency_contact_relation_re, emergency_contact_number_re, permanant_address_re, present_address_re, pan_number_re, edu_qualification_re, aadhar_number_re, insurance_re, insurance_number_re, gratuity_re, name_as_per_aadhar_re, total_exp_re, relevant_exp_re, job_title_re, location_re, primary_skill_re, sec_skill1_re, offered_dt_re, doj_re, status_re, relieved_date_re, resigned_month_re, relieving_reason_re, exit_typeid_re, employee_mode_re, bill_mode_re, billing_amt_re, bill_rate_re, salary_re, gross_margin_re, po_re, wo_re, deployed_bldg_address_re, pf_re, remarks_re, comments_re, created_at_re);";
                    SQL = SQL.replace('mapping_id_re', mappingId);
                    SQL = SQL.replace('prms_rmtspoc_id_re', removeString(rmtSpocId));
                    SQL = SQL.replace('rmtspoc_name_re', rmt_spoc);
                    SQL = SQL.replace('employee_id_re', employee_id);
                    SQL = SQL.replace('employee_name_re', employee_name);
                    SQL = SQL.replace('client_empid_re', client_empid);
                    SQL = SQL.replace('sourced_by_re', sourced_by);
                    SQL = SQL.replace('pgt_code_sourced_by_re', pgt_code_sourced_by);
                    SQL = SQL.replace('mobile_no_re', mobile_no);
                    SQL = SQL.replace('alternate_no_re', alternate_no);
                    SQL = SQL.replace('personal_emailid_re', personal_emailid);
                    SQL = SQL.replace('official_emailid_re', official_emailid);
                    SQL = SQL.replace('dob_re', dob);
                    SQL = SQL.replace('gender_re', gender);
                    SQL = SQL.replace('marital_status_re', marital_status);
                    SQL = SQL.replace('father_husband_name_re', father_husband_name);
                    SQL = SQL.replace('emergency_contact_name_re', emergency_contact_name);
                    SQL = SQL.replace('emergency_contact_relation_re', emergency_contact_relation);
                    SQL = SQL.replace('emergency_contact_number_re', emergency_contact_number);
                    SQL = SQL.replace('permanant_address_re', permanant_address);
                    SQL = SQL.replace('present_address_re', present_address);
                    SQL = SQL.replace('pan_number_re', pan_number);
                    SQL = SQL.replace('edu_qualification_re', edu_qualification);
                    SQL = SQL.replace('aadhar_number_re', aadhar_number);
                    SQL = SQL.replace('insurance_re', insurance);
                    SQL = SQL.replace('insurance_number_re', insurance_number);
                    SQL = SQL.replace('gratuity_re', gratuity);
                    SQL = SQL.replace('name_as_per_aadhar_re', name_as_per_aadhar);
                    SQL = SQL.replace('total_exp_re', total_exp);
                    SQL = SQL.replace('relevant_exp_re', relevant_exp);
                    SQL = SQL.replace('job_title_re', job_title);
                    SQL = SQL.replace('location_re', location);
                    SQL = SQL.replace('primary_skill_re', primary_skill);
                    SQL = SQL.replace('sec_skill1_re', sec_skill1);
                    SQL = SQL.replace('offered_dt_re', offered_dt);
                    SQL = SQL.replace('doj_re', doj);
                    SQL = SQL.replace('status_re', status);
                    SQL = SQL.replace('relieved_date_re', relieved_date);
                    SQL = SQL.replace('resigned_month_re', resigned_month);
                    SQL = SQL.replace('relieving_reason_re', relieving_reason);
                    SQL = SQL.replace('exit_typeid_re', exit_typeid);
                    SQL = SQL.replace('employee_mode_re', employee_mode);
                    SQL = SQL.replace('bill_mode_re', bill_mode);
                    SQL = SQL.replace('billing_amt_re', billing_amt);
                    SQL = SQL.replace('bill_rate_re', bill_rate);
                    SQL = SQL.replace('salary_re', salary);
                    SQL = SQL.replace('gross_margin_re', gross_margin);
                    SQL = SQL.replace('po_re', po);
                    SQL = SQL.replace('wo_re', wo);
                    SQL = SQL.replace('deployed_bldg_address_re', deployed_bldg_address);
                    SQL = SQL.replace('pf_re', pf);
                    SQL = SQL.replace('remarks_re', remarks);
                    SQL = SQL.replace('comments_re', comments);
                    SQL = SQL.replace('created_at_re', created_at);

                    console.log('\n > insertHrMaster[' + row + '] ---> ', SQL);
                    // setTimeout(() => { callbackMaster(); });
                    hrMasterSQL += SQL + "\n";
                    db.query(SQL, function (err, results, fields) {
                        if (err) {
                            console.log('\n > err insertHrMaster[' + row + '] err ---> ', err.sqlMessage);
                            errorLog(tableName, err.sqlMessage, row + ' - ' + employee_id);
                            setTimeout(() => { callbackMaster(); });
                        } else {
                            setTimeout(() => { callbackMaster(); });
                        }
                    });
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                data = moment(data).format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.invoiceMigration = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/timsheet-2021-05-15.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'invoice';
    invoiceMigrate(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > invoiceMigrate err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > invoiceMigrate success ---> ", data);
            const fs = require('fs')
            const content = data;
            const filePath = rootPath.resolve('.') + '/SQL/new/invoice.sql';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function invoiceMigrate(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertInvoiceSQL = "";
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));
                var invoice_number = removeString(data.INVOICE_NUMBER);
                var gst_number = removeString(data.GST_NUMBER);
                var invoice_date = removeString(data.INVOICE_DATE, 'DATE');
                var invoice_type = removeString(data.INVOICE_TYPE);
                var invoice_mode = removeString(data.INVOICE_MODE);
                var invoice_sources = removeString(data.INVOICE_SOURCES);
                var invoice_correction = removeString(data.invoice_correction);
                var created_by = removeString(data.CreatedBy);
                var created_at = removeString(data.CreatedDate, 'DATE_FORMAT');
                var updated_by = removeString(data.LAST_MODIFIED_BY);
                var updated_at = removeString(data.LAST_MODIFIED_DATE, 'DATE_FORMAT');

                var getInvoice = "SELECT * FROM prms_invoice WHERE invoice_number = invoice_number_re;";
                getInvoice = getInvoice.replace('invoice_number_re', invoice_number);

                var insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `invoice_sources`, `invoice_correction`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, invoice_sources_re, invoice_correction_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";

                insertInvoice = insertInvoice.replace('invoice_number_re', invoice_number);
                insertInvoice = insertInvoice.replace('gst_number_re', gst_number);
                insertInvoice = insertInvoice.replace('invoice_date_re', invoice_date);
                insertInvoice = insertInvoice.replace('invoice_type_re', invoice_type);
                insertInvoice = insertInvoice.replace('invoice_mode_re', invoice_mode);
                insertInvoice = insertInvoice.replace('invoice_sources_re', invoice_sources);
                insertInvoice = insertInvoice.replace('invoice_correction_re', invoice_correction);
                insertInvoice = insertInvoice.replace('created_by_re', created_by);
                insertInvoice = insertInvoice.replace('created_at_re', created_at);
                insertInvoice = insertInvoice.replace('updated_by_re', updated_by);
                insertInvoice = insertInvoice.replace('updated_at_re', updated_at);

                console.log('\n > getInvoice[' + row + '] ---> ', getInvoice);
                if (invoice_number) {
                    db.query(getInvoice, function (err, results, fields) {
                        if (results.length == 0) {
                            // console.log('\n > insertInvoice[' + row + '] ---> ', insertInvoice);
                            // row++;
                            // if (row >= json.length) {
                            //     callback(false, 'Migrated Successfully');
                            // } else {
                            //     insertRow(json, tableName);
                            // }

                            console.log('\n > insertInvoice[' + row + '] ---> ', insertInvoice);
                            insertInvoiceSQL += insertInvoice + "\n";
                            db.query(insertInvoice, function (err, results, fields) {
                                row++;
                                if (err) {
                                    console.log('\n > insertInvoice[' + row + '] err ---> ', err.sqlMessage);
                                    if (row >= json.length) {
                                        callback(false, insertInvoiceSQL);
                                    } else {
                                        errorLog(tableName, err.sqlMessage, row + ' - ' + invoice_number);
                                        insertRow(json, tableName);
                                    }
                                } else {
                                    if (row >= json.length) {
                                        callback(false, insertInvoiceSQL);
                                    } else {
                                        insertRow(json, tableName);
                                    }
                                }
                            });
                        } else {
                            row++;
                            if (row >= json.length) {
                                callback(false, insertInvoiceSQL);
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                } else {
                    row++;
                    if (row >= json.length) {
                        callback(false, insertInvoiceSQL);
                    } else {
                        insertRow(json, tableName);
                    }
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.timesheetMigration = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/final-timesheet-32.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'timesheet';
    timesheetMigrate(jsonData, tableName, function (err, data, sql) {
        if (err) {
            console.log("\n > timesheetMigrate err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > timesheetMigrate success ---> ", data);
            const fs = require('fs')

            const content = JSON.stringify(data);
            const filePath = rootPath.resolve('.') + '/SQL/new/timesheet.json';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });

            const content2 = sql;
            const filePath2 = rootPath.resolve('.') + '/SQL/new/timesheet.sql';
            fs.writeFile(filePath2, content2, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function timesheetMigrate(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var invalidMasterId = [];
            var insertTimsheetSQL = "";
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                var employee_id = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                var client_name = removeString(data.CLIENT_NAME);
                var cost_center = removeString(data.COST_CENTER);
                var employee_name = removeString(data.EMPLOYEE_NAME);
                var invoice_number = removeString(data.INVOICE_NUMBER);
                var year = removeString(data.TIMESHEET_YEAR);
                var month = removeString(data.TIMESHEET_MONTH);

                var getHrMaster = "SELECT h.hr_master_id, h.employee_name, cc.client_name, cc.cost_center FROM prms_pg_hr_master h INNER JOIN prms_client_cc_mapping cc ON cc.client_cc_mapping_id = h.prms_client_cc_mapping_id WHERE h.employee_name = employee_name_re AND cc.client_name = client_name_re AND cc.cost_center = cost_center_re;";

                // var getHrMaster = "SELECT h.hr_master_id, h.employee_name, cc.client_name, cc.cost_center FROM prms_pg_hr_master h INNER JOIN prms_client_cc_mapping cc ON cc.client_cc_mapping_id = h.prms_client_cc_mapping_id WHERE cc.client_name = client_name_re AND cc.cost_center = cost_center_re AND (h.employee_id = employee_id_re OR h.employee_name = employee_name_re);";

                var getHrMaster = "SELECT h.hr_master_id, h.employee_name, cc.client_name, cc.cost_center FROM prms_pg_hr_master h INNER JOIN prms_client_cc_mapping cc ON cc.client_cc_mapping_id = h.prms_client_cc_mapping_id WHERE cc.client_name = client_name_re AND (h.employee_id = employee_id_re OR h.employee_name = employee_name_re);";

                getHrMaster = getHrMaster.replace('client_name_re', client_name);
                getHrMaster = getHrMaster.replace('cost_center_re', cost_center);
                getHrMaster = getHrMaster.replace('employee_id_re', employee_id);
                getHrMaster = getHrMaster.replace('employee_name_re', employee_name);

                var getInvoiceNumber = "SELECT invoice_id FROM `prms_invoice` WHERE invoice_number = invoice_number_re;";
                getInvoiceNumber = getInvoiceNumber.replace('invoice_number_re', invoice_number);


                var SQL = getHrMaster + getInvoiceNumber;
                console.log('\n > getInvoiceNumber[' + row + '] ---> ', SQL);
                var hrMasterId = null;
                var invoiceId = null;
                db.query(SQL, function (err, results, fields) {
                    // if (results[0].length > 0) { 
                    //     row++;
                    //     if (row >= json.length) {
                    //         callback(false, hrMaster);
                    //     } else {
                    //         setTimeout(() => { insertRow(json, tableName); });
                    //     }
                    // } else {
                    //     row++;
                    //     if (row >= json.length) {
                    //         callback(false, hrMaster);
                    //     } else {
                    //         let obj = {
                    //             CLIENT_NAME: client_name,
                    //             COST_CENTER: cost_center,
                    //             PRMS_ID: data.PRMS_CONTR_PERSONAL_INFO_ID,
                    //             EMPLOYEE_ID: employee_id,
                    //             EMPLOYEE_NAME: employee_name,
                    //             INVOICE_MODE: invoice_number,
                    //             TIMESHEET_YEAR: year,
                    //             TIMESHEET_MONTH: month
                    //         };
                    //         hrMaster.push(obj);
                    //         setTimeout(() => { insertRow(json, tableName); });
                    //     }
                    // }
                    if (results[0].length > 0) {
                        hrMasterId = results[0][0].hr_master_id;
                        invoiceId = results[1].length > 0 ? results[1][0].invoice_id : null;
                        insertHrMaster(hrMasterId, invoiceId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, invalidMasterId, insertTimsheetSQL);
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    } else {
                        insertHrMaster(hrMasterId, invoiceId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, invalidMasterId, insertTimsheetSQL);
                            } else {
                                let obj = {
                                    CLIENT_NAME: client_name,
                                    COST_CENTER: cost_center,
                                    PRMS_ID: data.PRMS_CONTR_PERSONAL_INFO_ID,
                                    EMPLOYEE_NAME: employee_name,
                                    INVOICE_MODE: invoice_number,
                                    TIMESHEET_YEAR: year,
                                    TIMESHEET_MONTH: month
                                };
                                invalidMasterId.push(obj);
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    }
                });

                function firstAndLastDate(year, month, type) {
                    var moment = require('moment');
                    if (type === 'START' && year && month) {
                        var startDate = moment([year, month - 1]);
                        startDate = moment(startDate).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
                        return "'" + startDate + "'";
                    } else if (type === 'END' && year && month) {
                        var startDate = moment([year, month - 1]);
                        var endDate = moment(startDate).endOf('month');
                        endDate = moment(endDate).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
                        return "'" + endDate + "'";
                    } else {
                        return null;
                    }
                }
                function insertHrMaster(hrMasterId, invoiceId, rowData, tableName, callbackMaster) {
                    var hr_master_id = removeString(hrMasterId);
                    var prms_invoice_id = removeString(invoiceId);
                    var timesheet_date_from = firstAndLastDate(rowData.TIMESHEET_YEAR, rowData.TIMESHEET_MONTH, 'START');
                    var timesheet_date_to = firstAndLastDate(rowData.TIMESHEET_YEAR, rowData.TIMESHEET_MONTH, 'END');
                    var timesheet_month = removeString(rowData.TIMESHEET_MONTH);
                    var timesheet_year = removeString(rowData.TIMESHEET_YEAR);
                    var working_days = removeString(rowData.WORKING_DAYS);
                    var billing_days = removeString(rowData.BILLING_DAYS);
                    var payable_days = removeString(rowData.PAYABLE_DAYS);
                    var leaves_taken_cl = removeString(rowData.LEAVES_TAKEN_CL);
                    var leaves_taken_sl = removeString(rowData.LEAVES_TAKEN_SL);
                    var e_leaves_cl = removeString(rowData.E_LEAVES_CL);
                    var e_leaves_sl = removeString(rowData.E_LEAVES_SL);
                    var leave_bal_cl = removeString(rowData.LEAVE_BAL_CL);
                    var leave_bal_sl = removeString(rowData.LEAVE_BAL_SL);
                    var sal_date = removeString(rowData.SAL_DATE, 'DATE_FORMAT');
                    var timesheet_date = removeString(rowData.TIMESHEET_DATE, 'DATE_FORMAT');
                    var sal_month = removeString(rowData.SAL_MONTH);
                    var resource_sal = removeString(rowData.RESOURCE_SAL);
                    var amount = removeString(rowData.AMOUNT);
                    var cgst = removeString(rowData.CGST);
                    var sgst = removeString(rowData.SGST);
                    var igst = removeString(rowData.IGST);
                    var total_amount = removeString(rowData.TOTAL_AMOUNT);
                    var creditperiod = removeString(rowData.CREDITPERIOD);
                    var duedate = removeString(rowData.DUEDATE, 'DATE_FORMAT');
                    var comments = removeString(rowData.COMMENTS);
                    var remarks = removeString(rowData.REMARKS);
                    var status = removeString(rowData.Status);
                    var created_by = removeString(rowData.CreatedBy);
                    var created_at = removeString(rowData.CreatedDate, 'DATE_FORMAT');
                    var updated_by = removeString(rowData.LAST_MODIFIED_BY);
                    var updated_at = removeString(rowData.LAST_MODIFIED_DATE, 'DATE_FORMAT');
                    var salary_status = removeString(rowData.SALARY_STATUS);
                    var location = removeString(rowData.LOCATION);
                    var incentive = removeString(rowData.INCENTIVE);
                    var reimbursement = removeString(rowData.REIMBURSEMENT);
                    var additional_comments = removeString(rowData.ADDITIONAL_COMMENTS);
                    var due_amount = removeString(rowData.DUEAMOUNT);

                    var isertTimesheetSQL = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_date_from`, `timesheet_date_to`, `timesheet_month`, `timesheet_year`, `working_days`, `billing_days`, `payable_days`, `leaves_taken_cl`, `leaves_taken_sl`, `e_leaves_cl`, `e_leaves_sl`, `leave_bal_cl`, `leave_bal_sl`, `sal_date`, `timesheet_date`, `sal_month`, `resource_sal`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `creditperiod`, `duedate`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `salary_status`, `location`, `incentive`, `reimbursement`, `additional_comments`, `due_amount`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_date_from_re, timesheet_date_to_re, timesheet_month_re, timesheet_year_re, working_days_re, billing_days_re, payable_days_re, leaves_taken_cl_re, leaves_taken_sl_re, e_leaves_cl_re, e_leaves_sl_re, leave_bal_cl_re, leave_bal_sl_re, sal_date_re, timesheet_date_re, sal_month_re, resource_sal_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, creditperiod_re, duedate_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re, salary_status_re, location_re, incentive_re, reimbursement_re, additional_comments_re, due_amount_re);";

                    isertTimesheetSQL = isertTimesheetSQL.replace('hr_master_id_re', hr_master_id);
                    isertTimesheetSQL = isertTimesheetSQL.replace('prms_invoice_id_re', prms_invoice_id);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_from_re', timesheet_date_from);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_to_re', timesheet_date_to);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_month_re', timesheet_month);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_year_re', timesheet_year);
                    isertTimesheetSQL = isertTimesheetSQL.replace('working_days_re', working_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('billing_days_re', billing_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('payable_days_re', payable_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leaves_taken_cl_re', leaves_taken_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leaves_taken_sl_re', leaves_taken_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('e_leaves_cl_re', e_leaves_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('e_leaves_sl_re', e_leaves_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leave_bal_cl_re', leave_bal_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leave_bal_sl_re', leave_bal_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sal_date_re', sal_date);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_re', timesheet_date);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sal_month_re', sal_month);
                    isertTimesheetSQL = isertTimesheetSQL.replace('resource_sal_re', resource_sal);
                    isertTimesheetSQL = isertTimesheetSQL.replace('amount_re', amount);
                    isertTimesheetSQL = isertTimesheetSQL.replace('cgst_re', cgst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sgst_re', sgst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('igst_re', igst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('total_amount_re', total_amount);
                    isertTimesheetSQL = isertTimesheetSQL.replace('creditperiod_re', creditperiod);
                    isertTimesheetSQL = isertTimesheetSQL.replace('duedate_re', duedate);
                    isertTimesheetSQL = isertTimesheetSQL.replace('comments_re', comments);
                    isertTimesheetSQL = isertTimesheetSQL.replace('remarks_re', remarks);
                    isertTimesheetSQL = isertTimesheetSQL.replace('status_re', status);
                    isertTimesheetSQL = isertTimesheetSQL.replace('created_by_re', created_by);
                    isertTimesheetSQL = isertTimesheetSQL.replace('created_at_re', created_at);
                    isertTimesheetSQL = isertTimesheetSQL.replace('updated_by_re', updated_by);
                    isertTimesheetSQL = isertTimesheetSQL.replace('updated_at_re', updated_at);
                    isertTimesheetSQL = isertTimesheetSQL.replace('salary_status_re', salary_status);
                    isertTimesheetSQL = isertTimesheetSQL.replace('location_re', location);
                    isertTimesheetSQL = isertTimesheetSQL.replace('incentive_re', incentive);
                    isertTimesheetSQL = isertTimesheetSQL.replace('reimbursement_re', reimbursement);
                    isertTimesheetSQL = isertTimesheetSQL.replace('additional_comments_re', additional_comments);
                    isertTimesheetSQL = isertTimesheetSQL.replace('due_amount_re', due_amount);

                    console.log('\n > insertHrMaster[' + row + '] ---> ', isertTimesheetSQL);
                    // setTimeout(() => { callbackMaster(); });

                    if (hr_master_id) {
                        insertTimsheetSQL += isertTimesheetSQL + "\n";
                    }
                    db.query(isertTimesheetSQL, function (err, results, fields) {
                        if (err) {
                            console.log('\n > err insertHrMaster[' + row + '] err ---> ', err.sqlMessage);
                            errorLog(tableName, err.sqlMessage, row + ' - ' + invoice_number);
                            setTimeout(() => { callbackMaster(); });
                        } else {
                            setTimeout(() => { callbackMaster(); });
                        }
                    });
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.updateBankDetails = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/BANK-PAN-2021-05-21.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'hr_master_bank_detais';
    hrMaster(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > hrMaster err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > hrMaster success ---> ", data);
            const fs = require('fs')
            const content = data;
            const filePath = rootPath.resolve('.') + '/SQL/updateHrBankDetails.sql';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function hrMaster(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var hrMaster = "";
            var insertRow = function (json, tableName) {
                var data = json[row];
                // var PGTCODE = removeString(data.PGTCODE, 'PGTCODE');
                var PGTCODE = removeString(data.HR_ID);
                var DOB = removeString(data.DOB, 'DATE');
                var PAN_NO = removeString(data.PAN_NO);
                var BANK_AC_NO = removeString(data.BANK_AC_NO);
                var BANK_IFSC = removeString(data.BANK_IFSC);

                var updateHrMaster = "UPDATE prms_pg_hr_master SET SET_VALUE WHERE hr_master_id = PGTCODE_RE;";
                var SET_VALUE = '';
                if (DOB) {
                    SET_VALUE += "dob = " + DOB;
                    SET_VALUE += PAN_NO ? ", pan_number = " + PAN_NO : '';
                    SET_VALUE += BANK_AC_NO ? ", bank_ac_no = " + BANK_AC_NO : '';
                    SET_VALUE += BANK_IFSC ? ", bank_ifsc = " + BANK_IFSC : '';
                } else if (PAN_NO) {
                    SET_VALUE += "pan_number = " + PAN_NO;
                    SET_VALUE += BANK_AC_NO ? ", bank_ac_no = " + BANK_AC_NO : '';
                    SET_VALUE += BANK_IFSC ? ", bank_ifsc = " + BANK_IFSC : '';
                } else if (BANK_AC_NO) {
                    SET_VALUE += "bank_ac_no = " + BANK_AC_NO;
                    SET_VALUE += BANK_IFSC ? ", bank_ifsc = " + BANK_IFSC : '';
                } else if (BANK_IFSC) {
                    SET_VALUE += "bank_ifsc = " + BANK_IFSC;
                }

                updateHrMaster = updateHrMaster.replace('SET_VALUE', SET_VALUE);
                updateHrMaster = updateHrMaster.replace('PGTCODE_RE', PGTCODE);
                // updateHrMaster = updateHrMaster.replace('PGTCODE_RE', HR_ID);

                console.log('\n > updateHrMaster[' + row + '] ---> ', updateHrMaster);
                // row++;
                // if (row >= json.length) {
                //     callback(false, hrMaster);
                // } else {
                //     hrMaster += updateHrMaster + "\n";
                //     setTimeout(() => { insertRow(json, tableName); });
                // }
                db.query(updateHrMaster, function (err, results, fields) {
                    row++;
                    if (err) {
                        console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                        if (row >= json.length) {
                            callback(false, hrMaster);
                        } else {
                            errorLog(tableName, err.sqlMessage, PGTCODE);
                            insertRow(json, tableName);
                        }
                    } else {
                        if (row >= json.length) {
                            callback(false, hrMaster);
                        } else {
                            hrMaster += updateHrMaster + "\n";
                            insertRow(json, tableName);
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                let date = data.split("/");
                data = date[2] + "/" + date[1] + "/" + date[0];
                data = moment(data).format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.timesheetInvoiceMigration = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/timesheet-invoice-oct.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'timesheet';
    timesheetInvoiceMigrate(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > timesheetInvoiceMigrate err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > timesheetInvoiceMigrate success ---> ", data);
            const fs = require('fs')
            const content = JSON.stringify(data);
            const filePath = rootPath.resolve('.') + '/demo.txt';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function timesheetInvoiceMigrate(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var hrMaster = [];
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));
                var PGT_CODE = removeString(data.PGT_CODE, 'PGTCODE');
                var getHrMaster = "SELECT hr_master_id FROM prms_pg_hr_master WHERE employee_id = employee_id_re;";
                getHrMaster = getHrMaster.replace('employee_id_re', PGT_CODE);

                console.log('\n > getHrMaster[' + row + '] ---> ', getHrMaster);


                var hrMasterId = null;
                db.query(getHrMaster, function (err, results, fields) {
                    if (results.length > 0) {
                        hrMasterId = results[0].hr_master_id;
                        insertHrMaster(hrMasterId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    } else {
                        insertHrMaster(hrMasterId, data, tableName, function () {
                            row++;
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                                errorLog(tableName, 'Invlid Mapping', row + ' - ' + data.PGT_CODE + ' - ' + data.INVOICE_NUMBER);
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    }
                });

                function insertHrMaster(hrMasterId, rowData, tableName, callbackMaster) {
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                    var invoice_number = removeString(rowData.INVOICE_NUMBER);
                    var gst_number = removeString(rowData.GST);
                    var invoice_date = removeString(timesNow, 'DATE');
                    var invoice_type = removeString('Regular');
                    var invoice_mode = removeString('');
                    var invoice_sources = removeString('');
                    var invoice_correction = removeString('');
                    var created_by = removeString('');
                    var created_at = removeString(timesNow, 'DATE_FORMAT');
                    var updated_by = removeString('');
                    var updated_at = removeString(timesNow, 'DATE_FORMAT');

                    var insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `invoice_sources`, `invoice_correction`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, invoice_sources_re, invoice_correction_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";

                    insertInvoice = insertInvoice.replace('invoice_number_re', invoice_number);
                    insertInvoice = insertInvoice.replace('gst_number_re', gst_number);
                    insertInvoice = insertInvoice.replace('invoice_date_re', invoice_date);
                    insertInvoice = insertInvoice.replace('invoice_type_re', invoice_type);
                    insertInvoice = insertInvoice.replace('invoice_mode_re', invoice_mode);
                    insertInvoice = insertInvoice.replace('invoice_sources_re', invoice_sources);
                    insertInvoice = insertInvoice.replace('invoice_correction_re', invoice_correction);
                    insertInvoice = insertInvoice.replace('created_by_re', created_by);
                    insertInvoice = insertInvoice.replace('created_at_re', created_at);
                    insertInvoice = insertInvoice.replace('updated_by_re', updated_by);
                    insertInvoice = insertInvoice.replace('updated_at_re', updated_at);

                    console.log('\n > insertInvoice[' + row + '] ---> ', insertInvoice);
                    // setTimeout(() => { callbackMaster(); });
                    var invoiceId = null;
                    db.query(insertInvoice, function (err, results, fields) {
                        if (err) {
                            console.log('\n > err insertHrMaster[' + row + '] err ---> ', err.sqlMessage);
                            errorLog(tableName, err.sqlMessage, row + ' - ' + invoice_number);
                            setTimeout(() => { callbackMaster(); });
                        } else {
                            console.log('\n> ---> ', results.insertId);
                            invoiceId = results.insertId;
                            insertTimsheet(invoiceId, hrMasterId, rowData);
                            setTimeout(() => { callbackMaster(); });
                        }
                    });
                }

                function firstAndLastDate(year, month, type) {
                    var moment = require('moment');
                    if (type === 'START' && year && month) {
                        var startDate = moment([year, month - 1]);
                        startDate = moment(startDate).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
                        return "'" + startDate + "'";
                    } else if (type === 'END' && year && month) {
                        var startDate = moment([year, month - 1]);
                        var endDate = moment(startDate).endOf('month');
                        endDate = moment(endDate).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
                        return "'" + endDate + "'";
                    } else {
                        return null;
                    }
                }

                function insertTimsheet(invoiceId, hrMasterId, rowData) {
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                    var hr_master_id = removeString(hrMasterId);
                    var prms_invoice_id = removeString(invoiceId);
                    var timesheet_date_from = firstAndLastDate(rowData.YEAR, rowData.MONTH, 'START');
                    var timesheet_date_to = firstAndLastDate(rowData.YEAR, rowData.MONTH, 'END');
                    var timesheet_month = removeString(rowData.MONTH);
                    var timesheet_year = removeString(rowData.YEAR);
                    var working_days = removeString('0');
                    var billing_days = removeString('0');
                    var payable_days = removeString('0');
                    var leaves_taken_cl = removeString('0');
                    var leaves_taken_sl = removeString('0');
                    var e_leaves_cl = removeString('0');
                    var e_leaves_sl = removeString('0');
                    var leave_bal_cl = removeString('0');
                    var leave_bal_sl = removeString('0');
                    var sal_date = removeString(timesNow, 'DATE_FORMAT');
                    var timesheet_date = removeString(timesNow, 'DATE_FORMAT');
                    var sal_month = removeString('');
                    var resource_sal = removeString('');

                    var amount = rowData.TAX_AMOUNT ? rowData.TAX_AMOUNT : 0;
                    var cgst = Number(amount) * (9 / 100);
                    var sgst = Number(amount) * (9 / 100);
                    var igst = 0;
                    var total_amount = Number(amount) + cgst + sgst;

                    var creditperiod = removeString('');
                    var duedate = removeString(timesNow, 'DATE_FORMAT');
                    var comments = removeString('');
                    var remarks = removeString('');
                    var status = removeString('');
                    var created_by = removeString('');
                    var created_at = removeString(timesNow, 'DATE_FORMAT');
                    var updated_by = removeString('');
                    var updated_at = removeString(timesNow, 'DATE_FORMAT');
                    var salary_status = removeString('');
                    var location = removeString('');
                    var incentive = removeString('');
                    var reimbursement = removeString('');
                    var additional_comments = removeString('');
                    var due_amount = removeString(rowData.DUE_AMOUNT);

                    var isertTimesheetSQL = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_date_from`, `timesheet_date_to`, `timesheet_month`, `timesheet_year`, `working_days`, `billing_days`, `payable_days`, `leaves_taken_cl`, `leaves_taken_sl`, `e_leaves_cl`, `e_leaves_sl`, `leave_bal_cl`, `leave_bal_sl`, `sal_date`, `timesheet_date`, `sal_month`, `resource_sal`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `creditperiod`, `duedate`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`, `salary_status`, `location`, `incentive`, `reimbursement`, `additional_comments`, `due_amount`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_date_from_re, timesheet_date_to_re, timesheet_month_re, timesheet_year_re, working_days_re, billing_days_re, payable_days_re, leaves_taken_cl_re, leaves_taken_sl_re, e_leaves_cl_re, e_leaves_sl_re, leave_bal_cl_re, leave_bal_sl_re, sal_date_re, timesheet_date_re, sal_month_re, resource_sal_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, creditperiod_re, duedate_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re, salary_status_re, location_re, incentive_re, reimbursement_re, additional_comments_re, due_amount_re);";

                    isertTimesheetSQL = isertTimesheetSQL.replace('hr_master_id_re', hr_master_id);
                    isertTimesheetSQL = isertTimesheetSQL.replace('prms_invoice_id_re', prms_invoice_id);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_from_re', timesheet_date_from);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_to_re', timesheet_date_to);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_month_re', timesheet_month);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_year_re', timesheet_year);
                    isertTimesheetSQL = isertTimesheetSQL.replace('working_days_re', working_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('billing_days_re', billing_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('payable_days_re', payable_days);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leaves_taken_cl_re', leaves_taken_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leaves_taken_sl_re', leaves_taken_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('e_leaves_cl_re', e_leaves_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('e_leaves_sl_re', e_leaves_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leave_bal_cl_re', leave_bal_cl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('leave_bal_sl_re', leave_bal_sl);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sal_date_re', sal_date);
                    isertTimesheetSQL = isertTimesheetSQL.replace('timesheet_date_re', timesheet_date);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sal_month_re', sal_month);
                    isertTimesheetSQL = isertTimesheetSQL.replace('resource_sal_re', resource_sal);
                    isertTimesheetSQL = isertTimesheetSQL.replace('amount_re', amount);
                    isertTimesheetSQL = isertTimesheetSQL.replace('cgst_re', cgst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('sgst_re', sgst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('igst_re', igst);
                    isertTimesheetSQL = isertTimesheetSQL.replace('total_amount_re', total_amount);
                    isertTimesheetSQL = isertTimesheetSQL.replace('creditperiod_re', creditperiod);
                    isertTimesheetSQL = isertTimesheetSQL.replace('duedate_re', duedate);
                    isertTimesheetSQL = isertTimesheetSQL.replace('comments_re', comments);
                    isertTimesheetSQL = isertTimesheetSQL.replace('remarks_re', remarks);
                    isertTimesheetSQL = isertTimesheetSQL.replace('status_re', status);
                    isertTimesheetSQL = isertTimesheetSQL.replace('created_by_re', created_by);
                    isertTimesheetSQL = isertTimesheetSQL.replace('created_at_re', created_at);
                    isertTimesheetSQL = isertTimesheetSQL.replace('updated_by_re', updated_by);
                    isertTimesheetSQL = isertTimesheetSQL.replace('updated_at_re', updated_at);
                    isertTimesheetSQL = isertTimesheetSQL.replace('salary_status_re', salary_status);
                    isertTimesheetSQL = isertTimesheetSQL.replace('location_re', location);
                    isertTimesheetSQL = isertTimesheetSQL.replace('incentive_re', incentive);
                    isertTimesheetSQL = isertTimesheetSQL.replace('reimbursement_re', reimbursement);
                    isertTimesheetSQL = isertTimesheetSQL.replace('additional_comments_re', additional_comments);
                    isertTimesheetSQL = isertTimesheetSQL.replace('due_amount_re', due_amount);
                    console.log('\n > isertTimesheetSQL[' + row + '] ---> ', isertTimesheetSQL);
                    db.query(isertTimesheetSQL, function (err, results, fields) {
                        console.log('\n > Successfull ---> ');
                    });
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                data = moment(data).format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.migrateTables = function (req, res, next) {
    let params = req.params;
    let filePath = rootPath.resolve('.') + '/app/xlsx/databases/' + params.tableName + '.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = params.tableName;

    if (jsonData.length > 0 && tableName == 'Clients') {
        migrateClientTable(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateClientTable err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateClientTable success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateClientTable(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.ClientID);
                    var clientName = removeString(data.Name);
                    var SQL = "INSERT INTO prms_clients (client_id, name) VALUES (CLIENT_ID, CLIENT_NAME);";

                    SQL = SQL.replace('CLIENT_ID', primaryId);
                    SQL = SQL.replace('CLIENT_NAME', clientName);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames);
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'CostCenters') {
        migrateCostCenters(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateCostCenters err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateCostCenters success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateCostCenters(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.CostCenterID);
                    var clientId = removeString(data.ClientID);
                    var name = removeString(data.Name);

                    var SQL = "INSERT INTO `prms_cost_centers` (`cost_center_id`, `client_id`, `name`) VALUES (costCenterIdRe, clientIdRe, nameRe);";

                    SQL = SQL.replace('costCenterIdRe', primaryId);
                    SQL = SQL.replace('clientIdRe', clientId);
                    SQL = SQL.replace('nameRe', name);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames);
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'CLIENT_WISE_LEAVE_DETAILS') {
        migrateClientLeave(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateClientLeave err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateClientLeave success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateClientLeave(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.CLIENT_WISE_LEAVE_DETAILS);
                    var clientName = removeString(data.CLIENT_NAME);
                    var financeEl = removeString(data.FINANCE_EL);
                    var financeSl = removeString(data.FINANCE_SL);
                    var status = removeString(data.STATUS);
                    var elPlBillable = removeString(data.EL_PL_Billable);
                    var lwwPaidDetails = removeString(data.LWW_Paid_Details);
                    var mspRatePercent = removeString(data.MSP_RATE_PERCENT);
                    var mspRate = removeString(data.MSP_RATE);
                    var portalRate = removeString(data.PORTAL_RATE);
                    var vmsCwpRate = removeString(data.VMS_CWP_RATE);
                    var bgvBillable = removeString(data.BGV_Billable);
                    var bgvCost = removeString(data.BGV_COST);
                    var volumeOfDiscount = removeString(data.VOLUME_OF_DISCOUNT);
                    var financeSpoc = removeString(data.FINANCE_SPOC);
                    var hrSpoc = removeString(data.HR_SPOC);
                    var detailed = removeString(data.DETAILED);
                    var remarks = removeString(data.Remarks);
                    var finance = removeString(data.FINANCE);

                    var SQL = "INSERT INTO `prms_client_wise_leave_details` (`client_wise_leave_details_id`, `client_name`, `finance_el`, `finance_sl`, `status`, `el_pl_billable`, `lww_paid_details`, `msp_rate_percent`, `msp_rate`, `portal_rate`, `vms_cwp_rate`, `bgv_billable`, `bgv_cost`, `volume_of_discount`, `finance_spoc`, `hr_spoc`, `detailed`, `remarks`, `finance`) VALUES (clientWiseIdRe, clientNameRe, financeElRe, financeSlRe, statusRe, elPlBillableRe, lwwPaidDetailsRe, mspRatePercentRe, mspRateRe, portalRateRe, vmsCwpRateRe, bgvBillableRe, bgvCostRe, volumeOfDiscountRe, financeSpocRe, hrSpocRe, detailedRe, remarksRe, financeRe);";
                    SQL = SQL.replace('clientWiseIdRe', primaryId);
                    SQL = SQL.replace('clientNameRe', clientName);
                    SQL = SQL.replace('financeElRe', financeEl);
                    SQL = SQL.replace('financeSlRe', financeSl);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('elPlBillableRe', elPlBillable);
                    SQL = SQL.replace('lwwPaidDetailsRe', lwwPaidDetails);
                    SQL = SQL.replace('mspRatePercentRe', mspRatePercent);
                    SQL = SQL.replace('mspRateRe', mspRate);
                    SQL = SQL.replace('portalRateRe', portalRate);
                    SQL = SQL.replace('vmsCwpRateRe', vmsCwpRate);
                    SQL = SQL.replace('bgvBillableRe', bgvBillable);
                    SQL = SQL.replace('bgvCostRe', bgvCost);
                    SQL = SQL.replace('volumeOfDiscountRe', volumeOfDiscount);
                    SQL = SQL.replace('financeSpocRe', financeSpoc);
                    SQL = SQL.replace('hrSpocRe', hrSpoc);
                    SQL = SQL.replace('detailedRe', detailed);
                    SQL = SQL.replace('remarksRe', remarks);
                    SQL = SQL.replace('financeRe', finance);
                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_GRATEUP_SALARY') {
        migrateGrateupSalary(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateGrateupSalary err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateGrateupSalary success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateGrateupSalary(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_GRATEUP_SALARY_ID);
                    var personalInfoId = removeString(data.PRMS_CONTR_PERSONAL_INFO_ID);
                    var serviceMonth = removeString(data.SERVICE_MONTH);
                    var serviceYear = removeString(data.SERVICE_YEAR);
                    var grateupSalary = removeString(data.GRATEUP_SALARY);
                    var createdBy = removeString(data.CREATEDBY);
                    var createdAt = removeString(data.CREATEDDATE, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LASTMODIFIEDBY);
                    var updatedAt = removeString(data.LASTMODIFIEDDATE, 'DATE_FORMAT');

                    var SQL = "INSERT INTO `prms_grateup_salary` (`grateup_salary_id`, `prms_contr_personal_info_id`, `service_month`, `service_year`, `grateup_salary`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (salaryIdRe, personalInfoIdRe, serviceMonthRe, serviceYearRe, grateupSalaryRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe);";

                    SQL = SQL.replace('salaryIdRe', primaryId);
                    SQL = SQL.replace('personalInfoIdRe', personalInfoId);
                    SQL = SQL.replace('serviceMonthRe', serviceMonth);
                    SQL = SQL.replace('serviceYearRe', serviceYear);
                    SQL = SQL.replace('grateupSalaryRe', grateupSalary);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'NET_ADDITIONS_SUMMARY') {
        migrateNetAdditionSummary(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateNetAdditionSummary err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateNetAdditionSummary success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateNetAdditionSummary(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.NET_ADDITIONS_ID);
                    var clientName = removeString(data.CLIENT_NAME);
                    var costCenter = removeString(data.COST_CENTER);
                    var revenue = removeString(data.Revenue);
                    var grossMargin = removeString(data.Gross_margin);
                    var fyear = removeString(data.fyear);
                    var billedConsultants = removeString(data.Billed_consultants);
                    var fmonth = removeString(data.fmonth);

                    var SQL = "INSERT INTO `prms_net_additions_summary` (`net_additions_summary_id`, `client_name`, `cost_center`, `revenue`, `gross_margin`, `fyear`, `billed_consultants`, `fmonth`) VALUES (netAdditionsIdRe, clientNameRe, costCenterRe, revenueRe, grossMarginRe, fyearRe, billedConsultantsRe, fmonthRe)";

                    SQL = SQL.replace('netAdditionsIdRe', primaryId);
                    SQL = SQL.replace('clientNameRe', clientName);
                    SQL = SQL.replace('costCenterRe', costCenter);
                    SQL = SQL.replace('revenueRe', revenue);
                    SQL = SQL.replace('grossMarginRe', grossMargin);
                    SQL = SQL.replace('fyearRe', fyear);
                    SQL = SQL.replace('billedConsultantsRe', billedConsultants);
                    SQL = SQL.replace('fmonthRe', fmonth);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);
                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_OPERATIONAL_COST') {
        migrateOperationalCost(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateOperationalCost err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateOperationalCost success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateOperationalCost(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_OPERATIONAL_COST_ID);
                    var costMonth = removeString(data.cost_month);
                    var costYear = removeString(data.cost_year);
                    var employeeName = removeString(data.EMPLOYEE_NAME);
                    var employeeId = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                    var type = removeString(data.type);
                    var costCenterCost = removeString(data.Cost_Center_Cost);
                    var travelExpenses = removeString(data.Travel_Expenses);
                    var mobileExpenses = removeString(data.Mobile_Expenses);
                    var corporateCost = removeString(data.Corporate_Cost);
                    var incentivesTa = removeString(data.Incentives_TA);
                    var incentivesTl = removeString(data.Incentives_TL);
                    var incentivesAm = removeString(data.Incentives_AM);
                    var incentivesBd = removeString(data.Incentives_BD);
                    var clientEntertainment = removeString(data.Client_Entertainment);
                    var teamEntertainmentMorale = removeString(data.Team_Entertainment_Morale);
                    var creditNoteRefund = removeString(data.Credit_Note_Refund);
                    var otherExpenses = removeString(data.Other_Expenses);
                    var createdBy = removeString(data.CreatedBy);
                    var updatedBy = removeString(data.LastModifiedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');
                    var status = removeString(data.STATUS);
                    var liaisonSubContractorFund = removeString(data.Liaison_Sub_Contractor_Fund);
                    var bgvCost = removeString(data.BGV_COST);

                    var SQL = "INSERT INTO `prms_operational_cost` (`operational_cost_id`, `cost_month`, `cost_year`, `employee_name`, `employee_id`, `type`, `cost_center_cost`, `travel_expenses`, `mobile_expenses`, `corporate_cost`, `incentives_ta`, `incentives_tl`, `incentives_am`, `incentives_bd`, `client_entertainment`, `team_entertainment_morale`, `credit_note_refund`, `other_expenses`, `created_by`, `created_at`, `updated_by`, `updated_at`, `status`, `liaison_sub_contractor_fund`, `bgv_cost`) VALUES (operationalCostIdRe, costMonthRe, costYearRe, employeeNameRe, employeeIdRe, typeRe, costCenterCostRe, travelExpensesRe, mobileExpensesRe, corporateCostRe, incentivesTaRe, incentivesTlRe, incentivesAmRe, incentivesBdRe, clientEntertainmentRe, teamEntertainmentMoraleRe, creditNoteRefundRe, otherExpensesRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, statusRe, liaisonSubContractorFundRe, bgvCostRe);";
                    SQL = SQL.replace('operationalCostIdRe', primaryId);
                    SQL = SQL.replace('costMonthRe', costMonth);
                    SQL = SQL.replace('costYearRe', costYear);
                    SQL = SQL.replace('employeeNameRe', employeeName);
                    SQL = SQL.replace('employeeIdRe', employeeId);
                    SQL = SQL.replace('typeRe', type);
                    SQL = SQL.replace('costCenterCostRe', costCenterCost);
                    SQL = SQL.replace('travelExpensesRe', travelExpenses);
                    SQL = SQL.replace('mobileExpensesRe', mobileExpenses);
                    SQL = SQL.replace('corporateCostRe', corporateCost);
                    SQL = SQL.replace('incentivesTaRe', incentivesTa);
                    SQL = SQL.replace('incentivesTlRe', incentivesTl);
                    SQL = SQL.replace('incentivesAmRe', incentivesAm);
                    SQL = SQL.replace('incentivesBdRe', incentivesBd);
                    SQL = SQL.replace('clientEntertainmentRe', clientEntertainment);
                    SQL = SQL.replace('teamEntertainmentMoraleRe', teamEntertainmentMorale);
                    SQL = SQL.replace('creditNoteRefundRe', creditNoteRefund);
                    SQL = SQL.replace('otherExpensesRe', otherExpenses);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('liaisonSubContractorFundRe', liaisonSubContractorFund);
                    SQL = SQL.replace('bgvCostRe', bgvCost);

                    // console.log('\n > SQL[' + row + '] ---> ', SQL);
                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_REIMBURSEMENT_INVOICE') {
        migrateReimsurement(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateReimsurement err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateReimsurement success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateReimsurement(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_REIMBURSEMENT_INVOICE_ID);
                    var contrPersonalInfoId = removeString(data.PRMS_CONTR_PERSONAL_INFO_ID);
                    var remYear = removeString(data.REM_YEAR);
                    var invoiceDate = removeString(data.INVOICE_DATE, 'DATE_FORMAT');
                    var remMonth = removeString(data.REM_MONTH);
                    var gstNumber = removeString(data.GST_NUMBER);
                    var amount = removeString(data.AMOUNT);
                    var cgst = removeString(data.CGST);
                    var sgst = removeString(data.SGST);
                    var igst = removeString(data.IGST);
                    var totalAmount = removeString(data.TOTAL_AMOUNT);
                    var invoiceNumber = removeString(data.INVOICE_NUMBER);
                    var creditperiod = removeString(data.CREDITPERIOD);
                    var duedate = removeString(data.DUEDATE, 'DATE_FORMAT');
                    var comments = removeString(data.COMMENTS);
                    var remarks = removeString(data.REMARKS);
                    var isActive = removeString(data.Active);
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LAST_MODIFIED_BY);
                    var updatedAt = removeString(data.LAST_MODIFIED_DATE, 'DATE_FORMAT');
                    var invoiceType = removeString(data.INVOICE_TYPE);
                    var invoiceMode = removeString(data.INVOICE_MODE);
                    var erPr = removeString(data.ER_PR);

                    var SQL = "INSERT INTO `prms_reimbursement_invoice` (`reimbursement_invoice_id`, `prms_contr_personal_info_id`, `rem_year`, `invoice_date`, `rem_month`, `gst_number`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `invoice_number`, `creditperiod`, `duedate`, `comments`, `remarks`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`, `invoice_type`, `invoice_mode`, `er_pr`) VALUES (reimbursementInvoiceIdRe, contrPersonalInfoIdRe, remYearRe, invoiceDateRe, remMonthRe, gstNumberRe, amountRe, cgstRe, sgstRe, igstRe, totalAmountRe, invoiceNumberRe, creditperiodRe, duedateRe, commentsRe, remarksRe, isActiveRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, invoiceTypeRe, invoiceModeRe, erPrRe);";

                    SQL = SQL.replace('reimbursementInvoiceIdRe', primaryId);
                    SQL = SQL.replace('contrPersonalInfoIdRe', contrPersonalInfoId);
                    SQL = SQL.replace('remYearRe', remYear);
                    SQL = SQL.replace('invoiceDateRe', invoiceDate);
                    SQL = SQL.replace('remMonthRe', remMonth);
                    SQL = SQL.replace('gstNumberRe', gstNumber);
                    SQL = SQL.replace('amountRe', amount);
                    SQL = SQL.replace('cgstRe', cgst);
                    SQL = SQL.replace('sgstRe', sgst);
                    SQL = SQL.replace('igstRe', igst);
                    SQL = SQL.replace('totalAmountRe', totalAmount);
                    SQL = SQL.replace('invoiceNumberRe', invoiceNumber);
                    SQL = SQL.replace('creditperiodRe', creditperiod);
                    SQL = SQL.replace('duedateRe', duedate);
                    SQL = SQL.replace('commentsRe', comments);
                    SQL = SQL.replace('remarksRe', remarks);
                    SQL = SQL.replace('isActiveRe', isActive);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('invoiceTypeRe', invoiceType);
                    SQL = SQL.replace('invoiceModeRe', invoiceMode);
                    SQL = SQL.replace('erPrRe', erPr);
                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_TARGETS') {
        migrateTargets(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateTargets err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateTargets success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateTargets(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_TARGETS_ID);
                    var ecode = removeString(data.ECode, 'PGTCODE');
                    var name = removeString(data.Name);
                    var target = removeString(data.Target);
                    var quater = removeString(data.QUATER);
                    var incentive = removeString(data.INCENTIVE);
                    var grossMargin = removeString(data.GROSS_MARGIN);
                    var targetAchieved = removeString(data.TARGET_ACHIEVED);
                    var status = removeString(data.STATUS);
                    var fyear = removeString(data.fyear);
                    var createdBy = removeString(data.createdby);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');
                    var operationalCost = removeString(data.operational_cost);

                    var SQL = "INSERT INTO `prms_targets` (`target_id`, `ecode`, `name`, `target`, `quater`, `incentive`, `gross_margin`, `target_achieved`, `status`, `fyear`, `created_by`, `created_at`, `updated_by`, `updated_at`, `operational_cost`) VALUES (targetIdRe, ecodeRe, nameRe, targetRe, quaterRe, incentiveRe, grossMarginRe, targetAchievedRe, statusRe, fyearRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, operationalCostRe);";

                    SQL = SQL.replace('targetIdRe', primaryId);
                    SQL = SQL.replace('ecodeRe', ecode);
                    SQL = SQL.replace('nameRe', name);
                    SQL = SQL.replace('targetRe', target);
                    SQL = SQL.replace('quaterRe', quater);
                    SQL = SQL.replace('incentiveRe', incentive);
                    SQL = SQL.replace('grossMarginRe', grossMargin);
                    SQL = SQL.replace('targetAchievedRe', targetAchieved);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('fyearRe', fyear);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('operationalCostRe', operationalCost);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_TARGETS_CLIENTWISE') {
        migrateTargetsClientWise(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateTargetsClientWise err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateTargetsClientWise success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateTargetsClientWise(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_CLIENTWISE_TARGETS_ID);
                    var ecode = removeString(data.ECode, 'PGTCODE');
                    var name = removeString(data.Name);
                    var clientName = removeString(data.ClientName);
                    var quater = removeString(data.Quater);
                    var fyear = removeString(data.fyear);
                    var target = removeString(data.Target);
                    var status = removeString(data.status);
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');

                    var SQL = "INSERT INTO `prms_targets_clientwise` (`targets_clientwise_id`, `ecode`, `name`, `client_name`, `quater`, `fyear`, `target`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (targetsClientIdRe, ecodeRe, nameRe, clientNameRe, quaterRe, fyearRe, targetRe, statusRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe);";

                    SQL = SQL.replace('targetsClientIdRe', primaryId);
                    SQL = SQL.replace('ecodeRe', ecode);
                    SQL = SQL.replace('nameRe', name);
                    SQL = SQL.replace('clientNameRe', clientName);
                    SQL = SQL.replace('quaterRe', quater);
                    SQL = SQL.replace('fyearRe', fyear);
                    SQL = SQL.replace('targetRe', target);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'Book4') {
        migrateRmtSpoc(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateRmtSpoc err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateRmtSpoc success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateRmtSpoc(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    // var primaryId = removeString(data.RMPTSPOCID);
                    var rmtSpocName = removeString(data.RMT_SPOC);
                    var clientName = removeString(data.CLIENT_NAME);
                    var costCenter = removeString(data.COST_CENTER);
                    var isActive = removeString('1');
                    var createdBy = removeString('');
                    var createdAt = removeString(timesNow, 'DATE_FORMAT');
                    var updatedBy = removeString('');
                    var updatedAt = removeString(timesNow, 'DATE_FORMAT');

                    var SQL = "INSERT INTO `prms_rmtspoc` (`rmt_spoc_name`, `client_name`, `cost_center`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (rmt_spoc_name_re, client_name_re, cost_center_re, is_active_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";

                    SQL = SQL.replace('rmt_spoc_name_re', rmtSpocName);
                    SQL = SQL.replace('client_name_re', clientName);
                    SQL = SQL.replace('cost_center_re', costCenter);
                    SQL = SQL.replace('is_active_re', isActive);
                    SQL = SQL.replace('created_by_re', createdBy);
                    SQL = SQL.replace('created_at_re', createdAt);
                    SQL = SQL.replace('updated_by_re', updatedBy);
                    SQL = SQL.replace('updated_at_re', updatedAt);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRIM_Clients_BGVcost') {
        migrateClientBgvCost(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateClientBgvCost err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateClientBgvCost success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateClientBgvCost(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.Client_bgvID);
                    var bgvCost = removeString(data.BGV_COst);
                    var clientId = removeString(data.ClientID);
                    var isActive = removeString(data.Active);
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');
                    var clientName = removeString(data.CLIENT_NAME);

                    var SQL = "INSERT INTO `prms_clients_bgvcost` (`client_bgvcost_id`, `bgv_cost`, `client_id`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`, `client_name`) VALUES (primaryIdRe, bgvCostRe, clientIdRe, isActiveRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, clientNameRe);";

                    SQL = SQL.replace('primaryIdRe', primaryId);
                    SQL = SQL.replace('bgvCostRe', bgvCost);
                    SQL = SQL.replace('clientIdRe', clientId);
                    SQL = SQL.replace('isActiveRe', isActive);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('clientNameRe', clientName);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_SECURITY_USERS') {
        migrateUsers(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateUsers err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateUsers success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateUsers(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var employeeCode = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                    var name = removeString(data.EMPLOYEE_NAME);
                    var emailId = removeString(data.EMAIL);
                    var userType = removeString(data.UserType, 'ROLES');
                    var status = removeString(data.Status, 'UPPERCASE');
                    var target = removeString(data.Target);
                    var designation = removeString('');
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');
                    var isRms = removeString('1');

                    var SQL = "INSERT INTO `prms_app_employees` (`employee_code`, `name`, `email_id`, `user_type`, `status`, `target`, `designation`, `created_by`, `created_at`, `updated_by`, `updated_at`, `is_rms`) VALUES (employeeCodeRe, nameRe, emailIdRe, userTypeRe, statusRe, targetRe, designationRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, isRmsRe);";

                    SQL = SQL.replace('employeeCodeRe', employeeCode);
                    SQL = SQL.replace('nameRe', name);
                    SQL = SQL.replace('emailIdRe', emailId);
                    SQL = SQL.replace('userTypeRe', userType);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('targetRe', target);
                    SQL = SQL.replace('designationRe', designation);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('isRmsRe', isRms);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, employeeCode);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_APP_EMPLOYEES') {
        migrateAppEmployees(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateAppEmployees err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateAppEmployees success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateAppEmployees(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var employeeCode = removeString(data.EMPLOYEE_CODE, 'PGTCODE');
                    var name = removeString(data.NAME);
                    var emailId = removeString(data.EMAIL_ID);
                    var userType = removeString(data.USER_TYPE, 'ROLES');
                    var status = removeString(data.STATUS, 'STATUS');
                    var target = removeString(data.TARGET);
                    var designation = removeString(data.DESIGNATION, 'DESIGNATION');
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.Createddate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LasModifiedDate, 'DATE_FORMAT');
                    var isRms = removeString('0');

                    var SQL = "INSERT INTO `prms_app_employees` (`employee_code`, `name`, `email_id`, `user_type`, `status`, `target`, `designation`, `created_by`, `created_at`, `updated_by`, `updated_at`, `is_rms`) VALUES (employeeCodeRe, nameRe, emailIdRe, userTypeRe, statusRe, targetRe, designationRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, isRmsRe);";

                    SQL = SQL.replace('employeeCodeRe', employeeCode);
                    SQL = SQL.replace('nameRe', name);
                    SQL = SQL.replace('emailIdRe', emailId);
                    SQL = SQL.replace('userTypeRe', userType);
                    SQL = SQL.replace('statusRe', status);
                    SQL = SQL.replace('targetRe', target);
                    SQL = SQL.replace('designationRe', designation);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('isRmsRe', isRms);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, employeeCode);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'PRMS_CLIENT_CC_MAPPING') {
        migrateClientMapping(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateClientMapping err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                console.log("\n > migrateClientMapping success ---> ", data);
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateClientMapping(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var primaryId = removeString(data.PRMS_CLIENT_CC_MAPPING_ID);
                    var clientName = removeString(data.CLIENT_NAME);
                    var costCenter = removeString(data.COST_CENTER);
                    var hrSpoc = removeString(data.HR_SPOC);
                    var finSpoc = removeString(data.FIN_SPOC);
                    var plHead = removeString(data.PL_HEAD);
                    var acctMgr = removeString(data.ACCT_MGR);
                    var dlvryMgr = removeString(data.DLVRY_MGR);
                    var bsnsMgr = removeString('');
                    var pgtCodeBsnsMgr = removeString('');
                    var rmtSpoc = removeString(data.RMT_SPOC);
                    var rmtSpocEmailid = removeString(data.RMT_SPOC_EMAILID);
                    var rmtSpocContactdetails = removeString(data.RMT_SPOC_CONTACTDETAILS);
                    var rmtSpocDesignation = removeString(data.RMT_SPOC_DESIGNATION);
                    var msbaName = removeString(data.MSBA_NAME);
                    var eLeavesCl = removeString(data.E_LEAVES_CL);
                    var eLeavesSl = removeString(data.E_LEAVES_SL);
                    var createdBy = removeString(data.CreatedBy);
                    var createdAt = removeString(data.CreatedDate, 'DATE_FORMAT');
                    var updatedBy = removeString(data.LastModifiedBy);
                    var updatedAt = removeString(data.LastModifiedDate, 'DATE_FORMAT');
                    var isActive = removeString(data.active);
                    var pgtCodePandlHead = removeString(data.PGT_Code_PandL_Head, 'PGTCODE');
                    var pgtCodeAccountManager = removeString(data.PGT_Code_Account_Manager, 'PGTCODE');
                    var pgtCodeDeliveryManager = removeString(data.PGT_Code_Delivery_Manager, 'PGTCODE');
                    var mspName = removeString(data.MSP_NAME);
                    var mspFee = removeString(data.MSP_FEE);
                    if (acctMgr && dlvryMgr) {
                        bsnsMgr = acctMgr;
                        pgtCodeBsnsMgr = pgtCodeAccountManager;
                    } else if (acctMgr) {
                        bsnsMgr = acctMgr;
                        pgtCodeBsnsMgr = pgtCodeAccountManager;
                    } else if (dlvryMgr) {
                        bsnsMgr = dlvryMgr;
                        pgtCodeBsnsMgr = pgtCodeDeliveryManager;
                    }
                    var SQL = "INSERT INTO `prms_client_cc_mapping` (`client_cc_mapping_id`, `client_name`, `cost_center`, `hr_spoc`, `fin_spoc`, `pl_head`, `acct_mgr`, `dlvry_mgr`, `bsns_mgr`, `pgt_code_bsns_mgr`, `rmt_spoc`, `rmt_spoc_emailid`, `rmt_spoc_contactdetails`, `rmt_spoc_designation`, `msba_name`, `e_leaves_cl`, `e_leaves_sl`, `created_by`, `created_at`, `updated_by`, `updated_at`, `is_active`, `pgt_code_pandl_head`, `pgt_code_account_manager`, `pgt_code_delivery_manager`, `msp_name`, `msp_fee`) VALUES (primaryIdRe, clientNameRe, costCenterRe, hrSpocRe, finSpocRe, plHeadRe, acctMgrRe, dlvryMgrRe, bsnsMgrRe, pgtCodeBsnsMgrRe, rmtSpocRe, rmtSpocEmailidRe, rmtSpocContactdetailsRe, rmtSpocDesignationRe, msbaNameRe, eLeavesClRe, eLeavesSlRe, createdByRe, createdAtRe, updatedByRe, updatedAtRe, isActiveRe, pgtCodePandlHeadRe, pgtCodeAccountManagerRe, pgtCodeDeliveryManagerRe, mspNameRe, mspFeeRe);";

                    SQL = SQL.replace('primaryIdRe', primaryId);
                    SQL = SQL.replace('clientNameRe', clientName);
                    SQL = SQL.replace('costCenterRe', costCenter);
                    SQL = SQL.replace('hrSpocRe', hrSpoc);
                    SQL = SQL.replace('finSpocRe', finSpoc);
                    SQL = SQL.replace('plHeadRe', plHead);
                    SQL = SQL.replace('acctMgrRe', acctMgr);
                    SQL = SQL.replace('dlvryMgrRe', dlvryMgr);
                    SQL = SQL.replace('bsnsMgrRe', bsnsMgr);
                    SQL = SQL.replace('pgtCodeBsnsMgrRe', pgtCodeBsnsMgr);
                    SQL = SQL.replace('rmtSpocRe', rmtSpoc);
                    SQL = SQL.replace('rmtSpocEmailidRe', rmtSpocEmailid);
                    SQL = SQL.replace('rmtSpocContactdetailsRe', rmtSpocContactdetails);
                    SQL = SQL.replace('rmtSpocDesignationRe', rmtSpocDesignation);
                    SQL = SQL.replace('msbaNameRe', msbaName);
                    SQL = SQL.replace('eLeavesClRe', (eLeavesCl) ? eLeavesCl : 0);
                    SQL = SQL.replace('eLeavesSlRe', (eLeavesSl) ? eLeavesSl : 0);
                    SQL = SQL.replace('createdByRe', createdBy);
                    SQL = SQL.replace('createdAtRe', createdAt);
                    SQL = SQL.replace('updatedByRe', updatedBy);
                    SQL = SQL.replace('updatedAtRe', updatedAt);
                    SQL = SQL.replace('isActiveRe', (isActive) ? '1' : '0');
                    SQL = SQL.replace('pgtCodePandlHeadRe', pgtCodePandlHead);
                    SQL = SQL.replace('pgtCodeAccountManagerRe', pgtCodeAccountManager);
                    SQL = SQL.replace('pgtCodeDeliveryManagerRe', pgtCodeDeliveryManager);
                    SQL = SQL.replace('mspNameRe', mspName);
                    SQL = SQL.replace('mspFeeRe', mspFee);

                    console.log('\n > SQL[' + row + '] ---> ', SQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                errorLog(tableName, err.sqlMessage, primaryId);
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    } else if (jsonData.length > 0 && tableName == 'MISSING_RMT_SPOC') {
        migrateMissingRmtSpoc(jsonData, params.tableName, function (err, data) {
            if (err) {
                console.log("\n > migrateMissingRmtSpoc err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                // console.log("\n > migrateMissingRmtSpoc success ---> ", data);
                const fs = require('fs')
                const content = data;
                const filePath = rootPath.resolve('.') + '/SQL/new/rmt-spoc.sql';
                fs.writeFile(filePath, content, err => {
                    if (err) {
                        console.error(err);
                        return
                    }
                });
                res.status(200).json({ status: 'success', data: data });
            }
        });
        function migrateMissingRmtSpoc(jsonData, tableNames, callback) {
            try {
                var row = 0;
                var INSERTSQL = '';
                var insertRow = function (json, tableName) {
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var data = json[row];
                    console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                    var rmtSpocName = removeString(data.rmtspoc_name);
                    var clientName = removeString(data.client_name);
                    var costCenter = removeString(data.cost_center);

                    var getRmtSQL = "SELECT * FROM `prms_rmtspoc` WHERE client_name = client_name_re AND cost_center = cost_center_re AND rmt_spoc_name = rmt_spoc_name_re;";

                    getRmtSQL = getRmtSQL.replace(/rmt_spoc_name_re/g, rmtSpocName);
                    getRmtSQL = getRmtSQL.replace(/client_name_re/g, clientName);
                    getRmtSQL = getRmtSQL.replace(/cost_center_re/g, costCenter);

                    console.log('\n > getRmtSQL[' + row + '] ---> ', getRmtSQL);

                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }

                    db.query(getRmtSQL, function (err, results, fields) {
                        if (results.length == 0) {
                            var rmtSpocName = removeString(data.rmtspoc_name);
                            var clientName = removeString(data.client_name);
                            var costCenter = removeString(data.cost_center);
                            var isActive = removeString('1');
                            var createdBy = removeString('');
                            var createdAt = removeString(timesNow, 'DATE_FORMAT');
                            var updatedBy = removeString('');
                            var updatedAt = removeString(timesNow, 'DATE_FORMAT');

                            var rmtSQL = "INSERT INTO `prms_rmtspoc` (`rmt_spoc_name`, `client_name`, `cost_center`, `is_active`) VALUES (rmt_spoc_name_re, client_name_re, cost_center_re, is_active_re);";

                            rmtSQL = rmtSQL.replace(/rmt_spoc_name_re/g, rmtSpocName);
                            rmtSQL = rmtSQL.replace(/client_name_re/g, clientName);
                            rmtSQL = rmtSQL.replace(/cost_center_re/g, costCenter);
                            rmtSQL = rmtSQL.replace('is_active_re', isActive);
                            rmtSQL = rmtSQL.replace('created_by_re', createdBy);
                            rmtSQL = rmtSQL.replace('created_at_re', createdAt);
                            rmtSQL = rmtSQL.replace('updated_by_re', updatedBy);
                            rmtSQL = rmtSQL.replace('updated_at_re', updatedAt);

                            console.log('\n > rmtSQL[' + row + '] ---> ', rmtSQL);
                            INSERTSQL += rmtSQL + "\n"
                            db.query(rmtSQL, function (err, results, fields) {
                                row++;
                                if (err) {
                                    console.log('\n > rmtSQL[' + row + '] err ---> ', err.sqlMessage);
                                    if (row >= json.length) {
                                        callback(false, INSERTSQL);
                                    } else {
                                        errorLog(tableName, err.sqlMessage, rmtSpocName + clientName);
                                        insertRow(json, tableName);
                                    }
                                } else {
                                    if (row >= json.length) {
                                        callback(false, INSERTSQL);
                                    } else {
                                        insertRow(json, tableName);
                                    }
                                }
                            });
                        } else {
                            row++;
                            if (row >= json.length) {
                                callback(false, INSERTSQL);
                            } else {
                                insertRow(json, tableName);
                            }
                        }

                    });

                };
                insertRow(jsonData, tableNames.toString().toLowerCase());
            } catch (e) {
                console.log('\n > try catch error ---> ', e);
                callback(true, 'try catch error');
            }
        };
    }

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            data = formatType === 'STATUS' ? data.split(/\s/).join('_').toUpperCase() : data;
            if (formatType === 'ROLES') {
                if (data.split(/\s/).join('').toLowerCase() === ('SuperAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'SuperAdmin';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('Admin').split(/\s/).join('').toLowerCase()) {
                    data = 'Admin';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'HRAdmin';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('HREdit').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRReadOnly').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'PLHead';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('AM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('DM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('TA').split(/\s/).join('').toLowerCase()) {
                    data = 'TA';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                }
            } else if (formatType === 'DESIGNATION') {
                if (data.split(/\s/).join('').toLowerCase() === ('Account Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('Delivery Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('Finance SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'Finance SPOC';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('HR SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'HR SPOC';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('P&L Head').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('Sourced By').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('SourcedBy').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                } else if (data.split(/\s/).join('').toLowerCase() === ('Trainee TA').split(/\s/).join('').toLowerCase()) {
                    data = 'Trainee TA';
                    // data = data.replace(reg, '\\$&');
                    // return "'" + data + "'";
                }
            } else if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                // console.log('\n > Dateformat ---> ', data);
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }

};


exports.verticalMapping = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/vertical-mapping/verticals.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'verticalMapping';
    mapping(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > verticalMapping err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            errorLog(tableName, 'No Errors', 'Updated successfully');
            console.log("\n > verticalMapping success ---> ", data);
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function mapping(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                var HR_ID = removeString(data.HR_ID);
                var EMPLOYEE_ID = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                var EMPLOYEE_NAME = removeString(data.EMPLOYEE_NAME);
                var VERTICALS = removeString(data.VERTICALS);

                var getVerticals = "SELECT vertical_id ID, vertical_name verticalName FROM `prms_vertical` WHERE vertical_name = VERTICALS;";

                getVerticals = getVerticals.replace('VERTICALS', VERTICALS);

                console.log('\n > getVerticals[' + row + '] ---> ', getVerticals);

                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }

                db.query(getVerticals, function (err, results, fields) {
                    if (results.length > 0) {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            var updateVertical = "UPDATE prms_pg_hr_master SET vertical_id = vertical_id_re, vertical = vertical_re WHERE hr_master_id = hr_master_id_re;"
                            updateVertical = updateVertical.replace('vertical_id_re', removeString(results[0].ID));
                            updateVertical = updateVertical.replace('vertical_re', removeString(results[0].verticalName));
                            updateVertical = updateVertical.replace('hr_master_id_re', HR_ID);
                            console.log('\n > updateVertical[' + row + '] ---> ', updateVertical);
                            db.query(updateVertical, function (err, results, fields) {
                                row++;
                                if (row >= json.length) {
                                    callback(false, 'Migrated Successfully');
                                } else {
                                    insertRow(json, tableName);
                                }
                            });
                        }
                    } else if (row >= json.length) {
                        callback(false, 'Migrated Successfully');
                    } else {
                        errorLog(tableName, 'No vertical', row + ' - ' + HR_ID + ' - ' + EMPLOYEE_NAME);
                        row++;
                        insertRow(json, tableName);
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};


exports.endClientMapping = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/vertical-mapping/end-clients.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'endClientMapping';
    mapping(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > endClientMapping err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            errorLog(tableName, 'No Errors', 'Updated successfully');
            console.log("\n > endClientMapping success ---> ", data);
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function mapping(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                var HR_ID = removeString(data.HR_ID);
                var EMPLOYEE_ID = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                var EMPLOYEE_NAME = removeString(data.EMPLOYEE_NAME);
                var END_CLIENT = removeString(data.END_CLIENT);

                var getEndClients = "SELECT endclient_id ID, endclient_name endclientName FROM prms_endclient WHERE endclient_name = END_CLIENT;";

                getEndClients = getEndClients.replace('END_CLIENT', END_CLIENT);

                console.log('\n > getEndClients[' + row + '] ---> ', getEndClients);

                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }

                db.query(getEndClients, function (err, results, fields) {
                    if (results.length > 0) {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            var updateEndClients = "UPDATE prms_pg_hr_master SET end_client_id = end_client_id_re, end_client = end_client_re WHERE hr_master_id = hr_master_id_re;"
                            updateEndClients = updateEndClients.replace('end_client_id_re', removeString(results[0].ID));
                            updateEndClients = updateEndClients.replace('end_client_re', removeString(results[0].endclientName));
                            updateEndClients = updateEndClients.replace('hr_master_id_re', HR_ID);
                            console.log('\n > updateEndClients[' + row + '] ---> ', updateEndClients);
                            // row++;
                            // if (row >= json.length) {
                            //     callback(false, 'Migrated Successfully');
                            // } else {
                            //     insertRow(json, tableName);
                            // }
                            db.query(updateEndClients, function (err, results, fields) {
                                row++;
                                if (row >= json.length) {
                                    callback(false, 'Migrated Successfully');
                                } else {
                                    insertRow(json, tableName);
                                }
                            });
                        }
                    } else if (row >= json.length) {
                        callback(false, 'Migrated Successfully');
                    } else {
                        errorLog(tableName, 'No End Clients', row + ' - ' + HR_ID + ' - ' + EMPLOYEE_NAME);
                        row++;
                        insertRow(json, tableName);
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.horizontalMapping = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/vertical-mapping/horizontals.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'horizontalMapping';
    mapping(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > horizontalMapping err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > horizontalMapping success ---> ", data);
            errorLog(tableName, 'No Errors', 'Updated successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function mapping(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                var HR_ID = removeString(data.HR_ID);
                var EMPLOYEE_ID = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                var EMPLOYEE_NAME = removeString(data.EMPLOYEE_NAME);
                var DOMAIN = removeString(data.DOMAIN);

                var getHorizontal = "SELECT horizontal_id ID, horizontal_type horizontalType FROM prms_horizontal WHERE horizontal_type = DOMAIN;";

                getHorizontal = getHorizontal.replace('DOMAIN', DOMAIN);

                console.log('\n > getHorizontal[' + row + '] ---> ', getHorizontal);

                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }

                db.query(getHorizontal, function (err, results, fields) {
                    if (results.length > 0) {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            // "UPDATE `prms_pg_hr_master` SET `prms_domain_id` = '2', `domain_name` = 'asdf' WHERE `prms_pg_hr_master`.`hr_master_id` = 4097"
                            var updateHorizontal = "UPDATE prms_pg_hr_master SET new_horizontal_id = new_horizontal_id_re, new_horizontal = new_horizontal_re WHERE hr_master_id = hr_master_id_re;"
                            updateHorizontal = updateHorizontal.replace('new_horizontal_id_re', removeString(results[0].ID));
                            updateHorizontal = updateHorizontal.replace('new_horizontal_re', removeString(results[0].horizontalType));
                            updateHorizontal = updateHorizontal.replace('hr_master_id_re', HR_ID);
                            console.log('\n > updateHorizontal[' + row + '] ---> ', updateHorizontal);
                            // row++;
                            // if (row >= json.length) {
                            //     callback(false, 'Migrated Successfully');
                            // } else {
                            //     insertRow(json, tableName);
                            // }
                            db.query(updateHorizontal, function (err, results, fields) {
                                row++;
                                if (row >= json.length) {
                                    callback(false, 'Migrated Successfully');
                                } else {
                                    insertRow(json, tableName);
                                }
                            });
                        }
                    } else if (row >= json.length) {
                        callback(false, 'Migrated Successfully');
                    } else {
                        errorLog(tableName, 'No End Clients', row + ' - ' + HR_ID + ' - ' + EMPLOYEE_NAME);
                        row++;
                        insertRow(json, tableName);
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.updateSkillSet = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/vertical-mapping/skill-set.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'updateSkillSet';
    mapping(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > updateSkillSet err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > updateSkillSet success ---> ", data);
            errorLog(tableName, 'No Errors', 'Updated successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function mapping(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                var HR_ID = removeString(data.HR_ID);
                var EMPLOYEE_ID = removeString(data.EMPLOYEE_ID, 'PGTCODE');
                var EMPLOYEE_NAME = removeString(data.EMPLOYEE_NAME);
                var PRIMARY_SKILL = removeString(data.PRIMARY_SKILL);

                var updateSkillSet = "UPDATE prms_pg_hr_master SET primary_skill = primary_skill_re, old_primary_skill = primary_skill_re, new_primary_skill = '', new_skill_set_id = '' WHERE hr_master_id = hr_master_id_re;"
                updateSkillSet = updateSkillSet.replace(/primary_skill_re/g, PRIMARY_SKILL);
                updateSkillSet = updateSkillSet.replace('hr_master_id_re', HR_ID);
                console.log('\n > updateSkillSet[' + row + '] ---> ', updateSkillSet);

                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }
                db.query(updateSkillSet, function (err, results, fields) {
                    row++;
                    if (row >= json.length) {
                        callback(false, 'Migrated Successfully');
                    } else {
                        insertRow(json, tableName);
                    }
                });

            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.skillSetMapping = function (req, res, next) {
    var oldSkillSet = (req.body.oldSkillSet);
    var newSkillSet = (req.body.newSkillSet);
    if (oldSkillSet && newSkillSet) {
        compareStrings(oldSkillSet, newSkillSet, function (err, data) {
            if (err) {
                console.log("\n > skillSetMapping err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                // console.log("\n > skillSetMapping success ---> ", data);
                if (data.updateSQL) {
                    db.query(data.updateSQL, function (err, results, fields) {
                        res.status(200).json({ status: 'success', data: data });
                    });
                } else {
                    res.status(200).json({ status: 'success', data: data });
                }
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields' });
    }


    function compareStrings(searchString, newString, callback) {
        try {
            var getPrimarySkill = "SELECT skill_set_id ID, primary_skill primarySkill FROM `prms_skill_set` WHERE primary_skill = newString;";
            getPrimarySkill = getPrimarySkill.replace('newString', removeString(newString));

            console.log('\n > getPrimarySkill SQL ---> ', getPrimarySkill);
            db.query(getPrimarySkill, function (err, results, fields) {
                if (results.length > 0) {
                    let skillSetID = results[0].ID;
                    newString = results[0].primarySkill;
                    updateNewString(searchString, newString, skillSetID, function (err, data) {
                        callback(false, data);
                    });
                } else {
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var insertSkill = "INSERT INTO `prms_skill_set` (`primary_skill`, `is_active`, `created_at`, `updated_at`) VALUES (newString, '1', 'timesNow', 'timesNow');";
                    insertSkill = insertSkill.replace('newString', removeString(newString));
                    insertSkill = insertSkill.replace(/timesNow/g, timesNow);
                    console.log('\n > insertSkill SQL ---> ', insertSkill);
                    db.query(insertSkill, function (err, results, fields) {
                        let skillSetID = results.insertId;
                        updateNewString(searchString, newString, skillSetID, function (err, data) {
                            callback(false, data);
                        });
                    });
                }
            });
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function updateNewString(searchString, newString, skillSetID, callback) {
        try {
            var getHrMaster = "SELECT hr_master_id hrMasterId, doj, primary_skill primarySkill, old_primary_skill oldPrimarySkill, new_skill_set_id newSkillSetId, new_primary_skill newPrimarySkill FROM prms_pg_hr_master WHERE old_primary_skill LIKE '%searchString%' AND doj > '2021-03-31'";
            getHrMaster = getHrMaster.replace('searchString', searchString);

            console.log('\n > getHrMaster SQL ---> ', getHrMaster);
            db.query(getHrMaster, function (err, results, fields) {
                function replaceString(str, searchMask, replaceMask) {
                    var regEx = new RegExp(searchMask, "ig");
                    return str.replace(regEx, replaceMask);
                }
                if (results.length > 0) {
                    var updateHrSkill = "";
                    var updateSQL = "";
                    results.forEach(e => {
                        e.doj = moment(e.doj).format('DD-MM-YYYY');
                        e.oldPrimarySkill = replaceString(e.oldPrimarySkill, searchString, '');
                        e.newPrimarySkill = e.newPrimarySkill ? e.newPrimarySkill + ", " + newString : newString,
                            e.newSkillSetId = e.newSkillSetId ? e.newSkillSetId + ", " + skillSetID : skillSetID

                        updateHrSkill = "UPDATE prms_pg_hr_master SET old_primary_skill = 'old_primary_skill_re', new_primary_skill = 'new_primary_skill_re', new_skill_set_id = 'new_skill_set_id_re' WHERE hr_master_id = hr_master_id_re;";
                        updateHrSkill = updateHrSkill.replace('old_primary_skill_re', e.oldPrimarySkill);
                        updateHrSkill = updateHrSkill.replace('new_primary_skill_re', e.newPrimarySkill);
                        updateHrSkill = updateHrSkill.replace('new_skill_set_id_re', e.newSkillSetId);
                        updateHrSkill = updateHrSkill.replace('hr_master_id_re', e.hrMasterId);
                        updateSQL += updateHrSkill;
                    });
                    let data = {
                        'updateSQL': updateSQL,
                        'results': results
                    };
                    callback(false, data);

                } else {
                    callback(false, 'No Masters');
                }
            });
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.hrEditMiddleName = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/Hr-edit-First-Middle-Last-name.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'hrEditMiddleName';
    editMiddleName(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > editMiddleName err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > editMiddleName success ---> ", data);
            errorLog(tableName, 'No Errors', 'Updated successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function editMiddleName(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var data = json[row];
                console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                var hrMasterId = removeString(data.HR_ID);
                var employeeFirstName = removeString(data.EMPLOYEE_FIRST_NAME);
                var employeeMiddleName = removeString(data.EMPLOYEE_MIDDLE_NAME);
                var employeeLastName = removeString(data.EMPLOYEE_LAST_NAME);

                var SQL = "UPDATE `prms_pg_hr_master` SET `employee_first_name`=employeeFirstName,`employee_middle_name`=employeeMiddleName,`employee_last_name`=employeeLastName WHERE `hr_master_id` =hrMasterId;";

                SQL = SQL.replace('employeeFirstName', employeeFirstName);
                SQL = SQL.replace('employeeMiddleName', employeeMiddleName);
                SQL = SQL.replace('employeeLastName', employeeLastName);
                SQL = SQL.replace('hrMasterId', hrMasterId);

                console.log('\n > SQL[' + row + '] ---> ', SQL);

                db.query(SQL, function (err, results, fields) {
                    row++;
                    if (err) {
                        console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            errorLog(tableName, err.sqlMessage, row + ' - ' + clientName);
                            insertRow(json, tableName);
                        }
                    } else {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            insertRow(json, tableName);
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.mergeHrMaster = function (req, res, next) {
    var fromMappingId = (req.body.fromMappingId);
    var toMappingId = (req.body.toMappingId);
    var tableName = 'prms_pg_hr_master';
    if (fromMappingId && toMappingId) {
        updateMaster(fromMappingId, toMappingId, function (err, data) {
            if (err) {
                console.log("\n > mergeHrMaster err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                res.status(200).json({ status: 'success', data: data });
                errorLog(tableName, data.fromTo, data.sqlData);
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields' });
    }
    function updateMaster(fromWhere, toSet, callback) {
        try {
            var updateHrMapping = "UPDATE `prms_pg_hr_master` SET `prms_client_cc_mapping_id` = toMappingId WHERE `prms_client_cc_mapping_id` = fromMappingId;";
            updateHrMapping = updateHrMapping.replace('toMappingId', removeString(toSet));
            updateHrMapping = updateHrMapping.replace('fromMappingId', removeString(fromWhere));

            console.log('\n > updateHrMapping SQL ---> ', updateHrMapping);
            var fromTo = '', sqlData = '', data;
            fromTo = '> From: ' + fromWhere + ' - To: ' + toSet;
            sqlData = updateHrMapping;
            data = {
                fromTo: fromTo,
                sqlData: sqlData
            };
            // callback(false, data);
            db.query(updateHrMapping, function (err, results, fields) {
                callback(false, data);
            });
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};


exports.newHrMaster = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/wipro-mapping.xls';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res, i) => {
            // if (i < 100) {
            jsonData.push(res);
            // }
        });
    }
    // let tableName = params.tableName.toString().toLowerCase();
    let tableName = 'prms_pg_hr_master';
    hrMaster(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > hrMaster err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // console.log("\n > hrMaster success ---> ", data);
            const fs = require('fs')
            const content = data;
            const filePath = rootPath.resolve('.') + '/SQL/2021-06-07/newHrMaster.sql';
            fs.writeFile(filePath, content, err => {
                if (err) {
                    console.error(err)
                    return
                }
            });
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function hrMaster(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var hrMaster = "";
            var insertRow = function (json, tableName) {
                var data = json[row];
                var employee_id = removeString(data.employee_id, 'PGTCODE');
                var employee_name = removeString(data.employee_name);
                var employee_first_name = removeString(data.employee_first_name);
                var employee_middle_name = removeString(data.employee_middle_name);
                var employee_last_name = removeString(data.employee_last_name);
                var client_empid = removeString(data.client_empid);
                var prms_client_cc_mapping_id = removeString(data.prms_client_cc_mapping_id);
                var prms_rmtspoc_id = removeString(data.prms_rmtspoc_id);
                var rmtspoc_name = removeString(data.rmtspoc_name);
                var sourced_by = removeString(data.sourced_by);
                var pgt_code_sourced_by = removeString(data.pgt_code_sourced_by);
                var mobile_no = removeString(data.mobile_no);
                var alternate_no = removeString(data.alternate_no);
                var personal_emailid = removeString(data.personal_emailid, 'EMAIL');
                var official_emailid = removeString(data.official_emailid, 'EMAIL');
                var dob = removeString(data.dob, 'DATE');
                var gender = removeString(data.gender);
                var marital_status = removeString(data.marital_status);
                var father_husband_name = removeString(data.father_husband_name);
                var emergency_contact_name = removeString(data.emergency_contact_name);
                var emergency_contact_relation = removeString(data.emergency_contact_relation);
                var emergency_contact_number = removeString(data.emergency_contact_number);
                var permanant_address = removeString(data.permanant_address);
                var present_address = removeString(data.present_address);
                var pan_number = removeString(data.pan_number);
                var edu_qualification = removeString(data.edu_qualification);
                var aadhar_number = removeString(data.aadhar_number);
                var name_as_per_aadhar = removeString(data.name_as_per_aadhar);
                var insurance = removeString(data.insurance);
                var total_exp = removeString(data.total_exp);
                var relevant_exp = removeString(data.relevant_exp);
                var job_title = removeString(data.job_title);
                var location = removeString(data.location);
                var new_primary_skill = removeString(data.new_primary_skill);
                var new_skill_set_id = removeString(data.new_skill_set_id);
                var new_horizontal = removeString(data.new_horizontal);
                var new_horizontal_id = removeString(data.new_horizontal_id);
                var sec_skill1 = removeString(data.sec_skill1);
                var offered_dt = removeString(data.offered_dt, 'DATE');
                var doj = removeString(data.doj, 'DATE');
                var status = removeString(data.status);
                var employee_mode = removeString(data.employee_mode);
                var bill_mode = removeString(data.bill_mode);
                var billing_amt = removeString(data.billing_amt);
                var bill_rate = removeString(data.bill_rate);
                var salary = removeString(data.salary);
                var gross_margin = removeString(data.gross_margin);
                var relieved_date = removeString(data.relieved_date, 'DATE');
                var resigned_month = removeString(data.resigned_month);
                var relieving_reason = removeString(data.relieving_reason);
                var deployed_bldg_address = removeString(data.deployed_bldg_address);
                var pf = removeString(data.pf);
                var approval_status = removeString('Approved');

                var insertHrMaster = "INSERT INTO `prms_pg_hr_master` (`employee_id`, `employee_name`, `employee_first_name`, `employee_middle_name`, `employee_last_name`, `client_empid`, `prms_client_cc_mapping_id`, `prms_rmtspoc_id`, `rmtspoc_name`, `sourced_by`, `pgt_code_sourced_by`, `mobile_no`, `alternate_no`, `personal_emailid`, `official_emailid`, `dob`, `gender`, `marital_status`, `father_husband_name`, `emergency_contact_name`, `emergency_contact_relation`, `emergency_contact_number`, `permanant_address`, `present_address`, `pan_number`, `edu_qualification`, `aadhar_number`, `name_as_per_aadhar`, `insurance`, `total_exp`, `relevant_exp`, `job_title`, `location`, `new_primary_skill`, `new_skill_set_id`, `new_horizontal`, `new_horizontal_id`, `sec_skill1`, `offered_dt`, `doj`, `status`, `employee_mode`, `bill_mode`, `billing_amt`, `bill_rate`, `salary`, `gross_margin`, `relieved_date`, `resigned_month`, `relieving_reason`, `deployed_bldg_address`, `pf`, `approval_status`) VALUES (employee_id_re, employee_name_re, employee_first_name_re, employee_middle_name_re, employee_last_name_re, client_empid_re, prms_client_cc_mapping_id_re, prms_rmtspoc_id_re, rmtspoc_name_re, sourced_by_re, pgt_code_sourced_by_re, mobile_no_re, alternate_no_re, personal_emailid_re, official_emailid_re, dob_re, gender_re, marital_status_re, father_husband_name_re, emergency_contact_name_re, emergency_contact_relation_re, emergency_contact_number_re, permanant_address_re, present_address_re, pan_number_re, edu_qualification_re, aadhar_number_re, name_as_per_aadhar_re, insurance_re, total_exp_re, relevant_exp_re, job_title_re, location_re, new_primary_skill_re, new_skill_set_id_re, new_horizontal_re, new_horizontal_id_re, sec_skill1_re, offered_dt_re, doj_re, status_re, employee_mode_re, bill_mode_re, billing_amt_re, bill_rate_re, salary_re, gross_margin_re, relieved_date_re, resigned_month_re, relieving_reason_re, deployed_bldg_address_re, pf_re, approval_status_re);";

                insertHrMaster = insertHrMaster.replace('employee_id_re', employee_id);
                insertHrMaster = insertHrMaster.replace('employee_name_re', employee_name);
                insertHrMaster = insertHrMaster.replace('employee_first_name_re', employee_first_name);
                insertHrMaster = insertHrMaster.replace('employee_middle_name_re', employee_middle_name);
                insertHrMaster = insertHrMaster.replace('employee_last_name_re', employee_last_name);
                insertHrMaster = insertHrMaster.replace('client_empid_re', client_empid);
                insertHrMaster = insertHrMaster.replace('prms_client_cc_mapping_id_re', prms_client_cc_mapping_id);
                insertHrMaster = insertHrMaster.replace('prms_rmtspoc_id_re', prms_rmtspoc_id);
                insertHrMaster = insertHrMaster.replace('rmtspoc_name_re', rmtspoc_name);
                insertHrMaster = insertHrMaster.replace('sourced_by_re', sourced_by);
                insertHrMaster = insertHrMaster.replace('pgt_code_sourced_by_re', pgt_code_sourced_by);
                insertHrMaster = insertHrMaster.replace('mobile_no_re', mobile_no);
                insertHrMaster = insertHrMaster.replace('alternate_no_re', alternate_no);
                insertHrMaster = insertHrMaster.replace('personal_emailid_re', personal_emailid);
                insertHrMaster = insertHrMaster.replace('official_emailid_re', official_emailid);
                insertHrMaster = insertHrMaster.replace('dob_re', dob);
                insertHrMaster = insertHrMaster.replace('gender_re', gender);
                insertHrMaster = insertHrMaster.replace('marital_status_re', marital_status);
                insertHrMaster = insertHrMaster.replace('father_husband_name_re', father_husband_name);
                insertHrMaster = insertHrMaster.replace('emergency_contact_name_re', emergency_contact_name);
                insertHrMaster = insertHrMaster.replace('emergency_contact_relation_re', emergency_contact_relation);
                insertHrMaster = insertHrMaster.replace('emergency_contact_number_re', emergency_contact_number);
                insertHrMaster = insertHrMaster.replace('permanant_address_re', permanant_address);
                insertHrMaster = insertHrMaster.replace('present_address_re', present_address);
                insertHrMaster = insertHrMaster.replace('pan_number_re', pan_number);
                insertHrMaster = insertHrMaster.replace('edu_qualification_re', edu_qualification);
                insertHrMaster = insertHrMaster.replace('aadhar_number_re', aadhar_number);
                insertHrMaster = insertHrMaster.replace('name_as_per_aadhar_re', name_as_per_aadhar);
                insertHrMaster = insertHrMaster.replace('insurance_re', insurance);
                insertHrMaster = insertHrMaster.replace('total_exp_re', total_exp);
                insertHrMaster = insertHrMaster.replace('relevant_exp_re', relevant_exp);
                insertHrMaster = insertHrMaster.replace('job_title_re', job_title);
                insertHrMaster = insertHrMaster.replace('location_re', location);
                insertHrMaster = insertHrMaster.replace('new_primary_skill_re', new_primary_skill);
                insertHrMaster = insertHrMaster.replace('new_skill_set_id_re', new_skill_set_id);
                insertHrMaster = insertHrMaster.replace('new_horizontal_re', new_horizontal);
                insertHrMaster = insertHrMaster.replace('new_horizontal_id_re', new_horizontal_id);
                insertHrMaster = insertHrMaster.replace('sec_skill1_re', sec_skill1);
                insertHrMaster = insertHrMaster.replace('offered_dt_re', offered_dt);
                insertHrMaster = insertHrMaster.replace('doj_re', doj);
                insertHrMaster = insertHrMaster.replace('status_re', status);
                insertHrMaster = insertHrMaster.replace('employee_mode_re', employee_mode);
                insertHrMaster = insertHrMaster.replace('bill_mode_re', bill_mode);
                insertHrMaster = insertHrMaster.replace('billing_amt_re', billing_amt);
                insertHrMaster = insertHrMaster.replace('bill_rate_re', bill_rate);
                insertHrMaster = insertHrMaster.replace('salary_re', salary);
                insertHrMaster = insertHrMaster.replace('gross_margin_re', gross_margin);
                insertHrMaster = insertHrMaster.replace('relieved_date_re', relieved_date);
                insertHrMaster = insertHrMaster.replace('resigned_month_re', resigned_month);
                insertHrMaster = insertHrMaster.replace('relieving_reason_re', relieving_reason);
                insertHrMaster = insertHrMaster.replace('deployed_bldg_address_re', deployed_bldg_address);
                insertHrMaster = insertHrMaster.replace('pf_re', pf);
                insertHrMaster = insertHrMaster.replace('approval_status_re', approval_status);

                console.log('\n > insertHrMaster[' + row + '] ---> ', insertHrMaster);
                row++;
                if (row >= json.length) {
                    callback(false, hrMaster);
                } else {
                    hrMaster += insertHrMaster + "\n";
                    setTimeout(() => { insertRow(json, tableName); });
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'EMAIL' ? data.split(/\s/).join('').toLowerCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            if (formatType === 'DATE') {
                let date = data.split("/");
                data = date[2] + "/" + date[1] + "/" + date[0];
                data = moment(data).format("YYYY-MM-DD");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};

exports.updateRouting = function (req, res, next) {
    var routingName = (req.body.routingName);
    var tableName = 'prms_pg_hr_master';
    if (routingName) {
        updateMaster(routingName, function (err, data) {
            if (err) {
                console.log("\n > updateRouting err ---> ", data);
                res.status(200).json({ status: 'error', data: data });
            } else {
                res.status(200).json({ status: 'success', data: data });
                // errorLog(tableName, data.fromTo, data.sqlData);
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields' });
    }
    function updateMaster(routingName, callback) {
        try {
            var selectRouting = "SELECT * FROM `prms_app_routing` WHERE routing_name = routing_name_re;";
            selectRouting = selectRouting.replace('routing_name_re', removeString(routingName));

            console.log('\n > selectRouting SQL ---> ', selectRouting);
            db.query(selectRouting, function (err, results, fields) {
                if (results.length > 0) {
                    var updateRouting = "UPDATE `prms_pg_hr_master` SET `is_routing` = '1', `sourced_id` = routing_id WHERE `sourced_by` = sourced_by_re;";
                    updateRouting = updateRouting.replace('routing_id', removeString(results[0].routing_id));
                    updateRouting = updateRouting.replace('sourced_by_re', removeString(routingName));
                    console.log('\n > updateRouting SQL ---> ', updateRouting);
                    db.query(updateRouting, function (err1, results1, fields1) {
                        console.log('\n> updateRouting result ---> ', JSON.stringify(results1));
                        callback(false, results1);
                    });
                } else {
                    callback(false, 'No Routing Found');
                }
                // var fromTo = '', sqlData = '', data;
                // fromTo = '> From: '+ routingName + ' - To: '+ toSet;
                // sqlData = selectRouting;
                // data = {
                //     fromTo: fromTo,
                //     sqlData: sqlData
                // };
                // // callback(false, data);
            });
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            if (formatType === 'DATE_FORMAT') {
                data = moment(data).utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.migrateRouting = function (req, res, next) {
    var tableName = 'prms_pg_hr_master';
    var selectMaster = "select DISTINCT(sourced_by) sourced_by, pgt_code_sourced_by pgt_code from prms_pg_hr_master where is_routing = 0 AND sourced_id is null;";
    console.log('\n > selectMaster SQL ---> ', selectMaster);
    db.query(selectMaster, function (err, results, fields) {
        // res.status(200).json({ status: 'error', data: results });
        if (results.length > 0) {
            updateMaster(results, tableName, function (err, data) {
                if (err) {
                    console.log("\n > migrateRouting err ---> ", data);
                    res.status(200).json({ status: 'error', data: data });
                } else {
                    res.status(200).json({ status: 'success', data: data });
                    errorLog(tableName, 'Sourced By', 'Updated Successfully');
                }
            });
        } else {
            res.status(200).json({ status: 'success', data: 'Record not found' });
        }
    });

    function updateMaster(hrMaster, tableName, callback) {
        try {
            // callback(false, hrMaster);
            var row = 0;
            hrMasterSQL = "";
            var insertRow = function (json, tableName) {
                var data = json[row];
                var selectRouting = "SELECT app_employee_id sourcedID, name sourcedName FROM `prms_app_employees` WHERE user_type = 'TA' AND name = routing_name_re AND employee_code = pgt_code_re";
                selectRouting = selectRouting.replace('routing_name_re', removeString(data.sourced_by));
                selectRouting = selectRouting.replace('pgt_code_re', removeString(data.pgt_code));
                console.log('\n > selectRouting SQL ---> ', selectRouting);
                // row++;
                // if (row >= json.length) {
                //     callback(false, hrMaster);
                // } else {
                //     setTimeout(() => { insertRow(json, tableName); });
                // }
                db.query(selectRouting, function (err, results, fields) {
                    if (results.length > 0) {
                        var updateRouting = "UPDATE `prms_pg_hr_master` SET `is_routing` = '0', `sourced_id` = routing_id WHERE `sourced_by` = sourced_by_re AND pgt_code_sourced_by = pgt_code_re;";
                        updateRouting = updateRouting.replace('routing_id', removeString(results[0].sourcedID));
                        updateRouting = updateRouting.replace('sourced_by_re', removeString(data.sourced_by));
                        updateRouting = updateRouting.replace('pgt_code_re', removeString(data.pgt_code));
                        console.log('\n > updateRouting SQL ---> ', updateRouting);
                        // row++;
                        // if (row >= json.length) {
                        //     callback(false, hrMaster);
                        // } else {
                        //     setTimeout(() => { insertRow(json, tableName); });
                        // }
                        db.query(updateRouting, function (err1, results1, fields1) {
                            console.log('\n> updateRouting result ---> ', JSON.stringify(results1));
                            row++;
                            if (row >= json.length) {
                                callback(false, hrMaster);
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    } else {
                        row++;
                        if (row >= json.length) {
                            callback(false, hrMaster);
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                    }
                });
            };
            insertRow(hrMaster, tableName);
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.newSkillSetAdded = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/hr-master-multiple-skills-shwetha.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '' });
        temp.forEach((res, i) => {
            jsonData.push(res);
        });
    }
    let tableName = 'prms_skill_set';
    insertSkillNew(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > insertSkillNew err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // const fs = require('fs')
            // const content = data;
            // const filePath = rootPath.resolve('.') + '/SQL/2021-06-07/newSkillSetAdded.sql';
            // fs.writeFile(filePath, content, err => {
            //     if (err) {
            //         console.error(err)
            //         return
            //     }
            // });
            errorLog(tableName, 'prms_skill_set', 'Updated Successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function insertSkillNew(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var data = json[row];
                var newSkill = removeString(data.newSkill);
                var selectSkill = "SELECT COUNT(*) cnt FROM `prms_skill_set` WHERE primary_skill = SKILL_RE;";

                selectSkill = selectSkill.replace('SKILL_RE', newSkill);
                console.log('\n > selectSkill[' + row + '] ---> ', selectSkill);
                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     setTimeout(() => { insertRow(json, tableName); });
                // }
                db.query(selectSkill, function (err1, results1, fields1) {
                    // console.log('\n > results1[' + row + '] ---> ', results1[0]);
                    if (results1[0].cnt) {
                        row++;
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                    } else {
                        var insertSkill = "INSERT INTO prms_skill_set (primary_skill, is_active) VALUES (SKILL_RE, 1);"
                        insertSkill = insertSkill.replace('SKILL_RE', newSkill);
                        console.log('\n > insertSkill[' + row + '] ---> ', insertSkill);
                        // row++;
                        // if (row >= json.length) {
                        //     callback(false, 'Migrated Successfully');
                        // } else {
                        //     setTimeout(() => { insertRow(json, tableName); });
                        // }
                        db.query(insertSkill, function (err1, results1, fields1) {
                            row++;
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                setTimeout(() => { insertRow(json, tableName); });
                            }
                        });
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.updateMasterSkill = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/Blank-Horizontals-verticals-2021-06-28.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '' });
        temp.forEach((res, i) => {
            jsonData.push(res);
        });
    }
    let tableName = 'prms_pg_hr_master';
    updateMaster(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > updateMaster err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            // const fs = require('fs')
            // const content = data;
            // const filePath = rootPath.resolve('.') + '/SQL/2021-06-07/updateMasterSkill.sql';
            // fs.writeFile(filePath, content, err => {
            //     if (err) {
            //         console.error(err)
            //         return
            //     }
            // });
            errorLog(tableName, 'prms_skill_set', 'Updated Successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function updateMaster(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var data = json[row];
                var newSkill = removeString(data.PRIMARY_SKILL);
                var hrId = removeString(data.HRID);
                var selectSkill = "SELECT skill_set_id skillId, prms_horizontal_id horzID FROM prms_skill_set where primary_skill = SKILL_RE LIMIT 1;";

                selectSkill = selectSkill.replace('SKILL_RE', newSkill);
                console.log('\n > selectSkill[' + row + '] ---> ', selectSkill);
                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     setTimeout(() => { insertRow(json, tableName); });
                // }
                db.query(selectSkill, function (err1, results1, fields1) {
                    // console.log('\n > results1[' + row + '] ---> ', results1[0]);
                    if (results1.length) {
                        var skillId = removeString(results1[0].skillId);
                        var horzID = removeString(results1[0].horzID);
                        var updateSkill = "UPDATE prms_pg_hr_master SET new_skill_set_id = SKILL_ID_RE, new_horizontal_id = HOR_ID_RE WHERE hr_master_id = MASTER_ID_RE;"
                        updateSkill = updateSkill.replace('SKILL_ID_RE', skillId);
                        updateSkill = updateSkill.replace('HOR_ID_RE', horzID);
                        updateSkill = updateSkill.replace('MASTER_ID_RE', hrId);
                        console.log('\n > updateSkill[' + row + '] ---> ', updateSkill);
                        row++;
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                        // db.query(updateSkill, function (err1, results1, fields1) {
                        //     row++;
                        //     if (row >= json.length) {
                        //         callback(false, 'Migrated Successfully');
                        //     } else {
                        //         setTimeout(() => { insertRow(json, tableName); });
                        //     }
                        // });
                    } else {
                        row++;
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.updateMasterHorizontal = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/Blank-Horizontals-verticals-2021-06-28.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '' });
        temp.forEach((res, i) => {
            jsonData.push(res);
        });
    }
    let tableName = 'prms_pg_hr_master';
    updateMaster(jsonData, tableName, function (err, data) {
        if (err) {
            console.log("\n > updateMaster err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            errorLog(tableName, 'prms_skill_set', 'Updated Successfully');
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function updateMaster(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var data = json[row];
                var newSkill = removeString(data.PRIMARY_SKILL);
                var hrId = removeString(data.HRID);
                var selectSkill = "SELECT skill.primary_skill PrimarySkill, domain.horizontal_type Horizontal, skill.prms_horizontal_id HorizontalID FROM prms_skill_set skill LEFT JOIN `prms_horizontal` domain ON domain.horizontal_id = skill.prms_horizontal_id where skill.primary_skill = SKILL_RE LIMIT 1;";

                selectSkill = selectSkill.replace('SKILL_RE', newSkill);
                console.log('\n > selectSkill[' + row + '] ---> ', selectSkill);
                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     setTimeout(() => { insertRow(json, tableName); });
                // }
                db.query(selectSkill, function (err1, results1, fields1) {
                    // console.log('\n > results1[' + row + '] ---> ', results1[0]);
                    if (results1.length) {
                        console.log('\n > results1[' + row + '] ---> ', JSON.stringify(results1));
                        var HorizontalID = removeString(results1[0].HorizontalID);
                        var Horizontal = removeString(results1[0].Horizontal);

                        var updateSkill = "UPDATE prms_pg_hr_master SET new_horizontal = new_horizontal_RE, new_horizontal_id = new_horizontal_id_RE WHERE hr_master_id = MASTER_ID_RE;"
                        updateSkill = updateSkill.replace('new_horizontal_RE', Horizontal);
                        updateSkill = updateSkill.replace('new_horizontal_id_RE', HorizontalID);
                        updateSkill = updateSkill.replace('MASTER_ID_RE', hrId);
                        console.log('\n > updateSkill[' + row + '] ---> ', updateSkill);
                        row++;
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                        // db.query(updateSkill, function (err1, results1, fields1) {
                        //     row++;
                        //     if (row >= json.length) {
                        //         callback(false, 'Migrated Successfully');
                        //     } else {
                        //         setTimeout(() => { insertRow(json, tableName); });
                        //     }
                        // });
                    } else {
                        row++;
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            setTimeout(() => { insertRow(json, tableName); });
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }

    function errorLog(tableName, error, jsonData) {
        var SQL = 'INSERT INTO `prms_migration_error_logs` (`table_name`, `error`, `json_data`) VALUES ("tableNameRe", "errorRe", "jsonDataRe");';
        SQL = SQL.replace('tableNameRe', tableName);
        SQL = SQL.replace('errorRe', error);
        SQL = SQL.replace('jsonDataRe', jsonData);
        console.log('\n > errorLog SQL ---> ', SQL);
        db.query(SQL, function (err, results, fields) { });
    }
};

exports.migrateInternelEmpNames = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/migrate/internel-employees.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }

    internelEmps(jsonData, 'prms_app_employees', function (err, data) {
        if (err) {
            console.log("\n > internelEmps err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > internelEmps success ---> ", data);
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function internelEmps(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                // var name = removeString(data.EMP_NAME);
                var fName = removeString(data.FIRST_NAME);
                var mName = removeString(data.MIDDLE_NAME);
                var lName = removeString(data.LAST_NAME);
                // var reCode = removeString(data.RE_CODE, 'PGTCODE');
                // var emailId = removeString(data.EMAIL_ID);
                // var userType = removeString(data.USER_TYPE, 'ROLES');
                // var designation = removeString(data.DESIGNATION, 'DESIGNATION');
                // var status = removeString(data.STATUS, 'STATUS');
                var employeeCode = removeString(data.EMP_ID, 'PGTCODE');


                // var SQL = "UPDATE `prms_app_employees` SET `name` = nameRe, `first_name` = fNameRe, `middle_name` = mNameRe, `last_name` = lNameRe, `report_manager_id` = (SELECT * FROM (SELECT `app_employee_id` FROM `prms_app_employees` WHERE `employee_code` = eCodeRe) AS temp), `email_id` = emailIdRe, `user_type` = userTypeRe, `designation` = designationRe, `status` = statusRe WHERE `employee_code` = employeeCodeRe;";
                var SQL = "UPDATE `prms_app_employees` SET `first_name` = fNameRe, `middle_name` = mNameRe, `last_name` = lNameRe WHERE `employee_code` = employeeCodeRe;";

                // SQL = SQL.replace('nameRe', name);
                SQL = SQL.replace('fNameRe', fName);
                SQL = SQL.replace('mNameRe', mName);
                SQL = SQL.replace('lNameRe', lName);
                // SQL = SQL.replace('eCodeRe', reCode);
                // SQL = SQL.replace('emailIdRe', emailId);
                // SQL = SQL.replace('userTypeRe', userType);
                // SQL = SQL.replace('designationRe', designation);
                // SQL = SQL.replace('statusRe', status);
                SQL = SQL.replace('employeeCodeRe', employeeCode);

                console.log('\n\n > SQL[' + row + '] ---> ', SQL);
                // row++;
                // if (row >= json.length) {
                //     callback(false, 'Migrated Successfully');
                // } else {
                //     insertRow(json, tableName);
                // }

                db.query(SQL, function (err, results, fields) {
                    row++;
                    if (err) {
                        console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            insertRow(json, tableName);
                        }
                    } else {
                        if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            insertRow(json, tableName);
                        }
                    }
                });
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            data = formatType === 'STATUS' ? data.split(/\s/).join('_').toUpperCase() : data;
            if (formatType === 'ROLES') {
                if (data.split(/\s/).join('').toLowerCase() === ('SuperAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'SuperAdmin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Admin').split(/\s/).join('').toLowerCase()) {
                    data = 'Admin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'HRAdmin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HREdit').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRReadOnly').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'PLHead';
                } else if (data.split(/\s/).join('').toLowerCase() === ('AM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                } else if (data.split(/\s/).join('').toLowerCase() === ('DM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                } else if (data.split(/\s/).join('').toLowerCase() === ('TA').split(/\s/).join('').toLowerCase()) {
                    data = 'TA';
                }
            } else if (formatType === 'DESIGNATION') {
                if (data.split(/\s/).join('').toLowerCase() === ('Account Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Delivery Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Finance SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'Finance SPOC';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HR SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'HR SPOC';
                } else if (data.split(/\s/).join('').toLowerCase() === ('P&L Head').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Sourced By').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                } else if (data.split(/\s/).join('').toLowerCase() === ('SourcedBy').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Trainee TA').split(/\s/).join('').toLowerCase()) {
                    data = 'Trainee TA';
                }
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }
}

exports.migrateInternelReportingManages = function (req, res, next) {
    let filePath = rootPath.resolve('.') + '/app/xlsx/migrate/internel-employees.xlsx';
    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
    var sheets = file.SheetNames;
    let jsonData = [];
    for (let i = 0; i < sheets.length; i++) {
        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
        temp.forEach((res) => {
            jsonData.push(res)
        });
    }

    internelEmps(jsonData, 'prms_app_employees', function (err, data) {
        if (err) {
            console.log("\n > internelEmps err ---> ", data);
            res.status(200).json({ status: 'error', data: data });
        } else {
            console.log("\n > internelEmps success ---> ", data);
            res.status(200).json({ status: 'success', data: data });
        }
    });
    function internelEmps(jsonData, tableNames, callback) {
        try {
            var row = 0;
            var insertRow = function (json, tableName) {
                var data = json[row];
                // console.log('\n > ' + tableName + ' -- insertRow[' + row + '] ---> ', JSON.stringify(data));

                // var name = removeString(data.EMP_NAME);
                // var fName = removeString(data.FIRST_NAME);
                // var mName = removeString(data.MIDDLE_NAME);
                // var lName = removeString(data.LAST_NAME);
                var reCode = removeString(data.RE_CODE, 'PGTCODE');
                // var emailId = removeString(data.EMAIL_ID);
                // var userType = removeString(data.USER_TYPE, 'ROLES');
                // var designation = removeString(data.DESIGNATION, 'DESIGNATION');
                // var status = removeString(data.STATUS, 'STATUS');
                var employeeCode = removeString(data.EMP_ID, 'PGTCODE');


                var SQL = "UPDATE `prms_app_employees` SET `report_manager_id` = (SELECT * FROM (SELECT `app_employee_id` FROM `prms_app_employees` WHERE `employee_code` = eCodeRe) AS temp) WHERE `employee_code` = employeeCodeRe AND (`report_manager_id` = '' OR `report_manager_id` IS NULL);";
                // var SQL = "UPDATE `prms_app_employees` SET `first_name` = fNameRe, `middle_name` = mNameRe, `last_name` = lNameRe WHERE `employee_code` = employeeCodeRe;";

                // SQL = SQL.replace('nameRe', name);
                // SQL = SQL.replace('fNameRe', fName);
                // SQL = SQL.replace('mNameRe', mName);
                // SQL = SQL.replace('lNameRe', lName);
                SQL = SQL.replace('eCodeRe', reCode);
                // SQL = SQL.replace('emailIdRe', emailId);
                // SQL = SQL.replace('userTypeRe', userType);
                // SQL = SQL.replace('designationRe', designation);
                // SQL = SQL.replace('statusRe', status);
                SQL = SQL.replace('employeeCodeRe', employeeCode);
                // console.log('\n\n > SQL[' + row + '] ---> ', reCode);

                if (reCode === "'NA'") {
                    row++;
                    if (row >= json.length) {
                        callback(false, 'Migrated Successfully');
                    } else {
                        insertRow(json, tableName);
                    }
                } else {
                    console.log('\n\n > SQL[' + row + '] ---> ', SQL);
                    db.query(SQL, function (err, results, fields) {
                        row++;
                        if (err) {
                            console.log('\n > SQL[' + row + '] err ---> ', err.sqlMessage);
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        } else {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                insertRow(json, tableName);
                            }
                        }
                    });
                }
            };
            insertRow(jsonData, tableNames.toString().toLowerCase());
        } catch (e) {
            console.log('\n > try catch error ---> ', e);
            callback(true, 'try catch error');
        }
    };

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = formatType === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            data = formatType === 'UPPERCASE' ? data.toUpperCase() : data;
            data = formatType === 'STATUS' ? data.split(/\s/).join('_').toUpperCase() : data;
            if (formatType === 'ROLES') {
                if (data.split(/\s/).join('').toLowerCase() === ('SuperAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'SuperAdmin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Admin').split(/\s/).join('').toLowerCase()) {
                    data = 'Admin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRAdmin').split(/\s/).join('').toLowerCase()) {
                    data = 'HRAdmin';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HREdit').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HRReadOnly').split(/\s/).join('').toLowerCase()) {
                    data = 'HR';
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'PLHead';
                } else if (data.split(/\s/).join('').toLowerCase() === ('AM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                } else if (data.split(/\s/).join('').toLowerCase() === ('DM').split(/\s/).join('').toLowerCase()) {
                    data = 'BM';
                } else if (data.split(/\s/).join('').toLowerCase() === ('TA').split(/\s/).join('').toLowerCase()) {
                    data = 'TA';
                }
            } else if (formatType === 'DESIGNATION') {
                if (data.split(/\s/).join('').toLowerCase() === ('Account Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Delivery Manager').split(/\s/).join('').toLowerCase()) {
                    data = 'Business Manager';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Finance SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'Finance SPOC';
                } else if (data.split(/\s/).join('').toLowerCase() === ('HR SPOC').split(/\s/).join('').toLowerCase()) {
                    data = 'HR SPOC';
                } else if (data.split(/\s/).join('').toLowerCase() === ('P&L Head').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                } else if (data.split(/\s/).join('').toLowerCase() === ('PLHead').split(/\s/).join('').toLowerCase()) {
                    data = 'P&L Head';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Sourced By').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                } else if (data.split(/\s/).join('').toLowerCase() === ('SourcedBy').split(/\s/).join('').toLowerCase()) {
                    data = 'Sourced By';
                } else if (data.split(/\s/).join('').toLowerCase() === ('Trainee TA').split(/\s/).join('').toLowerCase()) {
                    data = 'Trainee TA';
                }
            }
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }
}

exports.getInternelEmps = function (req, res, next) {
    var SQL = "";
    SQL += "SELECT COUNT(*) names FROM `prms_app_employees` WHERE first_name IS NOT NULL;";
    SQL += "SELECT COUNT(*) reprortingManager FROM `prms_app_employees` WHERE report_manager_id IS NOT NULL;";
    db.query(SQL, function (err, results, fields) {
        if (err) {
            console.log("\n > getInternelEmps err ---> ", err);
            res.status(200).json({ status: 'error', data: err });
        } else {
            let DATA = {
                names: results[0][0].names,
                reprortingManager: results[1][0].reprortingManager
            };
            console.log("\n > getInternelEmps success ---> ", DATA);
            res.status(200).json({ status: 'success', data: DATA });
        }
    });
}

exports.employeeMoveNextFinancialYear = function (req, res, next) {
    const financialYear = new Date().getFullYear()+'-'+(new Date().getFullYear()+1);
    
    SQL1 = "SELECT `hr_master_id`,`prms_client_cc_mapping_id`,`billing_amt`,`bill_rate`,`salary`,`bgv_cost`,`gross_margin` FROM `prms_pg_hr_master` WHERE (`relieved_date` IS NULL OR `relieved_date`> '2021-03-31') AND `employee_mode`='Contract' AND `status` NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed') AND approval_status = 'Approved';";
    db.query(SQL1, function (err, results, fields) {
        if (err) {
            console.log("\n > getOnboardings err ---> ", err);
            res.status(200).json({ status: 'error', data: err });
        } else {

            SQL2 = "INSERT INTO `prms_mapping_history`(`hr_master_id`, `client_cc_mapping_id`, `billing_amt`, `bill_rate`, `salary`, `bgv_cost`, `gross_margin`, `financial_year`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES INSERT_VALUE";
            INSERT_VALUE = "(hr_master_id_re, client_cc_mapping_id_re, billing_amt_re, bill_rate_re, salary_re, bgv_cost_re, gross_margin_re, financial_year_re,loggedInUser,timesNow,loggedInUser, timesNow)";
            INSERT_VALUE_RE = "";

            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

            results.forEach(result => {
               var temp = INSERT_VALUE;
 
               temp = temp.replace("hr_master_id_re",removeString(result.hr_master_id,true));
               temp = temp.replace("client_cc_mapping_id_re",removeString(result.prms_client_cc_mapping_id,true));
               temp = temp.replace("billing_amt_re",removeString(result.billing_amt,true));
               temp = temp.replace("bill_rate_re",removeString(result.bill_rate,true));
               temp = temp.replace("salary_re",removeString(result.salary,true));
               temp = temp.replace("bgv_cost_re",removeString(result.bgv_cost,true));
               temp = temp.replace("gross_margin_re",removeString(result.gross_margin,true));
               temp = temp.replace("financial_year_re",removeString(financialYear,true));
               temp = temp.replace(/loggedInUser/g,removeString('733',true));
               temp = temp.replace(/timesNow/g,removeString(timesNow,true));

               INSERT_VALUE_RE+= (INSERT_VALUE_RE =="") ? temp : ","+temp;
            });

            SQL2 = SQL2.replace("INSERT_VALUE",INSERT_VALUE_RE);
            db.query(SQL2, function (err, results, fields) {});
            res.status(200).json({ status: 'success' });
        }
    });

    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
}