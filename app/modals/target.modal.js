var db = require('../db/manageDB');
var query = require('../queries/target.query');
var moment = require('moment-timezone');

exports.getTarget = function (req, res, next) {
    var SQL = query.getTarget;
    console.log("\n > getTarget SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getTarget SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getTargetById = function (req, res, next) {
    var params = req.params;
    var SQL = query.getTargetById;
    SQL = SQL.replace("TARGETID", params.id.toString().trim());

    console.log("\n > getTargetById SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getTargetById SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = "";
            if (results.length > 0) {
                data = { status: 'success', data: results[0] };
            } else {
                data = { status: 'success', data: "Record Not Found" };
            }
            res.status(200).json(data);
        }
    });
};

exports.createTarget = function (req, res, next) {
    var params = req.body;
    if (params.employeeId == undefined || params.employeeName == undefined || params.year == undefined || params.quater == undefined || params.target == undefined || params.operationalCost == undefined || params.loggedInUserId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.employeeId.toString().trim() == "" || params.employeeName.toString().trim() == "" || params.year.toString().trim() == "" || params.quater.toString().trim() == "" || params.target.toString().trim() == "" || params.operationalCost.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.createTargetDetails;
        SQL = SQL.replace("ecode_re", params.employeeId.toString().trim());
        SQL = SQL.replace("name_re", params.employeeName.toString().trim());
        SQL = SQL.replace("target_re", params.target.toString().trim());
        SQL = SQL.replace("quater_re", params.quater.toString().trim());
        SQL = SQL.replace("incentive_re", '0');
        SQL = SQL.replace("gross_margin_re", '0');
        SQL = SQL.replace("target_achieved_re", '0');
        SQL = SQL.replace("status_re", 'ACTIVE');
        SQL = SQL.replace("fyear_re", params.year.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("created_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("created_at_re", timesNow);
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("operational_cost_re", params.operationalCost.toString().trim());

        console.log("\n > createTarget SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createTarget SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Target Created Successfully' });
            }
        });
    }
};

exports.updateTarget = function (req, res, next) {
    var params = req.body;
    if (params.targetId == undefined || params.target == undefined || params.operationalCost == undefined || params.loggedInUserId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.targetId.toString().trim() == "" || params.target.toString().trim() == "" || params.operationalCost.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateTargetDetails;
        SQL = SQL.replace("target_re", params.target.toString().trim());
        SQL = SQL.replace("operational_cost_re", params.operationalCost.toString().trim());
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("target_id_re", params.targetId.toString().trim());

        console.log("\n > updateTarget SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateTarget SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Target Updated Successfully' });
            }
        });
    }
};

exports.updateTargetStatus = function (req, res, next) {
    var params = req.body;
    if (params.targetId == undefined || params.status == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.targetId.toString().trim() == "" || params.status.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateTargetStatus;
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("target_id_re", params.targetId.toString().trim());

        console.log("\n > updateTargetStatus SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateTargetStatus SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Target Status Updated Successfully' });
            }
        });
    }
};

exports.deleteTarget = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deleteTargetDetails;
        SQL = SQL.replace("is_delete_re", '1');
        SQL = SQL.replace("target_id_re", params.id.toString().trim());

        console.log("\n > deleteTarget SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deleteTarget SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Target Deleted Successfully' });
            }
        });
    }
};