var db = require('../db/manageDB');
var query = require('../queries/endClient.query');
var moment = require('moment-timezone');

exports.getAllEndClient = function (req, res, next) {
    var SQL = query.getAllEndClient;
    console.log("\n > getAllEndClient SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getAllEndClient SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
}

exports.createEndClient = function (req, res, next) {
    params = req.body;
    if (params.verticalId == undefined || params.endClientName == undefined || params.status == undefined || params.loginUserId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.verticalId.toString().trim() == "" || params.endClientName.toString().trim() == "" || params.status.toString().trim() == "" || params.loginUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.insertEndClient;
        SQL = SQL.replace("prms_vertical_id_re", params.verticalId.toString().trim());
        SQL = SQL.replace("endclient_name_re", params.endClientName.toString().trim());
        SQL = SQL.replace("is_active_re", params.status.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("created_by_re", params.loginUserId.toString().trim());
        SQL = SQL.replace("created_at_re", timesNow);
        SQL = SQL.replace("updated_by_re", params.loginUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);

        console.log("\n > insertEndClient SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > insertEndClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Endclient Created Successfully' });
            }
        });
    }
};


exports.updateEndClient = function (req, res, next) {
    params = req.body;
    params.endClientId = req.params.endClientId;

    if (params.endClientId == undefined || params.verticalId == undefined || params.status == undefined || params.endClientName == undefined || params.loginUserId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.endClientId.toString().trim() == "" || params.verticalId.toString().trim() == "" || params.status.toString().trim() == "" || params.endClientName.toString().trim() == "" || params.loginUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateEndClient;
        SQL = SQL.replace("prms_vertical_id_re", params.verticalId.toString().trim());
        SQL = SQL.replace("endclient_name_re", params.endClientName.toString().trim());
        SQL = SQL.replace("is_active_re", params.status.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_by_re", params.loginUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("endclient_id_re", params.endClientId.toString().trim());

        console.log("\n > updateEndClient SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateEndClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Endclient Updated Successfully' });
            }
        });
    }
};

exports.getEndClientForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "AND v.vertical_name LIKE '%SEARCH_RE%' OR en.endclient_name LIKE '%SEARCH_RE%'  OR en.is_active LIKE '%SEARCH_RE%' OR en.created_by LIKE '%SEARCH_RE%' OR en.created_at LIKE '%SEARCH_RE%' OR en.updated_by LIKE '%SEARCH_RE%' OR en.updated_at LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getEndClientForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getEndClientForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEndClientForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};