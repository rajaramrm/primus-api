var db = require('../db/manageDB');
var query = require('../queries/clientMap.query');
var moment = require('moment-timezone');

exports.getClientMaps = function (req, res, next) {
    var SQL = query.getClientMaps;
    var WHERE = '';
    if (req.params.status) {
        WHERE = 'WHERE cm.is_active = ' + req.params.status;
    }
    SQL = SQL.replace('WHERERE', WHERE);
    console.log("\n > getClientMaps SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClientMaps SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};
exports.getClientMapByRole = function (req, res, next) {
    // var SQL = "SELECT client_cc_mapping_id AS clientMappingId,CONCAT(client_name,IF(cost_center = 'NA','',CONCAT(' (',cost_center,')')),IF(msp_name = 'NA','',CONCAT(' - ',msp_name))) AS clientName, cost_center AS costCenter, hr_spoc AS hrSpoc, fin_spoc AS financeSpoc, pl_head AS plHead, bsns_mgr AS businessManager, pgt_code_pandl_head AS plHeadCode, pgt_code_bsns_mgr AS businessManagerCode, msp_name AS mspName, is_active AS status FROM `prms_client_cc_mapping` WHERERE;";
    // SQL = "SELECT cm.client_cc_mapping_id AS clientMappingId, CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')')),IF((msp.name = '' OR msp.name IS NULL),'',CONCAT(' - ',msp.name))) AS clientName, cs.name AS costCenter, cm.hr_spoc AS hrSpoc, cm.fin_spoc AS financeSpoc, cm.pl_head AS plHead, cm.bsns_mgr AS businessManager, cm.pgt_code_pandl_head AS plHeadCode, cm.pgt_code_bsns_mgr AS businessManagerCode, IF((msp.name = '' OR msp.name IS NULL),'NA',msp.name) AS mspName, cm.is_active AS status FROM prms_client_cc_mapping cm LEFT JOIN prms_clients AS cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id LEFT JOIN prms_msp_names AS msp ON msp.client_id = cm.client_id AND msp.cost_center_id = cm.cost_center_id WHERERE;";
    var SQL = query.getClientMapByRole;
    var WHERE = '';
    if (req.body.userRole && req.body.pgtCode) {
        if (req.body.userRole === 'BUHead') {
            WHERE = 'AND pgt_code_pandl_head = "' + req.body.pgtCode + '"';
        } else if (req.body.userRole === 'BM') {
            WHERE = 'AND pgt_code_bsns_mgr = "' + req.body.pgtCode + '"';
        } else if(req.body.userRole !== 'SuperAdmin' && req.body.userRole !== 'Admin' && req.body.userRole !== 'HRAdmin' && req.body.userRole !== 'HR' && req.body.userRole !== 'Finance' && req.body.userRole !== 'FinanceAdmin' && req.body.userRole !== 'FinanceReadOnly' && req.body.userRole !== 'TA' && req.body.userRole !== 'PayrollAdmin' && req.body.userRole !== 'HRAdminReadOnly') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
        }
        SQL = SQL.replace('WHERERE', WHERE);
        console.log("\n > getClientMapByRole SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClientMapByRole SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent Role, PGTCode' });
    }
};
// PGT4222
// PGT6721

exports.getClientNames = function (req, res, next) {
    var SQL = query.getClientNames;
    console.log("\n > getClientNames SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClientNames SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getEmployeeByRole = function (req, res, next) {
    var params = req.params;
    var roleType = params.roleType.toString().trim();
    var SQL = query.getEmployeeByRole;
    if (roleType === 'HR') {
        SQL = SQL.replace("ROLETYPE", "'HRAdmin','HR'");
    } else if (roleType === 'Finance') {
        SQL = SQL.replace("ROLETYPE", "'Finance','FinanceAdmin','FinanceReadOnly'");
    } else {
        SQL = SQL.replace("ROLETYPE", "'" + roleType + "'");
    }

    console.log("\n > getEmployeeByRole SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEmployeeByRole SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let obj = {
                'empId': '',
                'empCode': 'NA',
                'empName': 'NA'
            };
            if (roleType === 'BM') {
                results.push(obj);
            }
            let data = { status: 'success', data: results };
            res.status(200).json(data);
        }
    });
};

exports.getCostCenter = function (req, res, next) {
    var params = req.params;
    var SQL = query.costCenterByClient;
    SQL = SQL.replace("client_id_re", params.clientId.toString().trim());
    console.log("\n > getCostCenter SQL ---> ", SQL);

    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getCostCenter SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = { status: 'success', data: results };
            res.status(200).json(data);
        }
    });
};

exports.getMsp = function (req, res, next) {
    var clientId = req.params.clientId;
    var costCenterId = req.params.costCenterId;
    // var SQL = "SELECT DISTINCT(`name`) text, value, id FROM `prms_msp_names`;";
    // var SQL = "SELECT id mspId, name mspName FROM prms_msp_names WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe';";
    var SQL = "SELECT client_id clientId, cost_center_id costCenterId, GROUP_CONCAT(id) mspId, GROUP_CONCAT(name) mspName, GROUP_CONCAT(fee_type) mspType, SUM(value) mspFee FROM prms_msp_names WHERE fee_type='MSP' AND client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND `is_active`=1 GROUP BY client_id, cost_center_id;"
    SQL = SQL.replace('clientIdRe', clientId);
    SQL = SQL.replace('costCenterIdRe', costCenterId);
    console.log("\n > getMsp SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getRmt SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results[0] });
        }
    });
    // let data = { status: 'success', data: [{ "id": "Allegis", "text": "Allegis" }, { "id": "GBS", "text": "GBS" }, { "id": "Manpower", "text": "Manpower" }, { "id": "ObjectWin", "text": "ObjectWin" }, { "id": "Randstad", "text": "Randstad" }, { "id": "Zerochaos", "text": "Zerochaos" }] };
    // res.status(200).json(data);
};

exports.getMspNames = function (req, res, next) {
    SQL = "SELECT DISTINCT(`name`) mspName, id, value mspValue FROM `prms_msp_names` ORDER BY id DESC;";
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getRmt SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.createMsp = function (req, res, next) {
    var params = req.body;
    if (
        params.mspName == undefined
        || params.mspValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        let mspName = params.mspName.toString().trim();
        let mspValue = params.mspValue.toString().trim();

        if (
            mspName == ""
            || mspValue == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        } else {
            var SQL = "INSERT INTO prms_msp_names (name, value) VALUES ('mspName', 'mspValue');";
            SQL = SQL.replace("mspName", mspName);
            SQL = SQL.replace("mspValue", mspValue);
            console.log("\n > createMspName SQL ---> ", SQL);

            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > getRmt SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Created Successfully' });
                }
            });
        }
    }
}


exports.updateMsp = function (req, res, next) {
    var params = req.body;
    var mspId = req.params.id;
    if (
        params.mspName == undefined
        || params.mspValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        let mspName = params.mspName.toString().trim();
        let mspValue = params.mspValue.toString().trim();

        if (
            mspName == ""
            || mspValue == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        } else {
            var SQL = "UPDATE prms_msp_names SET name = 'mspName', value = 'mspValue' WHERE id = 'mspId';";
            SQL = SQL.replace("mspName", mspName);
            SQL = SQL.replace("mspValue", mspValue);
            SQL = SQL.replace("mspId", mspId);
            console.log("\n > updateMsp SQL ---> ", SQL);

            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > getRmt SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Updated Succefully' });
                }
            });
        }
    }
}

exports.getRmt = function (req, res, next) {
    var SQL = query.getRmt;
    console.log("\n > getRmt SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getRmt SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.createClientMap = function (req, res, next) {
    var params = req.body;
    if (
        params.clientNameNew === undefined
        || params.costCenterNew === undefined
        || params.clientId === undefined
        || params.clientName === undefined
        || params.costCenterId === undefined
        || params.costCenter === undefined
        || params.mspId === undefined
        || params.mspName === undefined
        || params.mspType === undefined
        || params.mspFee === undefined
        || params.hrSpoc === undefined
        || params.hrSpocId === undefined
        || params.financeSpoc === undefined
        || params.financeSpocId === undefined
        || params.plHead === undefined
        || params.pgtPlHeadCode === undefined
        || params.businessManager === undefined
        || params.pgtBusinessManagerCode === undefined
        || params.loggedInUserId === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        let clientNameNew = params.clientNameNew;
        let costCenterNew = params.costCenterNew;

        var clientName = params.clientName.toString().trim();
        var clientId = params.clientId ? params.clientId.toString().trim() : '';
        var costCenter = params.costCenter.toString().trim();
        var costCenterId = params.costCenterId ? params.costCenterId.toString().trim() : '';
        var hrSpoc = params.hrSpoc.toString().trim();
        var hrSpocId = params.hrSpocId.toString().trim();
        var financeSpoc = params.financeSpoc.toString().trim();
        var financeSpocId = params.financeSpocId.toString().trim();
        var plHead = params.plHead.toString().trim();
        var pgtPlHeadCode = params.pgtPlHeadCode.toString().trim();
        var businessManager = params.businessManager.toString().trim();
        var pgtBusinessManagerCode = params.pgtBusinessManagerCode.toString().trim();
        var isActive = '1';
        var loggedInUserId = params.loggedInUserId.toString().trim();

        var mspId = params.mspId.toString().trim();
        var mspName = params.mspName.toString().trim();
        var mspType = params.mspType.toString().trim();
        var mspFee = params.mspFee.toString().trim();


        if (
            clientName === ""
            || clientId === ""
            || costCenter === ""
            || costCenterId === ""
            || hrSpoc === ""
            || hrSpocId === ""
            || plHead === ""
            || pgtPlHeadCode === ""
            || financeSpoc === ""
            || financeSpocId === ""
            || loggedInUserId === ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        }
        else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.createClientMap;
            SQL = SQL.replace("clientName", clientName);
            SQL = SQL.replace("clientId", clientId);
            SQL = SQL.replace("cost_Center_Re", costCenter);
            SQL = SQL.replace("costCenterIdRe", costCenterId);
            SQL = SQL.replace("hrSpoc", hrSpoc);
            SQL = SQL.replace("pgtHrSpoc", hrSpocId);
            SQL = SQL.replace("plHead", plHead);
            SQL = SQL.replace("pgtPlHeadCode", pgtPlHeadCode);
            SQL = SQL.replace("financeSpoc", financeSpoc);
            SQL = SQL.replace("pgtFinanceSpoc", financeSpocId);
            SQL = SQL.replace("businessManager", businessManager);
            SQL = SQL.replace("pgtBusinessManagerCode", pgtBusinessManagerCode);
            SQL = SQL.replace("isActive", isActive);
            SQL = SQL.replace(/loggedInUserId/g, loggedInUserId);
            SQL = SQL.replace(/timesNow/g, timesNow);
            SQL = SQL.replace("newMspIdRe", mspId);
            SQL = SQL.replace("newMspNameRe", mspName);
            SQL = SQL.replace("newMspTypeRe", mspType);
            SQL = SQL.replace("newMspFeeRe", mspFee);

            console.log("\n > createClientMap SQL ---> ", SQL);
            if (clientNameNew && costCenterNew) {
                addClientNameNew(clientName, costCenter);
            } else if (costCenterNew) {
                addCostCenterNew(clientId, costCenter);
            }
            // res.status(200).json({ status: 'success', data: SQL });
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > getClientMapList SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    console.log("\n > createClientMap Created Successfully");
                    res.status(200).json({ status: 'success', data: 'Created Successfully' });
                    // updateMspFee(results, mspName);
                }
            });
        }
    }

    function addClientNameNew(clientName, costCenter) {
        var clientCheck = "SELECT * FROM `prms_clients` WHERE `name` = '" + clientName + "'";
        var clientInsert = "INSERT INTO `prms_clients` (`name`) VALUES ('" + clientName + "')";
        var costCenterInsert = "INSERT INTO `prms_cost_centers` (`client_id`,`name`) VALUES ('clientID', 'costCENTER');";
        console.log('\n > clientCheck ---> ', clientCheck);
        db.query(clientCheck, function (error1, results1, fields1) {
            if (results1.length == 0) {
                console.log('\n > clientInsert ---> ', clientInsert);
                db.query(clientInsert, function (error2, results2, fields2) {
                    addCostCenterNew(results2.insertId, costCenter);
                    // costCenterInsert = costCenterInsert.replace('clientID', results2.insertId);
                    // costCenterInsert = costCenterInsert.replace('costCENTER', costCenter);
                    // console.log('\n > costCenterInsert ---> ', costCenterInsert);
                    // console.log('\n > clientInsert results ---> ', JSON.stringify(results2));
                    // db.query(costCenterInsert, function (error2, results2, fields2) { });
                });

            } else {
                addCostCenterNew(results1[0].client_id, costCenter);
                // console.log('\n > clientInsert results ---> ', JSON.stringify(results1[0]));
                // costCenterInsert = costCenterInsert.replace('clientID', results1[0].client_id);
                // costCenterInsert = costCenterInsert.replace('costCENTER', costCenter);
                // console.log('\n > costCenterInsert ---> ', costCenterInsert);
                // db.query(costCenterInsert, function (error2, results2, fields2) {});
            }
        });
    }

    function addCostCenterNew(clientId, costCenter) {
        console.log("\n > addCostCenterNew clientId ---> ", clientId);
        console.log("\n > addCostCenterNew costCenter ---> ", costCenter);
        var clientCheck = "SELECT * FROM `prms_cost_centers` WHERE `client_id` = '" + clientId + "' AND  name = '" + costCenter + "'";
        var costCenterInsert = "INSERT INTO `prms_cost_centers` (`client_id`,`name`) VALUES ('clientID', 'costCENTER');";
        console.log('\n > clientCheck ---> ', clientCheck);
        db.query(clientCheck, function (error1, results1, fields1) {
            if (results1.length == 0) {
                costCenterInsert = costCenterInsert.replace('clientID', clientId);
                costCenterInsert = costCenterInsert.replace('costCENTER', costCenter);
                console.log('\n > costCenterInsert ---> ', costCenterInsert);
                db.query(costCenterInsert, function (error2, results2, fields2) { });
            }
        });
    }

    // function addClientNameNew(clientName, costCenter) {
    //     var clientCheck = "SELECT * FROM `prms_clients` WHERE `name` = '" + clientName + "'";
    //     var clientInsert = "INSERT INTO `prms_clients` (`name`) VALUES ('" + clientName + "')";
    //     var costCenterInsert = "INSERT INTO `prms_cost_centers` (`client_id`,`name`) VALUES ('clientID', 'costCENTER');";
    //     console.log('\n > clientCheck ---> ', clientCheck);
    //     db.query(clientCheck, function (error1, results1, fields1) {
    //         if (results1.length == 0) {
    //             console.log('\n > clientInsert ---> ', clientInsert);
    //             db.query(clientInsert, function (error2, results2, fields2) {
    //                 costCenterInsert = costCenterInsert.replace('clientID', results2.insertId);
    //                 costCenterInsert = costCenterInsert.replace('costCENTER', costCenter);
    //                 console.log('\n > costCenterInsert ---> ', costCenterInsert);
    //                 console.log('\n > clientInsert results ---> ', JSON.stringify(results2));
    //                 db.query(costCenterInsert, function (error2, results2, fields2) { });
    //             });
    //         }
    //     });
    // }

    // function addRmtSpocNew(rmtSpoc) {
    //     var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    //     var rmtCheck = "SELECT * FROM `prms_rmtspoc` WHERE `name` = '" + rmtSpoc + "';";
    //     var rmtInsert = "INSERT INTO `prms_rmtspoc` (`name`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES ('NAMERE', '1', '1', 'timesNow', '1', 'timesNow');";
    //     rmtInsert = rmtInsert.replace('NAMERE', rmtSpoc);
    //     rmtInsert = rmtInsert.replace(/timesNow/g, timesNow);
    //     console.log('\n > rmtCheck ---> ', rmtCheck);
    //     db.query(rmtCheck, function (error1, results1, fields1) {
    //         if (results1.length == 0) {
    //             console.log('\n > rmtInsert ---> ', rmtInsert);
    //             db.query(rmtInsert, function (error2, results2, fields2) {
    //                 console.log('\n > rmtInsert results ---> ', JSON.stringify(results2));
    //             });
    //         }
    //     });
    // }

};

exports.updateClientMap = function (req, res, next) {
    var params = req.body;
    if (params.clientMappingId == undefined || params.isActive == undefined || params.serviceMonth == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var serviceMonth = params.serviceMonth.toString().trim();
        var clientMappingId = params.clientMappingId.toString().trim();
        var isActive = params.isActive.toString().trim();
        if (clientMappingId == "" || isActive == "" || serviceMonth == "") {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        }
        else if (isActive == 1) {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.updateClientMap;
            SQL = SQL.replace("isActive", isActive);
            SQL = SQL.replace("clientMappingId", clientMappingId);
            SQL = SQL.replace(/timesNow/g, timesNow);
            console.log("\n > updateClientMap SQL  ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateClientMap SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                }
            });
        }
        else {
            var CSQL = query.checkActiveEmp;
            CSQL = CSQL.replace(/SERVICEMONTH/g, serviceMonth);
            CSQL = CSQL.replace(/ClientMappingIdRe/g, clientMappingId);

            console.log("\ncheck emp-->", CSQL);
            db.query(CSQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateClientMap SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                }
                else {
                    console.log("\narraylength", results.length);
                    if (results.length == 0) {
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        var SQL = query.updateClientMap;
                        SQL = SQL.replace("isActive", isActive);
                        SQL = SQL.replace("clientMappingId", clientMappingId);
                        SQL = SQL.replace(/timesNow/g, timesNow);
                        console.log("\n > updateClientMap SQL  ---> ", SQL);
                        db.query(SQL, function (error, results, fields) {
                            if (error) {
                                console.log("\n > updateClientMap SQL Err ---> ", error.code);
                                res.status(400).json({ status: 'failure', error: error.code });
                            } else {
                                res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                            }
                        });
                    }
                    else {
                        res.status(400).json({ status: 'failure', error: "Please move active employees to other mappings" });
                    }
                }
            });
        }
    }
};

exports.getMspForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "WHERE name LIKE '%SEARCH_RE%' OR value LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getMspForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getMspForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getMspForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};


exports.getEmpFromClintmap = function (req, res, next) {
    var SQL = "";
    if (req.params.userType === 'buHead') {
        SQL = query.getBUHeadFromClintmap;
        console.log("\n > getEmpFromClintmap SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else if (req.params.userType === 'businessManager') {
        SQL = query.getBusinessManagerFromClintmap;
        console.log("\n > getEmpFromClintmap SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Invalid User Type!' });
    }
};

exports.getEmpFromEmployee = function (req, res, next) {
    var SQL = "";
    if (req.params.userType === 'buHead') {
        SQL = query.getBUHeadFromEmployee;
        SQL = SQL.replace('PGTCODERE', req.params.PGTCODE);
        console.log("\n > getEmpFromEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else if (req.params.userType === 'businessManager') {
        SQL = query.getBusinessManagerFromEmployee;
        SQL = SQL.replace('PGTCODERE', req.params.PGTCODE);
        console.log("\n > getEmpFromEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Invalid User Type!' });
    }
};

exports.getClientmapByEmployee = function (req, res, next) {
    var SQL = "";
    if (req.params.userType === 'buHead') {
        var SQL = query.getClientmapByBUHead;
        SQL = SQL.replace('PGTCODERE', req.params.PGTCODE);
        console.log("\n > getClientmapByEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else if (req.params.userType === 'businessManager') {
        var SQL = query.getClientmapByBusinessManager;
        SQL = SQL.replace('PGTCODERE', req.params.PGTCODE);
        console.log("\n > getClientmapByEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            }
            res.status(200).json({ status: "success", data: results });
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Invalid User Type!' });
    }
};

exports.employeeMoveToAnotherClientMapping = function (req, res) {
    var params = req.body;
    if (params.userType === undefined || params.pgtCodeFrom === undefined || params.pgtCodeTo === undefined || params.clientMapIds === undefined || params.loggedInUserId === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else if (params.userType.toString().trim() === '' || params.pgtCodeFrom.toString().trim() === '' || params.pgtCodeTo.toString().trim() === '' || params.clientMapIds.toString().trim() === '' || params.loggedInUserId.toString().trim() === '') {
        res.status(400).json({ status: 'failure', error: 'Please send all required fields' });
    }
    else {

        SQL = query.getClientMapByIds;
        SQL = SQL.replace("client_cc_mapping_id_re", removeString(params.clientMapIds).toString());

        console.log("SQL===>", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                moveToAnotherMapping(params, results, function (error, result) {
                    if (error) {
                        res.status(400).json({ status: 'failure', data: error });
                    } else {
                        res.status(200).json({ status: "success", data: result });
                    }
                });
            }
        });
    }

    function moveToAnotherMapping(params, mappings, callback) {
        var userType = removeString(params.userType);
        var pgtCodeFrom = removeString(params.pgtCodeFrom);
        var empNameFrom = removeString(params.empNameFrom);
        var pgtCodeTo = removeString(params.pgtCodeTo);
        var empNameTo = removeString(params.empNameTo);
        var clientMapIds = removeString(params.clientMapIds).toString();
        var loggedInUserId = removeString(params.loggedInUserId);

        if (mappings.length > 0) {
            var count = 0;
            var checkAndMoveMapping = function (mappings) {
                var mapping = mappings[count];

                SQL = query.checkClientMapByValues;
                SQL = SQL.replace("client_id_re", removeString(mapping.client_id, true));
                SQL = SQL.replace("cost_center_id_re", removeString(mapping.cost_center_id, true));
                SQL = SQL.replace("new_msp_id_re", removeString(mapping.new_msp_id, true));
                SQL = SQL.replace("pgt_code_hr_spoc_re", removeString(mapping.pgt_code_hr_spoc, true));
                SQL = SQL.replace("pgt_code_fin_spoc_re", removeString(mapping.pgt_code_fin_spoc, true));
                SQL = SQL.replace("pgt_code_pandl_head_re", (userType == "buHead") ? removeString(pgtCodeTo, true) : removeString(mapping.pgt_code_pandl_head, true));
                SQL = SQL.replace("pgt_code_bsns_mgr_re", (userType == "businessManager") ? removeString(pgtCodeTo, true) : removeString(mapping.pgt_code_bsns_mgr, true));

                console.log("SQL 1===>", SQL);

                db.query(SQL, function (error, results, fields) {
                    count++;
                    if (error) {
                        if (count == mappings.length) {
                            callback(true, error.code);
                        } else {
                            checkAndMoveMapping(mappings);
                        }
                    } else {
                        if (results.length == 0) {
                            SQL2 = query.createClientMap;
                            SQL2 = SQL2.replace("clientId", mapping.client_id);
                            SQL2 = SQL2.replace("clientName", mapping.client_name);
                            SQL2 = SQL2.replace("costCenterIdRe", mapping.cost_center_id);
                            SQL2 = SQL2.replace("cost_Center_Re", mapping.cost_center);
                            SQL2 = SQL2.replace("newMspIdRe", mapping.new_msp_id);
                            SQL2 = SQL2.replace("newMspNameRe", mapping.new_msp_name);
                            SQL2 = SQL2.replace("newMspTypeRe", mapping.new_msp_type);
                            SQL2 = SQL2.replace("newMspFeeRe", mapping.new_msp_fee);
                            SQL2 = SQL2.replace("hrSpoc", mapping.hr_spoc);
                            SQL2 = SQL2.replace("pgtHrSpoc", mapping.pgt_code_hr_spoc);
                            SQL2 = SQL2.replace("financeSpoc", mapping.fin_spoc);
                            SQL2 = SQL2.replace("pgtFinanceSpoc", mapping.pgt_code_fin_spoc);
                            SQL2 = SQL2.replace("plHead", (userType == "buHead") ? empNameTo : mapping.pl_head);
                            SQL2 = SQL2.replace("pgtPlHeadCode", (userType == "buHead") ? pgtCodeTo : mapping.pgt_code_pandl_head);
                            SQL2 = SQL2.replace("businessManager", (userType == "businessManager") ? empNameTo : mapping.bsns_mgr);
                            SQL2 = SQL2.replace("pgtBusinessManagerCode", (userType == "businessManager") ? pgtCodeTo : mapping.pgt_code_bsns_mgr);
                            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                            SQL2 = SQL2.replace(/loggedInUserId/g, loggedInUserId);
                            SQL2 = SQL2.replace(/timesNow/g, timesNow);
                            SQL2 = SQL2.replace("isActive", 1);

                            console.log("SQL 2===>", SQL2);

                            db.query(SQL2, function (error, results2, fields) {
                                if (error) {
                                    if (count == mappings.length) {
                                        callback(true, error.code);
                                    } else {
                                        checkAndMoveMapping(mappings);
                                    }
                                } else {
                                    var date = new Date();
                                    var currentMonth = date.getMonth() + 1;
                                    var currentYear = date.getFullYear();

                                    if (currentMonth >= 4) {
                                        currentYear = currentYear;
                                    } else {
                                        currentYear = currentYear - 1;
                                    }
                                    var serviceMonth = currentYear + "-" + '04';

                                    SQL3 = query.moveEmployeesToNewMapping;
                                    SQL3 = SQL3.replace("new_client_cc_mapping_id_re", results2.insertId);
                                    SQL3 = SQL3.replace("old_client_cc_mapping_id_re", mapping.client_cc_mapping_id);
                                    SQL3 = SQL3.replace("SERVICEMONTH", serviceMonth);

                                    console.log("SQL 3===>", SQL3);

                                    db.query(SQL3, function (error, results3, fields) {
                                        if (error) {
                                            if (count == mappings.length) {
                                                callback(true, error.code);
                                            } else {
                                                checkAndMoveMapping(mappings);
                                            }
                                        } else {
                                            if (count == mappings.length) {
                                                callback(false, "Moved Successfully");
                                            } else {
                                                checkAndMoveMapping(mappings);
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            var date = new Date();
                            var currentMonth = date.getMonth() + 1;
                            var currentYear = date.getFullYear();

                            if (currentMonth >= 4) {
                                currentYear = currentYear;
                            } else {
                                currentYear = currentYear - 1;
                            }
                            var serviceMonth = currentYear + "-" + '04';

                            // console.log('\n results 1 ---> ', results.client_cc_mapping_id);
                            // console.log('\n results 2 ---> ', results[0].client_cc_mapping_id);
                            SQL3 = query.moveEmployeesToNewMapping;
                            SQL3 = SQL3.replace("new_client_cc_mapping_id_re", results[0].client_cc_mapping_id);
                            SQL3 = SQL3.replace("old_client_cc_mapping_id_re", mapping.client_cc_mapping_id);
                            SQL3 = SQL3.replace("SERVICEMONTH", serviceMonth);

                            console.log("SQL 3===>", SQL3);

                            db.query(SQL3, function (error, results3, fields) {
                                if (error) {
                                    if (count == mappings.length) {
                                        callback(true, error.code);
                                    } else {
                                        checkAndMoveMapping(mappings);
                                    }
                                } else {
                                    if (count == mappings.length) {
                                        callback(false, "Moved Successfully");
                                    } else {
                                        checkAndMoveMapping(mappings);
                                    }
                                }
                            });
                        }
                    }
                });

            }
            checkAndMoveMapping(mappings);
        }
    }

    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};