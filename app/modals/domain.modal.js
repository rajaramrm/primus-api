var db = require('../db/manageDB');
var query = require('../queries/domain.query');
var moment = require('moment-timezone');

exports.getAllDomians = function (req, res, next) {
    var SQL = query.getAllDomians;
    console.log("\n > getAllDomians SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getAllDomians SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.skillSet = function (req, res, next) {
    var SQL = query.skillSet;
    console.log("\n > skillSet SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > skillSet SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.select2SkillSet = function (req, res, next) {
    var SQL = query.select2SkillSet;
    console.log("\n > select2SkillSet SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > select2SkillSet SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.skillSetByDomain = function (req, res, next) {
    var domainId = req.params.domainId;
    if (domainId == "") {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var SQL = query.skillSetByDomain;
        SQL = SQL.replace('byDomainId', domainId);

        console.log("\n > skillSetByDomain SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > skillSetByDomain SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }

};

exports.createDomain = function (req, res, next) {
    var data = req.body;
    if (
        data.domainName == undefined
        || data.loginUserId == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var domainName = data.domainName.toString().trim();
        var loginUserId = data.loginUserId.toString().trim();
        if (
            domainName == ""
            || loginUserId == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent all requird fields' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.createDomain;
            SQL = SQL.replace('domainName', domainName);
            SQL = SQL.replace(/loginUserId/g, loginUserId);
            SQL = SQL.replace(/timesNow/g, timesNow);
            console.log("\n > createDomain SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > createDomain SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Successfully Created' });
                }
            });
        }
    }
};

exports.createSkillSet = function (req, res, next) {
    // res.status(400).json({ status: 'failure', error: req.body });

    var data = req.body;
    if (
        data.domainId == undefined
        || data.loginUserId == undefined
        || data.skillSet == undefined
        || data.status == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var domainId = data.domainId.toString().trim();
        var skillSet = data.skillSet.toString().trim();
        var loginUserId = data.loginUserId.toString().trim();
        var status = 1;
        if (
            domainId == ""
            || loginUserId == ""
            || skillSet == ""
            || status == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent all requird fields' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.createSkillSet;
            SQL = SQL.replace('domainId', domainId);
            SQL = SQL.replace('skillSet', skillSet);
            SQL = SQL.replace('status', status);
            SQL = SQL.replace(/loginUserId/g, loginUserId);
            SQL = SQL.replace(/timesNow/g, timesNow);
            console.log("\n > createSkillSet SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > createSkillSet SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Successfully Created' });
                }
            });
        }
    }
};

exports.updateDomain = function (req, res, next) {
    var domainId = req.params.domainId;
    var data = req.body;
    if (domainId == '') {
        res.status(400).json({ status: 'failure', error: 'Please sent domain id' });
    } else if (
        data.domainName == undefined
        || data.status == undefined
        || data.loginUserId == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var domainName = data.domainName.toString().trim();
        var loginUserId = data.loginUserId.toString().trim();
        var status = data.status.toString().trim();
        if (
            domainName == ""
            || loginUserId == ""
            || status == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent all requird fields' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.updateDomain;
            SQL = SQL.replace('domainName', domainName);
            SQL = SQL.replace('status', status);
            SQL = SQL.replace('loginUserId', loginUserId);
            SQL = SQL.replace('timesNow', timesNow);
            SQL = SQL.replace('domainId', domainId);
            console.log("\n > updateDomian SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateDomian SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Successfully Updated' });
                }
            });
        }
    }
};


exports.updateSkillSet = function (req, res, next) {
    var skillSetId = req.params.skillSetId;
    var data = req.body;
    if (skillSetId == '') {
        res.status(400).json({ status: 'failure', error: 'Please sent skill set id' });
    } else if (
        data.domainId == undefined
        || data.domainIdOld == undefined
        || data.skillSet == undefined
        || data.status == undefined
        || data.loginUserId == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var domainId = data.domainId.toString().trim();
        var domainIdOld = data.domainIdOld.toString().trim();
        var skillSet = data.skillSet.toString().trim();
        var loginUserId = data.loginUserId.toString().trim();
        var status = data.status.toString().trim();
        if (
            domainId == ""
            || domainIdOld ==""
            || skillSet == ""
            || loginUserId == ""
            || status == ""
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent all requird fields' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            var SQL = query.updateSkillSet;
            SQL = SQL.replace('domainId', domainId);
            SQL = SQL.replace('status', status);
            SQL = SQL.replace('loginUserId', loginUserId);
            SQL = SQL.replace('timesNow', timesNow);
            SQL = SQL.replace('skillSet', skillSet);
            SQL = SQL.replace('skillSetId', skillSetId);
            console.log("\n > updateSkillSet SQL ---> ", SQL);

            if(domainIdOld != domainId){
                var SQL2 = query.updateHrmasterHorizontal;
                SQL2 = SQL2.replace('new_horizontal_id_re', domainId);
                SQL2 = SQL2.replace('old_horizontal_id_re', domainIdOld);
                SQL2 = SQL2.replace('new_skill_set_id_re', skillSetId);

                SQL+=SQL2;
            }

            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateSkillSet SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Successfully Updated' });
                }
            });
        }
    }
};


exports.getDomainForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "WHERE horizontal_type LIKE '%SEARCH_RE%' OR is_active LIKE '%SEARCH_RE%' OR created_by LIKE '%SEARCH_RE%' OR created_at LIKE '%SEARCH_RE%' OR updated_by LIKE '%SEARCH_RE%' OR updated_at LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getDomainForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getDomainForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getDomainForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getSkillForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "WHERE domain.horizontal_type LIKE '%SEARCH_RE%' OR skill.primary_skill LIKE '%SEARCH_RE%' OR skill.is_active LIKE '%SEARCH_RE%' OR skill.created_by LIKE '%SEARCH_RE%' OR skill.created_at LIKE '%SEARCH_RE%' OR skill.updated_by LIKE '%SEARCH_RE%' OR skill.updated_at LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getSkillSetForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getSkillSetForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getSkillSetForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};