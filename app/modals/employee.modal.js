var db = require('../db/manageDB');
var query = require('../queries/employee.query');
var moment = require('moment-timezone');
var xlsx = require('xlsx');
var config = require('../../config');
var mkdirp = require('mkdirp');
var fs = require('fs');
var formidable = require('formidable');

exports.getEmployee = function (req, res, next) {
    var SQL = query.getEmployee;
    console.log("\n > getClientMapList SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClientMapList SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getEmpByStatus = function (req, res, next) {
    var SQL = query.getEmpByStatus;
    var WHERE = '';
    console.log('> Status ---> ', req.params.status);
    if (req.params.status.toString() === '1') {
        WHERE = "WHERE A.status = 'ACTIVE'";
    } else {
        WHERE = "WHERE A.status = 'IN_ACTIVE'";
    }
    SQL = SQL.replace('CONDITIONWHERERE', WHERE);
    console.log("\n > getEmpByStatus SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEmpByStatus SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.checkEmpCode = function (req, res, next) {
    var params = req.params;
    var SQL = query.checkEmpCode;
    SQL = SQL.replace("EMPCODE", params.empCode.toString().trim());
    SQL = SQL.replace("APPEMPID", params.appEmpId.toString().trim());
    console.log("\n > checkEmpCode SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > checkEmpCode SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results[0] });
        }
    });
};

exports.checkEmail = function (req, res, next) {
    var params = req.params;
    var SQL = query.checkEmail;
    SQL = SQL.replace("EMAIL", params.emailId.toString().trim());
    SQL = SQL.replace("APPEMPID", params.appEmpId.toString().trim());
    console.log("\n > checkEmail SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > checkEmail SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results[0] });
        }
    });
}

exports.getEmployeeById = function (req, res, next) {
    var params = req.params;
    var SQL = query.getEmployeeById;
    SQL = SQL.replace("EMPID", params.empId.toString().trim());
    console.log("\n > getEmployeeById SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEmployeeById SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = "";
            if (results.length > 0) {
                data = { status: 'success', data: results[0] };
            } else {
                data = { status: 'success', data: "Record Not Found" };
            }
            res.status(200).json(data);
        }
    });
};

exports.createEmployee = function (req, res, next) {
    var params = req.body;
    if (params.employeeId == undefined || params.employeeFirstName == undefined || params.employeeLastName == undefined || params.emailId == undefined || params.userType == undefined || params.designation == undefined || params.loggedInUserId == undefined || params.department == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.employeeId.toString().trim() == "" || params.employeeFirstName.toString().trim() == "" || params.employeeLastName.toString().trim() == "" || params.emailId.toString().trim() == "" || params.userType.toString().trim() == "" || params.designation.toString().trim() == "" || params.loggedInUserId.toString().trim() == "" || params.department.toString().toString() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.createEmployee;
        SQL = SQL.replace("employee_code_re", params.employeeId.toString().trim());
        SQL = SQL.replace("name_re", params.employeeFirstName.toString().trim() + (params.employeeMiddleName.toString().trim() ? (" " + params.employeeMiddleName.toString().trim() + " ") : " ") + params.employeeLastName.toString().trim());
        SQL = SQL.replace("first_name_re", params.employeeFirstName.toString().trim());
        SQL = SQL.replace("middle_name_re", params.employeeMiddleName.toString().trim());
        SQL = SQL.replace("last_name_re", params.employeeLastName.toString().trim());
        SQL = SQL.replace("report_manager_id_re", params.reportManager.toString().trim());
        SQL = SQL.replace("email_id_re", params.emailId.toString().trim());
        SQL = SQL.replace("user_type_re", params.userType.toString().trim());
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("rms_re", params.rms.toString().trim());
        SQL = SQL.replace("target_re", 0);
        SQL = SQL.replace("designation_re", params.designation.toString().trim());
        SQL = SQL.replace("department_re", params.department.toString().trim());
        SQL = SQL.replace("created_by_re", params.loggedInUserId.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("created_at_re", timesNow);
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        console.log("\n > createEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createEmployee SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Employee Created Successfully' });
            }
        });
    }
};

exports.updateEmployee = function (req, res, next) {
    var params = req.body;
    if (params.appEmployeeId == undefined || params.employeeFirstName == undefined || params.employeeLastName == undefined || params.emailId == undefined || params.userType == undefined || params.target == undefined || params.designation == undefined || params.loggedInUserId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.appEmployeeId.toString().trim() == "" || params.employeeFirstName.toString().trim() == "" || params.employeeLastName.toString().trim() == "" || params.emailId.toString().trim() == "" || params.userType.toString().trim() == "" || params.target.toString().trim() == "" || params.designation.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateEmployee;
        // UPDATE `prms_app_employees` SET `name`='name_re',`email_id`='email_id_re',`user_type`='user_type_re',`target`='target_re',`designation`='designation_re',`updated_by`='updated_by_re',`updated_at`='updated_at_re', `is_rms`='is_rms_re', `status`='status_re' WHERE `app_employee_id` = 'app_employee_id_re';
        SQL = SQL.replace("employee_code_re", params.employeeId.toString().trim());
        SQL = SQL.replace("email_id_re", params.emailId.toString().trim());
        SQL = SQL.replace("name_re", params.employeeFirstName.toString().trim() + (params.employeeMiddleName.toString().trim() ? (" " + params.employeeMiddleName.toString().trim() + " ") : " ") + params.employeeLastName.toString().trim());
        SQL = SQL.replace("first_name_re", params.employeeFirstName.toString().trim());
        SQL = SQL.replace("middle_name_re", params.employeeMiddleName.toString().trim());
        SQL = SQL.replace("last_name_re", params.employeeLastName.toString().trim());
        SQL = SQL.replace("report_manager_id_re", params.reportManager.toString().trim());
        SQL = SQL.replace("user_type_re", params.userType.toString().trim());
        SQL = SQL.replace("target_re", params.target.toString().trim());
        SQL = SQL.replace("designation_re", params.designation.toString().trim());
        SQL = SQL.replace("department_re", params.department.toString().trim());
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("is_rms_re", params.rms.toString().trim());
        SQL = SQL.replace("status_re", params.status.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("app_employee_id_re", params.appEmployeeId.toString().trim());
        console.log("\n > updateEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateEmployee SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Employee Updated Successfully' });
            }
        });
    }
};

exports.updateEmployeeStatus = function (req, res, next) {
    var params = req.body;
    if (params.appEmployeeId == undefined || params.status == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.appEmployeeId.toString().trim() == "" || params.status.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateEmployeeStatus;
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("app_employee_id_re", params.appEmployeeId.toString().trim());
        console.log("\n > updateEmployeeStatus SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateEmployeeStatus SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Employee Status Updated Successfully' });
            }
        });
    }
};

exports.deleteEmployee = function (req, res, next) {
    var params = req.params;
    if (params.empId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.empId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deleteEmployee;
        SQL = SQL.replace("is_delete_re", '1');
        SQL = SQL.replace("app_employee_id_re", params.empId.toString().trim());
        console.log("\n > deleteEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deleteEmployee SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Employee Deleted Successfully' });
            }
        });
    }
};

exports.getEmployeeForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue !== undefined && params.searchValue !== null && params.searchValue !== "") {
        WHERE_RE = "WHERE em.employee_code LIKE '%SEARCH_RE%' OR em.name LIKE '%SEARCH_RE%' OR em.email_id LIKE '%SEARCH_RE%' OR em.user_type LIKE '%SEARCH_RE%' OR em.designation LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
        if (params.status !== undefined && params.status !== null && params.status.toString() === '1') {
            WHERE_RE += " AND em.status = 'ACTIVE'";
        } else if (params.status !== undefined && params.status !== null && params.status.toString() === '0') {
            WHERE_RE += " AND em.status = 'IN_ACTIVE'";
        }
    } else {
        if (params.status !== undefined && params.status !== null && params.status.toString() === '1') {
            WHERE_RE = "WHERE em.status = 'ACTIVE'";
        } else if (params.status !== undefined && params.status !== null && params.status.toString() === '0') {
            WHERE_RE = "WHERE em.status = 'IN_ACTIVE'";
        }
    }
    var SQL = query.getEmployeeForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getEmployeeForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEmployeeForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};


exports.getReportManager = function (req, res, next) {
    var params = req.body;
    if (params.designation == undefined || params.employeeId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.designation.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {

        var WHERE_RE = "";
        if (params.designation.toString().trim() == 'HR Admin' || params.designation.toString().trim() == 'HR SPOC') {
            WHERE_RE = "WHERE (`user_type`='HRAdmin' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'Finance Admin' || params.designation.toString().trim() == 'Finance SPOC') {
            WHERE_RE = "WHERE (`user_type`='FinanceAdmin' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'PayrollAdmin') {
            WHERE_RE = "WHERE (`user_type`='PayrollAdmin' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'BU Head') {
            WHERE_RE = "WHERE (`user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'Senior Business Manager') {
            WHERE_RE = "WHERE (`user_type`='BUHead' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'Business Manager') {
            WHERE_RE = "WHERE (`designation`='Senior Business Manager' OR `user_type`='BUHead' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        } else if (params.designation.toString().trim() == 'TA' || params.designation.toString().trim() == 'Sourced By') {
            WHERE_RE = "WHERE (`user_type`='BM' OR `user_type`='BUHead' OR `user_type`='SuperAdmin') AND `app_employee_id`<>'" + params.employeeId.toString().trim() + "' AND `status`='ACTIVE' AND `app_employee_id`<>'733'";
        }
        var SQL = query.getReportManager;
        SQL = SQL.replace("WHERE_RE", WHERE_RE);
        console.log("\n > getEmployeeForExport SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getReportManager SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};


exports.importInternalEmpsCheck = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < 1; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            internalEmpsCheck(data, function (err, message) {
                                if (err) {
                                    console.log("\n > internalEmpsCheck SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > internalEmpsCheck SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < 1; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    internalEmpsCheck(data, function (err, message) {
                        if (err) {
                            console.log("\n > internalEmpsCheck SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > internalEmpsCheck SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function internalEmpsCheck(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0, xlsError = [];
                var insertRow = function (input) {
                    var rootObj = {};
                    var row = input[count];
                    rootObj['ROWS'] = "ROW_" + (count + 1);
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var employeeCode = removeString(row.EMPLOYEE_CODE);
                    var emailId = removeString(row.EMAIL_ID);
                    var reportManagerEmailId = removeString(row.REPORT_MANAGER_EMAIL_ID);

                    var SQL_1 = query.checkImportEmployeeCode;
                    SQL_1 = SQL_1.replace('employee_code_re', removeString(employeeCode, true));

                    var SQL_2 = query.checkImportEmail;
                    SQL_2 = SQL_2.replace('email_id_re', removeString(emailId, true));

                    var SQL_3 = query.checkImportEmail;
                    SQL_3 = SQL_3.replace('email_id_re', removeString(reportManagerEmailId, true));

                    SQL = SQL_1 + SQL_2 + SQL_3;
                    console.log("\n> internalEmpsCheck SQL (" + count + ") ---> ", SQL);

                    db.query(SQL, function (err, results, fields) {
                        count++;
                        if (err) {
                            console.log("\n > internalEmpsCheck SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, xlsError);
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            var ALREADY_EXISTS = [];
                            if (results[0].length > 0) {
                                ALREADY_EXISTS.push('EMPLOYEE_CODE');
                            }
                            if (results[1].length > 0) {
                                ALREADY_EXISTS.push('EMAIL_ID');
                            }                            
                            if (ALREADY_EXISTS.length != 0) {                                
                                rootObj['ALREADY_EXISTS'] = ALREADY_EXISTS.toString();
                            }
                            var INVALID_ERROR = [];
                            if (results[2].length == 0) {
                                INVALID_ERROR.push('REPORT_MANAGER');
                            }
                            if (INVALID_ERROR.length != 0) {
                                rootObj['INVALID_ERROR'] = INVALID_ERROR.toString();
                            }
                            if (Object.keys(rootObj).length != 1) {                                
                                xlsError.push(rootObj);
                            }

                            if (count == input.length) {
                                callback(false, xlsError);
                                console.log("Test==>1",xlsError);
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.importInternalEmps = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < 1; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            insertInternalEmps(data, function (err, message) {
                                if (err) {
                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < 1; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    insertInternalEmps(data, function (err, message) {
                        if (err) {
                            console.log("\n > createTimesheet SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > createTimesheet SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function insertInternalEmps(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var employeeCode = removeString(row.EMPLOYEE_CODE);
                    var firstName = removeString(row.FIRST_NAME);
                    var middleName = removeString(row.MIDDLE_NAME);
                    var lastName = removeString(row.LAST_NAME);
                    var employeeName =  firstName + (middleName ? (" " + middleName + " ") : " ") + lastName;
                    var emailId = removeString(row.EMAIL_ID);
                    var userType = removeString(row.USER_TYPE);
                    var designation = removeString(row.DESIGNATION);
                    var department=removeString(row.DEPARTMENT);
                    var reportManagerEmailId = removeString(row.REPORT_MANAGER_EMAIL_ID);
                    var status = removeString('ACTIVE');
                    var isRms = removeString(row.IS_RMS) == 'YES' ? 1 : 0;
                    var loginUserId = removeString(row.loginUserId) ? removeString(row.loginUserId) : 1;

                    var SQL = query.importInternalEmps;
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace('employee_code_re', removeString(employeeCode, true));                   
                    SQL = SQL.replace('first_name_re', removeString(firstName, true));
                    SQL = SQL.replace('middle_name_re', removeString(middleName, true));
                    SQL = SQL.replace('last_name_re', removeString(lastName, true));
                    SQL = SQL.replace('name_re', removeString(employeeName, true));
                    SQL = SQL.replace('report_manager_id_re', '(SELECT * FROM (SELECT `app_employee_id` FROM `prms_app_employees` WHERE `email_id`=' + removeString(reportManagerEmailId, true) + '  LIMIT 1) AS RM)');
                    SQL = SQL.replace('email_id_re', removeString(emailId, true));
                    SQL = SQL.replace('user_type_re', removeString(userType, true));
                    SQL = SQL.replace('status_re', removeString(status, true));
                    SQL = SQL.replace('designation_re', removeString(designation, true));
                    SQL = SQL.replace('department_re', removeString(department, true));
                    SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('is_rms_re', removeString(isRms, true));


                    db.query(SQL, function (err, results, fields) {
                        if (err) {
                            count++;
                            console.log("\n > insertInternalEmps SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, 'Successfully Created');
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            count++;
                            if (count == input.length) {
                                callback(false, 'Successfully Created');
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.importInactiveInternalEmps = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < 1; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            updateInternalEmps(data, function (err, message) {
                                if (err) {
                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < 1; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    updateInternalEmps(data, function (err, message) {
                        if (err) {
                            console.log("\n > createTimesheet SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > createTimesheet SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function updateInternalEmps(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var employeeCode = removeString(row.EMP_ID);                   
                    var status = removeString(row.STATUS);                    
                    var loginUserId = removeString(row.loginUserId) ? removeString(row.loginUserId) : 1;

                    var SQL = query.importInactiveInternalEmps;
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace('employee_code_re', removeString(employeeCode, true));
                    SQL = SQL.replace('status_re', removeString(status, true));                  
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));                    

                    db.query(SQL, function (err, results, fields) {
                        if (err) {
                            count++;
                            console.log("\n > insertInternalEmps SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, 'Successfully Updated');
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            count++;
                            if (count == input.length) {
                                callback(false, 'Successfully Updated');
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.getRouting = function (req, res) {
    db.query(query.getRouting, function (error, result) {
        if (error) {
            res.status(400).send({ status: "failure", message: "Something went wrong" + error });
        } else {
            var data = { status: "success", data: result };
            res.status(200).send(data);
        }
    });
};

exports.createRouting = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");    
    var routingName = removeString(data.routingName);
    var loginUser = removeString(data.loginUser);
    timesNow = removeString(timesNow);

    if (!routingName || !loginUser) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else {
        var SQL = query.createRouting;       
        SQL = SQL.replace('routingNameRe', routingName);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        console.log("\n > createRouting SQL ---> ", SQL);
        
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createRouting SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Created' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};


exports.updateRouting = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");  
    var routingId = removeString(data.routingId);
    var routingName = removeString(data.routingName);
    var loginUser = removeString(data.loginUser);
    timesNow = removeString(timesNow);

    if (!routingId || !routingName || !loginUser) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else {
        var SQL = query.updateRouting;
        SQL = SQL.replace('routingNameRe', routingName);
        SQL = SQL.replace(/loginIdRe/g, loginUser);        
        SQL = SQL.replace(/routingIdRe/g, routingId);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        console.log("\n > updateRouting SQL ---> ", SQL);
        
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateRouting SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Updated' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};