var db = require('../db/manageDB');
var query = require('../queries/targetClientWise.query');
var moment = require('moment-timezone');

exports.getTargetClient = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var WHERE = "";
        if (params.searchValue) {
            WHERE = "AND E.`app_employee_id`='" + params.searchValue + "'";
        }
        var SQL = query.getTargetClient;
        SQL = SQL.replace("FYEAR_RE", params.year);
        SQL = SQL.replace("CONDITIONALWHERE_RE", WHERE);

        console.log("\n > getTargetClient SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getTargetClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.getTargetClientById = function (req, res, next) {
    var params = req.params;
    var SQL = query.getTargetClientById;
    SQL = SQL.replace("WHEREID", params.id.toString().trim());

    console.log("\n > getTargetClientById SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getTargetClientById SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = "";
            if (results.length > 0) {
                data = { status: 'success', data: results[0] };
            } else {
                data = { status: 'success', data: "Record Not Found" };
            }
            res.status(200).json(data);
        }
    });
};

exports.createTargetClientWise = function (req, res, next) {
    var params = req.body;
    // if (params.employeeId == undefined || params.client == undefined || params.targetFrom == undefined || params.targetTo == undefined || params.target == undefined || params.loggedInUserId == undefined) {
    //     res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    // } else if (params.employeeId.toString().trim() == "" || params.client.toString().trim() == "" || params.targetFrom.toString().trim() == "" || params.targetTo.toString().trim() == "" || params.target.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
    //     res.status(400).json({ status: 'failure', error: 'Please send required field' });
    // } else {
    var SQL = query.createTargetClient;
    SQL = SQL.replace("app_employee_id_re", params.employeeId.toString().trim());
    SQL = SQL.replace("client_id_re", params.client.toString().trim());
    SQL = SQL.replace("cost_center_idRe", params.costCenterId.toString().trim());
    SQL = SQL.replace("financial_year_re", params.fYear.toString().trim());   
    SQL = SQL.replace("from_re", params.targetFrom.toString().trim());
    SQL = SQL.replace("to_re", params.targetTo.toString().trim());
    SQL = SQL.replace("april_target_re", params.AprilTarget.toString().trim());
    SQL = SQL.replace("may_target_re", params.MayTarget.toString().trim());
    SQL = SQL.replace("june_target_re", params.JuneTarget.toString().trim());
    SQL = SQL.replace("july_target_re", params.JulyTarget.toString().trim());
    SQL = SQL.replace("august_target_re", params.AugustTarget.toString().trim());
    SQL = SQL.replace("september_target_re", params.SeptemberTarget.toString().trim());
    SQL = SQL.replace("october_target_re", params.OctoberTarget.toString().trim());
    SQL = SQL.replace("november_target_re", params.NovemberTarget.toString().trim());
    SQL = SQL.replace("december_target_re", params.DecemberTarget.toString().trim());
    SQL = SQL.replace("january_target_re", params.JanuaryTarget.toString().trim());
    SQL = SQL.replace("february_target_re", params.FebruaryTarget.toString().trim());
    SQL = SQL.replace("march_target_re", params.MarchTarget.toString().trim());
    SQL = SQL.replace("total_target_re", params.totalTarget.toString().trim());
    SQL = SQL.replace("april_gp_target_re", params.AprilGpTarget.toString().trim());
    SQL = SQL.replace("may_gp_target_re", params.MayGpTarget.toString().trim());
    SQL = SQL.replace("june_gp_target_re", params.JuneGpTarget.toString().trim());
    SQL = SQL.replace("july_gp_target_re", params.JulyGpTarget.toString().trim());
    SQL = SQL.replace("august_gp_target_re", params.AugustGpTarget.toString().trim());
    SQL = SQL.replace("september_gp_target_re", params.SeptemberGpTarget.toString().trim());
    SQL = SQL.replace("october_gp_target_re", params.OctoberGpTarget.toString().trim());
    SQL = SQL.replace("november_gp_target_re", params.NovemberGpTarget.toString().trim());
    SQL = SQL.replace("december_gp_target_re", params.DecemberGpTarget.toString().trim());
    SQL = SQL.replace("january_gp_target_re", params.JanuaryGpTarget.toString().trim());
    SQL = SQL.replace("february_gp_target_re", params.FebruaryGpTarget.toString().trim());
    SQL = SQL.replace("march_gp_target_re", params.MarchGpTarget.toString().trim());
    SQL = SQL.replace("total_gp_target_re", params.totalGpTarget.toString().trim());
    SQL = SQL.replace("april_headcount_target_re", params.AprilHeadCountTarget.toString().trim());
    SQL = SQL.replace("may_headcount_target_re", params.MayHeadCountTarget.toString().trim());
    SQL = SQL.replace("june_headcount_target_re", params.JuneHeadCountTarget.toString().trim());
    SQL = SQL.replace("july_headcount_target_re", params.JulyHeadCountTarget.toString().trim());
    SQL = SQL.replace("august_headcount_target_re", params.AugustHeadCountTarget.toString().trim());
    SQL = SQL.replace("september_headcount_target_re", params.SeptemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("october_headcount_target_re", params.OctoberHeadCountTarget.toString().trim());
    SQL = SQL.replace("november_headcount_target_re", params.NovemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("december_headcount_target_re", params.DecemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("january_headcount_target_re", params.JanuaryHeadCountTarget.toString().trim());
    SQL = SQL.replace("february_headcount_target_re", params.FebruaryHeadCountTarget.toString().trim());
    SQL = SQL.replace("march_headcount_target_re", params.MarchHeadCountTarget.toString().trim());
    SQL = SQL.replace("total_headcount_target_re", params.totalHeadCountTarget.toString().trim());
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    SQL = SQL.replace("created_by_re", params.loggedInUserId.toString().trim());
    SQL = SQL.replace("created_at_re", timesNow);
    SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
    SQL = SQL.replace("updated_at_re", timesNow);

    console.log("\n > createTargetClientWise SQL ---> ", SQL);

    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > createTargetClientWise SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: 'TargetClientWise Created Successfully' });
        }
    });
    // }
};

exports.updateTargetClientWise = function (req, res, next) {
    var params = req.body;
    // if (params.targetId == undefined || params.employeeId == undefined || params.client == undefined || params.targetType == undefined || params.targetFrom == undefined || params.targetTo == undefined || params.target == undefined || params.operationalCost == undefined || params.loggedInUserId == undefined ) {
    //     res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    // } else if (params.employeeId.toString().trim() == "" || params.client.toString().trim() == "" || params.targetType.toString().trim() == ""  || params.target.toString().trim() == "" || params.operationalCost.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
    //     res.status(400).json({ status: 'failure', error: 'Please send required field' });
    // } else {
    var SQL = query.updateTargetClient;
    SQL = SQL.replace("app_employee_id_re", params.employeeId.toString().trim());
    SQL = SQL.replace("client_id_re", params.client.toString().trim());
    SQL = SQL.replace("cost_center_idRE", params.costCenterId.toString().trim());
    SQL = SQL.replace("financial_year_re", params.fYear.toString().trim());    
    SQL = SQL.replace("from_re", params.targetFrom.toString().trim());
    SQL = SQL.replace("to_re", params.targetTo.toString().trim());
    SQL = SQL.replace("april_target_re", params.AprilTarget.toString().trim());
    SQL = SQL.replace("may_target_re", params.MayTarget.toString().trim());
    SQL = SQL.replace("june_target_re", params.JuneTarget.toString().trim());
    SQL = SQL.replace("july_target_re", params.JulyTarget.toString().trim());
    SQL = SQL.replace("august_target_re", params.AugustTarget.toString().trim());
    SQL = SQL.replace("september_target_re", params.SeptemberTarget.toString().trim());
    SQL = SQL.replace("october_target_re", params.OctoberTarget.toString().trim());
    SQL = SQL.replace("november_target_re", params.NovemberTarget.toString().trim());
    SQL = SQL.replace("december_target_re", params.DecemberTarget.toString().trim());
    SQL = SQL.replace("january_target_re", params.JanuaryTarget.toString().trim());
    SQL = SQL.replace("february_target_re", params.FebruaryTarget.toString().trim());
    SQL = SQL.replace("march_target_re", params.MarchTarget.toString().trim());
    SQL = SQL.replace("total_target_re", params.totalTarget.toString().trim());
    SQL = SQL.replace("april_gp_target_re", params.AprilGpTarget.toString().trim());
    SQL = SQL.replace("may_gp_target_re", params.MayGpTarget.toString().trim());
    SQL = SQL.replace("june_gp_target_re", params.JuneGpTarget.toString().trim());
    SQL = SQL.replace("july_gp_target_re", params.JulyGpTarget.toString().trim());
    SQL = SQL.replace("august_gp_target_re", params.AugustGpTarget.toString().trim());
    SQL = SQL.replace("september_gp_target_re", params.SeptemberGpTarget.toString().trim());
    SQL = SQL.replace("october_gp_target_re", params.OctoberGpTarget.toString().trim());
    SQL = SQL.replace("november_gp_target_re", params.NovemberGpTarget.toString().trim());
    SQL = SQL.replace("december_gp_target_re", params.DecemberGpTarget.toString().trim());
    SQL = SQL.replace("january_gp_target_re", params.JanuaryGpTarget.toString().trim());
    SQL = SQL.replace("february_gp_target_re", params.FebruaryGpTarget.toString().trim());
    SQL = SQL.replace("march_gp_target_re", params.MarchGpTarget.toString().trim());
    SQL = SQL.replace("total_gp_target_re", params.totalGpTarget.toString().trim());
    SQL = SQL.replace("april_headcount_target_re", params.AprilHeadCountTarget.toString().trim());
    SQL = SQL.replace("may_headcount_target_re", params.MayHeadCountTarget.toString().trim());
    SQL = SQL.replace("june_headcount_target_re", params.JuneHeadCountTarget.toString().trim());
    SQL = SQL.replace("july_headcount_target_re", params.JulyHeadCountTarget.toString().trim());
    SQL = SQL.replace("august_headcount_target_re", params.AugustHeadCountTarget.toString().trim());
    SQL = SQL.replace("september_headcount_target_re", params.SeptemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("october_headcount_target_re", params.OctoberHeadCountTarget.toString().trim());
    SQL = SQL.replace("november_headcount_target_re", params.NovemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("december_headcount_target_re", params.DecemberHeadCountTarget.toString().trim());
    SQL = SQL.replace("january_headcount_target_re", params.JanuaryHeadCountTarget.toString().trim());
    SQL = SQL.replace("february_headcount_target_re", params.FebruaryHeadCountTarget.toString().trim());
    SQL = SQL.replace("march_headcount_target_re", params.MarchHeadCountTarget.toString().trim());
    SQL = SQL.replace("total_headcount_target_re", params.totalHeadCountTarget.toString().trim());
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
    SQL = SQL.replace("updated_at_re", timesNow);
    SQL = SQL.replace("targets_id_re", params.targetId.toString().trim());

    console.log("\n > updateTargetClientWise SQL ---> ", SQL);

    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > updateTargetClientWise SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: 'TargetClientWise Updated Successfully' });
        }
    });
    // }
};

exports.updateTargetClientWiseStatus = function (req, res, next) {
    var params = req.body;
    if (params.targetClientWiseId == undefined || params.status == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.targetClientWiseId.toString().trim() == "" || params.status.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateTargetClientWiseStatus;
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("targets_clientwise_id_re", params.targetClientWiseId.toString().trim());

        console.log("\n > updateTargetClientWiseStatus SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateTargetClientWiseStatus SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'TargetClientWise Status Updated Successfully' });
            }
        });
    }
};

exports.deleteTargetClientWise = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deleteTargetClient;
        SQL = SQL.replace("is_active_re", '0');
        SQL = SQL.replace("targets_id_re", params.id.toString().trim());

        console.log("\n > deleteTargetClientWise SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deleteTargetClientWise SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'TargetClientWise Deleted Successfully' });
            }
        });
    }
};

exports.getEmployee = function (req, res, next) {
    var SQL = query.getEmployee;
    console.log("\n > getEmployee SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getEmployee SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getClient = function (req, res, next) {
    var params = req.params;
    var employeeType = params.employeeType.toString().trim();
    var employeeId = params.employeeId.toString().trim();
    if (employeeType == undefined || employeeId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (employeeType == "" || employeeId == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.getClient;

        if (employeeType == "BU Head") {
            SQL = SQL.replace("WHERERE", "AND `pgt_code_pandl_head`='" + employeeId + "'");
        } else if(employeeType == "Senior Business Manager") {
            SQL = "";
            var SQL1 = query.getClient,SQL2 = query.getClient;
            
            SQL1 = SQL1.replace("WHERERE", "AND `pgt_code_bsns_mgr`='" + employeeId + "'");           
            SQL2 = SQL2.replace("WHERERE", "AND `pgt_code_bsns_mgr` IN (SELECT `employee_code` FROM `prms_app_employees` WHERE `user_type`='BM' AND `report_manager_id` = (SELECT `app_employee_id` FROM `prms_app_employees` WHERE `employee_code`='" + employeeId + "'))");

            SQL = SQL1+' UNION '+SQL2;
        } else {
            SQL = SQL.replace("WHERERE", "AND `pgt_code_bsns_mgr`='" + employeeId + "'");
        }

        console.log("\n > getClient SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.getTargetAchievedOld = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var financialYears = params.year.split('-');
        var currentMonth = moment().subtract(1, 'months').format("MMM");        
        var monthlyAchievedSQL = "";

        var currentMonthNum = moment().subtract(1, 'months').format("M");
        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "", PGT_CODE_RE = "";

        if (params.searchBy == "BU Head" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_pandl_head='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        } else if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else if (params.searchBy == "Senior Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else {
            ACHWHERE = "";
            TARWHERE = "";
            DESIG_RE = "BU Head";
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        }

        var APR_SQL = query.getMonthlyAchievedDetails;
        APR_SQL = APR_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-04');
        APR_SQL = APR_SQL.replace("MONTH_RE", "04");
        APR_SQL = APR_SQL.replace("YEAR_RE", financialYears[0]);
        APR_SQL = APR_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        APR_SQL = APR_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += APR_SQL;

        var MAY_SQL = query.getMonthlyAchievedDetails;
        MAY_SQL = MAY_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-05');
        MAY_SQL = MAY_SQL.replace("MONTH_RE", "05");
        MAY_SQL = MAY_SQL.replace("YEAR_RE", financialYears[0]);
        MAY_SQL = MAY_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        MAY_SQL = MAY_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += MAY_SQL;

        var JUN_SQL = query.getMonthlyAchievedDetails;
        JUN_SQL = JUN_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-06');
        JUN_SQL = JUN_SQL.replace("MONTH_RE", "06");
        JUN_SQL = JUN_SQL.replace("YEAR_RE", financialYears[0]);
        JUN_SQL = JUN_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        JUN_SQL = JUN_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += JUN_SQL;

        var JUL_SQL = query.getMonthlyAchievedDetails;
        JUL_SQL = JUL_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-07');
        JUL_SQL = JUL_SQL.replace("MONTH_RE", "07");
        JUL_SQL = JUL_SQL.replace("YEAR_RE", financialYears[0]);
        JUL_SQL = JUL_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        JUL_SQL = JUL_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += JUL_SQL;

        var AUG_SQL = query.getMonthlyAchievedDetails;
        AUG_SQL = AUG_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-08');
        AUG_SQL = AUG_SQL.replace("MONTH_RE", "08");
        AUG_SQL = AUG_SQL.replace("YEAR_RE", financialYears[0]);
        AUG_SQL = AUG_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        AUG_SQL = AUG_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += AUG_SQL;

        var SEP_SQL = query.getMonthlyAchievedDetails;
        SEP_SQL = SEP_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-09');
        SEP_SQL = SEP_SQL.replace("MONTH_RE", "09");
        SEP_SQL = SEP_SQL.replace("YEAR_RE", financialYears[0]);
        SEP_SQL = SEP_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        SEP_SQL = SEP_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += SEP_SQL;

        var OCT_SQL = query.getMonthlyAchievedDetails;
        OCT_SQL = OCT_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-10');
        OCT_SQL = OCT_SQL.replace("MONTH_RE", "10");
        OCT_SQL = OCT_SQL.replace("YEAR_RE", financialYears[0]);
        OCT_SQL = OCT_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        OCT_SQL = OCT_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += OCT_SQL;

        var NOV_SQL = query.getMonthlyAchievedDetails;
        NOV_SQL = NOV_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-11');
        NOV_SQL = NOV_SQL.replace("MONTH_RE", "11");
        NOV_SQL = NOV_SQL.replace("YEAR_RE", financialYears[0]);
        NOV_SQL = NOV_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        NOV_SQL = NOV_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += NOV_SQL;

        var DEC_SQL = query.getMonthlyAchievedDetails;
        DEC_SQL = DEC_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[0] + '-12');
        DEC_SQL = DEC_SQL.replace("MONTH_RE", "12");
        DEC_SQL = DEC_SQL.replace("YEAR_RE", financialYears[0]);
        DEC_SQL = DEC_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        DEC_SQL = DEC_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += DEC_SQL;

        var JAN_SQL = query.getMonthlyAchievedDetails;
        JAN_SQL = JAN_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[1] + '-01');
        JAN_SQL = JAN_SQL.replace("MONTH_RE", "01");
        JAN_SQL = JAN_SQL.replace("YEAR_RE", financialYears[1]);
        JAN_SQL = JAN_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        JAN_SQL = JAN_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += JAN_SQL;

        var FEB_SQL = query.getMonthlyAchievedDetails;
        FEB_SQL = FEB_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[1] + '-02');
        FEB_SQL = FEB_SQL.replace("MONTH_RE", "02");
        FEB_SQL = FEB_SQL.replace("YEAR_RE", financialYears[1]);
        FEB_SQL = FEB_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        FEB_SQL = FEB_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += FEB_SQL;

        var MAR_SQL = query.getMonthlyAchievedDetails;
        MAR_SQL = MAR_SQL.replace(/SERVICE_MONTH_RE/g, financialYears[1] + '-03');
        MAR_SQL = MAR_SQL.replace("MONTH_RE", "03");
        MAR_SQL = MAR_SQL.replace("YEAR_RE", financialYears[1]);
        MAR_SQL = MAR_SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
        MAR_SQL = MAR_SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
        monthlyAchievedSQL += MAR_SQL;

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", params.year);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results[0].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].AprAchieved = e.targetAchieved;
                        results[12][index].AprProjected = e.projected;
                        if (currentMonth == 'Apr') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[1].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].MayAchieved = e.targetAchieved;
                        results[12][index].MayProjected = e.projected;
                        if (currentMonth == 'May') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[2].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].JunAchieved = e.targetAchieved;
                        results[12][index].JunProjected = e.projected;
                        if (currentMonth == 'Jun') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[3].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {                    
                        results[12][index].JulAchieved = e.targetAchieved;
                        results[12][index].JulProjected = e.projected;
                        if (currentMonth == 'Jul') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[4].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].AugAchieved = e.targetAchieved;
                        results[12][index].AugProjected = e.projected;
                        if (currentMonth == 'Aug') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });


                results[5].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].SepAchieved = e.targetAchieved;
                        results[12][index].SepProjected = e.projected;
                        if (currentMonth == 'Sep') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[6].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].OctAchieved = e.targetAchieved;
                        results[12][index].OctProjected = e.projected;
                        if (currentMonth == 'Oct') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[7].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].NovAchieved = e.targetAchieved;
                        results[12][index].NovProjected = e.projected;
                        if (currentMonth == 'Nov') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[8].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].DecAchieved = e.targetAchieved;
                        results[12][index].DecProjected = e.projected;
                        if (currentMonth == 'Dec') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[9].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].JanAchieved = e.targetAchieved;
                        results[12][index].JanProjected = e.projected;
                        if (currentMonth == 'Jan') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[10].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].FebAchieved = e.targetAchieved;
                        results[12][index].FebProjected = e.projected;
                        if (currentMonth == 'Feb') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[11].forEach((e) => {
                    var index = results[12].findIndex(function (item, i) {
                        return item.clientCostName === e.clientCostName && item.empName === e.empName
                    });
                    if (index != -1) {
                        results[12][index].MarAchieved = e.targetAchieved;
                        results[12][index].MarProjected = e.projected;
                        if (currentMonth == 'Mar') {
                            results[12][index].onsiteEmps = e.onsiteEmpsAll;
                        }
                    }
                });

                results[12].forEach((e) => {
                    e.revenueAchieved = e.AprAchieved + e.MayAchieved + e.JunAchieved + e.JulAchieved + e.AugAchieved + e.SepAchieved + e.OctAchieved + e.NovAchieved + e.DecAchieved + e.JanAchieved + e.FebAchieved + e.MarAchieved;
                    e.revenueProjected = e.AprProjected + e.MayProjected + e.JunProjected + e.JulProjected + e.AugProjected + e.SepProjected + e.OctProjected + e.NovProjected + e.DecProjected + e.JanProjected + e.FebProjected + e.MarProjected;
                    e.revenuePending = e.totalTarget - e.revenueAchieved;
                    e.avgBilling = (e[currentMonth + 'Projected'] / e.onsiteEmps).toFixed(2);
                    e.avgBilling = (e.onsiteEmps == 0) ? 0 : e.avgBilling;
                    e.netAdditions = parseInt(((e.revenuePending / e.avgBilling) / remainingMonth) - e.onsiteEmps);            
                    e.netAdditions = (e.onsiteEmps == 0) ? 0 : e.netAdditions;
                });


                res.status(200).json({ status: 'success', data: results[12] });
            }
        });
    }

};

exports.getTargetAchieved = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.month === undefined
        || params.searchType === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "", PGT_CODE_RE = "";

        if (params.searchBy == "BU Head" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_pandl_head='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        } else if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else if (params.searchBy == "Senior Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else {
            ACHWHERE = "";
            TARWHERE = "";
            DESIG_RE = "BU Head";
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        }

        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_target` AS "+monthNameShort+"Target,0 AS "+monthNameShort+"Projected,0 AS "+monthNameShort+"Achieved";

            var SQL = query.getMonthlyAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_target` AS totalTarget,0 AS onsiteEmps";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalTarget=[],revenueProjected=[],revenueAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalTarget.push(monthNameShort+"Target");
                    revenueProjected.push(monthNameShort+"Projected");
                    revenueAchieved.push(monthNameShort+"Achieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[results.length-1][index][monthNameShort+'Achieved'] = e.targetAchieved;
                            results[results.length-1][index][monthNameShort+'Projected'] = e.projected;
                            if (currentMonth == monthNameShort) {
                                results[results.length-1][index].onsiteEmps = e.onsiteEmpsAll;
                            }
                        }
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });       
                        
                        e.revenuePending = e.totalTarget - e.revenueAchieved;
                        e.avgBilling = (e[currentMonth + 'Projected'] / e.onsiteEmps).toFixed(2);
                        e.avgBilling = (e.onsiteEmps == 0) ? 0 : e.avgBilling;
                        e.netAdditions = parseInt(((e.revenuePending / e.avgBilling) / remainingMonth) - e.onsiteEmps);            
                        e.netAdditions = (e.onsiteEmps == 0 || remainingMonth==0) ? 0 : e.netAdditions;

                    } else if(params.searchType=="quarterly"){

                        e.totalTarget = 0;
                        totalTarget.forEach((tT) => {
                            e.totalTarget +=  e[tT];
                        });  
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });                                  
                        e.revenuePending = e.totalTarget - e.revenueAchieved;
                        delete e.onsiteEmps;

                    } else {
                        e.revenuePending = e[totalTarget[0]] - e[revenueAchieved[0]];
                        delete e.totalTarget;
                        delete e.onsiteEmps;
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getRevenueTargetAchieved = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.month === undefined
        || params.searchType === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "", PGT_CODE_RE = "";

        if (params.searchBy == "BU Head" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_pandl_head='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        } else if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else if (params.searchBy == "Senior Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else {
            ACHWHERE = "";
            TARWHERE = "";
            DESIG_RE = "BU Head";
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        }

        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_target` AS "+monthNameShort+"RevenueTarget,0 AS "+monthNameShort+"RevenueProjected,0 AS "+monthNameShort+"RevenueAchieved";

            var SQL = query.getMonthlyRevenueAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_target` AS totalRevenueTarget,0 AS onsiteEmps";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalTarget=[],revenueProjected=[],revenueAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalTarget.push(monthNameShort+"RevenueTarget");
                    revenueProjected.push(monthNameShort+"RevenueProjected");
                    revenueAchieved.push(monthNameShort+"RevenueAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[results.length-1][index][monthNameShort+'RevenueAchieved'] = e.targetAchieved;
                            results[results.length-1][index][monthNameShort+'RevenueProjected'] = e.projected;
                            if(params.searchType=="yearly") {
                                if (currentMonth == monthNameShort) {
                                    results[results.length-1][index].onsiteEmps = e.onsiteEmps;
                                }
                            } else {
                                (e.onsiteEmps != 0)? (results[results.length-1][index].onsiteEmps == 0) ? results[results.length-1][index].onsiteEmps = e.onsiteEmps : results[results.length-1][index].onsiteEmps += ','+e.onsiteEmps : '';
                            }                                                        
                        }
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {                       
                    if(params.searchType=="yearly"){
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });       
                        
                        e.revenuePending = e.totalRevenueTarget - e.revenueAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            
                        e.avgBilling = (e[currentMonth + 'RevenueProjected'] / e.onsiteEmps).toFixed(2);
                        e.avgBilling = (e.onsiteEmps == 0) ? 0 : e.avgBilling;
                        e.netAdditions = parseInt(((e.revenuePending / e.avgBilling) / remainingMonth) - e.onsiteEmps);            
                        e.netAdditions = (e.onsiteEmps == 0 || remainingMonth==0) ? 0 : e.netAdditions;

                    } else if(params.searchType=="quarterly"){

                        e.totalRevenueTarget = 0;
                        totalTarget.forEach((tT) => {
                            e.totalRevenueTarget +=  e[tT];
                        });  
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });                                  
                        e.revenuePending = e.totalRevenueTarget - e.revenueAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            

                    } else {
                        e.revenuePending = e[totalTarget[0]] - e[revenueAchieved[0]];
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            
                        delete e.totalRevenueTarget;                        
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getGpTargetAchieved = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.month === undefined
        || params.searchType === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "", PGT_CODE_RE = "";

        if (params.searchBy == "BU Head" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_pandl_head='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        } else if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else if (params.searchBy == "Senior Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else {
            ACHWHERE = "";
            TARWHERE = "";
            DESIG_RE = "BU Head";
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        }

        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_gp_target` AS "+monthNameShort+"GpTarget,0 AS "+monthNameShort+"GpProjected,0 AS "+monthNameShort+"GpAchieved";

            var SQL = query.getMonthlyGpAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_gp_target` AS totalGpTarget,0 AS onsiteEmps";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalGpTarget=[],gpProjected=[],gpAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalGpTarget.push(monthNameShort+"GpTarget");
                    gpProjected.push(monthNameShort+"GpProjected");
                    gpAchieved.push(monthNameShort+"GpAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[results.length-1][index][monthNameShort+'GpAchieved'] = e.gpAchieved;
                            results[results.length-1][index][monthNameShort+'GpProjected'] = e.gpProjected;
                            (e.onsiteEmps != 0)? (results[results.length-1][index].onsiteEmps == 0) ? results[results.length-1][index].onsiteEmps = e.onsiteEmps : results[results.length-1][index].onsiteEmps += ','+e.onsiteEmps : '';                    
                        }                        
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){
                        e.gpProjected = 0;
                        gpProjected.forEach((rP) => {
                            e.gpProjected += e[rP];
                        });          
                        e.gpAchieved = 0;
                        gpAchieved.forEach((rA) => {
                            e.gpAchieved += e[rA];
                        });       
                        
                        e.gpPending = e.totalGpTarget - e.gpAchieved;     
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            

                    } else if(params.searchType=="quarterly"){

                        e.totalGpTarget = 0;
                        totalGpTarget.forEach((tT) => {
                            e.totalGpTarget +=  e[tT];
                        });  
                        e.gpProjected = 0;
                        gpProjected.forEach((rP) => {
                            e.gpProjected += e[rP];
                        });          
                        e.gpAchieved = 0;
                        gpAchieved.forEach((rA) => {
                            e.gpAchieved += e[rA];
                        });                                  
                        e.gpPending = e.totalGpTarget - e.gpAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;

                    } else {
                        e.gpPending = e[totalGpTarget[0]] - e[gpAchieved[0]];                        
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;                        
                        delete e.totalGpTarget;                        
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getHeadCountTargetAchieved = function (req, res, next) {
    var params = req.body;
    if (
        params.year === undefined
        || params.month === undefined
        || params.searchType === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "", PGT_CODE_RE = "";

        if (params.searchBy == "BU Head" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_pandl_head='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        } else if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else if (params.searchBy == "Senior Business Manager" && params.searchValue && params.searchCode) {
            ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
            TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";
            DESIG_RE = params.searchBy;
            PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        } else {
            ACHWHERE = "";
            TARWHERE = "";
            DESIG_RE = "BU Head";
            PGT_CODE_RE = "CM.pgt_code_pandl_head";
        }

        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_headcount_target` AS "+monthNameShort+"HeadCountTarget,0 AS "+monthNameShort+"HeadCountAchieved";

            var SQL = query.getMonthlyHeadCountAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_headcount_target` AS totalHeadCountTarget";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalHeadCountTarget=[],headcountAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalHeadCountTarget.push(monthNameShort+"HeadCountTarget");                  
                    headcountAchieved.push(monthNameShort+"HeadCountAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {                            
                            (e.headcountAchieved != 0)? (results[results.length-1][index][monthNameShort+'HeadCountAchieved'] == 0) ? results[results.length-1][index][monthNameShort+'HeadCountAchieved'] = e.headcountAchieved : results[results.length-1][index][monthNameShort+'HeadCountAchieved'] += ','+e.headcountAchieved : '';                                                   
                        }
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){                              
                        e.headcountAchieved = 0;
                        headcountAchieved.forEach((rA) => {                           
                            (e[rA] != 0)? (e.headcountAchieved == 0) ? e.headcountAchieved = e[rA] : e.headcountAchieved += ','+e[rA] : '';
                            e[rA] = (e[rA] != 0)? [...new Set(e[rA].split(","))].length : e[rA]; 
                        });       
                        e.headcountAchieved = (e.headcountAchieved != 0)? [...new Set(e.headcountAchieved.split(","))].length : e.headcountAchieved;                        
                        e.headcountPending = e.totalHeadCountTarget - e.headcountAchieved;                       

                    } else if(params.searchType=="quarterly"){

                        e.totalHeadCountTarget = 0;
                        totalHeadCountTarget.forEach((tT) => {
                            e.totalHeadCountTarget +=  e[tT];
                        });                                 
                        e.headcountAchieved = 0;
                        headcountAchieved.forEach((rA) => {
                            (e[rA] != 0)? (e.headcountAchieved == 0) ? e.headcountAchieved = e[rA] : e.headcountAchieved += ','+e[rA] : '';
                            e[rA] = (e[rA] != 0)? [...new Set(e[rA].split(","))].length : e[rA]; 
                        });     
                        e.headcountAchieved = (e.headcountAchieved != 0)? [...new Set(e.headcountAchieved.split(","))].length : e.headcountAchieved;                            
                        e.headcountPending = e.totalHeadCountTarget - e.headcountAchieved;                        

                    } else {                                   
                        e[headcountAchieved[0]]=(e[headcountAchieved[0]] != 0)? [...new Set(e[headcountAchieved[0]].split(","))].length : 0;            
                        e.headcountPending = e[totalHeadCountTarget[0]] - e[headcountAchieved[0]];   
                        delete e.totalHeadCountTarget;                   
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getTargetAchievedBMOld = function (req, res, next) {
    var params = req.body;
    if (
        params.month === undefined
        || params.year === undefined
        || params.userRole === undefined
        || params.userDesignation === undefined
        || params.userCode === undefined
        || params.userId === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var financialYears = params.year.split('-');
        var searchMonth = params.month;  
        var searchYear = (Number(searchMonth)>=4)? financialYears[0]:financialYears[1];
        var searchMonthName = moment(searchYear +'-'+searchMonth+'-01','YYYY-MM-DD').format("MMMM"); 

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "Business Manager", EMP_NAME_RE = "CM.bsns_mgr";


        if (
            params.userRole === "SuperAdmin" ||
            params.userRole === "Admin" ||
            params.userRole === "HRAdmin" ||
            params.userRole === "HRAdminReadOnly"
          ) {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
             ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
             TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
            }
          } else if (params.userRole === "BUHead" || params.userDesignation === "Senior Business Manager") {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
             } else {
              var RBMCode ="select employee_code from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMCode = RBMCode.replace(/PGTCODE/g, params.userCode);

              var RBMId ="select app_employee_id from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMId = RBMId.replace(/PGTCODE/g, params.userCode);

              ACHWHERE = "AND CM.pgt_code_bsns_mgr IN(" + RBMCode + ")";
              TARWHERE = "AND AE.app_employee_id IN(" + RBMId + ")";                
             }
          } else if (params.userDesignation === "Business Manager") {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.userCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.userId + "'";  
          } else if (params.userRole !== 'HR' && params.userRole !== 'Finance' && params.userRole !== 'FinanceAdmin' && params.userRole !== 'FinanceReadOnly' && params.userRole !== 'TA' && params.userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }

        var SQL = query.getTargetAchievedDetailsBM;

        SQL = SQL.replace("MONTHLYTARGET_RE", searchMonthName.toLowerCase()+'_target');
        SQL = SQL.replace("FYEAR_RE", params.year);
        SQL = SQL.replace("DESIG_RE", DESIG_RE);
        SQL = SQL.replace("TARCONDITIONALWHERE_RE", TARWHERE);
        SQL = SQL.replace(/SERVICE_MONTH_RE/g, searchYear +'-'+searchMonth);
        SQL = SQL.replace("MONTH_RE", searchMonth);
        SQL = SQL.replace("YEAR_RE", searchYear);
        SQL = SQL.replace(/EMP_NAME_RE/g, EMP_NAME_RE);
        SQL = SQL.replace("ACHCONDITIONALWHERE_RE", ACHWHERE);
   
        console.log("\n > getTargetAchievedDetailsBM SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {            
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }

};

exports.getTargetAchievedBM = function (req, res, next) {
    var params = req.body;
    if (
        params.month === undefined
        || params.year === undefined
        || params.searchType === undefined
        || params.userRole === undefined
        || params.userDesignation === undefined
        || params.userCode === undefined
        || params.userId === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {

        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "Business Manager", EMP_NAME_RE = "CM.bsns_mgr";


        if (
            params.userRole === "SuperAdmin" ||
            params.userRole === "Admin" ||
            params.userRole === "HRAdmin" ||
            params.userRole === "HRAdminReadOnly"
          ) {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
             ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
             TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
            }
          } else if (params.userRole === "BUHead" || params.userDesignation === "Senior Business Manager") {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
             } else {
              var RBMCode ="select employee_code from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMCode = RBMCode.replace(/PGTCODE/g, params.userCode);

              var RBMId ="select app_employee_id from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMId = RBMId.replace(/PGTCODE/g, params.userCode);

              ACHWHERE = "AND CM.pgt_code_bsns_mgr IN(" + RBMCode + ")";
              TARWHERE = "AND AE.app_employee_id IN(" + RBMId + ")";                
             }
          } else if (params.userDesignation === "Business Manager") {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.userCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.userId + "'";  
          } else if (params.userRole !== 'HR' && params.userRole !== 'Finance' && params.userRole !== 'FinanceAdmin' && params.userRole !== 'FinanceReadOnly' && params.userRole !== 'TA' && params.userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }

          var months = month.split(",");
          var SQL = "";
  
          months.forEach(month => {
              var splitYear = fyear.split("-");
              var year;
  
              if (month >= 4) {
               year = splitYear[0];
              } else {
               year = splitYear[1];
              }
  
              var serviceMonth = year + "-" + month.toString().padStart(2, "0");
              
              var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
              var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
  
                SQL += query.getTargetAchievedDetailsBM;

                SQL = SQL.replace("MONTHLYTARGET_RE", monthNameFull+'_target');
                SQL = SQL.replace("FYEAR_RE", fyear);
                SQL = SQL.replace("DESIG_RE", DESIG_RE);
                SQL = SQL.replace("TARCONDITIONALWHERE_RE", TARWHERE);
                SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
                SQL = SQL.replace("MONTH_RE",  month.toString().padStart(2, "0"));
                SQL = SQL.replace("YEAR_RE", year);
                SQL = SQL.replace(/EMP_NAME_RE/g, EMP_NAME_RE);
                SQL = SQL.replace("ACHCONDITIONALWHERE_RE", ACHWHERE);
          });        
   
        console.log("\n > getTargetAchievedDetailsBM SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {      
                
                months.shift();
                var indx = 1;
                months.forEach(month => {
                    results[indx].forEach((e) => {
                        var index = results[0].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[0][index]['revenueTarget'] += e.revenueTarget;
                            results[0][index]['revenueAchieved'] += e.revenueAchieved;
                            results[0][index]['grossProfitAchieved'] += e.grossProfitAchieved;
                            results[0][index]['onsiteEmps'] = e.onsiteEmps;
                        } else {
                            results[0].pus({
                                clientCostName : e.clientCostName,
                                empName : e.empName,
                                grossProfitAchieved : e.grossProfitAchieved,
                                grossProfitAchievedPercentage: e.grossProfitAchievedPercentage,
                                grossProfitExpected : e.grossProfitExpected,
                                onsiteEmps : e.onsiteEmps,
                                revenueAchieved : e.revenueAchieved,
                                revenueTarget : e.revenueTarget,
                                totalTarget : e.totalTarget
                            });
                        }
                    });
                    indx++;
                });

                if(months.length!=0){
                    results[0].forEach(e=> {
                        e.grossProfitExpected = (e.revenueAchieved * 0.25);                       
                        e.grossProfitAchievedPercentage = (e.revenueAchieved == 0) ? 0 :  Math.round((e.grossProfitAchieved/e.revenueAchieved) * 100);
                    });
                    results = results[0];
                }

                
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }

};

exports.getRevenueTargetAchievedBM = function (req, res, next) {
    var params = req.body;
    if (
        params.month === undefined
        || params.year === undefined
        || params.searchType === undefined
        || params.userRole === undefined
        || params.userDesignation === undefined
        || params.userCode === undefined
        || params.userId === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "Business Manager", PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        if (
            params.userRole === "SuperAdmin" ||
            params.userRole === "Admin" ||
            params.userRole === "HRAdmin" ||
            params.userRole === "HRAdminReadOnly"
          ) {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
             ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
             TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
            }
          } else if (params.userRole === "BUHead" || params.userDesignation === "Senior Business Manager") {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
             } else {
              var RBMCode ="select employee_code from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMCode = RBMCode.replace(/PGTCODE/g, params.userCode);

              var RBMId ="select app_employee_id from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMId = RBMId.replace(/PGTCODE/g, params.userCode);

              ACHWHERE = "AND CM.pgt_code_bsns_mgr IN(" + RBMCode + ")";
              TARWHERE = "AND AE.app_employee_id IN(" + RBMId + ")";                
             }
          } else if (params.userDesignation === "Business Manager") {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.userCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.userId + "'";  
          } else if (params.userRole !== 'HR' && params.userRole !== 'Finance' && params.userRole !== 'FinanceAdmin' && params.userRole !== 'FinanceReadOnly' && params.userRole !== 'TA' && params.userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }


        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_target` AS "+monthNameShort+"RevenueTarget,0 AS "+monthNameShort+"RevenueProjected,0 AS "+monthNameShort+"RevenueAchieved";

            var SQL = query.getMonthlyRevenueAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_target` AS totalRevenueTarget,0 AS onsiteEmps";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalTarget=[],revenueProjected=[],revenueAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalTarget.push(monthNameShort+"RevenueTarget");
                    revenueProjected.push(monthNameShort+"RevenueProjected");
                    revenueAchieved.push(monthNameShort+"RevenueAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[results.length-1][index][monthNameShort+'RevenueAchieved'] = e.targetAchieved;
                            results[results.length-1][index][monthNameShort+'RevenueProjected'] = e.projected;
                            if(params.searchType=="yearly") {
                                if (currentMonth == monthNameShort) {
                                    results[results.length-1][index].onsiteEmps = e.onsiteEmps;
                                }
                            } else {
                                (e.onsiteEmps != 0)? (results[results.length-1][index].onsiteEmps == 0) ? results[results.length-1][index].onsiteEmps = e.onsiteEmps : results[results.length-1][index].onsiteEmps += ','+e.onsiteEmps : '';
                            }                                                        
                        }
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });       
                        
                        e.revenuePending = e.totalRevenueTarget - e.revenueAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            
                        e.avgBilling = (e[currentMonth + 'RevenueProjected'] / e.onsiteEmps).toFixed(2);
                        e.avgBilling = (e.onsiteEmps == 0) ? 0 : e.avgBilling;
                        e.netAdditions = parseInt(((e.revenuePending / e.avgBilling) / remainingMonth) - e.onsiteEmps);            
                        e.netAdditions = (e.onsiteEmps == 0 || remainingMonth==0) ? 0 : e.netAdditions;

                    } else if(params.searchType=="quarterly"){

                        e.totalRevenueTarget = 0;
                        totalTarget.forEach((tT) => {
                            e.totalRevenueTarget +=  e[tT];
                        });  
                        e.revenueProjected = 0;
                        revenueProjected.forEach((rP) => {
                            e.revenueProjected += e[rP];
                        });          
                        e.revenueAchieved = 0;
                        revenueAchieved.forEach((rA) => {
                            e.revenueAchieved += e[rA];
                        });                                  
                        e.revenuePending = e.totalRevenueTarget - e.revenueAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            

                    } else {
                        e.revenuePending = e[totalTarget[0]] - e[revenueAchieved[0]];
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            
                        delete e.totalRevenueTarget;                        
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getGpTargetAchievedBM = function (req, res, next) {
    var params = req.body;
    if (
        params.month === undefined
        || params.year === undefined
        || params.searchType === undefined
        || params.userRole === undefined
        || params.userDesignation === undefined
        || params.userCode === undefined
        || params.userId === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "Business Manager", PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        if (
            params.userRole === "SuperAdmin" ||
            params.userRole === "Admin" ||
            params.userRole === "HRAdmin" ||
            params.userRole === "HRAdminReadOnly"
          ) {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
             ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
             TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
            }
          } else if (params.userRole === "BUHead" || params.userDesignation === "Senior Business Manager") {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
             } else {
              var RBMCode ="select employee_code from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMCode = RBMCode.replace(/PGTCODE/g, params.userCode);

              var RBMId ="select app_employee_id from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMId = RBMId.replace(/PGTCODE/g, params.userCode);

              ACHWHERE = "AND CM.pgt_code_bsns_mgr IN(" + RBMCode + ")";
              TARWHERE = "AND AE.app_employee_id IN(" + RBMId + ")";                
             }
          } else if (params.userDesignation === "Business Manager") {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.userCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.userId + "'";  
          } else if (params.userRole !== 'HR' && params.userRole !== 'Finance' && params.userRole !== 'FinanceAdmin' && params.userRole !== 'FinanceReadOnly' && params.userRole !== 'TA' && params.userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }


        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_gp_target` AS "+monthNameShort+"GpTarget,0 AS "+monthNameShort+"GpProjected,0 AS "+monthNameShort+"GpAchieved";

            var SQL = query.getMonthlyGpAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_gp_target` AS totalGpTarget,0 AS onsiteEmps";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalGpTarget=[],gpProjected=[],gpAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalGpTarget.push(monthNameShort+"GpTarget");
                    gpProjected.push(monthNameShort+"GpProjected");
                    gpAchieved.push(monthNameShort+"GpAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {
                            results[results.length-1][index][monthNameShort+'GpAchieved'] = e.gpAchieved;
                            results[results.length-1][index][monthNameShort+'GpProjected'] = e.gpProjected;
                            (e.onsiteEmps != 0)? (results[results.length-1][index].onsiteEmps == 0) ? results[results.length-1][index].onsiteEmps = e.onsiteEmps : results[results.length-1][index].onsiteEmps += ','+e.onsiteEmps : '';                    
                        }                        
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){
                        e.gpProjected = 0;
                        gpProjected.forEach((rP) => {
                            e.gpProjected += e[rP];
                        });          
                        e.gpAchieved = 0;
                        gpAchieved.forEach((rA) => {
                            e.gpAchieved += e[rA];
                        });       
                        
                        e.gpPending = e.totalGpTarget - e.gpAchieved;     
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;            

                    } else if(params.searchType=="quarterly"){

                        e.totalGpTarget = 0;
                        totalGpTarget.forEach((tT) => {
                            e.totalGpTarget +=  e[tT];
                        });  
                        e.gpProjected = 0;
                        gpProjected.forEach((rP) => {
                            e.gpProjected += e[rP];
                        });          
                        e.gpAchieved = 0;
                        gpAchieved.forEach((rA) => {
                            e.gpAchieved += e[rA];
                        });                                  
                        e.gpPending = e.totalGpTarget - e.gpAchieved;
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;

                    } else {
                        e.gpPending = e[totalGpTarget[0]] - e[gpAchieved[0]];                        
                        e.onsiteEmps = (e.onsiteEmps != 0)? [...new Set(e.onsiteEmps.split(","))].length : e.onsiteEmps;                        
                        delete e.totalGpTarget;                        
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};

exports.getHeadCountTargetAchievedBM = function (req, res, next) {
    var params = req.body;
    if (
        params.month === undefined
        || params.year === undefined
        || params.searchType === undefined
        || params.userRole === undefined
        || params.userDesignation === undefined
        || params.userCode === undefined
        || params.userId === undefined
        || params.searchBy === undefined
        || params.searchValue === undefined
        || params.searchCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var fyear = params.year.toString().trim();

        var currentfYear = (moment().subtract(1, 'months').format("M")>= 4)? moment().subtract(1, 'months').format("YYYY")+"-"+moment().subtract(1, 'months').add(1,'years').format("YYYY") : moment().subtract(1, 'months').subtract(1, 'years').format("YYYY") +"-"+ moment().subtract(1, 'months').format("YYYY");
        var currentMonthNum = (currentfYear === fyear)? moment().subtract(1, 'months').format("M") : 3;
        var currentMonth = (currentfYear === fyear)? moment().subtract(1, 'months').format("MMM") : "Mar";

        var remainingMonth = 0;
        if (currentMonthNum >= 4 && currentMonthNum <= 12) {
            remainingMonth = 12 - currentMonthNum + 3;
        } else {
            remainingMonth = 3 - currentMonthNum;
        }

        var ACHWHERE = "", TARWHERE = "", DESIG_RE = "Business Manager", PGT_CODE_RE = "CM.pgt_code_bsns_mgr";
        if (
            params.userRole === "SuperAdmin" ||
            params.userRole === "Admin" ||
            params.userRole === "HRAdmin" ||
            params.userRole === "HRAdminReadOnly"
          ) {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
             ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
             TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
            }
          } else if (params.userRole === "BUHead" || params.userDesignation === "Senior Business Manager") {
            if (params.searchBy == "Business Manager" && params.searchValue && params.searchCode) {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.searchCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.searchValue + "'";            
             } else {
              var RBMCode ="select employee_code from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMCode = RBMCode.replace(/PGTCODE/g, params.userCode);

              var RBMId ="select app_employee_id from prms_app_employees where `designation` = 'Business Manager' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE'))";
              RBMId = RBMId.replace(/PGTCODE/g, params.userCode);

              ACHWHERE = "AND CM.pgt_code_bsns_mgr IN(" + RBMCode + ")";
              TARWHERE = "AND AE.app_employee_id IN(" + RBMId + ")";                
             }
          } else if (params.userDesignation === "Business Manager") {
                ACHWHERE = "AND CM.pgt_code_bsns_mgr='" + params.userCode + "'";
                TARWHERE = "AND AE.app_employee_id='" + params.userId + "'";  
          } else if (params.userRole !== 'HR' && params.userRole !== 'Finance' && params.userRole !== 'FinanceAdmin' && params.userRole !== 'FinanceReadOnly' && params.userRole !== 'TA' && params.userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }

        var months = month.split(",");
        var monthlyAchievedSQL = "",SELECT_RE = "";

        months.forEach(month => {
            var splitYear = fyear.split("-");
            var year;

            if (month >= 4) {
             year = splitYear[0];
            } else {
             year = splitYear[1];
            }

            var serviceMonth = year + "-" + month.toString().padStart(2, "0");
            
            var monthNameFull =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMMM").toLowerCase();
            var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");

            SELECT_RE += (SELECT_RE=="") ? "":",";
            SELECT_RE += "T.`"+monthNameFull+"_headcount_target` AS "+monthNameShort+"HeadCountTarget,0 AS "+monthNameShort+"HeadCountAchieved";

            var SQL = query.getMonthlyHeadCountAchievedDetails;
            SQL = SQL.replace(/SERVICE_MONTH_RE/g, serviceMonth);
            SQL = SQL.replace("MONTH_RE", month.toString().padStart(2, "0"));
            SQL = SQL.replace("YEAR_RE", year);
            SQL = SQL.replace(/PGT_CODE_RE/g, PGT_CODE_RE);
            SQL = SQL.replace("CONDITIONALWHERE_RE", ACHWHERE);
            monthlyAchievedSQL += SQL;
        });

        SELECT_RE += ",T.`total_headcount_target` AS totalHeadCountTarget";

        var TAR_SQL = query.getYearlyTargetDetails;
        TAR_SQL = TAR_SQL.replace("SELECT_RE", SELECT_RE);
        TAR_SQL = TAR_SQL.replace("FYEAR_RE", fyear);
        TAR_SQL = TAR_SQL.replace("DESIG_RE", DESIG_RE);
        TAR_SQL = TAR_SQL.replace("CONDITIONALWHERE_RE", TARWHERE);

        console.log("\n > monthlyAchievedSQL SQL ---> ", monthlyAchievedSQL + TAR_SQL);
        db.query(monthlyAchievedSQL + TAR_SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var indx = 0,totalHeadCountTarget=[],headcountAchieved=[];
                months.forEach(month => {

                    var splitYear = fyear.split("-");
                    var year;

                    if (month >= 4) {
                    year = splitYear[0];
                    } else {
                    year = splitYear[1];
                    }

                    var serviceMonth = year + "-" + month.toString().padStart(2, "0");                   
                    var monthNameShort =  moment(serviceMonth+'-01','YYYY-MM-DD').format("MMM");
                    totalHeadCountTarget.push(monthNameShort+"HeadCountTarget");                  
                    headcountAchieved.push(monthNameShort+"HeadCountAchieved");
                    
                    results[indx].forEach((e) => {
                        var index = results[results.length-1].findIndex(function (item, i) {
                            return item.clientCostName === e.clientCostName && item.empName === e.empName
                        });
                        if (index != -1) {                            
                            (e.headcountAchieved != 0)? (results[results.length-1][index][monthNameShort+'HeadCountAchieved'] == 0) ? results[results.length-1][index][monthNameShort+'HeadCountAchieved'] = e.headcountAchieved : results[results.length-1][index][monthNameShort+'HeadCountAchieved'] += ','+e.headcountAchieved : '';                                                   
                        }
                    });
                    indx++;
                });
               

                results[results.length-1].forEach((e) => {   
                    
                    if(params.searchType=="yearly"){                              
                        e.headcountAchieved = 0;
                        headcountAchieved.forEach((rA) => {                           
                            (e[rA] != 0)? (e.headcountAchieved == 0) ? e.headcountAchieved = e[rA] : e.headcountAchieved += ','+e[rA] : '';
                            e[rA] = (e[rA] != 0)? [...new Set(e[rA].split(","))].length : e[rA]; 
                        });       
                        e.headcountAchieved = (e.headcountAchieved != 0)? [...new Set(e.headcountAchieved.split(","))].length : e.headcountAchieved;                        
                        e.headcountPending = e.totalHeadCountTarget - e.headcountAchieved;                       

                    } else if(params.searchType=="quarterly"){

                        e.totalHeadCountTarget = 0;
                        totalHeadCountTarget.forEach((tT) => {
                            e.totalHeadCountTarget +=  e[tT];
                        });                                 
                        e.headcountAchieved = 0;
                        headcountAchieved.forEach((rA) => {
                            (e[rA] != 0)? (e.headcountAchieved == 0) ? e.headcountAchieved = e[rA] : e.headcountAchieved += ','+e[rA] : '';
                            e[rA] = (e[rA] != 0)? [...new Set(e[rA].split(","))].length : e[rA]; 
                        });     
                        e.headcountAchieved = (e.headcountAchieved != 0)? [...new Set(e.headcountAchieved.split(","))].length : e.headcountAchieved;                            
                        e.headcountPending = e.totalHeadCountTarget - e.headcountAchieved;                        

                    } else {                                   
                        e[headcountAchieved[0]]=(e[headcountAchieved[0]] != 0)? [...new Set(e[headcountAchieved[0]].split(","))].length : 0;            
                        e.headcountPending = e[totalHeadCountTarget[0]] - e[headcountAchieved[0]];   
                        delete e.totalHeadCountTarget;                   
                    }                   
                });


                res.status(200).json({ status: 'success', data: results[results.length-1] });
            }
        });
    }

};


