var db = require('../db/manageDB');
var query = require('../queries/costCenter.query');
var moment = require('moment-timezone');

exports.getCostCenter = function (req, res) {

    var params = req.params;
    var SQL = query.getCostCenter;

    SQL = SQL.replace(/isActiveRe/g, params.status);
    db.query(SQL, function (error, result) {
        console.log("Cost center-->", SQL);
        if (error) {
            res.status(400).send({ status: "failure", message: "Something went wrong" + error });
        } else {
            var data = { status: "success", data: result };
            res.status(200).send(data);
        }
    });
};

exports.createCostCenter = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientId = removeString(data.clientId);
    var costCenterName = removeString(data.costCenterName);
    var bsnsDy = removeString(data.bsnsDy);
    var loginUser = removeString(data.loginUser);
    timesNow = removeString(timesNow);
    var isActive = removeString(data.isActive, 'INT');
    var leaveType = removeString(data.leaveType);

    if (!costCenterName || !clientId || !bsnsDy || !loginUser || !isActive || !leaveType) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    } else {
        var SQL = query.createCostCenter;
        SQL = SQL.replace('clientIdRe', clientId);
        SQL = SQL.replace('costCenterNameRe', costCenterName);
        SQL = SQL.replace('bsnsDyRe', bsnsDy);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        SQL = SQL.replace(/isActiveRe/g, isActive)
        SQL = SQL.replace(/leaveTypeRe/g, leaveType);
        console.log("\n > createCostCenter SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createCostCenter SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Created' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};

exports.updateCostCenter = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientId = removeString(data.clientId);
    var costCenterId = removeString(data.costCenterId);
    var costCenterName = removeString(data.costCenterName);
    var bsnsDy = removeString(data.bsnsDy);
    var loginUser = removeString(data.loginUser);
    var isMergeInvoice = removeString(data.isMergeInvoice, 'INT');
    var isAssignInvoice = removeString(data.isAssignInvoice, 'INT');
    timesNow = removeString(timesNow);
    var isActive = removeString(data.isActive, 'INT');
    var leaveType = removeString(data.leaveType);

    if (!costCenterName || !costCenterId || !clientId || !bsnsDy || !loginUser || !isActive || !leaveType) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload' });
    }
    else if (isActive == 1) {
        var SQL = query.updateCostCenter;
        SQL = SQL.replace('costCenterNameRe', costCenterName);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/clientIdRe/g, clientId);
        SQL = SQL.replace(/costCenterIdRe/g, costCenterId);
        SQL = SQL.replace(/bsnsDyRe/g, bsnsDy);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        SQL = SQL.replace(/isActiveRe/g, isActive)
        SQL = SQL.replace(/leaveTypeRe/g, leaveType);
        console.log("\n > updateCostCenter SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateCostCenter SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Updated Successfully ' });
            }
        });
    }
    else {
        var CSQL = query.checkActiveEmp;
        CSQL = CSQL.replace(/costCenterIdRe/g, costCenterId);
        db.query(CSQL, function (error, results, fields) {
            if (error) {
                console.log("\n > checkActiveEmp Costcenter Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                if (results.length == 0) {

                    var SQL = query.updateCostCenter;
                    SQL = SQL.replace('costCenterNameRe', costCenterName);
                    SQL = SQL.replace(/loginIdRe/g, loginUser);
                    SQL = SQL.replace(/clientIdRe/g, clientId);
                    SQL = SQL.replace(/costCenterIdRe/g, costCenterId);
                    SQL = SQL.replace(/bsnsDyRe/g, bsnsDy);
                    SQL = SQL.replace(/timesNowRe/g, timesNow);
                    SQL = SQL.replace(/isActiveRe/g, isActive)
                    SQL = SQL.replace(/leaveTypeRe/g, leaveType);
                    console.log("\n > updateCostCenter SQL ---> ", SQL);
                    // res.status(200).json({ status: 'success', data: 'Successfully Created' });
                    db.query(SQL, function (error, results, fields) {
                        if (error) {
                            console.log("\n > updateCostCenter SQL Err ---> ", error.code);
                            res.status(400).json({ status: 'failure', error: error.code });
                        } else {
                            res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: 'failure', error: "Please inactivate client mappings first." });
                }

            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};

exports.getCostCenterInv = function (req, res) {
    db.query(query.getCostCenterInv, function (error, result) {
        if (error) {
            res.status(400).send({ status: "failure", message: "Something went wrong" + error });
        } else {
            var data = { status: "success", data: result };
            res.status(200).send(data);
        }
    });
};

exports.createCostCenterInv = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientId = removeString(data.clientId);
    var csiName = removeString(data.csiName);
    var loginUser = removeString(data.loginUser);
    timesNow = removeString(timesNow);

    if (!csiName || !clientId || !loginUser) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var SQL = query.createCostCenterInv;
        SQL = SQL.replace('clientIdRe', clientId);
        SQL = SQL.replace('csiNameRe', csiName);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        console.log("\n > createCostCenterInv SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createCostCenterInv SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Created' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};


exports.updateCostCenterInv = function (req, res) {
    var data = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var clientId = removeString(data.clientId);
    var csiId = removeString(data.csiId);
    var csiName = removeString(data.csiName);
    var loginUser = removeString(data.loginUser);
    timesNow = removeString(timesNow);

    if (!csiName || !csiId || !clientId || !loginUser) {
        res.status(400).json({ status: 'failure', error: 'Invlid payload' });
    } else {
        var SQL = query.updateCostCenterInv;
        SQL = SQL.replace('csiNameRe', csiName);
        SQL = SQL.replace(/loginIdRe/g, loginUser);
        SQL = SQL.replace(/clientIdRe/g, clientId);
        SQL = SQL.replace(/csiIdRe/g, csiId);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        console.log("\n > updateCostCenterInv SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'Successfully Created' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateCostCenterInv SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Successfully Created' });
            }
        });
    }
    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            if (formatType === 'INT') {
                return data ? data : 0;
            }
            return "'" + data + "'";
        } else {
            return null;
        }
    }
};