var db = require('../db/manageDB');
var query = require('../queries/user.query');
var moment = require('moment-timezone');

exports.userLogin = function (req, res, next) {
    var params = req.params;
    var SQL = query.userLogin;
    SQL = SQL.replace("EMAILID", params.email.toString().trim());

    console.log("\n > userLogin SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > userLogin SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = "";
            if (results.length > 0) {
                data = { status: 'success', data: results[0] };
            } else {
                data = { status: 'failure', data: "Invalid User!!" };
            }
            res.status(200).json(data);
        }
    });
};

exports.userTypes = function (req, res, next) {
    var SQL = query.userTypes;
    console.log("\n > userTypes SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > userTypes SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getUserRolePermission = function (req, res, next) {
    var params = req.params;
    var SQL = query.getUserRolePermission;
    SQL = SQL.replace("role_name_re",params.userRole.toString().trim());
    console.log("\n > getUserRolePermission SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getUserRolePermission SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            var result=[];
            var obj = {};
            results.forEach(element => {
              var objName = element.name;              
              obj[objName]= element;              
            });
            result.push(obj);
            res.status(200).json({ status: 'success', data: result[0]});
        }
    });
};
