var db = require('../db/manageDB');
var query = require('../queries/financeData.query');
var pquery = require('../queries/payrollTracker.query');
var moment = require('moment-timezone');
var xlsx = require('xlsx');
var mkdirp = require('mkdirp');
var fs = require('fs');
var config = require('../../config');
var formidable = require('formidable');

exports.attendenceTracker = function (req, res, next) {
    var params = req.body;

    if (
        params.dateFrom == undefined
        || params.dateTo == undefined
        || params.month == undefined
        || params.year == undefined
        || params.searchBy == undefined
        || params.searchValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var dateFrom = params.dateFrom.toString().trim();
        var dateTo = params.dateTo.toString().trim();
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var type = params.type.toString().trim();
        var SQL = query.attendenceTracker;
        var WHERE = "";
        var lastMonth = '';
        var lastYear = '';
        if (dateFrom == "" || dateTo == "") {
            dateFrom = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            dateTo = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
        } else {
            dateFrom = moment(dateFrom).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            dateTo = moment(dateTo).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
        }
        if (month == "" || year == "") {
            month = Number(moment.utc().tz("Asia/Kolkata").format("MM"));
            year = Number(moment.utc().tz("Asia/Kolkata").format("YYYY"));
        }
        if (month == 1) {
            lastMonth = 12;
            lastYear = year - 1;
        } else {
            lastMonth = month - 1;
            lastYear = year;
        }

        var serviceMonth = moment(dateFrom).utc().tz("Asia/Kolkata").format("YYYY-MM");

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.employee_id IN (" + data + ")";
                // WHERE = "AND H.employee_id = '" + searchValue + "'";
            } else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "invoiceNumber") {
                WHERE = "AND INV.invoice_number = '" + searchValue + "'";
            }
        }
        SQL = SQL.replace(/DATEFROM/g, dateFrom);
        SQL = SQL.replace(/DATETO/g, dateTo);
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, '0'));
        SQL = SQL.replace(/WHEREYEAR/g, year);
        SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
        SQL = SQL.replace(/LASTYEAR/g, lastYear);
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > params ---> ", JSON.stringify(params));
        console.log("\n > serviceMonth ---> ", serviceMonth);
        console.log("\n > month ---> ", month.toString().padStart(2, '0'));
        console.log("\n > year ---> ", year);
        console.log("\n > lastMonth ---> ", lastMonth.toString().padStart(2, '0'));
        console.log("\n > lastYear ---> ", lastYear);

        console.log("\n > attendenceTracker SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > attendenceTracker SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var days = function (month, year) {
                    return new Date(year, month, 0).getDate();
                };

                var noOfDays = function (month, year) {
                    var date1 = new Date(dateFrom);
                    var date2 = new Date(dateTo);

                    // To calculate the time difference of two dates
                    var Difference_In_Time = date2.getTime() - date1.getTime();
                    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                    return Difference_In_Days + 1;
                };
                results.forEach(e => {
                    e.timesheetInvoiceId = e.timesheetInvoiceId == null || e.timesheetInvoiceId == '' ? '' : e.timesheetInvoiceId;

                    e.workingDays = e.workingDays === null || e.workingDays === '' ? noOfDays(dateFrom, dateTo) : e.workingDays;
                    e.billingDays = e.billingDays === null || e.billingDays === '' ? noOfDays(dateFrom, dateTo) : e.billingDays;
                    e.leaveWithoutPay = e.leaveWithoutPay === null || e.leaveWithoutPay === '' ? 0 : e.leaveWithoutPay;
                    e.payableDays = e.payableDays === null || e.payableDays === '' ? noOfDays(dateFrom, dateTo) : e.payableDays;

                    e.takenLeaveCl = e.takenLeaveCl == null || e.takenLeaveCl == '' ? 0 : e.takenLeaveCl;
                    e.takenLeaveSl = e.takenLeaveSl == null || e.takenLeaveSl == '' ? 0 : e.takenLeaveSl;
                    e.eLeaveCl = e.eLeaveCl == null || e.eLeaveCl == '' ? 0 : e.eLeaveCl;
                    e.eLeaveSl = e.eLeaveSl == null || e.eLeaveSl == '' ? 0 : e.eLeaveSl;                   
                    e.lastMonthBalCl = (e.lastMonthBalCl == undefined || e.lastMonthBalCl == null) ? 0 : e.lastMonthBalCl;
                    e.lastMonthBalSl = (e.lastMonthBalSl == undefined || e.lastMonthBalSl == null) ? 0 : e.lastMonthBalSl;
                    e.balLeaveCl = e.balLeaveCl == null || e.balLeaveCl == '' ? 0 : e.balLeaveCl;
                    e.balLeaveSl = e.balLeaveSl == null || e.balLeaveSl == '' ? 0 : e.balLeaveSl;

                    if (type == 'NEW') {
                        e.balLeaveCl = e.eLeaveCl + e.lastMonthBalCl;
                        e.balLeaveSl = e.eLeaveSl + e.lastMonthBalSl;
                    } else {
                        e.balLeaveCl = (e.eLeaveCl + e.lastMonthBalCl) - e.takenLeaveCl;
                        e.balLeaveSl = (e.eLeaveSl + e.lastMonthBalSl) - e.takenLeaveSl;
                    }

                    e.timesheetFrom = e.timesheetFrom == null || e.timesheetFrom == '' ? dateFrom : e.timesheetFrom;
                    e.timesheetTo = e.timesheetTo == null || e.timesheetTo == '' ? dateTo : e.timesheetTo;

                    e.timesheetMonth = e.timesheetMonth == null || e.timesheetMonth == '' ? month : e.timesheetMonth;
                    e.timesheetMonth = e.timesheetMonth.toString().padStart(2, '0');
                    e.timesheetYear = e.timesheetYear == null || e.timesheetYear == '' ? year : e.timesheetYear;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.getInvoice = function (req, res, next) {
    var params = req.body;

    if (
        params.month == undefined
        || params.year == undefined
        || params.searchBy == undefined
        || params.searchValue == undefined
        || params.type == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var SQL = params.type == 'NEW' ? query.getInvoiceNew : params.type == 'PARTIALLY' ? query.getInvoicePartial : query.getInvoiceComplete;
        var WHERE = "";
        var lastMonth = '';
        var lastYear = '';
        if (month == 1) {
            lastMonth = 12;
            lastYear = year - 1;
        } else {
            lastMonth = month - 1;
            lastYear = year;
        }

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            }
            else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.employee_id IN (" + data + ")";
            }
            else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "invoiceNumber") {
                WHERE = "AND I.invoice_number = '" + searchValue + "'";
            }
        }

        SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, '0'));
        SQL = SQL.replace(/WHEREYEAR/g, year);
        SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
        SQL = SQL.replace(/LASTYEAR/g, lastYear);
        SQL = SQL.replace(/CONDITIONALWHERE/g, WHERE);
        var serviceMonth = year + '-' + month.toString().padStart(2, '0');
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);


        console.log("\n > getInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.getInvoiceSearch = function (req, res, next) {
    var params = req.body;

    if (
         params.searchValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var searchValue = params.searchValue.toString().trim();
        var SQL = query.getInvoiceSearch;
        SQL = SQL.replace(/INVOICENUMBER/g, searchValue);

        console.log("\n > getInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.searchInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.searchBy == undefined
        || params.searchValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = params.year.toString().trim();
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var SQL = query.searchInvoice;
        var WHERE = "";

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            }
            else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.employee_id IN (" + data + ")";
                // WHERE = "AND H.employee_id = '" + searchValue + "'";
            }
            else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "invoiceNumber") {
                WHERE = "AND T.invoice_number = '" + searchValue + "'";
            }
        }

        SQL = SQL.replace('WHEREMONTH', month.toString().padStart(2, '0'));
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > searchInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > searchInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.createTimesheet = function (req, res, next) {
    var data = req.body;
    insertTimesheet(data, function (err, message) {
        if (err) {
            console.log("\n > createTimesheet SQL Err ---> ", message);
            res.status(400).json({ status: 'failure', error: message });
        } else {
            console.log("\n > createTimesheet SQL success ---> ", message);
            res.status(200).json({ status: 'success', data: message });
        }
    });
    function insertTimesheet(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var actionType = (row.actionType == undefined) ? '' : row.actionType;
                    var hrMasterId = removeString(row.hrMasterId);
                    var timesheetMonth = removeString(row.timesheetMonth);
                    var timesheetYear = removeString(row.timesheetYear);
                    var timesheetFrom = removeString(row.timesheetFrom) ? moment(removeString(row.timesheetFrom)).utc().tz("Asia/Kolkata").format("YYYY-MM-DD") : removeString(row.timesheetFrom);
                    var timesheetTo = removeString(row.timesheetTo) ? moment(removeString(row.timesheetTo)).utc().tz("Asia/Kolkata").format("YYYY-MM-DD") : removeString(row.timesheetTo);
                    var workingDays = removeString(row.workingDays) ? removeString(row.workingDays) : 0;
                    var billingDays = removeString(row.billingDays) ? removeString(row.billingDays) : 0;
                    var leaveWithoutPay = removeString(row.leaveWithoutPay) ? removeString(row.leaveWithoutPay) : 0;
                    var payableDays = removeString(row.payableDays) ? removeString(row.payableDays) : 0;
                    var lastMonthBalCl = removeString(row.lastMonthBalCl) ? removeString(row.lastMonthBalCl) : 0;
                    var lastMonthBalSl = removeString(row.lastMonthBalSl) ? removeString(row.lastMonthBalSl) : 0;
                    var takenLeaveCl = removeString(row.takenLeaveCl) ? removeString(row.takenLeaveCl) : 0;
                    var takenLeaveSl = removeString(row.takenLeaveSl) ? removeString(row.takenLeaveSl) : 0;
                    var eLeaveCl = removeString(row.eLeaveCl) ? removeString(row.eLeaveCl) : 0;
                    var eLeaveSl = removeString(row.eLeaveSl) ? removeString(row.eLeaveSl) : 0;
                    var balLeaveCl = removeString(row.balLeaveCl) ? (removeString(row.balLeaveCl)<0 ? 0 : removeString(row.balLeaveCl)): 0;
                    var balLeaveSl = removeString(row.balLeaveSl) ? (removeString(row.balLeaveSl)<0 ? 0 : removeString(row.balLeaveSl)) : 0;
                    var salaryAmount = removeString(row.salaryAmount) ? removeString(row.salaryAmount) : 0;
                    var billingAmount = removeString(row.billingAmount) ? removeString(row.billingAmount) : 0;
                    // var casualLeave = (balLeaveCl < 0) ? balLeaveCl : 0;
                    // var sikLeave = (balLeaveSl < 0) ? balLeaveSl : 0;

                    var salaryPerDay = (workingDays == 0) ? 0 : billingAmount / workingDays;
                    // payableDays = Number(payableDays) + Number(casualLeave);
                    // payableDays = Number(payableDays) + Number(sikLeave);
                    var resorceSalary = salaryAmount;
                    var amount = Number(salaryPerDay) * Number(payableDays);

                    // console.log('\n> casualLeave ---> ', casualLeave);
                    // console.log('> sikLeave ---> ', sikLeave);
                    console.log('> payableDays ---> ', payableDays);
                    console.log('> salaryAmount ---> ', salaryAmount);
                    console.log('> salaryPerDay ---> ', salaryPerDay);
                    console.log('> resorceSalary ---> ', resorceSalary);

                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status) ? removeString(row.status) : 1;
                    var salaryStatus = removeString(row.salaryStatus) ? removeString(row.salaryStatus) : 'Salary Hold';
                    var incentive = removeString(row.incentive) ? removeString(row.incentive) : 0;
                    var reimbursement = removeString(row.reimbursement) ? removeString(row.reimbursement) : 0;
                    var loginUserId = removeString(row.loggedInUserId) ? removeString(row.loggedInUserId) : 1;
                    var timesheetInvoiceId = removeString(row.timesheetInvoiceId) ? removeString(row.timesheetInvoiceId) : '';

                    if (actionType == 'create') {
                        var SQL = query.createTimesheet;
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('hrMasterId', removeString(hrMasterId, true));
                        SQL = SQL.replace('timesheetMonth', removeString(timesheetMonth, true));
                        SQL = SQL.replace('timesheetYear', removeString(timesheetYear, true));
                        SQL = SQL.replace('timesheetFrom', removeString(timesheetFrom, true));
                        SQL = SQL.replace('timesheetTo', removeString(timesheetTo, true));
                        SQL = SQL.replace('workingDays', removeString(workingDays, true));
                        SQL = SQL.replace('billingDays', removeString(billingDays, true));
                        SQL = SQL.replace('leaveWithoutPay', removeString(leaveWithoutPay, true));
                        SQL = SQL.replace('payableDays', removeString(payableDays, true));
                        SQL = SQL.replace('takenLeaveCl', removeString(takenLeaveCl, true));
                        SQL = SQL.replace('takenLeaveSl', removeString(takenLeaveSl, true));
                        SQL = SQL.replace('eLeaveCl', removeString(eLeaveCl, true));
                        SQL = SQL.replace('eLeaveSl', removeString(eLeaveSl, true));
                        SQL = SQL.replace('balLeaveCl', removeString(balLeaveCl, true));
                        SQL = SQL.replace('balLeaveSl', removeString(balLeaveSl, true));
                        SQL = SQL.replace('commentsRe', removeString(comments, true));
                        SQL = SQL.replace('remarksRe', removeString(remarks, true));
                        SQL = SQL.replace('statusRe', removeString(status, true));
                        SQL = SQL.replace('salaryStatus', removeString(salaryStatus, true));
                        SQL = SQL.replace('incentiveRe', removeString(incentive, true));
                        SQL = SQL.replace('reimbursementRe', removeString(reimbursement, true));
                        SQL = SQL.replace('resourceSalRe', removeString(resorceSalary, true));
                        SQL = SQL.replace(/amountRe/g, removeString(amount, true));
                        SQL = SQL.replace(/loginUserId/g, removeString(loginUserId, true));
                        SQL = SQL.replace(/timesNow/g, timesNow);

                        var takenLeave = Number(takenLeaveCl) + Number(takenLeaveSl);
                        var lastMonthBal = Number(lastMonthBalCl) + Number(lastMonthBalSl);
                        var balLeave = Number(balLeaveCl) + Number(balLeaveSl);
                        var ctcPerDay = (workingDays == 0) ? 0 : resorceSalary / workingDays;
                        var ctc = Number(ctcPerDay) * Number(payableDays);

                        var SQL2 = pquery.createPayroll;
                        SQL2 = SQL2.replace('hr_master_id_RE', removeString(hrMasterId, true));
                        SQL2 = SQL2.replace('payroll_from_RE', null);
                        SQL2 = SQL2.replace('payroll_to_RE', null);
                        SQL2 = SQL2.replace('payroll_month_RE', removeString(timesheetMonth, true));
                        SQL2 = SQL2.replace('payroll_year_RE', removeString(timesheetYear, true));
                        SQL2 = SQL2.replace('working_days_RE', removeString(workingDays, true));
                        SQL2 = SQL2.replace('payable_days_RE', removeString(payableDays, true));
                        SQL2 = SQL2.replace('leave_without_pay_RE', removeString(leaveWithoutPay, true));
                        SQL2 = SQL2.replace('taken_leave_RE', removeString(takenLeave, true));
                        SQL2 = SQL2.replace('previous_leave_balance_RE', removeString(lastMonthBal, true));
                        SQL2 = SQL2.replace('current_leave_balance_RE', removeString(balLeave, true));
                        SQL2 = SQL2.replace('incentive_allowance_RE', removeString(incentive, true));
                        SQL2 = SQL2.replace('reimbursement_RE', removeString(reimbursement, true));
                        SQL2 = SQL2.replace('resourceSalary_RE', removeString(resorceSalary, true));
                        SQL2 = SQL2.replace('ctc_RE', removeString(ctc, true));
                        SQL2 = SQL2.replace('comments_RE', removeString(comments, true));
                        SQL2 = SQL2.replace(/loginUserId/g, removeString(loginUserId, true));
                        SQL2 = SQL2.replace(/timeNow/g, timesNow);

                        var CSQL = pquery.checkPayrollsheetForImport;
                        CSQL = CSQL.replace('hr_master_id_RE', removeString(hrMasterId, true));
                        CSQL = CSQL.replace('payroll_month_RE', removeString(timesheetMonth.toString().padStart(2, '0'), true));
                        CSQL = CSQL.replace('payroll_year_RE', removeString(timesheetYear, true));

                        console.log("\n> insertTimesheet SQL (" + count + ") ---> ", SQL);
                        // count++;
                        // if (count >= input.length) {
                        //     callback(false, 'Successfully Created');
                        // } else {
                        //     insertRow(input);
                        // }                        

                        db.query(CSQL, function (cerr, cresults, fields) {
                            count++;
                            if (cerr) {
                                console.log("\n > insertTimesheet SQL (" + count + ") err ---> ", err);
                                if (count == input.length) {
                                    callback(false, 'Successfully Created');
                                } else {
                                    insertRow(input);
                                }
                                // callback(true, err.sqlMessage);
                            } else {
                                if (cresults.length == 0) {
                                    SQL += SQL2;
                                }
                                db.query(SQL, function (err, results, fields) {
                                    if (err) {
                                        console.log("\n > insertTimesheet SQL (" + count + ") err ---> ", err);
                                        if (count == input.length) {
                                            callback(false, 'Successfully Created');
                                        } else {
                                            insertRow(input);
                                        }
                                        // callback(true, err.sqlMessage);
                                    } else {
                                        if (count == input.length) {
                                            callback(false, 'Successfully Created');
                                        } else {
                                            insertRow(input);
                                        }
                                    }
                                });

                            }
                        });
                    } else if (actionType == 'update' && timesheetInvoiceId) {
                        var SQL = query.updateTimesheet;
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('hrMasterId', removeString(hrMasterId, true));
                        SQL = SQL.replace('timesheetFrom', removeString(timesheetFrom, true));
                        SQL = SQL.replace('timesheetTo', removeString(timesheetTo, true));
                        SQL = SQL.replace('workingDays', removeString(workingDays, true));
                        SQL = SQL.replace('billingDays', removeString(billingDays, true));
                        SQL = SQL.replace('leaveWithoutPay', removeString(leaveWithoutPay, true));
                        SQL = SQL.replace('payableDays', removeString(payableDays, true));
                        SQL = SQL.replace('takenLeaveCl', removeString(takenLeaveCl, true));
                        SQL = SQL.replace('takenLeaveSl', removeString(takenLeaveSl, true));
                        SQL = SQL.replace('balLeaveCl', removeString(balLeaveCl, true));
                        SQL = SQL.replace('balLeaveSl', removeString(balLeaveSl, true));
                        SQL = SQL.replace('commentsRe', removeString(comments, true));
                        SQL = SQL.replace('remarksRe', removeString(remarks, true));
                        SQL = SQL.replace('statusRe', removeString(status, true));
                        SQL = SQL.replace('salaryStatus', removeString(salaryStatus, true));
                        SQL = SQL.replace('incentiveRe', removeString(incentive, true));
                        SQL = SQL.replace('reimbursementRe', removeString(reimbursement, true));
                        SQL = SQL.replace('resourceSalRe', removeString(resorceSalary, true));
                        SQL = SQL.replace(/amountRe/g, removeString(amount, true));
                        SQL = SQL.replace(/loginUserId/g, removeString(loginUserId, true));
                        SQL = SQL.replace(/timesNow/g, timesNow);
                        SQL = SQL.replace('timesheetInvoiceId', timesheetInvoiceId);

                        console.log("\n> updateTimesheet SQL (" + count + ") ---> ", SQL);
                        // count++;
                        // if (count >= input.length) {
                        //     callback(null, true);
                        // } else {
                        //     insertRow(input);
                        // }
                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > updateTimesheet SQL (" + count + ") err ---> ", err);
                                if (count == input.length) {
                                    callback(false, 'Successfully Updated');
                                } else {
                                    insertRow(input);
                                }
                            } else {
                                if (count == input.length) {
                                    callback(false, 'Successfully Updated');
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.length) {
                            callback(false, 'Successfully Updated');
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
}

exports.deleteTimesheet = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deleteTimesheet;
        SQL = SQL.replace("timesheetInvoiceId", params.id.toString().trim());

        console.log("\n > deleteTimesheet SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deleteTimesheet SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Deleted Successfully' });
            }
        });
    }
};

exports.createInvoice = function (req, res, next) {
    var data = req.body;
    if (data.invoice.actionType == "complete") {
        var getInvoiceSequence = query.getInvoiceSequence;
        db.query(getInvoiceSequence, function (err, results, fields) {
            console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);

            var financeYear = results[0].financeYear;
            var sequence = results[0].sequenceNo;
            data.invoice.invoiceNumber = 'PRIMUS' + financeYear + sequence;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    updateInvoiceSequence(data.invoice.actionType);
                    res.status(200).json({ status: 'success', data: 'Created Successfully' });
                }
            });
        });
    } else if (data.invoice.actionType == "draft" && data.invoice.invoiceNumber == "") {
        data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    } else {
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    }
    // data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
    function updateInvoiceSequence(data) {
        var updateInvoiceSequence = query.updateInvoiceSequence;
        console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
        db.query(updateInvoiceSequence, function (err, results, fields) {
            console.log("\n > updateInvoiceSequence SQL success ---> ");
        });
    }
    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];
                    // console.log('\n > row ---> ', row);
                    var actionType = (row.actionType == undefined) ? '' : row.actionType;
                    var taxableAmount = (row.taxableAmount == undefined || row.taxableAmount == null) ? 0 : row.taxableAmount;

                    if (actionType == 'create' || (com.actionType == "complete" && taxableAmount)) {
                        var invoiceNumber = (com.invoiceNumber == undefined || com.invoiceNumber == null) ? '' : com.invoiceNumber;
                        var gstNumber = (com.gstNumber == undefined || com.gstNumber == null) ? '' : com.gstNumber;
                        var invoiceDate = (com.invoiceDate == undefined || com.invoiceDate == null) ? '' : com.invoiceDate + " 00:00:00";
                        var invoiceType = (com.invoiceType == undefined || com.invoiceType == null) ? '' : com.invoiceType;
                        var invoiceMode = (com.invoiceMode == undefined || com.invoiceMode == null) ? '' : com.invoiceMode;
                        var loginUserId = (com.loginUserId == undefined || com.loginUserId == null) ? '' : com.loginUserId;

                        var taxableAmount = (row.taxableAmount == undefined || row.taxableAmount == null) ? 0 : row.taxableAmount;
                        var cgst = (row.cgst == undefined || row.cgst == null) ? 0 : row.cgst;
                        var sgst = (row.sgst == undefined || row.sgst == null) ? 0 : row.sgst;
                        var igst = (row.igst == undefined || row.igst == null) ? 0 : row.igst;
                        var totalAmount = (row.totalAmount == undefined || row.totalAmount == null) ? 0 : row.totalAmount;
                        var dueDate = (row.dueDate == undefined || row.dueDate == null) ? null : "'" + row.dueDate + " 00:00:00'";
                        var dueAmount = (row.dueAmount == undefined || row.dueAmount == null) ? 0 : row.dueAmount;
                        var timesheetInvoiceId = (row.timesheetInvoiceId == undefined || row.timesheetInvoiceId == null) ? '' : row.timesheetInvoiceId;

                        var gstState = gstNumber.substring(0, 2);
                        if (invoiceType == 'Regular') {
                            if (gstState == '29') {
                                cgst = Number(taxableAmount) * (9 / 100);
                                sgst = Number(taxableAmount) * (9 / 100);
                                igst = 0;
                                totalAmount = Number(taxableAmount) + cgst + sgst;
                            } else {
                                cgst = 0;
                                sgst = 0;
                                igst = Number(taxableAmount) * (18 / 100);
                                totalAmount = Number(taxableAmount) + igst;
                            }
                        } else {
                            cgst = 0;
                            sgst = 0;
                            igst = 0;
                            totalAmount = Number(taxableAmount);
                        }
                        var SQL = query.createInvoice;

                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('invoice_date_re', invoiceDate);
                        SQL = SQL.replace('gst_number_re', gstNumber);
                        SQL = SQL.replace('amount_re', taxableAmount);
                        SQL = SQL.replace('cgst_re', cgst);
                        SQL = SQL.replace('sgst_re', sgst);
                        SQL = SQL.replace('igst_re', igst);
                        SQL = SQL.replace('total_amount_re', totalAmount);
                        SQL = SQL.replace('invoice_number_re', invoiceNumber);
                        SQL = SQL.replace('invoice_type_re', invoiceType);
                        SQL = SQL.replace('duedate_re', dueDate);
                        SQL = SQL.replace('invoice_mode_re', invoiceMode);
                        SQL = SQL.replace('due_amount_re', dueAmount);
                        SQL = SQL.replace('updated_by_re', loginUserId);
                        SQL = SQL.replace('updated_at_re', timesNow);
                        SQL = SQL.replace('timesheet_invoice_id_re', timesheetInvoiceId);

                        console.log("\n > createInvoice SQL (" + count + ") ---> ", SQL);

                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > createInvoice SQL (" + count + ") err ---> ", err);
                            } else {
                                if (count == input.invoiceItems.length) {
                                    callback(null, true);
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.invoiceItems.length) {
                            callback(null, true);
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
}

exports.createTimesheetInvoice = function (req, res, next) {
    var params = req.body;
    if (params.actionType == "complete") {
        var getInvoiceSequence = query.getInvoiceSequence;
        db.query(getInvoiceSequence, function (err, results, fields) {
            console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);
            var financeYear = results[0].financeYear;
            var sequence = results[0].sequenceNo;
            params.invoiceNumber = 'PRIMUS' + financeYear + sequence;
            insertInvoice(params, function (err, message) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", message);
                    res.status(400).json({ status: 'failure', error: message });
                } else {
                    console.log("\n > createInvoice SQL ---> ", message);
                    updateInvoiceSequence();
                    res.status(200).json({ status: 'success', data: message });
                }
            });
        });
    } else if (params.actionType == "draft") {
        var draftNo = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
        params.invoiceNumber = params.invoiceNumber ? params.invoiceNumber : draftNo;
        console.log('\n > params ---> ', JSON.stringify(params));
        insertInvoice(params, function (err, message) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", message);
                res.status(400).json({ status: 'failure', error: message });
            } else {
                console.log("\n > createInvoice SQL ---> ", message);
                res.status(200).json({ status: 'success', data: message });
            }
        });
    }

    function insertInvoice(params, callback) {
        try {
            var actionType = removeString(params.actionType);
            var hrMasterId = removeString(params.hrMasterId);
            var invoiceId = removeString(params.invoiceId);
            var employeeMode = removeString(params.employeeMode);
            var billingDays = removeString(params.billingDays);
            var payableDays = removeString(params.payableDays);
            var timesheetInvoiceId = removeString(params.timesheetInvoiceId.toString());
            var invoiceNumber = removeString(params.invoiceNumber);
            var salaryDate = removeString(params.salaryDate);
            var gstNumber = removeString(params.gstNumber);
            var taxableAmount = removeString(params.taxableAmount);
            taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
            var cgst = removeString(params.cgst);
            var sgst = removeString(params.sgst);
            var igst = removeString(params.igst);
            var dueAmount = removeString(params.dueAmount);
            dueAmount = (dueAmount == null) ? 0 : dueAmount;
            var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
            var dueDate = removeString(params.dueDate);
            var totalAmount = removeString(params.totalAmount);
            var invoiceDate = removeString(params.invoiceDate);
            var invoiceType = removeString(params.invoiceType);
            invoiceType = invoiceType.toLowerCase();
            invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
            var invoiceMode = removeString(params.invoiceMode);
            var comments = removeString(params.comments);
            var remarks = removeString(params.remarks);
            var status = removeString(params.status);
            var loginUserId = removeString(params.loginUserId);

            var gstState = gstNumber.substring(0, 2);
            console.log('\n > invoiceType ---> ', invoiceType);
            console.log('\n > gstState ---> ', gstState);

            if (invoiceType == 'Regular') {
                if (gstState == 29) {
                    cgst = Number(taxableAmountCalc) * Number(9 / 100);
                    sgst = Number(taxableAmountCalc) * Number(9 / 100);
                    igst = Number(0);
                    totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(taxableAmountCalc) * Number(18 / 100);
                    totalAmount = Number(taxableAmountCalc) + Number(igst);
                }
            } else {
                cgst = Number(0);
                sgst = Number(0);
                igst = Number(0);
                totalAmount = Number(taxableAmountCalc);
            }

            if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                var SQL = query.createInvoice + query.updateTimesheetInvoiceSingle;
                var SQL2 = query.updateInvoiceId;
            } else {
                var SQL = query.updateInvoice + query.updateTimesheetInvoiceSingle;
                SQL = SQL.replace('invoice_id_re', removeString(invoiceId, true));
            }

            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
            SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
            SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
            SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
            SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
            SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
            SQL = SQL.replace('created_at_re', removeString(timesNow, true));
            SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
            SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

            SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
            SQL = SQL.replace('cgst_re', removeString(cgst, true));
            SQL = SQL.replace('sgst_re', removeString(sgst, true));
            SQL = SQL.replace('igst_re', removeString(igst, true));
            SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
            SQL = SQL.replace('duedate_re', removeString(dueDate, true));
            SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
            SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
            SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
            SQL = SQL.replace('commentsRe', removeString(comments, true));
            SQL = SQL.replace('remarksRe', removeString(remarks, true));
            SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));


            console.log("\n > createTimesheetInvoice SQL ---> ", SQL);
            db.query(SQL, function (err, results, fields) {
                if (err) {
                    console.log("\n > createTimesheetInvoice SQL err ---> ", err);
                    callback(true, err.sqlMessage);
                } else {
                    if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                        SQL2 = SQL2.replace('prms_invoice_id_re', removeString(results[0].insertId, true));
                        SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                        console.log("\n > createTimesheetInvoice SQL2 ---> ", SQL2);
                        db.query(SQL2, function () { });
                    }
                    callback(false, 'Invoice Number is ' + invoiceNumber);
                }
            });
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }

    function updateInvoiceSequence() {
        var updateInvoiceSequence = query.updateInvoiceSequence;
        console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
        db.query(updateInvoiceSequence, function (err, results, fields) {
            console.log("\n > updateInvoiceSequence SQL success ---> ");
        });
    }

    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }
}

exports.getInvoiceByNumber = function (req, res, next) {
    var params = req.params;
    if (params.invoiceNumber == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.invoiceNumber.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    } else {
        var invoiceNumber = params.invoiceNumber.toString().trim();

        var SQL = query.getInvoiceByNumber + query.getInvoiceItemsByNumber;
        SQL = SQL.replace(/invoice_number_re/g, invoiceNumber);

        console.log("\n > getInvoiceByNumber SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getInvoiceByNumber SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                let data = {
                    invoice: results[0][0],
                    invoiceItems: results[1]
                }
                res.status(200).json({ status: 'success', data: data });
            }
        });
    }
};

exports.searchMergeInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.clientName == undefined
        || params.costCenter == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var clientName = params.clientName.toString().trim();
        var costCenter = params.costCenter.map(x => "'" + x + "'").toString();
        var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
        var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
        var SQL = query.searchMergeInvoice;
        var WHERE = "";
        var lastMonth = '';
        var lastYear = '';
        if (month == 1) {
            lastMonth = 12;
            lastYear = year - 1;
        } else {
            lastMonth = month - 1;
            lastYear = year;
        }

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            }
            else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.employee_id IN (" + data + ")";
                // WHERE = "AND H.employee_id = '" + searchValue + "'";
            }
            else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "invoiceNumber") {
                WHERE = "AND I.invoice_number = '" + searchValue + "'";
            }
        }
        SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
        SQL = SQL.replace(/LASTYEAR/g, lastYear);
        SQL = SQL.replace('WHEREMONTH', month.toString().padStart(2, '0'));
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace('WHERECLIENTNAME', clientName);
        SQL = SQL.replace('WHERECOSTCENTER', costCenter);
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > searchMergeInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > searchMergeInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};


exports.createMergeInvoice = function (req, res, next) {
    var data = req.body;
    if (data.invoice.actionType == "complete") {
        var getInvoiceSequence = query.getInvoiceSequence;
        db.query(getInvoiceSequence, function (err, results, fields) {
            console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);

            var financeYear = results[0].financeYear;
            var sequence = results[0].sequenceNo;
            data.invoice.invoiceNumber = 'PRIMUS' + financeYear + sequence;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    updateInvoiceSequence(data.invoice.actionType);
                    res.status(200).json({ status: 'success', data: success });
                }
            });
        });
    } else if (data.invoice.actionType == "draft" && data.invoice.invoiceNumber == "") {
        data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: success });
            }
        });
    } else {
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: success });
            }
        });
    }
    // data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
    function updateInvoiceSequence(data) {
        var updateInvoiceSequence = query.updateInvoiceSequence;
        console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
        db.query(updateInvoiceSequence, function (err, results, fields) {
            console.log("\n > updateInvoiceSequence SQL success ---> ");
        });
    }

    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var actionType = removeString(com.actionType);
                    var taxableAmount = removeString(row.taxableAmount);

                    if (actionType == 'draft' || (actionType == "complete" && taxableAmount)) {

                        var hrMasterId = removeString(row.hrMasterId);
                        var invoiceId = removeString(com.invoiceId);
                        var employeeMode = removeString(row.employeeMode);
                        var billingDays = removeString(row.billingDays);
                        var payableDays = removeString(row.payableDays);
                        var timesheetInvoiceId = removeString(row.timesheetInvoiceId.toString());
                        var invoiceNumber = removeString(com.invoiceNumber);
                        var salaryDate = removeString(row.salaryDate);
                        var gstNumber = removeString(com.gstNumber);
                        var taxableAmount = removeString(row.taxableAmount);
                        taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                        var cgst = removeString(row.cgst);
                        var sgst = removeString(row.sgst);
                        var igst = removeString(row.igst);
                        var dueAmount = removeString(row.dueAmount);
                        dueAmount = (dueAmount == null) ? 0 : dueAmount;
                        var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                        var dueDate = removeString(row.dueDate);
                        var totalAmount = removeString(row.totalAmount);
                        var invoiceDate = removeString(com.invoiceDate);
                        var invoiceType = removeString(com.invoiceType);
                        invoiceType = invoiceType.toLowerCase();
                        invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                        var invoiceMode = removeString(com.invoiceMode);
                        var comments = removeString(row.comments);
                        var remarks = removeString(row.remarks);
                        var status = removeString(row.status);
                        var loginUserId = removeString(com.loginUserId);
                        var oldInvoiceNumber = removeString(com.oldInvoiceNumber);

                        var gstState = gstNumber.substring(0, 2);
                        if (invoiceType == 'Regular') {
                            if (gstState == '29') {
                                cgst = Number(taxableAmountCalc) * (9 / 100);
                                sgst = Number(taxableAmountCalc) * (9 / 100);
                                igst = 0;
                                totalAmount = Number(taxableAmountCalc) + cgst + sgst;
                            } else {
                                cgst = 0;
                                sgst = 0;
                                igst = Number(taxableAmountCalc) * (18 / 100);
                                totalAmount = Number(taxableAmountCalc) + igst;
                            }
                        } else {
                            cgst = 0;
                            sgst = 0;
                            igst = 0;
                            totalAmount = Number(taxableAmountCalc);
                        }
                        if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                            var SQL = query.createInvoice + query.updateTimesheetInvoice;
                            var SQL2 = query.updateInvoiceId;
                        } else if (row.selectRow == false) {
                            var SQL = query.deleteTimesheetInvoice;
                        } else {
                            var SQL = query.updateInvoice + query.updateTimesheetInvoice;
                        }

                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
                        SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
                        SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
                        SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
                        SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
                        SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                        SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                        SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
                        SQL = SQL.replace('cgst_re', removeString(cgst, true));
                        SQL = SQL.replace('sgst_re', removeString(sgst, true));
                        SQL = SQL.replace('igst_re', removeString(igst, true));
                        SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
                        SQL = SQL.replace('duedate_re', removeString(dueDate, true));
                        SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
                        SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                        SQL = SQL.replace('commentsRe', removeString(comments, true));
                        SQL = SQL.replace('remarksRe', removeString(remarks, true));
                        SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                        SQL = SQL.replace('prms_invoice_id_re', removeString(invoiceId, true));
                        SQL = SQL.replace('invoice_id_re', removeString(invoiceId, true));


                        console.log("\n > createInvoice SQL ---> ", SQL);
                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > createInvoice SQL err ---> ", err);
                                //callback(true, err.sqlMessage);
                            } else {
                                if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                                    input.invoice.invoiceId = results[0].insertId;
                                    SQL2 = SQL2.replace('prms_invoice_id_re', removeString(results[0].insertId, true));
                                    SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                                    db.query(SQL2, function () { });
                                }
                                if (count == input.invoiceItems.length) {
                                    callback(null, 'Invoice Number is ' + invoiceNumber);
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.invoiceItems.length) {
                            callback(null, 'Invoice Number is ' + invoiceNumber);
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
};

exports.getMergeInvoiceByNumber = function (req, res, next) {
    var params = req.params;
    if (params.invoiceNumber == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.invoiceNumber.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    } else {
        var invoiceNumber = params.invoiceNumber.toString().trim();

        var SQL = query.getMergeInvoiceByNumber;
        SQL = SQL.replace('invoice_number_re', invoiceNumber);

        console.log("\n > getInvoiceByNumber SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getInvoiceByNumber SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};


exports.importTimesheet = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < sheets.length; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            insertTimesheet(data, function (err, message) {
                                if (err) {
                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < sheets.length; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    insertTimesheet(data, function (err, message) {
                        if (err) {
                            console.log("\n > createTimesheet SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > createTimesheet SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function insertTimesheet(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var hrMasterId = removeString(row.PRMS_HR_MASTER_ID);
                    var timesheetMonth = removeString(row.TIMESHEET_MONTH);
                    var timesheetYear = removeString(row.TIMESHEET_YEAR);
                    var timesheetFrom = removeString(row.TIMESHEET_DATE_FROM) ? moment(removeString(row.TIMESHEET_DATE_FROM)).utc().tz("Asia/Kolkata").format("YYYY-MM-DD") : removeString(row.TIMESHEET_DATE_FROM);
                    var timesheetTo = removeString(row.TIMESHEET_DATE_TO) ? moment(removeString(row.TIMESHEET_DATE_TO)).utc().tz("Asia/Kolkata").format("YYYY-MM-DD") : removeString(row.TIMESHEET_DATE_TO);
                    var workingDays = removeString(row.WORKING_DAYS) ? removeString(row.WORKING_DAYS) : 0;
                    var billingDays = removeString(row.BILLING_DAYS) ? removeString(row.BILLING_DAYS) : 0;
                    var leaveWithoutPay = removeString(row.LEAVE_WITHOUT_PAY) ? removeString(row.LEAVE_WITHOUT_PAY) : 0;
                    var payableDays = removeString(row.PAYABLE_DAYS) ? removeString(row.PAYABLE_DAYS) : 0;
                    var lastMonthBalCl = removeString(row.PREVIOUS_BAL_CL) ? removeString(row.PREVIOUS_BAL_CL) : 0;
                    var lastMonthBalSl = removeString(row.PREVIOUS_BAL_SL) ? removeString(row.PREVIOUS_BAL_SL) : 0;
                    var takenLeaveCl = removeString(row.LEAVES_TAKEN_CL) ? removeString(row.LEAVES_TAKEN_CL) : 0;
                    var takenLeaveSl = removeString(row.LEAVES_TAKEN_SL) ? removeString(row.LEAVES_TAKEN_SL) : 0;
                    var eLeaveCl = removeString(row.E_LEAVES_CL) ? removeString(row.E_LEAVES_CL) : 0;
                    var eLeaveSl = removeString(row.E_LEAVES_SL) ? removeString(row.E_LEAVES_SL) : 0;
                    var balLeaveCl = removeString(row.LEAVE_BAL_CL) ? (removeString(row.LEAVE_BAL_CL) < 0 ? 0 : removeString(row.LEAVE_BAL_CL)): 0;
                    var balLeaveSl = removeString(row.LEAVE_BAL_SL) ? (removeString(row.LEAVE_BAL_SL) < 0 ? 0 : removeString(row.LEAVE_BAL_SL)): 0;
                    var salaryAmount = removeString(row.RESOURCE_SALARY) ? removeString(row.RESOURCE_SALARY) : 0;
                    // var casualLeave = (balLeaveCl < 0) ? balLeaveCl : 0;
                    // var sikLeave = (balLeaveSl < 0) ? balLeaveSl : 0;

                    var salaryPerDay = (workingDays == 0) ? 0 : salaryAmount / workingDays;
                    // payableDays = Number(payableDays) + Number(casualLeave);
                    // payableDays = Number(payableDays) + Number(sikLeave);

                    var resorceSalary = salaryAmount;
                    var amount = salaryPerDay * payableDays;
                    var totalAmount = amount;
                    var comments = removeString(row.COMMENTS) ? removeString(row.COMMENTS) : '';
                    var remarks = removeString(row.REMARKS) ? removeString(row.REMARKS) : '';
                    var status = removeString(row.STATUS) ? removeString(row.STATUS) : 1;
                    var salaryStatus = removeString(row.SALARY_STATUS) ? removeString(row.SALARY_STATUS) : 'Salary Hold';
                    var incentive = removeString(row.INCENTIVE) ? removeString(row.INCENTIVE) : 0;
                    var reimbursement = removeString(row.REIMBURSEMENT) ? removeString(row.REIMBURSEMENT) : 0;
                    var loginUserId = removeString(row.loginUserId) ? removeString(row.loginUserId) : 1;

                    var SQL = query.importTimesheet;
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace(/hrMasterId/g, removeString(hrMasterId, true));
                    SQL = SQL.replace('timesheetMonth', removeString(timesheetMonth.toString().padStart(2, '0'), true));
                    SQL = SQL.replace('timesheetYear', removeString(timesheetYear, true));
                    SQL = SQL.replace('timesheetFrom', removeString(timesheetFrom, true));
                    SQL = SQL.replace('timesheetTo', removeString(timesheetTo, true));
                    SQL = SQL.replace(/workingDaysRE/g, workingDays);
                    SQL = SQL.replace('workingDays', removeString(workingDays, true));
                    SQL = SQL.replace('billingDays', removeString(billingDays, true));
                    SQL = SQL.replace('leaveWithoutPay', removeString(leaveWithoutPay, true));
                    SQL = SQL.replace(/payableDaysRE/g, payableDays);
                    SQL = SQL.replace('payableDays', removeString(payableDays, true));
                    SQL = SQL.replace('takenLeaveCl', removeString(takenLeaveCl, true));
                    SQL = SQL.replace('takenLeaveSl', removeString(takenLeaveSl, true));
                    SQL = SQL.replace('eLeaveCl', removeString(eLeaveCl, true));
                    SQL = SQL.replace('eLeaveSl', removeString(eLeaveSl, true));
                    SQL = SQL.replace('balLeaveCl', removeString(balLeaveCl, true));
                    SQL = SQL.replace('balLeaveSl', removeString(balLeaveSl, true));
                    SQL = SQL.replace('commentsRe', removeString(comments, true));
                    SQL = SQL.replace('remarksRe', removeString(remarks, true));
                    SQL = SQL.replace('statusRe', removeString(status, true));
                    SQL = SQL.replace('salaryStatus', removeString(salaryStatus, true));
                    SQL = SQL.replace('incentiveRe', removeString(incentive, true));
                    SQL = SQL.replace('reimbursementRe', removeString(reimbursement, true));
                    SQL = SQL.replace('resourceSalRe', removeString(resorceSalary, true));
                    SQL = SQL.replace(/loginUserId/g, removeString(loginUserId, true));
                    SQL = SQL.replace(/timesNow/g, timesNow);
                    SQL = SQL.replace('amountRe', removeString(amount, true));
                    SQL = SQL.replace('totalAmountRe', removeString(totalAmount, true));

                    console.log("\n> insertTimesheet SQL (" + count + ") ---> ", SQL);

                    var takenLeave = Number(takenLeaveCl) + Number(takenLeaveSl);
                    var lastMonthBal = Number(lastMonthBalCl) + Number(lastMonthBalSl);
                    var balLeave = Number(balLeaveCl) + Number(balLeaveSl);
                    var ctcPerDay = (workingDays == 0) ? 0 : resorceSalary / workingDays;
                    var ctc = Number(ctcPerDay) * Number(payableDays);

                    var SQL2 = pquery.createPayroll;
                    SQL2 = SQL2.replace('hr_master_id_RE', removeString(hrMasterId, true));
                    SQL2 = SQL2.replace('payroll_from_RE', null);
                    SQL2 = SQL2.replace('payroll_to_RE', null);
                    SQL2 = SQL2.replace('payroll_month_RE', removeString(timesheetMonth, true));
                    SQL2 = SQL2.replace('payroll_year_RE', removeString(timesheetYear, true));
                    SQL2 = SQL2.replace('working_days_RE', removeString(workingDays, true));
                    SQL2 = SQL2.replace('payable_days_RE', removeString(payableDays, true));
                    SQL2 = SQL2.replace('leave_without_pay_RE', removeString(leaveWithoutPay, true));
                    SQL2 = SQL2.replace('taken_leave_RE', removeString(takenLeave, true));
                    SQL2 = SQL2.replace('previous_leave_balance_RE', removeString(lastMonthBal, true));
                    SQL2 = SQL2.replace('current_leave_balance_RE', removeString(balLeave, true));
                    SQL2 = SQL2.replace('incentive_allowance_RE', removeString(incentive, true));
                    SQL2 = SQL2.replace('reimbursement_RE', removeString(reimbursement, true));
                    SQL2 = SQL2.replace('resourceSalary_RE', removeString(resorceSalary, true));
                    SQL2 = SQL2.replace('ctc_RE', removeString(ctc, true));
                    SQL2 = SQL2.replace('comments_RE', removeString(comments, true));
                    SQL2 = SQL2.replace(/loginUserId/g, removeString(loginUserId, true));
                    SQL2 = SQL2.replace(/timeNow/g, timesNow);

                    var CSQL = pquery.checkPayrollsheetForImport;
                    CSQL = CSQL.replace('hr_master_id_RE', removeString(hrMasterId, true));
                    CSQL = CSQL.replace('payroll_month_RE', removeString(timesheetMonth.toString().padStart(2, '0'), true));
                    CSQL = CSQL.replace('payroll_year_RE', removeString(timesheetYear, true));


                    // var CSQL = query.checkTimesheetForImport;
                    // CSQL = CSQL.replace('hr_master_id_re', removeString(hrMasterId, true));
                    // CSQL = CSQL.replace('timesheet_month_re', removeString(timesheetMonth.toString().padStart(2, '0'), true));
                    // CSQL = CSQL.replace('timesheet_year_re', removeString(timesheetYear, true));

                    db.query(CSQL, function (cerr, cresults, fields) {
                        count++;
                        if (cerr) {
                            console.log("\n > insertTimesheet SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, 'Successfully Created');
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            if (cresults.length == 0) {
                                SQL += SQL2;
                            }
                            db.query(SQL, function (err, results, fields) {
                                if (err) {
                                    console.log("\n > insertTimesheet SQL (" + count + ") err ---> ", err);
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                    // callback(true, err.sqlMessage);
                                } else {
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                }
                            });
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.getPermInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.invoiceType == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } if (
        params.month == ''
        || params.year == ''
        || params.invoiceType == ''
    ) {
        res.status(400).json({ status: 'failure', error: 'Please send service month and year' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var serviceMonth = ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + month;
        var invoiceType = params.invoiceType.toString().trim();
        invoiceType = invoiceType == 'NEW' ? 'AND (I.invoice_id IS NULL OR I.invoice_number LIKE "%DRAFT%")' : 'AND I.invoice_id IS NOT NULL AND I.invoice_number NOT LIKE "%DRAFT%"';
        var SQL = query.getPermInvoice;
        SQL = SQL.replace('WHEREMONTH', month);
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace('SERVICEMONTH', serviceMonth);
        SQL = SQL.replace('INVOICE_TYPE_RE', invoiceType);

        console.log("\n > getPermInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getPermInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results.forEach(e => {
                    e.dueAmount = e.dueAmount ? e.dueAmount : 0;
                    e.taxableAmount = e.taxableAmount ? e.taxableAmount : 0;
                    e.cgst = e.cgst ? e.cgst : 0;
                    e.sgst = e.sgst ? e.sgst : 0;
                    e.igst = e.igst ? e.igst : 0;
                    e.totalAmount = e.totalAmount ? e.totalAmount : 0;
                    e.timesheetMonth = e.timesheetMonth ? e.timesheetMonth : month;
                    e.timesheetYear = e.timesheetYear ? e.timesheetYear : year;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.createPremInvoice = function (req, res, next) {
    var params = req.body;

    if (
        params.actionType === undefined
        || params.timesheetId === undefined
        || params.invoiceId === undefined
        || params.hrMasterId === undefined
        || params.invoiceDate === undefined
        || params.invoiceNumber === undefined
        || params.gstNumber === undefined
        || params.invoiceType === undefined
        || params.dueAmount === undefined
        || params.taxableAmount === undefined
        || params.comments === undefined
        || params.remarks === undefined
        || params.status === undefined
        || params.timesheetMonth === undefined
        || params.timesheetYear === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        // res.status(400).json({ status: 'success', data: 'Successfully Created' });
        if (params.actionType == "complete") {
            var getInvoiceSequence = query.getInvoiceSequence;
            db.query(getInvoiceSequence, function (err, results, fields) {
                console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);
                var financeYear = results[0].financeYear;
                var sequence = results[0].sequenceNo;
                params.invoiceNumber = 'PRIMUS' + financeYear + sequence;
                insertInvoice(params, function (err, message) {
                    if (err) {
                        console.log("\n > createInvoice SQL Err ---> ", message);
                        res.status(400).json({ status: 'failure', error: message });
                    } else {
                        console.log("\n > createInvoice SQL ---> ", message);
                        updateInvoiceSequence();
                        res.status(200).json({ status: 'success', data: message });
                    }
                });
            });
        } else if (params.actionType == "draft") {
            var draftNo = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
            params.invoiceNumber = params.invoiceNumber ? params.invoiceNumber : draftNo;
            console.log('\n > params ---> ', JSON.stringify(params));
            insertInvoice(params, function (err, message) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", message);
                    res.status(400).json({ status: 'failure', error: message });
                } else {
                    console.log("\n > createInvoice SQL ---> ", message);
                    res.status(200).json({ status: 'success', data: message });
                }
            });
        }

        function insertInvoice(params, callback) {
            try {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                var actionType = removeString(params.actionType);
                var timesheetId = removeString(params.timesheetId);
                var invoiceId = removeString(params.invoiceId);
                var hrMasterId = removeString(params.hrMasterId);
                var invoiceDate = removeString(params.invoiceDate);
                var invoiceNumber = removeString(params.invoiceNumber);
                var gstNumber = removeString(params.gstNumber);
                var invoiceType = removeString(params.invoiceType);
                invoiceType = invoiceType.toLowerCase();
                invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                var invoiceMode = removeString('Monthly');
                var comments = removeString(params.comments);
                var remarks = removeString(params.remarks);
                var status = removeString(params.status);
                var timesheetMonth = removeString(params.timesheetMonth);
                var timesheetYear = removeString(params.timesheetYear);
                var dueAmount = removeString(params.dueAmount);
                dueAmount = (dueAmount == null) ? 0 : dueAmount;
                var invoiceSources = removeString(params.invoiceSources);
                var invoiceCorrection = removeString(params.invoiceCorrection);
                var loginUserId = removeString(params.loginUserId);
                timesNow = removeString(timesNow);

                var cgst = 0;
                var sgst = 0;
                var igst = 0;
                var taxableAmount = removeString(params.taxableAmount);
                taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                var totalAmount = Number(taxableAmount);
                var gstState = gstNumber.substring(0, 2);

                console.log('\n > invoiceType ---> ', invoiceType);
                console.log('\n > gstState ---> ', gstState);

                if (invoiceType == 'Regular') {
                    if (gstState == 29) {
                        cgst = Number(taxableAmountCalc) * Number(9 / 100);
                        sgst = Number(taxableAmountCalc) * Number(9 / 100);
                        igst = Number(0);
                        totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(taxableAmountCalc) * Number(18 / 100);
                        totalAmount = Number(taxableAmountCalc) + Number(igst);
                    }
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(0);
                    totalAmount = Number(taxableAmountCalc);
                }

                // callback(false, 'Invoice Number is ' + invoiceNumber);
                var insertInvoice = "";
                var insertTimesheet = "";

                if (invoiceId && timesheetId) {
                    insertInvoice = "UPDATE `prms_invoice` SET `invoice_number` = invoice_number_re, `gst_number` = gst_number_re, `invoice_type` = invoice_type_re, `invoice_mode` = invoice_mode_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE `invoice_id` = invoice_id_re;";
                } else {
                    insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `invoice_sources`, `invoice_correction`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, invoice_sources_re, invoice_correction_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                }


                insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                insertInvoice = insertInvoice.replace('invoice_date_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                insertInvoice = insertInvoice.replace('invoice_sources_re', removeString(invoiceSources, true));
                insertInvoice = insertInvoice.replace('invoice_correction_re', removeString(invoiceCorrection, true));
                insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));

                console.log('\n > insertInvoice ---> ', insertInvoice);

                // callback(false, 'Invoice Number is ' + invoiceNumber);
                db.query(insertInvoice, function (err, results, fields) {
                    if (err) {
                        console.log("\n > createInvoice SQL err ---> ", err);
                        callback(true, err.sqlMessage);
                    } else if (invoiceId && timesheetId) {
                        console.log("\n > insertId ---> ", invoiceId);
                        insertTimesheet = "UPDATE `prms_timesheet_invoice` SET `amount` = amount_re, `cgst` = cgst_re, `sgst` = sgst_re, `igst` = igst_re, `total_amount` = total_amount_re, `due_amount` = due_amount_re, `comments` = comments_re, `remarks` = remarks_re, `status` = status_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE timesheet_invoice_id = timesheet_invoice_id_re;";
                        insertTimesheet = insertTimesheet.replace('timesheet_invoice_id_re', removeString(timesheetId, true));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                        insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                        insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                        insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                        insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                        insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                        insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                        insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                        insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));
                        if (taxableAmount != 0) {
                            insertTimesheet += query.updateBillingAmountPermanent;
                            insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        }
                        console.log('\n > insertTimesheet ---> ', insertTimesheet);
                        db.query(insertTimesheet, function () { });
                        callback(false, 'Invoice Number is ' + invoiceNumber);
                    } else {
                        console.log("\n > insertId ---> ", results.insertId);
                        insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                        insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(results.insertId));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                        insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                        insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                        insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                        insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                        insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                        insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                        insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                        insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                        insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                        insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));
                        if (taxableAmount != 0) {
                            insertTimesheet += query.updateBillingAmountPermanent;
                            insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        }
                        console.log('\n > insertTimesheet ---> ', insertTimesheet);
                        db.query(insertTimesheet, function () { });
                        callback(false, 'Invoice Number is ' + invoiceNumber);
                    }
                });
            } catch (e) {
                console.log(e);
                callback(true, e);
            }
        }

        function updateInvoiceSequence() {
            var updateInvoiceSequence = query.updateInvoiceSequence;
            console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
            db.query(updateInvoiceSequence, function (err, results, fields) {
                console.log("\n > updateInvoiceSequence SQL success ---> ");
            });
        }

        function removeString(data, type = null) {
            if (data == undefined || data == null || data == '') {
                return null;
            } else if (type) {
                return "'" + data + "'";
            } else {
                return data;
            }
        }
    }
};

exports.getReimburement = function (req, res, next) {
    var params = req.body;

    if (
        params.month == undefined
        || params.year == undefined
        || params.invoiceType == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } if (
        params.month == ''
        || params.year == ''
        || params.invoiceType == ''
    ) {
        res.status(400).json({ status: 'failure', error: 'Please send service month and year' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var serviceMonth = ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + month;
        var invoiceType = params.invoiceType.toString().trim();
        invoiceType = invoiceType == 'NEW' ? 'AND (R.reimbursement_invoice_id IS NULL OR R.invoice_number LIKE "%DRAFT%")' : 'AND R.reimbursement_invoice_id IS NOT NULL AND R.invoice_number NOT LIKE "%DRAFT%"';
        var SQL = query.getReimburement;

        SQL = SQL.replace('WHEREMONTH', month);
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace('INVOICE_TYPE_RE', invoiceType);


        console.log("\n > month ---> ", month);
        console.log("\n > year ---> ", year);
        console.log("\n > serviceMonth ---> ", serviceMonth);

        console.log("\n > getReimburement SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getReimburement SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results.forEach(e => {
                    e.taxableAmount = e.taxableAmount ? e.taxableAmount : 0;
                    e.cgst = e.cgst ? e.cgst : 0;
                    e.sgst = e.sgst ? e.sgst : 0;
                    e.igst = e.igst ? e.igst : 0;
                    e.totalAmount = e.totalAmount ? e.totalAmount : 0;
                    e.remMonth = e.remMonth ? e.remMonth : month;
                    e.remYear = e.remYear ? e.remYear : year;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};


exports.createReimburement = function (req, res, next) {
    var params = req.body;

    if (
        params.actionType === undefined
        || params.invoiceId === undefined
        || params.hrMasterId === undefined
        || params.invoiceDate === undefined
        || params.invoiceNumber === undefined
        || params.gstNumber === undefined
        || params.invoiceType === undefined
        || params.taxableAmount === undefined
        || params.comments === undefined
        || params.remarks === undefined
        || params.remMonth === undefined
        || params.remYear === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        // res.status(400).json({ status: 'success', data: 'Successfully Created' });
        if (params.actionType == "complete") {
            var getInvoiceSequence = query.getInvoiceSequence;
            db.query(getInvoiceSequence, function (err, results, fields) {
                console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);
                var financeYear = results[0].financeYear;
                var sequence = results[0].sequenceNo;
                params.invoiceNumber = 'PRIMUS' + financeYear + sequence;
                insertInvoice(params, function (err, message) {
                    if (err) {
                        console.log("\n > createInvoice SQL Err ---> ", message);
                        res.status(400).json({ status: 'failure', error: message });
                    } else {
                        console.log("\n > createInvoice SQL ---> ", message);
                        updateInvoiceSequence();
                        res.status(200).json({ status: 'success', data: message });
                    }
                });
            });
        } else if (params.actionType == "draft") {
            var draftNo = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
            params.invoiceNumber = params.invoiceNumber ? params.invoiceNumber : draftNo;
            console.log('\n > params ---> ', JSON.stringify(params));
            insertInvoice(params, function (err, message) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", message);
                    res.status(400).json({ status: 'failure', error: message });
                } else {
                    console.log("\n > createInvoice SQL ---> ", message);
                    res.status(200).json({ status: 'success', data: message });
                }
            });
        }

        function insertInvoice(params, callback) {
            try {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                var actionType = removeString(params.actionType);
                var invoiceId = removeString(params.invoiceId);
                var hrMasterId = removeString(params.hrMasterId);
                var invoiceNumber = removeString(params.invoiceNumber);
                var gstNumber = removeString(params.gstNumber);
                var invoiceType = removeString(params.invoiceType);
                invoiceType = invoiceType.toLowerCase();
                invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                var invoiceMode = removeString('Monthly');
                var comments = removeString(params.comments);
                var remarks = removeString(params.remarks);
                var remMonth = removeString(params.remMonth);
                var remYear = removeString(params.remYear);
                var loginUserId = removeString(params.loginUserId);
                timesNow = removeString(timesNow);
                var invoiceDate = timesNow;

                var cgst = 0;
                var sgst = 0;
                var igst = 0;
                var totalAmount = Number(taxableAmount);
                var taxableAmount = removeString(params.taxableAmount);
                var gstState = gstNumber.substring(0, 2);

                console.log('\n > invoiceType ---> ', invoiceType);
                console.log('\n > gstState ---> ', gstState);
                if (invoiceType == 'Regular') {
                    if (gstState == 29) {
                        cgst = Number(taxableAmount) * Number(9 / 100);
                        sgst = Number(taxableAmount) * Number(9 / 100);
                        igst = Number(0);
                        totalAmount = Number(taxableAmount) + Number(cgst) + Number(sgst);
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(taxableAmount) * Number(18 / 100);
                        totalAmount = Number(taxableAmount) + Number(igst);
                    }
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(0);
                    totalAmount = Number(taxableAmount);
                }
                // callback(false, 'Invoice Number is ' + invoiceNumber);
                var insertInvoice = "";
                if (invoiceId) {
                    insertInvoice = "UPDATE `prms_reimbursement_invoice` SET `gst_number` = gst_number_re, `amount` = amount_re, `cgst` = cgst_re, `sgst` = sgst_re, `igst` = igst_re, `total_amount` = total_amount_re, `invoice_number` = invoice_number_re, `comments` = comments_re, `remarks` = remarks_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re, `invoice_type` = invoice_type_re, `invoice_mode` = invoice_mode_re WHERE `reimbursement_invoice_id` = invoice_id_re;";
                } else {
                    insertInvoice = "INSERT INTO `prms_reimbursement_invoice` (`hr_master_id`, `rem_year`, `invoice_date`, `rem_month`, `gst_number`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `invoice_number`, `creditperiod`, `duedate`, `comments`, `remarks`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`, `invoice_type`, `invoice_mode`, `er_pr`) VALUES (hr_master_id_re, rem_year_re, invoice_date_re, rem_month_re, gst_number_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, invoice_number_re, creditperiod_re, duedate_re, comments_re, remarks_re, is_active_re, created_by_re, created_at_re, updated_by_re, updated_at_re, invoice_type_re, invoice_mode_re, er_pr_re);";
                }
                insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));
                insertInvoice = insertInvoice.replace('hr_master_id_re', removeString(hrMasterId, true));
                insertInvoice = insertInvoice.replace('rem_year_re', removeString(remYear, true));
                insertInvoice = insertInvoice.replace('invoice_date_re', removeString(invoiceDate, true));
                insertInvoice = insertInvoice.replace('rem_month_re', removeString(remMonth, true));
                insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                insertInvoice = insertInvoice.replace('amount_re', removeString(taxableAmount, true));
                insertInvoice = insertInvoice.replace('cgst_re', removeString(cgst, true));
                insertInvoice = insertInvoice.replace('sgst_re', removeString(sgst, true));
                insertInvoice = insertInvoice.replace('igst_re', removeString(igst, true));
                insertInvoice = insertInvoice.replace('total_amount_re', removeString(totalAmount, true));
                insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                insertInvoice = insertInvoice.replace('creditperiod_re', null);
                insertInvoice = insertInvoice.replace('duedate_re', null);
                insertInvoice = insertInvoice.replace('comments_re', removeString(comments, true));
                insertInvoice = insertInvoice.replace('remarks_re', removeString(remarks, true));
                insertInvoice = insertInvoice.replace('is_active_re', '1');
                insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                insertInvoice = insertInvoice.replace('er_pr_re', null);

                console.log('\n > insertInvoice ---> ', insertInvoice);

                // callback(false, 'Invoice Number is ' + invoiceNumber);
                if (insertInvoice) {
                    db.query(insertInvoice, function (err, results, fields) {
                        if (err) {
                            console.log("\n > createInvoice SQL err ---> ", err);
                            callback(true, err.sqlMessage);
                        } else {
                            callback(false, 'Invoice Number is ' + invoiceNumber);
                        }
                    });
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(0);
                    totalAmount = Number(taxableAmount);
                }
            } catch (e) {
                console.log(e);
                callback(true, e);
            }
        }

        function updateInvoiceSequence() {
            var updateInvoiceSequence = query.updateInvoiceSequence;
            console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
            db.query(updateInvoiceSequence, function (err, results, fields) {
                console.log("\n > updateInvoiceSequence SQL success ---> ");
            });
        }

        function removeString(data, type = null) {
            if (data == undefined || data == null || data == '') {
                return null;
            } else if (type) {
                return "'" + data + "'";
            } else {
                return data;
            }
        }
    }
};


exports.searchPermMergeInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.clientName == undefined
        || params.costCenter == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var serviceMonth = ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + month;
        var clientName = params.clientName.toString().trim();
        var costCenter = params.costCenter.toString().trim();
        var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
        var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
        var SQL = query.searchPermMergeInvoice;
        var WHERE = "";

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            }
            else if (searchBy == "empId") {
                WHERE = "AND H.employee_id = '" + searchValue + "'";
            }
            else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "invoiceNumber") {
                WHERE = "AND I.invoice_number = '" + searchValue + "'";
            }
        }

        SQL = SQL.replace('WHEREMONTH', month);
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace('SERVICEMONTH', serviceMonth);
        SQL = SQL.replace('WHERECLIENTNAME', clientName);
        SQL = SQL.replace('WHERECOSTCENTER', costCenter);
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > searchPermMergeInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > searchPermMergeInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results.forEach(e => {
                    e.dueAmount = e.dueAmount ? e.dueAmount : 0;
                    e.taxableAmount = e.taxableAmount ? e.taxableAmount : 0;
                    e.cgst = e.cgst ? e.cgst : 0;
                    e.sgst = e.sgst ? e.sgst : 0;
                    e.igst = e.igst ? e.igst : 0;
                    e.totalAmount = e.totalAmount ? e.totalAmount : 0;
                    e.timesheetMonth = e.timesheetMonth ? e.timesheetMonth : month;
                    e.timesheetYear = e.timesheetYear ? e.timesheetYear : year;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.createPermMergeInvoice = function (req, res, next) {
    var data = req.body;
    if (data.invoice.actionType == "complete") {
        var getInvoiceSequence = query.getInvoiceSequence;
        db.query(getInvoiceSequence, function (err, results, fields) {
            console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);

            var financeYear = results[0].financeYear;
            var sequence = results[0].sequenceNo;
            data.invoice.invoiceNumber = 'PRIMUS' + financeYear + sequence;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    updateInvoiceSequence(data.invoice.actionType);
                    res.status(200).json({ status: 'success', data: 'Created Successfully' });
                }
            });
        });
    } else if (data.invoice.actionType == "draft" && data.invoice.invoiceNumber == "") {
        data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    } else {
        insertInvoice(data, function (err, success) {
            if (err) {
                console.log("\n > createInvoice SQL Err ---> ", err);
            }
            else {
                console.log("\n > createInvoice SQL success ---> ", success);
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    }
    // data.invoice.invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
    function updateInvoiceSequence(data) {
        var updateInvoiceSequence = query.updateInvoiceSequence;
        console.log("\n > updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
        db.query(updateInvoiceSequence, function (err, results, fields) {
            console.log("\n > updateInvoiceSequence SQL success ---> ");
        });
    }

    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var actionType = removeString(com.actionType);
                    var taxableAmount = removeString(row.taxableAmount);

                    if (actionType == 'draft' || (actionType == "complete" && taxableAmount)) {

                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                        var timesheetId = removeString(row.timesheetId);
                        var invoiceId = removeString(com.invoiceId);
                        var hrMasterId = removeString(row.hrMasterId);
                        var invoiceDate = removeString(com.invoiceDate);
                        var invoiceNumber = removeString(com.invoiceNumber);
                        var gstNumber = removeString(com.gstNumber);
                        var invoiceType = removeString(com.invoiceType);
                        invoiceType = invoiceType.toLowerCase();
                        invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                        var invoiceMode = removeString('Monthly');
                        var comments = removeString(row.comments);
                        var remarks = removeString(row.remarks);
                        var status = removeString(row.status);
                        var timesheetMonth = removeString(row.timesheetMonth);
                        var timesheetYear = removeString(row.timesheetYear);
                        var dueAmount = removeString(row.dueAmount);
                        dueAmount = (dueAmount == null) ? 0 : dueAmount;
                        var invoiceSources = removeString(row.invoiceSources);
                        var invoiceCorrection = removeString(row.invoiceCorrection);
                        var loginUserId = removeString(com.loginUserId);
                        timesNow = removeString(timesNow);

                        var cgst = 0;
                        var sgst = 0;
                        var igst = 0;
                        var taxableAmount = removeString(row.taxableAmount);
                        taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                        var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                        var totalAmount = Number(taxableAmount);
                        var gstState = gstNumber.substring(0, 2);

                        console.log('\n > invoiceType[' + count + '] ---> ', invoiceType);
                        console.log('\n > gstState[' + count + '] ---> ', gstState);

                        if (invoiceType == 'Regular') {
                            if (gstState == 29) {
                                cgst = Number(taxableAmountCalc) * Number(9 / 100);
                                sgst = Number(taxableAmountCalc) * Number(9 / 100);
                                igst = Number(0);
                                totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                            } else {
                                cgst = Number(0);
                                sgst = Number(0);
                                igst = Number(taxableAmountCalc) * Number(18 / 100);
                                totalAmount = Number(taxableAmountCalc) + Number(igst);
                            }
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(0);
                            totalAmount = Number(taxableAmountCalc);
                        }

                        // callback(false, 'Invoice Number is ' + invoiceNumber);
                        var insertInvoice = "";
                        var insertTimesheet = "";

                        if (row.selectRow == false) {
                            insertInvoice = query.deletePermInvoice;
                            insertInvoice = insertInvoice.replace('timesheet_invoice_id_re', removeString(timesheetId, true));
                        }
                        else if (invoiceId) {
                            insertInvoice = "UPDATE `prms_invoice` SET `invoice_number` = invoice_number_re, `gst_number` = gst_number_re, `invoice_type` = invoice_type_re, `invoice_mode` = invoice_mode_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE `invoice_id` = invoice_id_re;";
                        }
                        else {
                            insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `invoice_sources`, `invoice_correction`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, invoice_sources_re, invoice_correction_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                        }


                        insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                        insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                        insertInvoice = insertInvoice.replace('invoice_date_re', removeString(timesNow, true));
                        insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                        insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                        insertInvoice = insertInvoice.replace('invoice_sources_re', removeString(invoiceSources, true));
                        insertInvoice = insertInvoice.replace('invoice_correction_re', removeString(invoiceCorrection, true));
                        insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                        insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                        insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                        insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                        insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));

                        console.log('\n > insertInvoice[' + count + '] ---> ', insertInvoice);

                        // callback(false, 'Invoice Number is ' + invoiceNumber);
                        db.query(insertInvoice, function (err, results, fields) {
                            if (err) {
                                console.log("\n > createInvoice SQL err ---> ", err);
                                //callback(true, err.sqlMessage);
                                count++;
                                if (count == input.invoiceItems.length) {
                                    callback(null, true);
                                } else {
                                    insertRow(input);
                                }
                            } else if (row.selectRow == false) {
                                count++;
                                if (count == input.invoiceItems.length) {
                                    callback(null, true);
                                } else {
                                    insertRow(input);
                                }
                            } else if (invoiceId && timesheetId) {
                                console.log("\n > insertId ---> ", invoiceId);
                                insertTimesheet = "UPDATE `prms_timesheet_invoice` SET `amount` = amount_re, `cgst` = cgst_re, `sgst` = sgst_re, `igst` = igst_re, `total_amount` = total_amount_re, `due_amount` = due_amount_re, `comments` = comments_re, `remarks` = remarks_re, `status` = status_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE timesheet_invoice_id = timesheet_invoice_id_re;";
                                insertTimesheet = insertTimesheet.replace('timesheet_invoice_id_re', removeString(timesheetId, true));
                                insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                                insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                                insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                                insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                                insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                                insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                                insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                                insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                                insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                                insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                                insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                                insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));
                                if (taxableAmount != 0) {
                                    insertTimesheet += query.updateBillingAmountPermanent;
                                    insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                                    insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                                }
                                console.log('\n > insertTimesheet ---> ', insertTimesheet);
                                db.query(insertTimesheet, function () { });
                                count++;
                                if (count == input.invoiceItems.length) {
                                    callback(null, 'Invoice Number is ' + invoiceNumber);
                                } else {
                                    insertRow(input);
                                }

                                //callback(false, 'Invoice Number is ' + invoiceNumber);
                            } else {
                                console.log("\n > insertId ---> ", results.insertId);
                                if (invoiceId) {
                                    results.insertId = invoiceId;
                                } else {
                                    input.invoice.invoiceId = results.insertId;
                                }
                                insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                                insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(results.insertId));
                                insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                                insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                                insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                                insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                                insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                                insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                                insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                                insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                                insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                                insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                                insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                                insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                                insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                                insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                                insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                                insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));
                                if (taxableAmount != 0) {
                                    insertTimesheet += query.updateBillingAmountPermanent;
                                    insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                                    insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                                }
                                console.log('\n > insertTimesheet[' + count + '] ---> ', insertTimesheet);
                                db.query(insertTimesheet, function () { });
                                count++;
                                if (count == input.invoiceItems.length) {
                                    callback(null, true);
                                } else {
                                    insertRow(input);
                                }
                                //callback(false, 'Invoice Number is ' + invoiceNumber);
                            }
                        });
                    } else {
                        count++;
                        if (count == input.invoiceItems.length) {
                            callback(null, true);
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
};

exports.getPermMergeInvoiceByNumber = function (req, res, next) {
    var params = req.params;
    if (params.invoiceNumber == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.invoiceNumber.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    } else {
        var invoiceNumber = params.invoiceNumber.toString().trim();

        var SQL = query.getPermMergeInvoiceByNumber;
        SQL = SQL.replace('invoice_number_re', invoiceNumber);

        console.log("\n > getPermMergeInvoiceByNumber SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getPermMergeInvoiceByNumber SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.importInvoice = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < sheets.length; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            if (jsonData[0].mergeInvoice.toUpperCase() == 'YES' && jsonData[0].assignInvoice.toUpperCase() == 'NO') {
                                var invoice = {
                                    "invoiceNumber": 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210),
                                    "invoiceId": jsonData[0].invoiceId,
                                    "gstNumber": jsonData[0].gstNumber,
                                    "invoiceDate": jsonData[0].invoiceDate,
                                    "invoiceType": jsonData[0].invoiceType,
                                    "invoiceMode": jsonData[0].invoiceMode,
                                    "loginUserId": jsonData[0].loginUserId
                                };
                                var data = {
                                    "invoice": invoice,
                                    "invoiceItems": jsonData
                                }

                                insertMergeInvoice(data, function (err, message) {
                                    if (err) {
                                        console.log("\n > createTimesheet SQL Err ---> ", message);
                                        res.status(400).json({ status: 'failure', error: message });
                                    } else {
                                        console.log("\n > createTimesheet SQL success ---> ", message);
                                        res.status(200).json({ status: 'success', data: message });
                                    }
                                });

                            } else if (jsonData[0].mergeInvoice.toUpperCase() == 'NO' && jsonData[0].assignInvoice.toUpperCase() == 'YES') {                               
                                var data = {                                    
                                    "invoiceItems": jsonData
                                }                            
                                insertAssignInvoice(data, function (err, message) {
                                    if (err) {
                                        console.log("\n > createTimesheet SQL Err ---> ", message);
                                        res.status(400).json({ status: 'failure', error: message });
                                    } else {
                                        console.log("\n > createTimesheet SQL success ---> ", message);
                                        res.status(200).json({ status: 'success', data: message });
                                    }
                                });                                       
                            } else if (jsonData[0].mergeInvoice.toUpperCase() == 'NO' && jsonData[0].assignInvoice.toUpperCase() == 'NO') {
                                var data = jsonData;

                                insertInvoice(data, function (err, message) {
                                    if (err) {
                                        console.log("\n > createTimesheet SQL Err ---> ", message);
                                        res.status(400).json({ status: 'failure', error: message });
                                    } else {
                                        console.log("\n > createTimesheet SQL success ---> ", message);
                                        res.status(200).json({ status: 'success', data: message });
                                    }
                                });
                            }

                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < sheets.length; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    if (jsonData[0].mergeInvoice.toUpperCase() == 'YES' && jsonData[0].assignInvoice.toUpperCase() == 'NO') {
                        var invoice = {
                            "invoiceNumber": 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210),
                            "invoiceId": jsonData[0].invoiceId,
                            "gstNumber": jsonData[0].gstNumber,
                            "invoiceDate": jsonData[0].invoiceDate,
                            "invoiceType": jsonData[0].invoiceType,
                            "invoiceMode": jsonData[0].invoiceMode,
                            "loginUserId": jsonData[0].loginUserId
                        };
                        var data = {
                            "invoice": invoice,
                            "invoiceItems": jsonData
                        }

                        insertMergeInvoice(data, function (err, message) {
                            if (err) {
                                console.log("\n > createTimesheet SQL Err ---> ", message);
                                res.status(400).json({ status: 'failure', error: message });
                            } else {
                                console.log("\n > createTimesheet SQL success ---> ", message);
                                res.status(200).json({ status: 'success', data: message });
                            }
                        });

                    } else if (jsonData[0].mergeInvoice.toUpperCase() == 'NO' && jsonData[0].assignInvoice.toUpperCase() == 'YES') {                        
                        var data = {                            
                            "invoiceItems": jsonData
                        }
                        insertAssignInvoice(data, function (err, message) {
                            if (err) {
                                console.log("\n > createTimesheet SQL Err ---> ", message);
                                res.status(400).json({ status: 'failure', error: message });
                            } else {
                                console.log("\n > createTimesheet SQL success ---> ", message);
                                res.status(200).json({ status: 'success', data: message });
                            }
                        });
                    } else if (jsonData[0].mergeInvoice.toUpperCase() == 'NO' && jsonData[0].assignInvoice.toUpperCase() == 'NO') {
                        var data = jsonData;

                        insertInvoice(data, function (err, message) {
                            if (err) {
                                console.log("\n > createTimesheet SQL Err ---> ", message);
                                res.status(400).json({ status: 'failure', error: message });
                            } else {
                                console.log("\n > createTimesheet SQL success ---> ", message);
                                res.status(200).json({ status: 'success', data: message });
                            }
                        });
                    }
                }
            });
        }
    });

    function insertMergeInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var taxableAmount = removeString(row.taxableAmount);

                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceId = removeString(com.invoiceId);
                    var employeeMode = removeString(row.employeeMode);
                    var billingDays = removeString(row.billingDays);
                    var payableDays = removeString(row.payableDays);
                    var timesheetInvoiceId = removeString(row.timesheetInvoiceId.toString());
                    var invoiceNumber = removeString(com.invoiceNumber);
                    var salaryDate = removeString(row.salaryDate);
                    var gstNumber = removeString(com.gstNumber);
                    var taxableAmount = removeString(row.taxableAmount);
                    taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                    var cgst = removeString(row.cgst);
                    var sgst = removeString(row.sgst);
                    var igst = removeString(row.igst);
                    var dueAmount = removeString(row.dueAmount);
                    dueAmount = (dueAmount == null) ? 0 : dueAmount;
                    var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                    var dueDate = removeString(row.dueDate);
                    var totalAmount = removeString(row.totalAmount);
                    var invoiceDate = removeString(com.invoiceDate);
                    var invoiceType = removeString(com.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString(com.invoiceMode);
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status);
                    var loginUserId = removeString(com.loginUserId);

                    var gstState = gstNumber.substring(0, 2);
                    if (invoiceType == 'Regular') {
                        if (gstState == '29') {
                            cgst = Number(taxableAmountCalc) * (9 / 100);
                            sgst = Number(taxableAmountCalc) * (9 / 100);
                            igst = 0;
                            totalAmount = Number(taxableAmountCalc) + cgst + sgst;
                        } else {
                            cgst = 0;
                            sgst = 0;
                            igst = Number(taxableAmountCalc) * (18 / 100);
                            totalAmount = Number(taxableAmountCalc) + igst;
                        }
                    } else {
                        cgst = 0;
                        sgst = 0;
                        igst = 0;
                        totalAmount = Number(taxableAmountCalc);
                    }
                    if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                        var SQL = query.createInvoice + query.updateTimesheetInvoice;
                        var SQL2 = query.updateInvoiceId;
                    } else {
                        var SQL = query.updateInvoice + query.updateTimesheetInvoice;
                    }

                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
                    SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
                    SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
                    SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
                    SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
                    SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                    SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
                    SQL = SQL.replace('cgst_re', removeString(cgst, true));
                    SQL = SQL.replace('sgst_re', removeString(sgst, true));
                    SQL = SQL.replace('igst_re', removeString(igst, true));
                    SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
                    SQL = SQL.replace('duedate_re', removeString(dueDate, true));
                    SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('commentsRe', removeString(comments, true));
                    SQL = SQL.replace('remarksRe', removeString(remarks, true));
                    SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                    SQL = SQL.replace('prms_invoice_id_re', removeString(invoiceId, true));
                    SQL = SQL.replace('invoice_id_re', removeString(invoiceId, true));


                    var getTimesheetByNumber = query.getTimesheetByNumber;
                    getTimesheetByNumber = getTimesheetByNumber.replace('timesheet_invoice_id_re', timesheetInvoiceId);

                    db.query(getTimesheetByNumber, function (err1, results1, fields1) {
                        if (err1) {
                            console.log("\n > getTimesheetByNumber SQL err ---> ", err1);
                            //callback(true, err.sqlMessage);
                        } else {
                            if (results1.length > 0 && results1[0].prms_invoice_id == null) {
                                console.log("\n > createInvoice SQL ---> ", SQL);
                                db.query(SQL, function (err, results, fields) {
                                    count++;
                                    if (err) {
                                        console.log("\n > createInvoice SQL err ---> ", err);
                                        //callback(true, err.sqlMessage);
                                    } else {
                                        if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                                            input.invoice.invoiceId = results[0].insertId;
                                            SQL2 = SQL2.replace('prms_invoice_id_re', removeString(results[0].insertId, true));
                                            SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                                            db.query(SQL2, function () { });
                                        }
                                        if (count == input.invoiceItems.length) {
                                            var rMsg='';
                                            if(alreadyInvoice.length>0){
                                                rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                            } 
                                            callback(null, rMsg);
                                        } else {
                                            insertRow(input);
                                        }
                                    }
                                });
                            } else {
                                alreadyInvoice.push(results1[0].pgt_code);
                                count++;
                                if (count == input.invoiceItems.length) {
                                    var rMsg='';
                                    if(alreadyInvoice.length>0){
                                        rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                    } 
                                    callback(null, rMsg);
                                } else {
                                    insertRow(input);
                                }
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function insertAssignInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var invalidInvoiceNumber =[];
                var insertRow = function (input) {                    
                    var row = input.invoiceItems[count];

                    var getInvoiceDetails = query.getInvoiceDetailsByNumber;
                    getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', row.invoiceNumber);
                   
                    db.query(getInvoiceDetails, function (err, results, fields) {                       
                        if (results.length > 0 && results[0].invoice_number.includes("PRIMUS") && results[0].client_name == row.clientName) {
                                                        
                            var hrMasterId = removeString(row.hrMasterId);
                            var invoiceId = removeString(results[0].invoice_id);
                            var employeeMode = removeString(row.employeeMode);
                            var billingDays = removeString(row.billingDays);
                            var payableDays = removeString(row.payableDays);
                            var timesheetInvoiceId = removeString(row.timesheetInvoiceId.toString());
                            var invoiceNumber = removeString(row.invoiceNumber);
                            var salaryDate = removeString(row.salaryDate);
                            var gstNumber = removeString(results[0].gst_number);
                            var taxableAmount = removeString(row.taxableAmount);
                            taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                            var cgst = removeString(row.cgst);
                            var sgst = removeString(row.sgst);
                            var igst = removeString(row.igst);
                            var dueAmount = removeString(row.dueAmount);
                            dueAmount = (dueAmount == null) ? 0 : dueAmount;
                            var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                            var dueDate = removeString(row.dueDate);
                            var totalAmount = removeString(row.totalAmount);
                            var invoiceDate = removeString(results[0].invoice_date);
                            var invoiceType = removeString(results[0].invoice_type);
                            invoiceType = invoiceType.toLowerCase();
                            invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                            var invoiceMode = removeString(results[0].invoice_mode);
                            var comments = removeString(row.comments);
                            var remarks = removeString(row.remarks);
                            var status = removeString(row.status);
                            var loginUserId = removeString(row.loginUserId);
                        
                            var gstState = gstNumber.substring(0, 2);
                            if (invoiceType == 'Regular') {
                                if (gstState == '29') {
                                    cgst = Number(taxableAmountCalc) * (9 / 100);
                                    sgst = Number(taxableAmountCalc) * (9 / 100);
                                    igst = 0;
                                    totalAmount = Number(taxableAmountCalc) + cgst + sgst;
                                } else {
                                    cgst = 0;
                                    sgst = 0;
                                    igst = Number(taxableAmountCalc) * (18 / 100);
                                    totalAmount = Number(taxableAmountCalc) + igst;
                                }
                            } else {
                                cgst = 0;
                                sgst = 0;
                                igst = 0;
                                totalAmount = Number(taxableAmountCalc);
                            }
                            
                            var SQL = query.updateTimesheetInvoiceSingle;
                            var SQL2 = query.updateInvoiceId;
                            
                            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                            SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
                            SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
                            SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
                            SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
                            SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
                            SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                            SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                            SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                            SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                            SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
                            SQL = SQL.replace('cgst_re', removeString(cgst, true));
                            SQL = SQL.replace('sgst_re', removeString(sgst, true));
                            SQL = SQL.replace('igst_re', removeString(igst, true));
                            SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
                            SQL = SQL.replace('duedate_re', removeString(dueDate, true));
                            SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
                            SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                            SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                            SQL = SQL.replace('commentsRe', removeString(comments, true));
                            SQL = SQL.replace('remarksRe', removeString(remarks, true));
                            SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                            SQL = SQL.replace('invoice_id_re', removeString(invoiceId, true));


                            console.log("\n > createInvoice SQL ---> ", SQL);

                            var getTimesheetByNumber = query.getTimesheetByNumber;
                            getTimesheetByNumber = getTimesheetByNumber.replace('timesheet_invoice_id_re', timesheetInvoiceId);

                            db.query(getTimesheetByNumber, function (err1, results1, fields1) {
                                if (err1) {
                                    console.log("\n > getTimesheetByNumber SQL err ---> ", err1);
                                    //callback(true, err.sqlMessage);
                                } else {
                                    if (results1.length > 0 && results1[0].prms_invoice_id == null) {
                                        db.query(SQL, function (err, results, fields) {
                                            count++;
                                            if (err) {
                                                console.log("\n > createInvoice SQL err ---> ", err);
                                                //callback(true, err.sqlMessage);
                                            } else {

                                                SQL2 = SQL2.replace('prms_invoice_id_re', removeString(invoiceId, true));
                                                SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                                                db.query(SQL2, function () { });
                                                if (count == input.invoiceItems.length) {
                                                    var rMsg='';
                                                    if(invalidInvoiceNumber.length>0){
                                                        rMsg+='Following Invoice Number(s) are Invalid \n'+invalidInvoiceNumber.toString()+'\n';
                                                    } 
                    
                                                    if(alreadyInvoice.length>0){
                                                        rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                                    } 
                    
                                                    callback(null, rMsg);
                                                } else {
                                                    insertRow(input);
                                                }
                                            }
                                        });
                                    } else {
                                        alreadyInvoice.push(results1[0].pgt_code);
                                        count++;
                                        if (count == input.invoiceItems.length) {
                                            var rMsg='';
                                            if(invalidInvoiceNumber.length>0){
                                                rMsg+='Following Invoice Number(s) are Invalid \n'+invalidInvoiceNumber.toString()+'\n';
                                            } 
            
                                            if(alreadyInvoice.length>0){
                                                rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                            } 
            
                                            callback(null, rMsg);
                                        } else {
                                            insertRow(input);
                                        }
                                    }
                                }
                            });
                                
                        } else {
                            count++;
                            if(!invalidInvoiceNumber.includes(row.invoiceNumber)){
                                invalidInvoiceNumber.push(row.invoiceNumber);
                            }
                            if (count == input.invoiceItems.length) {
                                var rMsg='';
                                if(invalidInvoiceNumber.length>0){
                                    rMsg+='Following Invoice Number(s) are Invalid \n'+invalidInvoiceNumber.toString()+'\n';
                                } 

                                if(alreadyInvoice.length>0){
                                    rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                } 

                                callback(null, rMsg);
                            } else {
                                insertRow(input);
                            }
                        }
                    });                    

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var insertRow = function (input) {
                    input[count].invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
                    input[count].loginUserId = '1';
                    var row = input[count];

                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceId = removeString(row.invoiceId);
                    var employeeMode = removeString(row.employeeMode);
                    var billingDays = removeString(row.billingDays);
                    var payableDays = removeString(row.payableDays);
                    var timesheetInvoiceId = removeString(row.timesheetInvoiceId.toString());
                    var invoiceNumber = removeString(row.invoiceNumber);
                    var salaryDate = removeString(row.salaryDate);
                    var gstNumber = removeString(row.gstNumber);
                    var taxableAmount = removeString(row.taxableAmount);
                    taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                    var cgst = removeString(row.cgst);
                    var sgst = removeString(row.sgst);
                    var igst = removeString(row.igst);
                    var dueAmount = removeString(row.dueAmount);
                    dueAmount = (dueAmount == null) ? 0 : dueAmount;
                    var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                    var dueDate = removeString(row.dueDate);
                    var totalAmount = removeString(row.totalAmount);
                    var invoiceDate = removeString(row.invoiceDate);
                    var invoiceType = removeString(row.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString(row.invoiceMode);
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status);
                    var loginUserId = removeString(row.loginUserId);

                    var gstState = gstNumber.substring(0, 2);
                    if (invoiceType == 'Regular') {
                        if (gstState == '29') {
                            cgst = Number(taxableAmountCalc) * (9 / 100);
                            sgst = Number(taxableAmountCalc) * (9 / 100);
                            igst = 0;
                            totalAmount = Number(taxableAmountCalc) + cgst + sgst;
                        } else {
                            cgst = 0;
                            sgst = 0;
                            igst = Number(taxableAmountCalc) * (18 / 100);
                            totalAmount = Number(taxableAmountCalc) + igst;
                        }
                    } else {
                        cgst = 0;
                        sgst = 0;
                        igst = 0;
                        totalAmount = Number(taxableAmountCalc);
                    }
                    if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                        var SQL = query.createInvoice + query.updateTimesheetInvoice;
                        var SQL2 = query.updateInvoiceId;
                    } else {
                        var SQL = query.updateInvoice + query.updateTimesheetInvoice;
                    }

                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
                    SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
                    SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
                    SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
                    SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
                    SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                    SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
                    SQL = SQL.replace('cgst_re', removeString(cgst, true));
                    SQL = SQL.replace('sgst_re', removeString(sgst, true));
                    SQL = SQL.replace('igst_re', removeString(igst, true));
                    SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
                    SQL = SQL.replace('duedate_re', removeString(dueDate, true));
                    SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                    SQL = SQL.replace('commentsRe', removeString(comments, true));
                    SQL = SQL.replace('remarksRe', removeString(remarks, true));
                    SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                    SQL = SQL.replace('prms_invoice_id_re', removeString(invoiceId, true));

                    var getTimesheetByNumber = query.getTimesheetByNumber;
                    getTimesheetByNumber = getTimesheetByNumber.replace('timesheet_invoice_id_re', timesheetInvoiceId);

                    db.query(getTimesheetByNumber, function (err1, results1, fields1) {
                        if (err1) {
                            console.log("\n > getTimesheetByNumber SQL err ---> ", err1);
                            //callback(true, err.sqlMessage);
                        } else {
                            if (results1.length > 0 && results1[0].prms_invoice_id == null) {
                                console.log("\n > createInvoice SQL ---> ", SQL);
                                db.query(SQL, function (err, results, fields) {
                                    count++;
                                    if (err) {
                                        console.log("\n > createInvoice SQL err ---> ", err);
                                        //callback(true, err.sqlMessage);
                                    } else {
                                        if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                                            SQL2 = SQL2.replace('prms_invoice_id_re', removeString(results[0].insertId, true));
                                            SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                                            db.query(SQL2, function () { });
                                        }
                                        if (count == input.length) {
                                            var rMsg='';
                                            if(alreadyInvoice.length>0){
                                                rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                            } 
                                            callback(null, rMsg);
                                        } else {
                                            insertRow(input);
                                        }
                                    }
                                });
                            } else {
                                alreadyInvoice.push(results1[0].pgt_code);
                                count++;
                                if (count == input.length) {
                                    var rMsg='';
                                    if(alreadyInvoice.length>0){
                                        rMsg+='Following PGTCODE Already Have Invoice Number \n'+alreadyInvoice.toString();
                                    } 
                                    callback(null, rMsg);
                                } else {
                                    insertRow(input);
                                }
                            }
                        }
                    });
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.createAssignInvoice = function (req, res, next) {
    var data = req.body;
    var getInvoiceDetails = query.getInvoiceDetailsByNumber;
    getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', data.invoice.invoiceNumber);
    db.query(getInvoiceDetails, function (err, results, fields) {
        if (results.length > 0 && results[0].invoice_number.includes("PRIMUS")) {
            data.invoice.gstNumber = results[0].gst_number;
            data.invoice.invoiceDate = results[0].invoice_date;
            data.invoice.invoiceType = results[0].invoice_type;
            data.invoice.invoiceMode = results[0].invoice_mode;
            data.invoice.invoiceId = results[0].invoice_id;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    res.status(200).json({ status: 'success', data: success });
                }
            });
        } else {
            res.status(400).json({ status: 'failure', error: 'Enter Valid Invoice Number' });
        }
    });


    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var actionType = removeString(com.actionType);
                    var taxableAmount = removeString(row.taxableAmount);

                    if (actionType == 'draft' || (actionType == "complete" && taxableAmount)) {

                        var hrMasterId = removeString(row.hrMasterId);
                        var invoiceId = removeString(com.invoiceId);
                        var employeeMode = removeString(row.employeeMode);
                        var billingDays = removeString(row.billingDays);
                        var payableDays = removeString(row.payableDays);
                        var timesheetInvoiceId = removeString(row.timesheetInvoiceId.toString());
                        var invoiceNumber = removeString(com.invoiceNumber);
                        var salaryDate = removeString(row.salaryDate);
                        var gstNumber = removeString(com.gstNumber);
                        var taxableAmount = removeString(row.taxableAmount);
                        taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                        var cgst = removeString(row.cgst);
                        var sgst = removeString(row.sgst);
                        var igst = removeString(row.igst);
                        var dueAmount = removeString(row.dueAmount);
                        dueAmount = (dueAmount == null) ? 0 : dueAmount;
                        var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                        var dueDate = removeString(row.dueDate);
                        var totalAmount = removeString(row.totalAmount);
                        var invoiceDate = removeString(com.invoiceDate);
                        var invoiceType = removeString(com.invoiceType);
                        invoiceType = invoiceType.toLowerCase();
                        invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                        var invoiceMode = removeString(com.invoiceMode);
                        var comments = removeString(row.comments);
                        var remarks = removeString(row.remarks);
                        var status = removeString(row.status);
                        var loginUserId = removeString(com.loginUserId);
                        var oldInvoiceNumber = removeString(com.oldInvoiceNumber);

                        var gstState = gstNumber.substring(0, 2);
                        if (invoiceType == 'Regular') {
                            if (gstState == '29') {
                                cgst = Number(taxableAmountCalc) * (9 / 100);
                                sgst = Number(taxableAmountCalc) * (9 / 100);
                                igst = 0;
                                totalAmount = Number(taxableAmountCalc) + cgst + sgst;
                            } else {
                                cgst = 0;
                                sgst = 0;
                                igst = Number(taxableAmountCalc) * (18 / 100);
                                totalAmount = Number(taxableAmountCalc) + igst;
                            }
                        } else {
                            cgst = 0;
                            sgst = 0;
                            igst = 0;
                            totalAmount = Number(taxableAmountCalc);
                        }
                        if (invoiceId == "" || invoiceId == null || invoiceId == undefined) {
                            var SQL = query.createInvoice + query.updateTimesheetInvoiceSingle;

                        } else if (row.selectRow == false) {
                            var SQL = query.deleteTimesheetInvoice;
                        } else {
                            var SQL = query.updateTimesheetInvoiceSingle;
                            var SQL2 = query.updateInvoiceId;
                        }

                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('invoice_number_re', removeString(invoiceNumber, true));
                        SQL = SQL.replace('gst_number_re', removeString(gstNumber, true));
                        SQL = SQL.replace('invoice_date_re', removeString(timesNow, true));
                        SQL = SQL.replace('invoice_type_re', removeString(invoiceType, true));
                        SQL = SQL.replace('invoice_mode_re', removeString(invoiceMode, true));
                        SQL = SQL.replace('created_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('created_at_re', removeString(timesNow, true));
                        SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                        SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
                        SQL = SQL.replace('cgst_re', removeString(cgst, true));
                        SQL = SQL.replace('sgst_re', removeString(sgst, true));
                        SQL = SQL.replace('igst_re', removeString(igst, true));
                        SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
                        SQL = SQL.replace('duedate_re', removeString(dueDate, true));
                        SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
                        SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                        SQL = SQL.replace('updated_at_re', removeString(timesNow, true));
                        SQL = SQL.replace('commentsRe', removeString(comments, true));
                        SQL = SQL.replace('remarksRe', removeString(remarks, true));
                        SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                        SQL = SQL.replace('invoice_id_re', removeString(invoiceId, true));


                        console.log("\n > createInvoice SQL ---> ", SQL);
                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > createInvoice SQL err ---> ", err);
                                //callback(true, err.sqlMessage);
                            } else {

                                SQL2 = SQL2.replace('prms_invoice_id_re', removeString(invoiceId, true));
                                SQL2 = SQL2.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));
                                db.query(SQL2, function () { });
                                if (count == input.invoiceItems.length) {
                                    callback(null, 'Invoice Number is ' + invoiceNumber);
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.invoiceItems.length) {
                            callback(null, 'Invoice Number is ' + invoiceNumber);
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
};

exports.searchReimbursementAssignInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.clientName == undefined
        || params.costCenter == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var serviceMonth = ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + month;
        var clientName = params.clientName.toString().trim();
        var costCenter = params.costCenter.map(x => "'" + x + "'").toString();
        var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
        var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
        var SQL = query.searchReimbursementAssignInvoice;
        var WHERE = "";

        if (searchBy != "" || searchValue != "") {
            if (searchBy == "clientName") {
                WHERE = "AND cl.name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "clientEmpId") {
                // WHERE = "AND H.client_empid = '" + searchValue + "'";
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE = "AND H.client_empid IN (" + data + ")";
            }
            else if (searchBy == "empId") {
                WHERE = "AND H.employee_id = '" + searchValue + "'";
            }
            else if (searchBy == "empName") {
                WHERE = "AND H.employee_name LIKE '%" + searchValue + "%'";
            }
            else if (searchBy == "invoiceNumber") {
                WHERE = "AND I.invoice_number = '" + searchValue + "'";
            }
        }

        SQL = SQL.replace('WHEREMONTH', month);
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace('WHERECLIENTNAME', clientName);
        SQL = SQL.replace('WHERECOSTCENTER', costCenter);
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > searchReimbursementAssignInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > searchReimbursementAssignInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results.forEach(e => {
                    e.taxableAmount = e.taxableAmount ? e.taxableAmount : 0;
                    e.cgst = e.cgst ? e.cgst : 0;
                    e.sgst = e.sgst ? e.sgst : 0;
                    e.igst = e.igst ? e.igst : 0;
                    e.totalAmount = e.totalAmount ? e.totalAmount : 0;
                    e.remMonth = e.remMonth ? e.remMonth : month;
                    e.remYear = e.remYear ? e.remYear : year;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.createReimbursementAssignInvoice = function (req, res, next) {
    var data = req.body;
    var getInvoiceDetails = query.getReimbursementInvoiceDetailsByNumber;
    getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', data.invoice.invoiceNumber);
    db.query(getInvoiceDetails, function (err, results, fields) {
        if (results.length > 0 && results[0].invoice_number.includes("PRIMUS")) {
            data.invoice.gstNumber = results[0].gst_number;
            data.invoice.invoiceDate = results[0].invoice_date;
            data.invoice.invoiceType = results[0].invoice_type;
            data.invoice.invoiceMode = results[0].invoice_mode;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    res.status(200).json({ status: 'success', data: success });
                }
            });
        } else {
            res.status(400).json({ status: 'failure', error: 'Enter Valid Invoice Number' });
        }
    });


    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var invoiceId = removeString(row.invoiceId);
                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceNumber = removeString(com.invoiceNumber);
                    var gstNumber = removeString(com.gstNumber);
                    var invoiceType = removeString(com.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString('Monthly');
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var remMonth = removeString(row.remMonth);
                    var remYear = removeString(row.remYear);
                    var loginUserId = removeString(com.loginUserId);
                    timesNow = removeString(timesNow);
                    var invoiceDate = timesNow;

                    var cgst = 0;
                    var sgst = 0;
                    var igst = 0;
                    var taxableAmount = removeString(row.taxableAmount);
                    var totalAmount = Number(taxableAmount);
                    var gstState = gstNumber.substring(0, 2);

                    console.log('\n > invoiceType ---> ', invoiceType);
                    console.log('\n > gstState ---> ', gstState);
                    if (invoiceType == 'Regular') {
                        if (gstState == 29) {
                            cgst = Number(taxableAmount) * Number(9 / 100);
                            sgst = Number(taxableAmount) * Number(9 / 100);
                            igst = Number(0);
                            totalAmount = Number(taxableAmount) + Number(cgst) + Number(sgst);
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(taxableAmount) * Number(18 / 100);
                            totalAmount = Number(taxableAmount) + Number(igst);
                        }
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(0);
                        totalAmount = Number(taxableAmount);
                    }
                    // callback(false, 'Invoice Number is ' + invoiceNumber);
                    var insertInvoice = "";
                    insertInvoice = "INSERT INTO `prms_reimbursement_invoice` (`hr_master_id`, `rem_year`, `invoice_date`, `rem_month`, `gst_number`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `invoice_number`, `creditperiod`, `duedate`, `comments`, `remarks`, `is_active`, `created_by`, `created_at`, `updated_by`, `updated_at`, `invoice_type`, `invoice_mode`, `er_pr`) VALUES (hr_master_id_re, rem_year_re, invoice_date_re, rem_month_re, gst_number_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, invoice_number_re, creditperiod_re, duedate_re, comments_re, remarks_re, is_active_re, created_by_re, created_at_re, updated_by_re, updated_at_re, invoice_type_re, invoice_mode_re, er_pr_re);";
                    insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));
                    insertInvoice = insertInvoice.replace('hr_master_id_re', removeString(hrMasterId, true));
                    insertInvoice = insertInvoice.replace('rem_year_re', removeString(remYear, true));
                    insertInvoice = insertInvoice.replace('invoice_date_re', removeString(invoiceDate, true));
                    insertInvoice = insertInvoice.replace('rem_month_re', removeString(remMonth, true));
                    insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                    insertInvoice = insertInvoice.replace('amount_re', removeString(taxableAmount, true));
                    insertInvoice = insertInvoice.replace('cgst_re', removeString(cgst, true));
                    insertInvoice = insertInvoice.replace('sgst_re', removeString(sgst, true));
                    insertInvoice = insertInvoice.replace('igst_re', removeString(igst, true));
                    insertInvoice = insertInvoice.replace('total_amount_re', removeString(totalAmount, true));
                    insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                    insertInvoice = insertInvoice.replace('creditperiod_re', null);
                    insertInvoice = insertInvoice.replace('duedate_re', null);
                    insertInvoice = insertInvoice.replace('comments_re', removeString(comments, true));
                    insertInvoice = insertInvoice.replace('remarks_re', removeString(remarks, true));
                    insertInvoice = insertInvoice.replace('is_active_re', '1');
                    insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                    insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                    insertInvoice = insertInvoice.replace('er_pr_re', null);


                    console.log('\n > insertInvoice ---> ', insertInvoice);

                    // callback(false, 'Invoice Number is ' + invoiceNumber);
                    db.query(insertInvoice, function (err, results, fields) {
                        count++;
                        if (err) {
                            console.log("\n > createInvoice SQL err ---> ", err);
                            //callback(true, err.sqlMessage);
                        } else {
                            if (count == input.invoiceItems.length) {
                                callback(null, 'Invoice Number is ' + invoiceNumber);
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
};

exports.checkTimesheetDateRange = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.hrMasterId == undefined
        || params.timesheetInvoiceId == undefined
        || params.timesheetFrom == undefined
        || params.timesheetTo == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var hrMasterId = removeString(params.hrMasterId);
        var timesheetMonth = removeString(params.month);
        var timesheetYear = removeString(params.year);
        var timesheetFrom = removeString(params.timesheetFrom) ? moment(removeString(params.timesheetFrom)).format("YYYY-MM-DD") : removeString(params.timesheetFrom);
        var timesheetTo = removeString(params.timesheetTo) ? moment(removeString(params.timesheetTo)).format("YYYY-MM-DD") : removeString(params.timesheetTo);
        var timesheetInvoiceId = removeString(params.timesheetInvoiceId) ? removeString(params.timesheetInvoiceId) : '';
        var SQL = query.checkTimesheetDateRange;
        var WHERE = '';
        if (timesheetInvoiceId) {
            WHERE = "AND `timesheet_invoice_id` <> " + removeString(timesheetInvoiceId, true);
        }

        SQL = SQL.replace('hr_master_id_re', removeString(hrMasterId, true));
        SQL = SQL.replace('timesheet_month_re', removeString(timesheetMonth, true));
        SQL = SQL.replace('timesheet_year_re', removeString(timesheetYear, true));
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > checkTimesheetDateRange SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > checkTimesheetDateRange SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                if (results.length > 0) {
                    var check = 0;
                    results.forEach((element, index) => {
                        var timesheet_date_from = moment(removeString(element.timesheet_date_from)).format("YYYY-MM-DD");
                        var timesheet_date_to = moment(removeString(element.timesheet_date_to)).format("YYYY-MM-DD");

                        if ((timesheetFrom >= timesheet_date_from && timesheetFrom <= timesheet_date_to) || (timesheetTo >= timesheet_date_from && timesheetTo <= timesheet_date_to)) {
                            check++;
                        }
                    });
                    if (check != 0) {
                        res.status(200).json({ status: 'success', data: true });
                    } else {
                        res.status(200).json({ status: 'success', data: false });
                    }
                } else {
                    res.status(200).json({ status: 'success', data: false });
                }
            }
        });
    }

    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.getConversionInvoice = function (req, res, next) {
    var params = req.body;
    if (
        params.month == undefined
        || params.year == undefined
        || params.invoiceType == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } if (
        params.month == ''
        || params.year == ''
        || params.invoiceType == ''
    ) {
        res.status(400).json({ status: 'failure', error: 'Please send service month and year' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var serviceMonth = ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + month;
        var invoiceType = params.invoiceType.toString().trim();
        invoiceTypeRE = invoiceType == 'NEW' ? 'AND (i3.invoice_id IS NULL OR i3.invoice_number LIKE "%DRAFT%")' : 'AND i3.invoice_id IS NOT NULL AND i3.invoice_number NOT LIKE "%DRAFT%"';
        var SQL = (invoiceType == 'NEW') ? query.getConversionInvoice : query.getConversionComInvoice;

        SQL = SQL.replace('WHEREMONTH', month);
        SQL = SQL.replace('WHEREYEAR', year);
        SQL = SQL.replace('SERVICEMONTH', serviceMonth);
        SQL = SQL.replace('INVOICE_TYPE_RE', invoiceTypeRE);

        console.log("\n > getConversionInvoice SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getConversionInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                results.forEach(e => {
                    e.dueAmount = e.dueAmount ? e.dueAmount : 0;
                    e.taxableAmount = e.taxableAmount ? e.taxableAmount : 0;
                    e.cgst = e.cgst ? e.cgst : 0;
                    e.sgst = e.sgst ? e.sgst : 0;
                    e.igst = e.igst ? e.igst : 0;
                    e.totalAmount = e.totalAmount ? e.totalAmount : 0;
                    e.timesheetMonth = e.timesheetMonth ? e.timesheetMonth : month;
                    e.timesheetYear = e.timesheetYear ? e.timesheetYear : year;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.createConvInvoice = function (req, res, next) {
    var params = req.body;

    if (
        params.actionType === undefined
        || params.timesheetId === undefined
        || params.invoiceId === undefined
        || params.hrMasterId === undefined
        || params.invoiceDate === undefined
        || params.invoiceNumber === undefined
        || params.gstNumber === undefined
        || params.invoiceType === undefined
        || params.dueAmount === undefined
        || params.taxableAmount === undefined
        || params.comments === undefined
        || params.remarks === undefined
        || params.tStatus === undefined
        || params.timesheetMonth === undefined
        || params.timesheetYear === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        // res.status(400).json({ status: 'success', data: 'Successfully Created' });
        if (params.actionType == "complete") {
            var getInvoiceSequence = query.getInvoiceSequence;
            db.query(getInvoiceSequence, function (err, results, fields) {
                console.log("\n > getInvoiceSequence SQL success ---> ", results[0]);
                var financeYear = results[0].financeYear;
                var sequence = results[0].sequenceNo;
                params.invoiceNumber = 'PRIMUS' + financeYear + sequence;
                insertConvInvoice(params, function (err, message) {
                    if (err) {
                        console.log("\n > createInvoice SQL Err ---> ", message);
                        res.status(400).json({ status: 'failure', error: message });
                    } else {
                        console.log("\n > createInvoice SQL ---> ", message);
                        updateInvoiceSequence();
                        res.status(200).json({ status: 'success', data: message });
                    }
                });
            });
        } else if (params.actionType == "draft") {
            var draftNo = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
            params.invoiceNumber = params.invoiceNumber ? params.invoiceNumber : draftNo;
            console.log('\n > params ---> ', JSON.stringify(params));
            insertConvInvoice(params, function (err, message) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", message);
                    res.status(400).json({ status: 'failure', error: message });
                } else {
                    console.log("\n > createInvoice SQL ---> ", message);
                    res.status(200).json({ status: 'success', data: message });
                }
            });
        }

        function insertConvInvoice(params, callback) {
            try {
                var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                var actionType = removeString(params.actionType);
                var timesheetId = removeString(params.timesheetId);
                var invoiceId = removeString(params.invoiceId);
                var hrMasterId = removeString(params.hrMasterId);
                var invoiceDate = removeString(params.invoiceDate);
                var invoiceNumber = removeString(params.invoiceNumber);
                var gstNumber = removeString(params.gstNumber);
                var invoiceType = removeString(params.invoiceType);
                invoiceType = invoiceType.toLowerCase();
                invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                var invoiceMode = removeString('Conversion');
                var comments = removeString(params.comments);
                var remarks = removeString(params.remarks);
                var status = removeString(params.status);
                var timesheetMonth = removeString(params.timesheetMonth);
                var timesheetYear = removeString(params.timesheetYear);
                var dueAmount = removeString(params.dueAmount);
                dueAmount = (dueAmount == null) ? 0 : dueAmount;
                var invoiceSources = removeString(params.invoiceSources);
                var invoiceCorrection = removeString(params.invoiceCorrection);
                var loginUserId = removeString(params.loginUserId);
                timesNow = removeString(timesNow);

                var cgst = 0;
                var sgst = 0;
                var igst = 0;
                var taxableAmount = removeString(params.taxableAmount);
                taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                var totalAmount = Number(taxableAmount);
                var gstState = gstNumber.substring(0, 2);

                console.log('\n > invoiceType ---> ', invoiceType);
                console.log('\n > gstState ---> ', gstState);

                if (invoiceType == 'Regular') {
                    if (gstState == 29) {
                        cgst = Number(taxableAmountCalc) * Number(9 / 100);
                        sgst = Number(taxableAmountCalc) * Number(9 / 100);
                        igst = Number(0);
                        totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(taxableAmountCalc) * Number(18 / 100);
                        totalAmount = Number(taxableAmountCalc) + Number(igst);
                    }
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(0);
                    totalAmount = Number(taxableAmountCalc);
                }

                // callback(false, 'Invoice Number is ' + invoiceNumber);
                var insertInvoice = "";
                var insertTimesheet = "";

                if (invoiceId && timesheetId) {
                    insertInvoice = "UPDATE `prms_invoice` SET `invoice_number` = invoice_number_re, `gst_number` = gst_number_re, `invoice_type` = invoice_type_re, `invoice_mode` = invoice_mode_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE `invoice_id` = invoice_id_re;";
                } else {
                    insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `invoice_sources`, `invoice_correction`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, invoice_sources_re, invoice_correction_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                }


                insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                insertInvoice = insertInvoice.replace('invoice_date_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                insertInvoice = insertInvoice.replace('invoice_sources_re', removeString(invoiceSources, true));
                insertInvoice = insertInvoice.replace('invoice_correction_re', removeString(invoiceCorrection, true));
                insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));

                console.log('\n > insertConvInvoice ---> ', insertInvoice);

                // callback(false, 'Invoice Number is ' + invoiceNumber);
                db.query(insertInvoice, function (err, results, fields) {
                    if (err) {
                        console.log("\n > createInvoice SQL err ---> ", err);
                        callback(true, err.sqlMessage);
                    } else if (invoiceId && timesheetId) {
                        console.log("\n > insertId insertConvInvoice ---> ", invoiceId);
                        insertTimesheet = "UPDATE `prms_timesheet_invoice` SET `amount` = amount_re, `cgst` = cgst_re, `sgst` = sgst_re, `igst` = igst_re, `total_amount` = total_amount_re, `due_amount` = due_amount_re, `comments` = comments_re, `remarks` = remarks_re, `status` = status_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE timesheet_invoice_id = timesheet_invoice_id_re;";
                        insertTimesheet = insertTimesheet.replace('timesheet_invoice_id_re', removeString(timesheetId, true));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                        insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                        insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                        insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                        insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                        insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                        insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                        insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                        insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));

                        if (taxableAmount != 0) {
                            insertTimesheet += query.updateConversionFee;
                            insertTimesheet = insertTimesheet.replace('conversion_fee_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        }
                        console.log('\n > insertTimesheet ---> ', insertTimesheet);
                        db.query(insertTimesheet, function () { });
                        callback(false, 'Invoice Number is ' + invoiceNumber);
                    } else {
                        console.log("\n > insertId insertConvInvoice ---> ", results.insertId);
                        insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                        insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(results.insertId));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                        insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                        insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                        insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                        insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                        insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                        insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                        insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                        insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                        insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                        insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));

                        if (taxableAmount != 0) {
                            insertTimesheet += query.updateConversionFee;
                            insertTimesheet = insertTimesheet.replace('conversion_fee_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        }
                        console.log('\n > insertTimesheet ---> ', insertTimesheet);
                        db.query(insertTimesheet, function () { });
                        callback(false, 'Invoice Number is ' + invoiceNumber);
                    }
                });
            } catch (e) {
                console.log(e);
                callback(true, e);
            }
        }

        function updateInvoiceSequence() {
            var updateInvoiceSequence = query.updateInvoiceSequence;
            console.log("\n > insertConvInvoice updateInvoiceSequence SQL ---> ", updateInvoiceSequence);
            db.query(updateInvoiceSequence, function (err, results, fields) {
                console.log("\n > insertConvInvoice updateInvoiceSequence SQL success ---> ");
            });
        }

        function removeString(data, type = null) {
            if (data == undefined || data == null || data == '') {
                return null;
            } else if (type) {
                return "'" + data + "'";
            } else {
                return data;
            }
        }
    }
};

exports.updateContractInvoice = function (req, res, next) {
    var params = req.body;
    console.log('\n > params ---> ', JSON.stringify(params));
    updateInvoice(params, function (err, message) {
        if (err) {
            console.log("\n > createInvoice SQL Err ---> ", message);
            res.status(400).json({ status: 'failure', error: message });
        } else {
            console.log("\n > createInvoice SQL ---> ", message);
            res.status(200).json({ status: 'success', data: message });
        }
    });

    function updateInvoice(params, callback) {
        try {
            var timesheetInvoiceId = removeString(params.timesheetInvoiceId.toString());
            var taxableAmount = removeString(params.taxableAmount);
            taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
            var gstNumber = removeString(params.gstNumber);
            var cgst = removeString(params.cgst);
            var sgst = removeString(params.sgst);
            var igst = removeString(params.igst);
            var dueAmount = removeString(params.dueAmount);
            dueAmount = (dueAmount == null) ? 0 : dueAmount;
            var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
            var totalAmount = removeString(params.totalAmount);
            var invoiceType = removeString(params.invoiceType);
            invoiceType = invoiceType.toLowerCase();
            invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
            var gstState = gstNumber.substring(0, 2);
            console.log('\n > invoiceType ---> ', invoiceType);
            console.log('\n > gstState ---> ', gstState);

            if (invoiceType == 'Regular') {
                if (gstState == 29) {
                    cgst = Number(taxableAmountCalc) * Number(9 / 100);
                    sgst = Number(taxableAmountCalc) * Number(9 / 100);
                    igst = Number(0);
                    totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                } else {
                    cgst = Number(0);
                    sgst = Number(0);
                    igst = Number(taxableAmountCalc) * Number(18 / 100);
                    totalAmount = Number(taxableAmountCalc) + Number(igst);
                }
            } else {
                cgst = Number(0);
                sgst = Number(0);
                igst = Number(0);
                totalAmount = Number(taxableAmountCalc);
            }


            var SQL = query.updateContractInvoice;

            SQL = SQL.replace('amount_re', removeString(taxableAmount, true));
            SQL = SQL.replace('cgst_re', removeString(cgst, true));
            SQL = SQL.replace('sgst_re', removeString(sgst, true));
            SQL = SQL.replace('igst_re', removeString(igst, true));
            SQL = SQL.replace('total_amount_re', removeString(totalAmount, true));
            SQL = SQL.replace('due_amount_re', removeString(dueAmount, true));
            SQL = SQL.replace('timesheet_invoice_id_re', removeString(timesheetInvoiceId, true));

            console.log("\n > updateContractInvoice SQL ---> ", SQL);
            db.query(SQL, function (err, results, fields) {
                if (err) {
                    console.log("\n > timesheet_invoice_id_re SQL err ---> ", err);
                    callback(true, err.sqlMessage);
                } else {
                    callback(false, 'Updated Successfully');
                }
            });
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }

    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }
}

exports.createPermAssignInvoice = function (req, res, next) {
    var data = req.body;
    var getInvoiceDetails = query.getInvoiceDetailsByNumber;
    getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', data.invoice.invoiceNumber);
    db.query(getInvoiceDetails, function (err, results, fields) {
        if (results.length > 0 && results[0].invoice_number.includes("PRIMUS")) {
            data.invoice.gstNumber = results[0].gst_number;
            data.invoice.invoiceDate = results[0].invoice_date;
            data.invoice.invoiceType = results[0].invoice_type;
            data.invoice.invoiceMode = results[0].invoice_mode;
            data.invoice.invoiceId = results[0].invoice_id;
            insertInvoice(data, function (err, success) {
                if (err) {
                    console.log("\n > createInvoice SQL Err ---> ", err);
                }
                else {
                    console.log("\n > createInvoice SQL success ---> ");
                    res.status(200).json({ status: 'success', data: success });
                }
            });
        } else {
            res.status(400).json({ status: 'failure', error: 'Enter Valid Invoice Number' });
        }
    });


    function removeString(data, type = null) {
        if (data == undefined || data == null || data == '') {
            return null;
        } else if (type) {
            return "'" + data + "'";
        } else {
            return data;
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var actionType = removeString(com.actionType);
                    var taxableAmount = removeString(row.taxableAmount);

                    if (actionType == 'draft' || (actionType == "complete" && taxableAmount)) {

                        var timesheetId = removeString(row.timesheetId);
                        var invoiceId = removeString(com.invoiceId);
                        var hrMasterId = removeString(row.hrMasterId);
                        var invoiceDate = removeString(com.invoiceDate);
                        var invoiceNumber = removeString(com.invoiceNumber);
                        var gstNumber = removeString(com.gstNumber);
                        var invoiceType = removeString(com.invoiceType);
                        invoiceType = invoiceType.toLowerCase();
                        invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                        var invoiceMode = removeString('Monthly');
                        var comments = removeString(row.comments);
                        var remarks = removeString(row.remarks);
                        var status = removeString(row.status);
                        var timesheetMonth = removeString(row.timesheetMonth);
                        var timesheetYear = removeString(row.timesheetYear);
                        var dueAmount = removeString(row.dueAmount);
                        dueAmount = (dueAmount == null) ? 0 : dueAmount;
                        var invoiceSources = removeString(row.invoiceSources);
                        var invoiceCorrection = removeString(row.invoiceCorrection);
                        var loginUserId = removeString(com.loginUserId);

                        var cgst = 0;
                        var sgst = 0;
                        var igst = 0;
                        var taxableAmount = removeString(row.taxableAmount);
                        taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                        var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                        var totalAmount = Number(taxableAmount);
                        var gstState = gstNumber.substring(0, 2);

                        console.log('\n > invoiceType[' + count + '] ---> ', invoiceType);
                        console.log('\n > gstState[' + count + '] ---> ', gstState);

                        if (invoiceType == 'Regular') {
                            if (gstState == 29) {
                                cgst = Number(taxableAmountCalc) * Number(9 / 100);
                                sgst = Number(taxableAmountCalc) * Number(9 / 100);
                                igst = Number(0);
                                totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                            } else {
                                cgst = Number(0);
                                sgst = Number(0);
                                igst = Number(taxableAmountCalc) * Number(18 / 100);
                                totalAmount = Number(taxableAmountCalc) + Number(igst);
                            }
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(0);
                            totalAmount = Number(taxableAmountCalc);
                        }

                        insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                        insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(invoiceId, true));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                        insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                        insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                        insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                        insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                        insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                        insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                        insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                        insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                        insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                        insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                        insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));
                        if (taxableAmount != 0) {
                            insertTimesheet += query.updateBillingAmountPermanent;
                            insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                        }

                        console.log("\n > insertTimesheet SQL ---> ", insertTimesheet);
                        db.query(insertTimesheet, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > createInvoice SQL err ---> ", err);
                                //callback(true, err.sqlMessage);
                            } else {
                                if (count == input.invoiceItems.length) {
                                    callback(null, 'Invoice Number is ' + invoiceNumber);
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.invoiceItems.length) {
                            callback(null, 'Invoice Number is ' + invoiceNumber);
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }
};

exports.importPermInvoice = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < sheets.length; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            if (jsonData[0].mergeInvoice == 'YES' && jsonData[0].assignInvoice == 'NO') {
                                var invoice = {
                                    "invoiceNumber": 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210),
                                    "invoiceId": jsonData[0].invoiceId,
                                    "gstNumber": jsonData[0].gstNumber,
                                    "invoiceDate": jsonData[0].invoiceDate,
                                    "invoiceType": jsonData[0].invoiceType,
                                    "invoiceMode": jsonData[0].invoiceMode,
                                    "loginUserId": jsonData[0].loginUserId
                                };
                                var data = {
                                    "invoice": invoice,
                                    "invoiceItems": jsonData
                                }

                                insertMergeInvoice(data, function (err, message) {
                                    if (err) {
                                        console.log("\n > createTimesheet SQL Err ---> ", message);
                                        res.status(400).json({ status: 'failure', error: message });
                                    } else {
                                        console.log("\n > createTimesheet SQL success ---> ", message);
                                        res.status(200).json({ status: 'success', data: message });
                                    }
                                });

                            } else if (jsonData[0].mergeInvoice == 'NO' && jsonData[0].assignInvoice == 'YES') {
                                var invoice = {
                                    "invoiceNumber": jsonData[0].invoiceNumber,
                                    "invoiceId": jsonData[0].invoiceId,
                                    "gstNumber": jsonData[0].gstNumber,
                                    "invoiceDate": jsonData[0].invoiceDate,
                                    "invoiceType": jsonData[0].invoiceType,
                                    "invoiceMode": jsonData[0].invoiceMode,
                                    "loginUserId": jsonData[0].loginUserId
                                };
                                var data = {
                                    "invoice": invoice,
                                    "invoiceItems": jsonData
                                }
                                var getInvoiceDetails = query.getInvoiceDetailsByNumber;
                                getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', data.invoice.invoiceNumber);
                                db.query(getInvoiceDetails, function (err, results, fields) {
                                    if (results.length > 0 && results[0].invoice_number.includes("PRIMUS")) {
                                        data.invoice.gstNumber = results[0].gst_number;
                                        data.invoice.invoiceDate = results[0].invoice_date;
                                        data.invoice.invoiceType = results[0].invoice_type;
                                        data.invoice.invoiceMode = results[0].invoice_mode;
                                        data.invoice.invoiceId = results[0].invoice_id;
                                        if (results[0].client_name != jsonData[0].clientName) {
                                            res.status(400).json({ status: 'failure', error: 'Client Name  Mismatch' });
                                        } else {
                                            insertAssignInvoice(data, function (err, message) {
                                                if (err) {
                                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                                    res.status(400).json({ status: 'failure', error: message });
                                                } else {
                                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                                    res.status(200).json({ status: 'success', data: message });
                                                }
                                            });
                                        }
                                    } else {
                                        res.status(400).json({ status: 'failure', error: 'Enter Valid Invoice Number' });
                                    }
                                });

                            } else if (jsonData[0].mergeInvoice == 'NO' && jsonData[0].assignInvoice == 'NO') {
                                var data = jsonData;

                                insertInvoice(data, function (err, message) {
                                    if (err) {
                                        console.log("\n > createTimesheet SQL Err ---> ", message);
                                        res.status(400).json({ status: 'failure', error: message });
                                    } else {
                                        console.log("\n > createTimesheet SQL success ---> ", message);
                                        res.status(200).json({ status: 'success', data: message });
                                    }
                                });
                            }

                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < sheets.length; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    if (jsonData[0].mergeInvoice == 'YES' && jsonData[0].assignInvoice == 'NO') {
                        var invoice = {
                            "invoiceNumber": 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210),
                            "invoiceId": jsonData[0].invoiceId,
                            "gstNumber": jsonData[0].gstNumber,
                            "invoiceDate": jsonData[0].invoiceDate,
                            "invoiceType": jsonData[0].invoiceType,
                            "invoiceMode": jsonData[0].invoiceMode,
                            "loginUserId": jsonData[0].loginUserId
                        };
                        var data = {
                            "invoice": invoice,
                            "invoiceItems": jsonData
                        }

                        insertMergeInvoice(data, function (err, message) {
                            if (err) {
                                console.log("\n > createTimesheet SQL Err ---> ", message);
                                res.status(400).json({ status: 'failure', error: message });
                            } else {
                                console.log("\n > createTimesheet SQL success ---> ", message);
                                res.status(200).json({ status: 'success', data: message });
                            }
                        });

                    } else if (jsonData[0].mergeInvoice == 'NO' && jsonData[0].assignInvoice == 'YES') {
                        var invoice = {
                            "invoiceNumber": jsonData[0].invoiceNumber,
                            "invoiceId": jsonData[0].invoiceId,
                            "gstNumber": jsonData[0].gstNumber,
                            "invoiceDate": jsonData[0].invoiceDate,
                            "invoiceType": jsonData[0].invoiceType,
                            "invoiceMode": jsonData[0].invoiceMode,
                            "loginUserId": jsonData[0].loginUserId
                        };
                        var data = {
                            "invoice": invoice,
                            "invoiceItems": jsonData
                        }
                        var getInvoiceDetails = query.getInvoiceDetailsByNumber;
                        getInvoiceDetails = getInvoiceDetails.replace('invoice_number_re', data.invoice.invoiceNumber);
                        db.query(getInvoiceDetails, function (err, results, fields) {
                            if (results.length > 0 && results[0].invoice_number.includes("PRIMUS")) {
                                data.invoice.gstNumber = results[0].gst_number;
                                data.invoice.invoiceDate = results[0].invoice_date;
                                data.invoice.invoiceType = results[0].invoice_type;
                                data.invoice.invoiceMode = results[0].invoice_mode;
                                data.invoice.invoiceId = results[0].invoice_id;
                                if (results[0].client_name != jsonData[0].clientName) {
                                    res.status(400).json({ status: 'failure', error: 'Client Name OR Cost Center Mismatch' });
                                } else {
                                    insertAssignInvoice(data, function (err, message) {
                                        if (err) {
                                            console.log("\n > createTimesheet SQL Err ---> ", message);
                                            res.status(400).json({ status: 'failure', error: message });
                                        } else {
                                            console.log("\n > createTimesheet SQL success ---> ", message);
                                            res.status(200).json({ status: 'success', data: message });
                                        }
                                    });
                                }
                            } else {
                                res.status(400).json({ status: 'failure', error: 'Enter Valid Invoice Number' });
                            }
                        });

                    } else if (jsonData[0].mergeInvoice == 'NO' && jsonData[0].assignInvoice == 'NO') {
                        var data = jsonData;

                        insertInvoice(data, function (err, message) {
                            if (err) {
                                console.log("\n > createTimesheet SQL Err ---> ", message);
                                res.status(400).json({ status: 'failure', error: message });
                            } else {
                                console.log("\n > createTimesheet SQL success ---> ", message);
                                res.status(200).json({ status: 'success', data: message });
                            }
                        });
                    }
                }
            });
        }
    });

    function insertMergeInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var timesheetId = removeString(row.timesheetId);
                    var invoiceId = removeString(com.invoiceId);
                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceDate = removeString(com.invoiceDate);
                    var invoiceNumber = removeString(com.invoiceNumber);
                    var gstNumber = removeString(com.gstNumber);
                    var invoiceType = removeString(com.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString('Monthly');
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status);
                    var timesheetMonth = removeString(row.timesheetMonth);
                    var timesheetYear = removeString(row.timesheetYear);
                    var dueAmount = removeString(row.dueAmount);
                    dueAmount = (dueAmount == null) ? 0 : dueAmount;
                    var invoiceSources = removeString(row.invoiceSources);
                    var invoiceCorrection = removeString(row.invoiceCorrection);
                    var loginUserId = removeString(com.loginUserId);
                    timesNow = removeString(timesNow);

                    var cgst = 0;
                    var sgst = 0;
                    var igst = 0;
                    var taxableAmount = removeString(row.taxableAmount);
                    taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                    var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                    var totalAmount = Number(taxableAmount);
                    var gstState = gstNumber.substring(0, 2);

                    console.log('\n > invoiceType[' + count + '] ---> ', invoiceType);
                    console.log('\n > gstState[' + count + '] ---> ', gstState);

                    if (invoiceType == 'Regular') {
                        if (gstState == 29) {
                            cgst = Number(taxableAmountCalc) * Number(9 / 100);
                            sgst = Number(taxableAmountCalc) * Number(9 / 100);
                            igst = Number(0);
                            totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(taxableAmountCalc) * Number(18 / 100);
                            totalAmount = Number(taxableAmountCalc) + Number(igst);
                        }
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(0);
                        totalAmount = Number(taxableAmountCalc);
                    }

                    if (invoiceId) {
                        insertInvoice = "UPDATE `prms_invoice` SET `invoice_number` = invoice_number_re, `gst_number` = gst_number_re, `invoice_type` = invoice_type_re, `invoice_mode` = invoice_mode_re, `updated_by` = updated_by_re, `updated_at` = updated_at_re WHERE `invoice_id` = invoice_id_re;";
                    }
                    else {
                        insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re,created_by_re, created_at_re, updated_by_re, updated_at_re);";
                    }

                    insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                    insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                    insertInvoice = insertInvoice.replace('invoice_date_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                    insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                    // insertInvoice = insertInvoice.replace('invoice_sources_re', removeString(invoiceSources, true));
                    // insertInvoice = insertInvoice.replace('invoice_correction_re', removeString(invoiceCorrection, true));
                    insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));

                    console.log('\n > insertInvoice[' + count + '] ---> ', insertInvoice);

                    // callback(false, 'Invoice Number is ' + invoiceNumber);
                    db.query(insertInvoice, function (err, results, fields) {
                        if (err) {
                            console.log("\n > createInvoice SQL err ---> ", err);
                            //callback(true, err.sqlMessage);
                            count++;
                            if (count == input.invoiceItems.length) {
                                callback(null, true);
                            } else {
                                insertRow(input);
                            }
                        } else {
                            console.log("\n > insertId ---> ", results.insertId);
                            if (invoiceId) {
                                results.insertId = invoiceId;
                            } else {
                                input.invoice.invoiceId = results.insertId;
                            }
                            insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                            insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(results.insertId));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                            insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                            insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                            insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                            insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                            insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                            insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                            insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                            insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                            insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                            insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                            insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                            insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                            insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                            insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));

                            if (taxableAmount != 0) {
                                insertTimesheet += query.updateBillingAmountPermanent;
                                insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                                insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                            }

                            console.log('\n > insertTimesheet[' + count + '] ---> ', insertTimesheet);
                            db.query(insertTimesheet, function () { });
                            count++;
                            if (count == input.invoiceItems.length) {
                                callback(false, 'Invoice Number is ' + invoiceNumber);
                            } else {
                                insertRow(input);
                            }
                            //callback(false, 'Invoice Number is ' + invoiceNumber);
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function insertAssignInvoice(data, callback) {
        try {
            if (data.invoiceItems.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var insertRow = function (input) {
                    var com = input.invoice;
                    var row = input.invoiceItems[count];

                    var timesheetId = removeString(row.timesheetId);
                    var invoiceId = removeString(com.invoiceId);
                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceDate = removeString(com.invoiceDate);
                    var invoiceNumber = removeString(com.invoiceNumber);
                    var gstNumber = removeString(com.gstNumber);
                    var invoiceType = removeString(com.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString('Monthly');
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status);
                    var timesheetMonth = removeString(row.timesheetMonth);
                    var timesheetYear = removeString(row.timesheetYear);
                    var dueAmount = removeString(row.dueAmount);
                    dueAmount = (dueAmount == null) ? 0 : dueAmount;
                    var invoiceSources = removeString(row.invoiceSources);
                    var invoiceCorrection = removeString(row.invoiceCorrection);
                    var loginUserId = removeString(com.loginUserId);

                    var cgst = 0;
                    var sgst = 0;
                    var igst = 0;
                    var taxableAmount = removeString(row.taxableAmount);
                    taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                    var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                    var totalAmount = Number(taxableAmount);
                    var gstState = gstNumber.substring(0, 2);

                    console.log('\n > invoiceType[' + count + '] ---> ', invoiceType);
                    console.log('\n > gstState[' + count + '] ---> ', gstState);

                    if (invoiceType == 'Regular') {
                        if (gstState == 29) {
                            cgst = Number(taxableAmountCalc) * Number(9 / 100);
                            sgst = Number(taxableAmountCalc) * Number(9 / 100);
                            igst = Number(0);
                            totalAmount = Number(taxableAmountCalc) + Number(cgst) + Number(sgst);
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(taxableAmountCalc) * Number(18 / 100);
                            totalAmount = Number(taxableAmountCalc) + Number(igst);
                        }
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(0);
                        totalAmount = Number(taxableAmountCalc);
                    }

                    insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                    insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(invoiceId, true));
                    insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                    insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                    insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                    insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                    insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                    insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                    insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                    insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                    insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                    insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                    insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                    insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                    insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                    insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                    insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));

                    if (taxableAmount != 0) {
                        insertTimesheet += query.updateBillingAmountPermanent;
                        insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                        insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                    }

                    console.log("\n > insertTimesheet SQL ---> ", insertTimesheet);
                    db.query(insertTimesheet, function (err, results, fields) {
                        count++;
                        if (err) {
                            console.log("\n > createInvoice SQL err ---> ", err);
                            //callback(true, err.sqlMessage);
                        } else {
                            if (count == input.invoiceItems.length) {
                                callback(null, 'Invoice Number is ' + invoiceNumber);
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function insertInvoice(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var alreadyInvoice = [];
                var insertRow = function (input) {
                    input[count].invoiceNumber = 'DRAFT' + Math.floor(100000 + Math.random() * 9876543210);
                    input[count].loginUserId = '1';
                    var row = input[count];
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    var timesheetId = removeString(row.timesheetId);
                    var invoiceId = removeString(row.invoiceId);
                    var hrMasterId = removeString(row.hrMasterId);
                    var invoiceDate = removeString(row.invoiceDate);
                    var invoiceNumber = removeString(row.invoiceNumber);
                    var gstNumber = removeString(row.gstNumber);
                    var invoiceType = removeString(row.invoiceType);
                    invoiceType = invoiceType.toLowerCase();
                    invoiceType = invoiceType.charAt(0).toUpperCase() + invoiceType.slice(1);
                    var invoiceMode = removeString('Monthly');
                    var comments = removeString(row.comments);
                    var remarks = removeString(row.remarks);
                    var status = removeString(row.status);
                    var timesheetMonth = removeString(row.timesheetMonth);
                    var timesheetYear = removeString(row.timesheetYear);
                    var dueAmount = removeString(row.dueAmount);
                    dueAmount = (dueAmount == null) ? 0 : dueAmount;
                    var invoiceSources = removeString(row.invoiceSources);
                    var invoiceCorrection = removeString(row.invoiceCorrection);
                    var loginUserId = removeString(row.loginUserId);
                    timesNow = removeString(timesNow);

                    var cgst = 0;
                    var sgst = 0;
                    var igst = 0;
                    var taxableAmount = removeString(row.taxableAmount);
                    taxableAmount = (taxableAmount == null) ? 0 : taxableAmount;
                    var taxableAmountCalc = Number(taxableAmount) + Number(dueAmount);
                    var totalAmount = Number(taxableAmount);
                    var gstState = gstNumber.substring(0, 2);

                    console.log('\n > invoiceType ---> ', invoiceType);
                    console.log('\n > gstState ---> ', gstState);

                    if (invoiceType == 'Regular') {
                        if (gstState == 29) {
                            cgst = Number(taxableAmountCalc) * Number(9 / 100);
                            sgst = Number(taxableAmountCalc) * Number(9 / 100);
                            igst = Number(0);
                            totalAmount = Number(taxtaxableAmountCalcableAmount) + Number(cgst) + Number(sgst);
                        } else {
                            cgst = Number(0);
                            sgst = Number(0);
                            igst = Number(taxableAmountCalc) * Number(18 / 100);
                            totalAmount = Number(taxableAmountCalc) + Number(igst);
                        }
                    } else {
                        cgst = Number(0);
                        sgst = Number(0);
                        igst = Number(0);
                        totalAmount = Number(taxableAmountCalc);
                    }


                    insertInvoice = "INSERT INTO `prms_invoice` (`invoice_number`, `gst_number`, `invoice_date`, `invoice_type`, `invoice_mode`,`created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (invoice_number_re, gst_number_re, invoice_date_re, invoice_type_re, invoice_mode_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";

                    insertInvoice = insertInvoice.replace('invoice_number_re', removeString(invoiceNumber, true));
                    insertInvoice = insertInvoice.replace('gst_number_re', removeString(gstNumber, true));
                    insertInvoice = insertInvoice.replace('invoice_date_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('invoice_type_re', removeString(invoiceType, true));
                    insertInvoice = insertInvoice.replace('invoice_mode_re', removeString(invoiceMode, true));
                    // insertInvoice = insertInvoice.replace('invoice_sources_re', removeString(invoiceSources, true));
                    // insertInvoice = insertInvoice.replace('invoice_correction_re', removeString(invoiceCorrection, true));
                    insertInvoice = insertInvoice.replace('created_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('created_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('updated_by_re', removeString(loginUserId, true));
                    insertInvoice = insertInvoice.replace('updated_at_re', removeString(timesNow, true));
                    insertInvoice = insertInvoice.replace('invoice_id_re', removeString(invoiceId, true));

                    console.log('\n > insertInvoice ---> ', insertInvoice);

                    // callback(false, 'Invoice Number is ' + invoiceNumber);
                    db.query(insertInvoice, function (err, results, fields) {
                        if (err) {
                            console.log("\n > createInvoice SQL err ---> ", err);
                            count++;
                            if (count == input.length) {
                                callback(null, true);
                            } else {
                                insertRow(input);
                            }
                        } else {
                            console.log("\n > insertId ---> ", results.insertId);
                            insertTimesheet = "INSERT INTO `prms_timesheet_invoice` (`hr_master_id`, `prms_invoice_id`, `timesheet_month`, `timesheet_year`, `amount`, `cgst`, `sgst`, `igst`, `total_amount`, `due_amount`, `comments`, `remarks`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (hr_master_id_re, prms_invoice_id_re, timesheet_month_re, timesheet_year_re, amount_re, cgst_re, sgst_re, igst_re, total_amount_re, due_amount_re, comments_re, remarks_re, status_re, created_by_re, created_at_re, updated_by_re, updated_at_re);";
                            insertTimesheet = insertTimesheet.replace('prms_invoice_id_re', removeString(results.insertId));
                            insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                            insertTimesheet = insertTimesheet.replace('timesheet_month_re', removeString(timesheetMonth, true));
                            insertTimesheet = insertTimesheet.replace('timesheet_year_re', removeString(timesheetYear, true));
                            insertTimesheet = insertTimesheet.replace('amount_re', removeString(taxableAmount, true));
                            insertTimesheet = insertTimesheet.replace('cgst_re', removeString(cgst, true));
                            insertTimesheet = insertTimesheet.replace('sgst_re', removeString(sgst, true));
                            insertTimesheet = insertTimesheet.replace('igst_re', removeString(igst, true));
                            insertTimesheet = insertTimesheet.replace('total_amount_re', removeString(totalAmount, true));
                            insertTimesheet = insertTimesheet.replace('due_amount_re', removeString(dueAmount, true));
                            insertTimesheet = insertTimesheet.replace('comments_re', removeString(comments, true));
                            insertTimesheet = insertTimesheet.replace('remarks_re', removeString(remarks, true));
                            insertTimesheet = insertTimesheet.replace('status_re', removeString(status, true));
                            insertTimesheet = insertTimesheet.replace('created_by_re', removeString(loginUserId, true));
                            insertTimesheet = insertTimesheet.replace('created_at_re', removeString(timesNow, true));
                            insertTimesheet = insertTimesheet.replace('updated_by_re', removeString(loginUserId, true));
                            insertTimesheet = insertTimesheet.replace('updated_at_re', removeString(timesNow, true));

                            if (taxableAmount != 0) {
                                insertTimesheet += query.updateBillingAmountPermanent;
                                insertTimesheet = insertTimesheet.replace('billing_amt_re', removeString(taxableAmount, true));
                                insertTimesheet = insertTimesheet.replace('hr_master_id_re', removeString(hrMasterId, true));
                            }

                            console.log('\n > insertTimesheet ---> ', insertTimesheet);
                            db.query(insertTimesheet, function () { });
                            count++;
                            if (count == input.length) {
                                callback(false, 'Invoice Number is ' + invoiceNumber);
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.deleteContractInvoice = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deleteContractInvoice;
        SQL = SQL.replace(/invoice_id_re/g, params.id.toString().trim());

        console.log("\n > deleteContractInvoice SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deleteContractInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Deleted Successfully' });
            }
        });
    }
};

exports.deletePermInvoice = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deletePermInvoice;
        SQL = SQL.replace(/invoice_id_re/g, params.id.toString().trim());

        console.log("\n > deletePermInvoice SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deletePermInvoice SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Deleted Successfully' });
            }
        });
    }
};

exports.attendenceTrackerByHrMasterId = function (req, res, next) {
    var params = req.body;

    if (
        params.dateFrom == undefined
        || params.dateTo == undefined
        || params.month == undefined
        || params.year == undefined
        || params.searchBy == undefined
        || params.searchValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var dateFrom = params.dateFrom.toString().trim();
        var dateTo = params.dateTo.toString().trim();
        var month = params.month.toString().trim();
        var year = params.year.toString().trim();
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var type = params.type.toString().trim();
        var SQL = query.attendenceTracker;
        var WHERE = "";
        var lastMonth = '';
        var lastYear = '';
        if (dateFrom == "" || dateTo == "") {
            dateFrom = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            dateTo = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
        } else {
            dateFrom = moment(dateFrom).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
            dateTo = moment(dateTo).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
        }
        if (month == "" || year == "") {
            month = Number(moment.utc().tz("Asia/Kolkata").format("MM"));
            year = Number(moment.utc().tz("Asia/Kolkata").format("YYYY"));
        }
        if (month == 1) {
            lastMonth = 12;
            lastYear = year - 1;
        } else {
            lastMonth = month - 1;
            lastYear = year;
        }

        var serviceMonth = moment(dateFrom).utc().tz("Asia/Kolkata").format("YYYY-MM");

        SQL = SQL.replace(/DATEFROM/g, dateFrom);
        SQL = SQL.replace(/DATETO/g, dateTo);
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, '0'));
        SQL = SQL.replace(/WHEREYEAR/g, year);
        SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
        SQL = SQL.replace(/LASTYEAR/g, lastYear);
        SQL = SQL.replace('CONDITIONALWHERE', "AND H.hr_master_id IN (" + searchValue + ")");

        console.log("\n > params ---> ", JSON.stringify(params));
        console.log("\n > serviceMonth ---> ", serviceMonth);
        console.log("\n > month ---> ", month.toString().padStart(2, '0'));
        console.log("\n > year ---> ", year);
        console.log("\n > lastMonth ---> ", lastMonth.toString().padStart(2, '0'));
        console.log("\n > lastYear ---> ", lastYear);

        console.log("\n > attendenceTracker SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > attendenceTracker SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                var days = function (month, year) {
                    return new Date(year, month, 0).getDate();
                };

                var noOfDays = function (month, year) {
                    var date1 = new Date(dateFrom);
                    var date2 = new Date(dateTo);

                    // To calculate the time difference of two dates
                    var Difference_In_Time = date2.getTime() - date1.getTime();
                    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                    return Difference_In_Days + 1;
                };
                results.forEach(e => {
                    e.timesheetInvoiceId = e.timesheetInvoiceId == null || e.timesheetInvoiceId == '' ? '' : e.timesheetInvoiceId;

                    e.workingDays = e.workingDays === null || e.workingDays === '' ? noOfDays(dateFrom, dateTo) : e.workingDays;
                    e.billingDays = e.billingDays === null || e.billingDays === '' ? noOfDays(dateFrom, dateTo) : e.billingDays;
                    e.leaveWithoutPay = e.leaveWithoutPay === null || e.leaveWithoutPay === '' ? 0 : e.leaveWithoutPay;
                    e.payableDays = e.payableDays === null || e.payableDays === '' ? noOfDays(dateFrom, dateTo) : e.payableDays;

                    e.takenLeaveCl = e.takenLeaveCl == null || e.takenLeaveCl == '' ? 0 : e.takenLeaveCl;
                    e.takenLeaveSl = e.takenLeaveSl == null || e.takenLeaveSl == '' ? 0 : e.takenLeaveSl;
                    e.eLeaveCl = e.eLeaveCl == null || e.eLeaveCl == '' ? 0 : e.eLeaveCl;
                    e.eLeaveSl = e.eLeaveSl == null || e.eLeaveSl == '' ? 0 : e.eLeaveSl;
                    e.balLeaveCl = e.balLeaveCl == null || e.balLeaveCl == '' ? 0 : e.balLeaveCl;
                    e.balLeaveSl = e.balLeaveSl == null || e.balLeaveSl == '' ? 0 : e.balLeaveSl;
                    e.lastMonthBalCl = (e.lastMonthBalCl == undefined || e.lastMonthBalCl == null) ? 0 : e.lastMonthBalCl;
                    e.lastMonthBalSl = (e.lastMonthBalSl == undefined || e.lastMonthBalSl == null) ? 0 : e.lastMonthBalSl;


                    if (type == 'NEW') {
                        e.balLeaveCl = e.eLeaveCl + e.lastMonthBalCl;
                        e.balLeaveSl = e.eLeaveSl + e.lastMonthBalSl;
                    } else {
                        e.balLeaveCl = (e.eLeaveCl + e.lastMonthBalCl) - e.takenLeaveCl;
                        e.balLeaveSl = (e.eLeaveSl + e.lastMonthBalSl) - e.takenLeaveSl;
                    }

                    e.timesheetFrom = e.timesheetFrom == null || e.timesheetFrom == '' ? dateFrom : e.timesheetFrom;
                    e.timesheetTo = e.timesheetTo == null || e.timesheetTo == '' ? dateTo : e.timesheetTo;

                    e.timesheetMonth = e.timesheetMonth == null || e.timesheetMonth == '' ? month : e.timesheetMonth;
                    e.timesheetMonth = e.timesheetMonth.toString().padStart(2, '0');
                    e.timesheetYear = e.timesheetYear == null || e.timesheetYear == '' ? year : e.timesheetYear;
                });
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};