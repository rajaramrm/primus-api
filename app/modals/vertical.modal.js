var db = require('../db/manageDB');
var query = require('../queries/vertical.query');
var moment = require('moment-timezone');

exports.getAllVertical = function (req, res, next) {
    var SQL = query.getAllVertical;
    console.log("\n > getAllVertical SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getAllVertical SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.createVertical = function (req, res, next) {
    var params = req.body;
    let verticalName = params.verticalName.toString().trim();
    let status = params.status.toString().trim();
    let createdBy = params.loginUserId.toString().trim();

    if (verticalName == undefined || status == undefined || createdBy == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    }
    else if (verticalName == "" || status == "" || createdBy == "") {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    } else {
        var SQL = query.insertVertical;
        SQL = SQL.replace("vertical_name_RE", verticalName);
        SQL = SQL.replace("is_active_RE", status);
        SQL = SQL.replace("created_by_RE", createdBy);
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("created_at_RE", timesNow);
        SQL = SQL.replace("updated_by_RE", createdBy);
        SQL = SQL.replace("updated_at_RE", timesNow);
        console.log("\n > insertVertical SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > insertVertical SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Vertical Created Successfully' });
            }
        });
    }
}

exports.updateVertical = function (req, res, next) {
    var params = req.body;
    let verticalName = params.verticalName.toString().trim();
    let status = params.status.toString().trim();
    let createdBy = params.loginUserId.toString().trim();
    let verticalId = req.params.verticalId.toString().trim();

    if (verticalName == undefined || status == undefined || createdBy == undefined || verticalId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (verticalName == "" || status == "" || createdBy == "" || verticalId == "") {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    }
    else {
        var SQL = query.updateVertical;
        SQL = SQL.replace("vertical_name_RE", verticalName);
        SQL = SQL.replace("is_active_RE", status);
        SQL = SQL.replace("created_by_RE", createdBy);
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_by_RE", createdBy);
        SQL = SQL.replace("updated_at_RE", timesNow);
        SQL = SQL.replace("vertical_id_RE", verticalId);

        console.log("\n > updateVertical SQL  ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateVertical SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Vertical Updated Successfully' });
            }
        });
    }
}

exports.getVerticalForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "WHERE v.vertical_name LIKE '%SEARCH_RE%' OR v.is_active LIKE '%SEARCH_RE%' OR v.created_by LIKE '%SEARCH_RE%' OR v.created_at LIKE '%SEARCH_RE%' OR v.updated_by LIKE '%SEARCH_RE%' OR v.updated_at LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getVerticalForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getVerticalForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getVerticalForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};