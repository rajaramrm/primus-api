var db = require('../db/manageDB');
var moment = require('moment-timezone');
var config = require('../../config');
var query = require('../queries/payrollTracker.query');
var xlsx = require('xlsx');
var mkdirp = require('mkdirp');
var fs = require('fs');
var formidable = require('formidable');


// exports.payrollTracker = function (req, res, next) {
//     var params = req.body;
//     if (
//         params.searchBy === undefined
//         || params.searchValue === undefined
//         || params.status === undefined
//         || params.month === undefined
//         || params.year === undefined
//     ) {
//         res.status(200).json({ status: 'failure', error: 'Invalid payload data!' });
//     } else if (
//         params.month.toString().trim() === ''
//         || params.year.toString().trim() === ''
//     ) {
//         res.status(200).json({ status: 'failure', error: 'Please Sent Service Month!' });
//     } else {
//         var reqData = {
//             searchBy: params.searchBy.toString().trim(),
//             searchValue: params.searchValue.toString().trim(),
//             type: params.type.toString().trim().toLowerCase(),
//             status: params.status.toString().trim(),
//             month: params.month.toString().trim(),
//             year: params.year.toString().trim(),
//             serviceMonth: params.year + "-" + params.month,
//         };
//         getBusinessDay(reqData, function (error, message) {
//             if (error) {
//                 res.status(200).json({ status: 'failure', error: message });
//             } else {
//                 res.status(200).json({ status: 'success', data: message });
//             }
//         });

//         function getBusinessDay(reqData, callBack) {
//             let dateNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
//             let currentServiceMonth = moment.utc().tz("Asia/Kolkata").subtract(1, "months").endOf("month").format("YYYY-MM");
//             let currentDay = moment.utc().tz("Asia/Kolkata").format("DD");
//             if (currentServiceMonth === reqData.serviceMonth) {
//                 if (currentDay == 3 || currentDay == 4) {
//                     console.log('Six Clinet Only ---> ' + dateNow + ' - ' + currentDay);
//                     reqData.condition = true;
//                     getTimesheet(reqData, function (err, msg) {
//                         if (err) {
//                             callBack(true, msg);
//                         } else {
//                             callBack(false, msg);
//                         }
//                     });
//                 } else if (currentDay > 3) {
//                     console.log('All Clinet ---> ' + dateNow + ' - ' + currentDay);
//                     reqData.condition = false;
//                     getTimesheet(reqData, function (err, msg) {
//                         if (err) {
//                             callBack(true, msg);
//                         } else {
//                             callBack(false, msg);
//                         }
//                     });
//                 } else {
//                     console.log('No Records Found --->  ' + dateNow + ' - ' + currentDay);
//                     callBack(false, []);
//                 }
//             } else if (currentServiceMonth > reqData.serviceMonth) {
//                 console.log('Past service month --->  ' + dateNow + ' - ' + currentDay);
//                 reqData.condition = false;
//                 getTimesheet(reqData, function (err, msg) {
//                     if (err) {
//                         callBack(true, msg);
//                     } else {
//                         callBack(false, msg);
//                     }
//                 });
//             } else if (currentServiceMonth < reqData.serviceMonth) {
//                 console.log('Upcomming service month --->  ' + dateNow + ' - ' + currentDay);
//                 callBack(false, []);
//             } else {
//                 console.log('Upcomming service month --->  ' + dateNow + ' - ' + currentDay);
//                 callBack(false, []);
//             }

//         }

//         function getTimesheet(reqData, callBack) {
//             var CONDITION_WHERE = "";
//             var CONDITION_TYPE = "";
//             var CONDITION_STATUS = "";
//             var DYNAMIC_WHERE = '';

//             var lastMonth = '';
//             var lastYear = '';

//             if (reqData.month == 1) {
//                 lastMonth = 12;
//                 lastYear = reqData.year - 1;
//             } else {
//                 lastMonth = reqData.month - 1;
//                 lastYear = reqData.year;
//             }

//             if (reqData.searchBy != "" || reqData.searchValue != "") {
//                 if (reqData.searchBy == "clientName") {
//                     CONDITION_WHERE = "AND cl.name LIKE '%" + reqData.searchValue + "%'";
//                 } else if (reqData.searchBy == "empId") {
//                     reqData.searchValue = reqData.searchValue.toString().replace(/,/g, ' ');
//                     reqData.searchValue = reqData.searchValue.split(' ');
//                     var data = '';
//                     reqData.searchValue.forEach(e => {
//                         if (e) {
//                             data += "'" + e + "', ";
//                         }
//                     });
//                     data += "''";
//                     console.log('\n> search value ---> ', data);
//                     CONDITION_WHERE = "AND hr.employee_id IN (" + data + ")";
//                 }
//             }
//             if (reqData.status != "") {
//                 if (reqData.status == 1) {
//                     CONDITION_STATUS = "AND pt.is_paid=1"
//                 } else {
//                     CONDITION_STATUS = "AND pt.is_paid=0";
//                 }
//             }

//             if (reqData.type == "edit") {
//                 CONDITION_TYPE = "HAVING pt.id IS NOT NULL";
//             } else {
//                 CONDITION_TYPE = "HAVING pt.id IS NULL";
//             }

//             if (reqData.condition) {
//                 // DYNAMIC_WHERE += "AND (";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Accenture%' AND cs.name LIKE '%ATCI%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Accenture%' AND cs.name LIKE '%IO%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Accenture%' AND cs.name LIKE '%BPO%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%VMware%' AND cs.name LIKE '%NA%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Caterpillar%' AND cs.name LIKE '%NA%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Oracle%' AND cs.name LIKE '%India%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Oracle%' AND cs.name LIKE '%SSI - Non Billable%') OR ";
//                 // DYNAMIC_WHERE += "(cl.name LIKE '%Oracle%' AND cs.name LIKE '%FSS - Non Billable%')";
//                 // DYNAMIC_WHERE += ")";
//                 DYNAMIC_WHERE = "AND cm.client_id IN (SELECT client_id FROM `prms_payroll_bussiness_day` WHERE bussiness_day = '03') AND cm.cost_center_id IN (SELECT cost_center_id FROM `prms_payroll_bussiness_day` WHERE bussiness_day = '03')";
//             }

//             var SQL = query.payrollTracker;
//             SQL = SQL.replace(/MONTH_RE/g, reqData.month.toString().padStart(2, '0'));
//             SQL = SQL.replace(/YEAR_RE/g, reqData.year);
//             SQL = SQL.replace(/YEAR_MONTH/g, reqData.serviceMonth);
//             SQL = SQL.replace(/WHERE_CON/g, CONDITION_WHERE);
//             SQL = SQL.replace(/TYPE_RE/g, CONDITION_TYPE);
//             SQL = SQL.replace(/STATUS_CON/g, CONDITION_STATUS);
//             SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
//             SQL = SQL.replace(/LASTYEAR/g, lastYear);
//             SQL = SQL.replace(/DYNAMIC_WHERE/g, DYNAMIC_WHERE);


//             console.log("\n > reqData ---> ", JSON.stringify(reqData));
//             console.log("\n > Month ---> ", reqData.month);
//             console.log("\n > Year ---> ", reqData.year);
//             console.log("\n > MonthYear--->", reqData.serviceMonth);

//             console.log("\n > PayrollTracker SQL ---> ", SQL);
//             db.query(SQL, function (error, results, fields) {
//                 if (error) {
//                     console.log("\n > PayrollTimesheet SQL Err ---> ", error.code);
//                     callBack(true, error.code);
//                 } else {
//                     function daysInMonth(month, year) {
//                         return new Date(year, month, 0).getDate();
//                     }
//                     results.forEach(e => {
//                         e.payrollId = e.payrollId == null || e.payrollId == '' ? '' : e.payrollId;
//                         e.payrollMonth = e.payrollMonth == null || e.payrollMonth == '' ? reqData.month : e.payrollMonth;
//                         e.payrollMonth = e.payrollMonth.toString().padStart(2, '0');
//                         e.payrollYear = e.payrollYear == null || e.payrollYear == '' ? reqData.year : e.payrollYear;
//                         e.workingDays = e.workingDays === null || e.workingDays === '' ? daysInMonth(e.payrollMonth, e.payrollYear) : e.workingDays;
//                         e.payableDays = e.payableDays === null || e.payableDays === '' ? daysInMonth(e.payrollMonth, e.payrollYear) : e.payableDays;
//                         e.takenLeave = e.takenLeave == null || e.takenLeave == '' ? 0 : e.takenLeave;
//                         e.pLeaveBal = e.pLeaveBal == null || e.pLeaveBal == '' ? 0 : e.pLeaveBal;
//                         e.cLeaveBal = e.cLeaveBal == null || e.cLeaveBal == '' ? 0 : e.cLeaveBal;
//                         e.lastMonthLeaveBal = e.lastMonthLeaveBal == null || e.lastMonthLeaveBal == '' ? 0 : e.lastMonthLeaveBal;
//                         if (reqData.type != "edit") e.cLeaveBal = e.lastMonthLeaveBal
//                         e.incentive = e.incentive == null || e.incentive == '' ? 0 : e.incentive;
//                         e.reimbursement = e.reimbursement == null || e.reimbursement == '' ? 0 : e.reimbursement;
//                     });
//                     callBack(false, results);
//                 }
//             });
//         }
//     }
// }

exports.payrollTracker = function (req, res, next) {
    var params = req.body;
    if (
        params.searchBy === undefined
        || params.searchValue === undefined
        || params.status === undefined
        || params.month === undefined
        || params.year === undefined
        || params.userRole === undefined
        || params.pgtCode === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data!' });
    } else if (
        params.month.toString().trim() === ''
        || params.year.toString().trim() === ''
    ) {
        res.status(400).json({ status: 'failure', error: 'Please Sent Service Month!' });
    } else {
        var reqData = {
            searchBy: params.searchBy.toString().trim(),
            searchValue: params.searchValue.toString().trim(),
            type: params.type.toString().trim().toLowerCase(),
            status: params.status.toString().trim(),
            month: params.month.toString().trim(),
            year: (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1],
            serviceMonth: ((params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1]) + "-" + params.month,
            userRole: params.userRole.toString().trim(),
            pgtCode: params.pgtCode.toString().trim()
        };
       
        getTimesheet(reqData, function (error, message) {
            if (error) {
                res.status(400).json({ status: 'failure', error: message });
            } else {
                res.status(200).json({ status: 'success', data: message });
            }
        });

        function getTimesheet(reqData, callBack) {
            var CONDITION_WHERE = "";
            var CONDITION_TYPE = "";
            var CONDITION_STATUS = "";
            var DYNAMIC_WHERE = '';
            var ROLE_WHERE = '';

            var lastMonth = '';
            var lastYear = '';

            if (reqData.month == 1) {
                lastMonth = 12;
                lastYear = reqData.year - 1;
            } else {
                lastMonth = reqData.month - 1;
                lastYear = reqData.year;
            }

            if (reqData.searchBy != "" || reqData.searchValue != "") {
                if (reqData.searchBy == "clientName") {
                    CONDITION_WHERE = "AND cm.client_id ='" + params.client + "' AND cm.cost_center_id ='" + params.costCenter + "'";
                } else if (reqData.searchBy == "empId") {
                    reqData.searchValue = reqData.searchValue.toString().replace(/,/g, ' ');
                    reqData.searchValue = reqData.searchValue.split(' ');
                    var data = '';
                    reqData.searchValue.forEach(e => {
                        if (e) {
                            data += "'" + e + "', ";
                        }
                    });
                    data += "''";
                    console.log('\n> search value ---> ', data);
                    CONDITION_WHERE = "AND hr.employee_id IN (" + data + ")";
                } else if (reqData.searchBy == "bsDay") {
                    DYNAMIC_WHERE = "AND cm.cost_center_id IN (SELECT cost_center_id FROM `prms_cost_centers` WHERE business_day IN (BS_DAYRE))";
                    DYNAMIC_WHERE = DYNAMIC_WHERE.replace(/BS_DAYRE/g, reqData.searchValue);
                }
            }
            if (reqData.type == "edit") {
                CONDITION_TYPE = "HAVING pt.id IS NOT NULL";
            } else {
                CONDITION_TYPE = "HAVING pt.id IS NULL";
            }

            if (reqData.userRole == "Finance") {
                ROLE_WHERE = "AND cm.pgt_code_fin_spoc='" + reqData.pgtCode + "'";
            } else if(reqData.userRole !== 'SuperAdmin' && reqData.userRole !== 'Admin' && reqData.userRole !== 'HRAdmin' && reqData.userRole !== 'HR' && reqData.userRole !== 'BUHead' && reqData.userRole !== 'BM' && reqData.userRole !== 'FinanceAdmin' && reqData.userRole !== 'FinanceReadOnly' && reqData.userRole !== 'TA' && reqData.userRole !== 'PayrollAdmin' && reqData.userRole !== 'HRAdminReadOnly') {
                res.status(400).json({ status: 'failure', error: "Invalid Role" });
                return;
            }

            var SQL = query.payrollTracker;
            if (reqData.status === 'NotPaid') {
                //CONDITION_STATUS = "AND hr.status = 'Active' AND pt.is_paid='0'";
                CONDITION_STATUS = "AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') <> 'YEAR_MONTH' OR hr.relieved_date IS NULL OR hr.relieved_date='') AND pt.is_paid='0'";
                SQL = query.payrollExistTracker;
            } else if (reqData.status === 'Paid') {
                CONDITION_STATUS = "AND pt.is_paid='1'";
                SQL = query.payrollExistTracker;
            } else if (reqData.status === 'Exist') {
                //CONDITION_STATUS = "AND hr.status <> 'Active' AND pt.is_paid='0'";
                CONDITION_STATUS = "AND DATE_FORMAT(hr.relieved_date,'%Y-%m') = 'YEAR_MONTH' AND pt.is_paid='0'";
                SQL = query.payrollExistTracker;
            }

            SQL = SQL.replace(/MONTH_RE/g, reqData.month.toString().padStart(2, '0'));
            SQL = SQL.replace(/YEAR_RE/g, reqData.year);           
            SQL = SQL.replace(/WHERE_CON/g, CONDITION_WHERE);
            SQL = SQL.replace(/TYPE_RE/g, CONDITION_TYPE);
            SQL = SQL.replace(/STATUS_CON/g, CONDITION_STATUS);
            SQL = SQL.replace(/LASTMONTH/g, lastMonth.toString().padStart(2, '0'));
            SQL = SQL.replace(/LASTYEAR/g, lastYear);
            SQL = SQL.replace(/YEAR_MONTH/g, reqData.serviceMonth);
            SQL = SQL.replace(/DYNAMIC_WHERE/g, DYNAMIC_WHERE);
            SQL = SQL.replace(/WHERE_ROLE/g, ROLE_WHERE);

            console.log("\n > reqData ---> ", JSON.stringify(reqData));
            console.log("\n > Month ---> ", reqData.month);
            console.log("\n > Year ---> ", reqData.year);
            console.log("\n > MonthYear--->", reqData.serviceMonth);

            console.log("\n > PayrollTracker SQL (" + reqData.status + ") ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > PayrollTimesheet SQL Err ---> ", error.code);
                    callBack(true, error.code);
                } else {
                    function daysInMonth(month, year) {
                        return new Date(year, month, 0).getDate();
                    }
                    results.forEach(e => {
                        e.payrollId = e.payrollId === null || e.payrollId === '' ? '' : e.payrollId;
                        e.payrollMonth = e.payrollMonth === null || e.payrollMonth === '' ? reqData.month : e.payrollMonth;
                        e.payrollMonth = e.payrollMonth.toString().padStart(2, '0');
                        e.payrollYear = e.payrollYear === null || e.payrollYear === '' ? reqData.year : e.payrollYear;
                        e.workingDays = e.workingDays === null || e.workingDays === '' ? 0 : e.workingDays;
                        e.payableDays = e.payableDays === null || e.payableDays === '' ? 0 : e.payableDays;
                        e.leaveWithoutPay = e.leaveWithoutPay === null || e.leaveWithoutPay === '' ? 0 : e.leaveWithoutPay;
                        e.takenLeave = e.takenLeave === null || e.takenLeave === '' ? 0 : e.takenLeave;
                        e.pLeaveBal = e.pLeaveBal === null || e.pLeaveBal === '' ? 0 : e.pLeaveBal;
                        e.cLeaveBal = e.cLeaveBal === null || e.cLeaveBal === '' ? 0 : (e.pLeaveBal - e.takenLeave);
                        e.lastMonthLeaveBal = e.lastMonthLeaveBal === null || e.lastMonthLeaveBal === '' ? 0 : e.lastMonthLeaveBal;
                        e.payrollsheet_date = e.payrollsheet_date === null || e.payrollsheet_date === '' ? moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD") : e.payrollsheet_date;
                        if (reqData.type != "edit") e.cLeaveBal = e.lastMonthLeaveBal
                        e.incentive = e.incentive === null || e.incentive === '' ? 0 : e.incentive;
                        e.reimbursement = e.reimbursement === null || e.reimbursement === '' ? 0 : e.reimbursement;
                    });
                    callBack(false, results);
                }
            });
        }
    }
}

exports.createPayroll = function (req, res, next) {
    var data = req.body;
    insertPayroll(data, function (err, message) {
        if (err) {
            console.log("\n > createPayroll SQL Err ---> ", message);
            res.status(400).json({ status: 'failure', error: message });
        } else {
            console.log("\n > createPayroll SQL success ---> ", message);
            res.status(200).json({ status: 'success', data: message });
        }
    });
    function insertPayroll(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var actionType = (row.actionType == undefined) ? '' : row.actionType;
                    var hrMasterId = removeString(row.hrMasterId);
                    var payrollFrom = row.payrollFrom == undefined ? null : removeString(row.payrollFrom);
                    var payrollTo = row.payrollTo == undefined ? null : removeString(row.payrollTo);
                    var payrollMonth = removeString(row.payrollMonth);
                    var payrollYear = removeString(row.payrollYear);
                    var workingDays = removeString(row.workingDays) ? removeString(row.workingDays) : 0;
                    var leaveWithoutPay = removeString(row.leaveWithoutPay) ? removeString(row.leaveWithoutPay) : 0;
                    var payableDays = removeString(row.payableDays) ? removeString(row.payableDays) : 0;
                    var takenLeave = removeString(row.takenLeave) ? removeString(row.takenLeave) : 0;
                    var pLeaveBal = removeString(row.pLeaveBal) ? removeString(row.pLeaveBal) : 0;
                    var cLeaveBal = removeString(row.cLeaveBal) ? (removeString(row.cLeaveBal) < 0 ? 0 : removeString(row.cLeaveBal)) : 0;
                    var incentive = removeString(row.incentive) ? removeString(row.incentive) : 0;
                    var reimbursement = removeString(row.reimbursement) ? removeString(row.reimbursement) : 0;
                    var resourceSalary = removeString(row.resourceSalary) ? removeString(row.resourceSalary) : 0;

                    var perDaySalary = (resourceSalary && workingDays) ? resourceSalary / workingDays : 0;
                    var ctc = perDaySalary * payableDays;

                    console.log('\n >resourceSalary ---> ', resourceSalary);
                    console.log('\n >workingDays ---> ', workingDays);
                    console.log('\n >perDaySalary ---> ', perDaySalary);
                    console.log('\n >ctc ---> ', ctc);
                    // pLeaveBal=Number(pLeaveBal)-Number(takenLeave);
                    //cLeaveBal = Number(pLeaveBal) - Number(takenLeave);
                    console.log('\n> takenLeave ---> ', takenLeave);
                    console.log('> payableDays ---> ', payableDays);
                    var comments = removeString(row.comments);
                    var loginUserId = removeString(row.loggedInUserId) ? removeString(row.loggedInUserId) : 1;
                    var payrollId = removeString(row.payrollId) ? removeString(row.payrollId) : '';


                    if (actionType == 'create') {
                        var SQL = query.createPayroll;
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('hr_master_id_RE', removeString(hrMasterId, true));

                        SQL = SQL.replace('payroll_from_RE', removeString(payrollFrom, true));
                        SQL = SQL.replace('payroll_to_RE', removeString(payrollTo, true));
                        SQL = SQL.replace('payroll_month_RE', removeString(payrollMonth, true));
                        SQL = SQL.replace('payroll_year_RE', removeString(payrollYear, true));

                        SQL = SQL.replace('working_days_RE', removeString(workingDays, true));
                        SQL = SQL.replace('payable_days_RE', removeString(payableDays, true));
                        SQL = SQL.replace('leave_without_pay_RE', removeString(leaveWithoutPay, true));
                        SQL = SQL.replace('taken_leave_RE', removeString(takenLeave, true));
                        SQL = SQL.replace('previous_leave_balance_RE', removeString(pLeaveBal, true));
                        SQL = SQL.replace('current_leave_balance_RE', removeString(cLeaveBal, true));
                        SQL = SQL.replace('incentive_allowance_RE', removeString(incentive, true));
                        SQL = SQL.replace('reimbursement_RE', removeString(reimbursement, true));
                        SQL = SQL.replace('resourceSalary_RE', removeString(resourceSalary, true));
                        SQL = SQL.replace('ctc_RE', removeString(ctc, true));
                        SQL = SQL.replace('comments_RE', removeString(comments, true));
                        SQL = SQL.replace(/loginUserId/g, removeString(loginUserId, true));
                        SQL = SQL.replace(/timeNow/g, timesNow);

                        console.log("\n> insertPayroll SQL (" + count + ") ---> ", SQL);
                        // count++;
                        // if (count >= input.length) {
                        //     callback(false, 'Successfully Created');
                        // } else {
                        //     insertRow(input);
                        // }
                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > insertPayroll SQL (" + count + ") err ---> ", err);
                                if (count == input.length) {
                                    callback(false, 'Successfully Created');
                                } else {
                                    insertRow(input);
                                }
                                // callback(true, err.sqlMessage);
                            } else {
                                if (count == input.length) {
                                    callback(false, 'Successfully Created');
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else if (actionType == 'update' && payrollId) {
                        var SQL = query.updatePayroll;
                        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                        SQL = SQL.replace('hr_master_id_RE', removeString(hrMasterId, true));

                        SQL = SQL.replace('payroll_from_RE', removeString(payrollFrom, true));
                        SQL = SQL.replace('payroll_to_RE', removeString(payrollTo, true));
                        SQL = SQL.replace('payroll_month_RE', removeString(payrollMonth, true));
                        SQL = SQL.replace('payroll_year_RE', removeString(payrollYear, true));
                        SQL = SQL.replace('working_days_RE', removeString(workingDays, true));
                        SQL = SQL.replace('payable_days_RE', removeString(payableDays, true));
                        SQL = SQL.replace('leave_without_pay_RE', removeString(leaveWithoutPay, true));
                        SQL = SQL.replace('taken_leave_RE', removeString(takenLeave, true));
                        SQL = SQL.replace('previous_leave_balance_RE', removeString(pLeaveBal, true));
                        SQL = SQL.replace('current_leave_balance_RE', removeString(cLeaveBal, true));
                        SQL = SQL.replace('incentive_allowance_RE', removeString(incentive, true));
                        SQL = SQL.replace('reimbursement_RE', removeString(reimbursement, true));
                        SQL = SQL.replace('resourceSalary_RE', removeString(resourceSalary, true));
                        SQL = SQL.replace('ctc_RE', removeString(ctc, true));
                        SQL = SQL.replace('comments_RE', removeString(comments, true));
                        SQL = SQL.replace(/loginUserId/g, removeString(loginUserId, true));
                        SQL = SQL.replace(/timeNow/g, timesNow);
                        SQL = SQL.replace('payrollId', payrollId);

                        console.log("\n> updatePayroll SQL (" + count + ") ---> ", SQL);
                        // count++;
                        // if (count >= input.length) {
                        //     callback(null, true);
                        // } else {
                        //     insertRow(input);
                        // }
                        db.query(SQL, function (err, results, fields) {
                            count++;
                            if (err) {
                                console.log("\n > updateTimesheet SQL (" + count + ") err ---> ", err);
                                if (count == input.length) {
                                    callback(false, 'Successfully Updated');
                                } else {
                                    insertRow(input);
                                }
                            } else {
                                if (count == input.length) {
                                    callback(false, 'Successfully Updated');
                                } else {
                                    insertRow(input);
                                }
                            }
                        });
                    } else {
                        count++;
                        if (count == input.length) {
                            callback(false, 'Successfully Updated');
                        } else {
                            insertRow(input);
                        }
                    }
                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
}


/* exports.deletePayroll = function (req, res, next) {
    var params = req.params;
    if (params.id == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.id.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.deletePayroll;
        SQL = SQL.replace("payrollId_RE", params.id.toString().trim());

        console.log("\n > deletePayroll SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > deletePayroll SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Deleted Successfully' });
            }
        });
    }
}; */
exports.importPayrollsheet = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < sheets.length; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = fields.loginUserId;
                                    res.loginEmpCode = fields.empCode;
                                    res.loginUserType = fields.userType;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            insertTimesheet(data, function (err, message) {
                                if (err) {
                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < sheets.length; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = fields.loginUserId;
                            res.loginEmpCode = fields.empCode;
                            res.loginUserType = fields.userType;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    insertPayroll(data, function (err, message) {
                        if (err) {
                            console.log("\n > createTimesheet SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > createTimesheet SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function insertPayroll(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    console.log('\n > row ---> ', JSON.stringify(row));
                    var hrMasterId = removeString(row.HR_MASTER_ID);
                    var payrollsheetMonth = removeString(row.PAYROLL_MONTH);
                    var payrollsheetYear = removeString(row.PAYROLL_YEAR);
                    var payrollsheetFrom = null;
                    var payrollsheetTo = null;
                    var workingDays = removeString(row.WORKING_DAYS) ? removeString(row.WORKING_DAYS) : 0;
                    var payableDays = removeString(row.PAYABLE_DAYS) ? removeString(row.PAYABLE_DAYS) : 0;
                    var leaveWithoutPay = removeString(row.LEAVE_WITHOUT_PAY) ? removeString(row.LEAVE_WITHOUT_PAY) : 0;
                    var takenLeave = removeString(row.TAKEN_LEAVE) ? removeString(row.TAKEN_LEAVE) : 0;
                    var previousLeaveBal = removeString(row.PREVIOUS_LEAVE_BAL) ? removeString(row.PREVIOUS_LEAVE_BAL) : 0;
                    var currentLeaveBal = removeString(row.CURRENT_LEAVE_BAL) ? (removeString(row.CURRENT_LEAVE_BAL)<0 ? 0 : removeString(row.CURRENT_LEAVE_BAL)): 0;
                    var comments = removeString(row.COMMENTS) ? removeString(row.COMMENTS) : '';
                    var empStatus = removeString(row.EMP_STATUS) ? removeString(row.EMP_STATUS) : 1;
                    var incentive = removeString(row.INCENTIVE) ? removeString(row.INCENTIVE) : 0;
                    var reimbursement = removeString(row.REIMBURSEMENT) ? removeString(row.REIMBURSEMENT) : 0;
                    var loginUserId = removeString(row.loginUserId) ? removeString(row.loginUserId) : 1;
                    var loginEmpCode = removeString(row.loginEmpCode);
                    var loginUserType = removeString(row.loginUserType);
                    var resourceSalary = removeString(row.RESOURCE_SALARY) ? removeString(row.RESOURCE_SALARY) : 0;
                    var perDaySalary = (resourceSalary && workingDays) ? (resourceSalary / workingDays) : 0;
                    var ctc = perDaySalary * payableDays;

                    var SQL = query.importPayrollsheet;
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
                    SQL = SQL.replace('hr_master_id_RE', removeString(hrMasterId, true));
                    SQL = SQL.replace('payroll_month_RE', removeString(payrollsheetMonth.toString().padStart(2, '0'), true));
                    SQL = SQL.replace('payroll_year_RE', removeString(payrollsheetYear, true));
                    SQL = SQL.replace('payroll_from_RE', payrollsheetFrom);
                    SQL = SQL.replace('payroll_to_RE', payrollsheetTo);
                    SQL = SQL.replace('working_days_RE', removeString(workingDays, true));
                    SQL = SQL.replace('payable_days_RE', removeString(payableDays, true));
                    SQL = SQL.replace('leave_without_pay_RE', removeString(leaveWithoutPay, true));
                    SQL = SQL.replace('taken_leave_RE', removeString(takenLeave, true));
                    SQL = SQL.replace('previous_leave_balance_RE', removeString(previousLeaveBal, true));
                    SQL = SQL.replace('current_leave_balance_RE', removeString(currentLeaveBal, true));
                    SQL = SQL.replace('incentive_allowance_RE', removeString(incentive, true));
                    SQL = SQL.replace('comments_RE', removeString(comments, true));
                    SQL = SQL.replace('incentive_allowance_RE', removeString(incentive, true));
                    SQL = SQL.replace('reimbursement_RE', removeString(reimbursement, true));
                    SQL = SQL.replace('resourceSalary_RE', removeString(resourceSalary, true));
                    SQL = SQL.replace('ctc_RE', removeString(ctc, true));
                    SQL = SQL.replace(/loginUserID/g, removeString(loginUserId, true));
                    SQL = SQL.replace(/timesNow/g, timesNow);
                    console.log("\n> insertPayrollsheet SQL (" + count + ") ---> ", SQL);

                    var CSQL = query.checkPayrollsheetForImport;
                    CSQL = CSQL.replace('hr_master_id_RE', removeString(hrMasterId, true));
                    CSQL = CSQL.replace('payroll_month_RE', removeString(payrollsheetMonth.toString().padStart(2, '0'), true));
                    CSQL = CSQL.replace('payroll_year_RE', removeString(payrollsheetYear, true));
                    
                    var CSQL2 = query.checkLoginUserMappingEmp;
                    CSQL2 = CSQL2.replace('hr_master_id_re', removeString(hrMasterId, true));
                    CSQL2 = CSQL2.replace('pgt_code_fin_spoc_re', removeString(loginEmpCode, true));

                    db.query(CSQL+CSQL2, function (cerr, cresults, fields) {
                        count++;
                        if (cerr) {
                            console.log("\n > insertPayrollsheet SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, 'Successfully Created');
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else if (cresults[0].length == 0 && cresults[1].length != 0 && loginUserType=="Finance") {
                            // console.log("Test===>Fin",JSON.stringify(cresults));
                            db.query(SQL, function (err, results, fields) {
                                if (err) {
                                    console.log("\n > insertPayrollsheet SQL (" + count + ") err ---> ", err);
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                    // callback(true, err.sqlMessage);
                                } else {
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                }
                            });
                        } else if (cresults[0].length == 0 && loginUserType!="Finance") {
                            // console.log("Test===>Admin",JSON.stringify(cresults));
                            db.query(SQL, function (err, results, fields) {
                                if (err) {
                                    console.log("\n > insertPayrollsheet SQL (" + count + ") err ---> ", err);
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                    // callback(true, err.sqlMessage);
                                } else {
                                    if (count == input.length) {
                                        callback(false, 'Successfully Created');
                                    } else {
                                        insertRow(input);
                                    }
                                }
                            });
                        } else {
                            if (count == input.length) {
                                callback(false, 'Successfully Created');
                            } else {
                                insertRow(input);
                            }
                        }
                    });
                }
                insertRow(data);
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};


exports.updatePayrollStatus = function (req, res, next) {
    var params = req.body;

    console.log(params);
    var loginUserId = params.loggedInUserId.toString().trim();
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    /*  if (params.payrollId == undefined
         || params.status == undefined
     ) {
         res.status(200).json({ status: 'failure', error: 'Invalid payload data' });
     }
     else if (params.payrollId.toString().trim() == "" || params.status.toString().trim() == "") {
         res.status(400).json({ status: 'failure', error: 'Please send required field' });
     }
     else { */
    var SQL = query.updatePayrollStatus;


    SQL = SQL.replace(/timeNow/g, timesNow);
    SQL = SQL.replace(/loginUserId/g, loginUserId);
    SQL = SQL.replace("UPDATE_ID_RE", params.udpateIds);


    console.log("\n > updatePayrollStatus SQL ---> ", SQL);

    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > updatePayrollStatus SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: 'Updated Successfully' });
        }
    });

};

exports.getClientCostByFinanceSpoc = function (req, res, next) {
    var finSpoc = req.params.finSpoc;
    var SQL = "SELECT CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) clientCostName, cl.client_id clientId, cl.name clientName, cl.client_account_type clientType, cs.cost_center_id costCenterId, cs.name costCenterName FROM prms_clients cl LEFT JOIN prms_cost_centers cs ON cs.client_id = cl.client_id INNER JOIN `prms_client_cc_mapping` AS cc ON cl.client_id=cc.client_id AND cs.cost_center_id=cc.cost_center_id WHERE cs.client_id IS NOT NULL AND cc.`pgt_code_fin_spoc`='finSpoc_RE' GROUP BY cl.name,cs.name ORDER BY cl.name, cs.name;";
    SQL = SQL.replace("finSpoc_RE", finSpoc);
    console.log("\n > getClientCost SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClientCost SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getNonPgtEmployee = function (req, res, next) {
    var params = req.body;

    if (
        params.month == undefined
        || params.year == undefined
        || params.searchBy == undefined
        || params.searchValue == undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var month = params.month.toString().trim();
        var year = (params.month>=4) ? params.year.toString().trim().split("-")[0] : params.year.toString().trim().split("-")[1];
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();

        var SQL = query.getNonPgtEmployee;
        var WHERE = "";

        if (month && year) {
            var serviceMonth = year + "-" + month;
            WHERE = "AND DATE_FORMAT(hr.doj,'%Y-%m') = 'SERVICEMONTH' AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL)";
            WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        }

        if (searchBy && searchValue) {
            if (searchBy == "clientName") {
                WHERE += "AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "empName") {
                WHERE += "AND hr.employee_name LIKE '%" + searchValue + "%'";
            }
        }
        SQL = SQL.replace('CONDITIONALWHERE', WHERE);

        console.log("\n > getNonPgtEmployee SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getNonPgtEmployee SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });

    }
};

exports.importPgtCheck = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < 1; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            pgtCheck(data, function (err, message) {
                                if (err) {
                                    console.log("\n > pgtCheck SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > pgtCheck SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < 1; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    pgtCheck(data, function (err, message) {
                        if (err) {
                            console.log("\n > pgtCheck SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > pgtCheck SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function pgtCheck(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0, xlsError = [];
                var insertRow = function (input) {
                    var row = input[count];
                    rootObj = {};
                    rootObj['ROWS'] = "ROW_" + (count + 1);
                    var employeeId = removeString(row.EMP_ID);

                    var SQL = query.checkImportPgtCode;
                    SQL = SQL.replace('employee_id_re', removeString(employeeId, 'PGTCODE'));

                    console.log("\n> pgtCheck SQL (" + count + ") ---> ", SQL);

                    db.query(SQL, function (err, results, fields) {
                        count++;
                        if (err) {
                            console.log("\n > pgtCheck SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, xlsError);
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            var ALREADY_EXISTS = [];
                            if (results.length > 0) {
                                ALREADY_EXISTS.push(employeeId);
                            }
                            if (ALREADY_EXISTS.length != 0) {
                                rootObj['ALREADY_EXISTS'] = ALREADY_EXISTS.toString();
                            }
                            if (Object.keys(rootObj).length != 1) {
                                xlsError.push(rootObj);
                            }
                            if (count == input.length) {
                                callback(false, xlsError);
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = type === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.importPgtCodeUpdate = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        //console.log('\n> fields ----> ', fields);
        // console.log("Test Req==>",req);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < 1; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    res.loginUserId = removeString(fields.loginUserId) ? removeString(fields.loginUserId) : 1;
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            pgtCodeUpdate(data, function (err, message) {
                                if (err) {
                                    console.log("\n > pgtCodeUpdate SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > pgtCodeUpdate SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < 1; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            res.loginUserId = removeString(fields.loginUserId) ? removeString(fields.loginUserId) : 1;
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    pgtCodeUpdate(data, function (err, message) {
                        if (err) {
                            console.log("\n > pgtCodeUpdate SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > pgtCodeUpdate SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function pgtCodeUpdate(data, callback) {
        try {
            if (data.length > 0) {
                var count = 0;
                var insertRow = function (input) {
                    var row = input[count];
                    var employeeId = removeString(row.EMP_ID);
                    var hrMasterId = removeString(row.HR_ID);
                    var loginUserId = row.loginUserId;
                    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

                    var SQL = query.importPgtCodeUpdate;
                    SQL = SQL.replace('employee_id_re', removeString(employeeId, 'PGTCODE'));
                    SQL = SQL.replace('hr_master_id_re', removeString(hrMasterId, true));
                    SQL = SQL.replace('updated_by_re', removeString(loginUserId, true));
                    SQL = SQL.replace('updated_at_re', removeString(timesNow, true));

                    console.log("\n> importPgtCodeUpdate SQL (" + count + ") ---> ", SQL);

                    db.query(SQL, function (err, results, fields) {
                        count++;
                        if (err) {
                            console.log("\n > importPgtCodeUpdate SQL (" + count + ") err ---> ", err);
                            if (count == input.length) {
                                callback(false, 'Successfully Updated');
                            } else {
                                insertRow(input);
                            }
                            // callback(true, err.sqlMessage);
                        } else {
                            if (count == input.length) {
                                callback(false, 'Successfully Updated');
                            } else {
                                insertRow(input);
                            }
                        }
                    });

                }
                insertRow(data);
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (type && data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = type === 'PGTCODE' ? data.split(/\s/).join('').toUpperCase() : data;
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }

};
