var db = require('../db/manageDB');
var moment = require('moment-timezone');
var config = require('../../config');
var query = require('../queries/recruiter.query');
const e = require('connect-flash');
var fs = require("fs");
var mkdirp = require("mkdirp");
const formatAmount = require('indian-currency-formatter');

exports.getSelectSourcedBy = function (req, res) {
    db.query(query.selectSourcedBy, function (error, results, fields) {
        if (error) {
            console.log("\n > getSelectSourcedBy SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });

}

exports.getRecruiterPerformance = function (req, res, next) {
    var params = req.body;   
    if (
      params.userRole === undefined ||
      params.pgtCode === undefined ||
      params.searchBy === undefined ||
      params.searchValue === undefined ||
      params.year === undefined ||
      params.month === undefined
    ) {
      res.status(400).json({ status: "failure", error: "Invalid payload data" });
    } else {
      var month = params.month.toString().trim();
      console.log("\n>Month ---> ", month.split(","));
      month = month.split(",");
      var reportJson = [];
      getReports(month, params, reportJson, function (err, results) {
        if (err) {
          console.log("\n > getReports err ---> ", results);
          res.status(200).json({ status: "error", data: results });
        } else {
          var myObj = {};
          var totalRevTemp = 0;
          results.forEach(item => {
           
            var revenue = 0;
            if (!(item.clientName+'||'+item.sourcedBy in myObj)) {
              myObj[item.clientName+'||'+item.sourcedBy] = {
                employeeMode:"",
                onboardings: [],
                offers: 0,
                declines: 0,
                revenue,
                gross_margin: 0,
                percentage: 0
              };

              myObj[item.clientName+'||'+item.sourcedBy].employeeMode = item.employeeMode;
              myObj[item.clientName+'||'+item.sourcedBy].offers +=item.isOffer ? 1 : 0;
              myObj[item.clientName+'||'+item.sourcedBy].declines +=item.isDecline ? 1 : 0;
              (item.hrID!=0) ? myObj[item.clientName+'||'+item.sourcedBy].onboardings.push(item.hrID) : '';
              
              myObj[item.clientName+'||'+item.sourcedBy].revenue += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName+'||'+item.sourcedBy].gross_margin += item.grossMargin;
                
            } else if (item.clientName+'||'+item.sourcedBy) {

              myObj[item.clientName+'||'+item.sourcedBy].employeeMode = item.employeeMode;
              myObj[item.clientName+'||'+item.sourcedBy].offers +=item.isOffer ? 1 : 0;
              myObj[item.clientName+'||'+item.sourcedBy].declines +=item.isDecline ? 1 : 0;
              (item.hrID!=0) ? myObj[item.clientName+'||'+item.sourcedBy].onboardings.push(item.hrID) : '';
              
              myObj[item.clientName+'||'+item.sourcedBy].revenue += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName+'||'+item.sourcedBy].gross_margin += item.grossMargin;
              
            }
          });          
          var data = [];
          for (i in myObj) {
            let ob = {
              clientName: i.split("||")[0],
              sourcedBy: i.split("||")[1],  
              employeeMode : myObj[i].employeeMode,           
              onboardings: [...new Set(myObj[i].onboardings)].length,
              offers: myObj[i].offers,
              declines: myObj[i].declines,
              revenue: Math.round(myObj[i].revenue),
              gross_margin: Math.round(myObj[i].gross_margin),
              percentage: Math.round((myObj[i].gross_margin / myObj[i].revenue) * 100)
            };
            data.push(ob);
          }
          res.status(200).json({ status: "success", data: data });
        }
      });
  
     
    }
  };
  function getReports(months, params, reportJson, callback) {
    try {
      var row = 0;
      var insertRow = function (months, params, reportJson) {
        var month = months[row];
        var fyear = params.year.toString().trim();
        var userRole = params.userRole.toString().trim();
        var pgtCode = params.pgtCode.toString().trim();
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var seniorManager = params.seniorManager ? true : false;
        var CONDITIONWHERE = "";
        var WHERE_HR_STATUS = "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";
  
        var splitYear = fyear.split("-");
        var year;
        if (month >= 4) {
          year = splitYear[0];
        } else {
          year = splitYear[1];
        }
  
        var serviceMonth = year + "-" + month;
        var SQL = (params.employeeMode == "Contract") ? query.recruiterPerformanceReportCon : query.recruiterPerformanceReportPerm;
        var SBM = "";
        if (
          userRole === "SuperAdmin" ||
          userRole === "Admin" ||
          userRole === "HRAdmin" ||
          userRole === "HRAdminReadOnly"
        ) {
          if (searchBy == "BUHead" && searchValue) {
            CONDITIONWHERE =
              "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
          } else if (searchBy == "BM" && searchValue && seniorManager) {
            SBM =
              "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
            SBM = SBM.replace(/PGTCODE/g, searchValue);
            console.log("\n > SBM SQL ---> ", SBM);
            CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          } else if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE =
              "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          } else if (searchBy === "SourcedBy" && searchValue) {
            CONDITIONWHERE = "AND hr.pgt_code_sourced_by = '" + searchValue + "'";
          }
        } else if (userRole === "BUHead") {
          CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
          /* New Concept */
          if (searchBy == "BM" && searchValue && seniorManager) {
            SBM =
              "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
            SBM = SBM.replace(/PGTCODE/g, searchValue);
            console.log("\n > SBM SQL ---> ", SBM);
            CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          } else if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE +=
              " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          } else if (searchBy === "SourcedBy" && searchValue) {
            CONDITIONWHERE = "AND hr.pgt_code_sourced_by = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole === "BM" && seniorManager) {
          SBM =
            "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
          SBM = SBM.replace(/PGTCODE/g, pgtCode);
          console.log("\n > SBM SQL ---> ", SBM);
          CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          /* New Concept */
          if (searchBy == "BM" && searchValue) {
            CONDITIONWHERE +=
              " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          } else if (searchBy === "SourcedBy" && searchValue) {
            CONDITIONWHERE = "AND hr.pgt_code_sourced_by = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole === "BM") {
          CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
          /* New Concept */
          if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE +=
              " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          } else if (searchBy === "SourcedBy" && searchValue) {
            CONDITIONWHERE = "AND hr.pgt_code_sourced_by = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
          res.status(400).json({ status: 'failure', error: "Invalid Role" });
          return;
        }
  
        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
        SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);
        
  
        console.log("\n > recruiterPerformanceReport SQL(" + row + ") ---> ", SQL);
  
        db.query(SQL, function (error, results, fields) {
          if (error) {
            console.log(
              "\n > recruiterPerformanceReport SQL Err ---> ",
              error.code
            );
            res.status(400).json({ status: "failure", error: error.code });
          } else {
            reportJson = reportJson.concat(results);
            row++;
            if (row >= months.length) {
              callback(false, reportJson);
            } else {
              setTimeout(() => {
                insertRow(months, params, reportJson);
              });
            }
          }
        });
      };
      insertRow(months, params, reportJson);
    } catch (e) {
      console.log("\n > try catch error ---> ", e);
      callback(true, "try catch error");
    }
  }
  exports.getRecruiterPerformancePdf = function (req, res, next) {
  
    var params = req.body;
    if (
      params.userRole === undefined ||
      params.pgtCode === undefined ||
      params.searchBy === undefined ||
      params.searchValue === undefined ||
      params.year === undefined ||
      params.month === undefined
    ) {
      res.status(400).json({ status: "failure", error: "Invalid payload data" });
    } else {
      var month = params.month.toString().trim();
      console.log("\n>Month ---> ", month.split(","));
      month = month.split(",");
      var reportJson = [];
      getReports(month, params, reportJson, function (err, results) {
        if (err) {
          console.log("\n > getReports err ---> ", results);
          res.status(200).json({ status: "error", data: results });
        } else {
          var myObj = {};
          var totalRevTemp = 0;
          results.forEach(item => {
  
            var revenue = 0;
            if (!(item.clientName + '||' + item.sourcedBy in myObj)) {
              myObj[item.clientName + '||' + item.sourcedBy] = {
                employeeMode: "",
                onboardings: [],
                offers: 0,
                declines: 0,
                revenue,
                gross_margin: 0,
                percentage: 0
              };
  
              myObj[item.clientName + '||' + item.sourcedBy].employeeMode = item.employeeMode;
              myObj[item.clientName + '||' + item.sourcedBy].offers += item.isOffer ? 1 : 0;
              myObj[item.clientName + '||' + item.sourcedBy].declines += item.isDecline ? 1 : 0;
              (item.hrID != 0) ? myObj[item.clientName + '||' + item.sourcedBy].onboardings.push(item.hrID) : '';
  
              myObj[item.clientName + '||' + item.sourcedBy].revenue += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName + '||' + item.sourcedBy].gross_margin += item.grossMargin;
  
            } else if (item.clientName + '||' + item.sourcedBy) {
  
              myObj[item.clientName + '||' + item.sourcedBy].employeeMode = item.employeeMode;
              myObj[item.clientName + '||' + item.sourcedBy].offers += item.isOffer ? 1 : 0;
              myObj[item.clientName + '||' + item.sourcedBy].declines += item.isDecline ? 1 : 0;
              (item.hrID != 0) ? myObj[item.clientName + '||' + item.sourcedBy].onboardings.push(item.hrID) : '';
  
              myObj[item.clientName + '||' + item.sourcedBy].revenue += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName + '||' + item.sourcedBy].gross_margin += item.grossMargin;
  
            }
          });
          var data = [];
          for (i in myObj) {
            let ob = {
              clientName: i.split("||")[0],
              sourcedBy: i.split("||")[1],
              employeeMode: myObj[i].employeeMode,
              onboardings: [...new Set(myObj[i].onboardings)].length,
              offers: myObj[i].offers,
              declines: myObj[i].declines,
              revenue: Math.round(myObj[i].revenue),
              gross_margin: Math.round(myObj[i].gross_margin),
              percentage: Math.round((myObj[i].gross_margin / myObj[i].revenue) * 100)
            };
            data.push(ob);
          }
          var offers = 0;
          var onboardings = 0;
          var gross_margin = 0;
          var declines=0;
          var reven = 0;
          data.forEach(element => {
            offers += element.offers;
            onboardings += element.onboardings;
            gross_margin += element.gross_margin;
            reven += element.revenue;
            declines+=element.declines;
          });
          let ob = {
            clientName: "Total",
            sourcedBy: '',
            employeeMode: '',
            onboardings: onboardings,
            offers: offers,
            declines: declines,
            revenue: 0,
            gross_margin: gross_margin,
            percentage: Math.round((gross_margin / reven) * 100)
          };
          data.push(ob);
          generatePDF(data, function (err, data) {
            if (err) {
              console.log("\n> generatePDF CallBack Err ---> ", data);
            } else {
              console.log("\n> generatePDF CallBack Success ---> ", data);
              res.download(data);
            }
          });
          //res.status(200).json({ status: "success", data: data });
        }
      });
    }
  
  }
  function generatePDF(jsonData, callback) {
    var filePath = config.upload_path + "/downloads";
    console.log("\n -------------> filePath ---> ", filePath);
    if (!fs.existsSync(filePath)) {
      mkdirp(filePath, function (err) {
        generate(jsonData, filePath, function (err, data) {
          callback(err, data);
        });
      });
    } else {
      generate(jsonData, filePath, function (err, data) {
        callback(err, data);
      });
    }
  }
  
  function generate(jsonData, filePath, callback) {
  
    const PDFDocument = require("pdfkit");
    const PDFTable = require("voilab-pdf-table");
    const PDF = new PDFDocument({
      bufferPages: true,
      autoFirstPage: false,
      size: "A4",
      layout: "landscape",
      margin: 30
    });
  
    /* Recruiter Start */
    PDF.addPage();
    PDF.addContent()
      .fontSize(18)
      .text("Recruiter's Performance Report").fontSize(10)
      .text("Total: " + jsonData.length, { align: "right" })
      .fontSize(5).fontSize(10);
    const recruiter = new PDFTable(PDF, { bottomMargin: 0 });
    recruiter.setColumnsDefaults({
      headerBorder: ["T", "B"],
      border: ["T", "B"],
      borderOpacity: 0.2,
      align: "center",
      valign: "center",
      lineGap: 0,
      headerPadding: [5, 0, 5, 0]
    });
  
    recruiter.addColumns([
      {
        id: "clientName",
        header: "CLIENT",
        align: "left",
        width: 180,
        padding: [3, 0, 3, 0],
        valign: "center"
      },
      {
        id: "sourcedBy",
        header: "EMP NAME",
        align: "left",
        width: 150,
        padding: [3, 0, 3, 0],
        valign: "center"
      },
      {
        id: "employeeMode",
        header: "EMP MODE",
        align: "left",
        width: 80,
        padding: [3, 0, 3, 0],
        valign: "center"
      },
      {
        id: "offers",
        header: "NO OF OFFERS",
        align: "left",
        width: 70,
        padding: [3, 0, 3, 0],
        valign: "center",
        renderer: function (tb, data) {
          return (data.offers != 0 ? data.offers : 0+"");
        }
      },
      {
        id: "onboardings",
        header: "NO OF ONBOARDINGS",
        align: "left",
        width: 70,
        padding: [3, 0, 3, 0],
        valign: "center",
        renderer: function (tb, data) {
          return (data.onboardings != 0 ? data.onboardings : 0+"");
        }
      },
      {
        id: "declines",
        header: "NO OF DECLINES",
        align: "left",
        width: 70,
        padding: [3, 0, 3, 0],
        valign: "center",
        renderer: function (tb, data) {
          return (data.declines != 0 ? data.declines : 0+"");
        }
        
      },
      {
        id: "gross_margin",
        header: "GP EARNED",
        align: "left",
        width: 90,
        padding: [3, 0, 3, 0],
        valign: "center",
        renderer: function (tb, data) {
          return "Rs " + (data.gross_margin != null ? formatAmount(data.gross_margin) : 0) + "/-";
        }
      },
      {
        id: "percentage",
        header: "GM %",
        align: "left",
        width: 80,
        padding: [3, 0, 3, 0],
        valign: "center",
        renderer: function (tb, data) {
          return (data.percentage) + "%";
        }
      },
  
    ]);
  
    recruiter.onPageAdded(function (tb) {
      tb.addHeader();
    });
    recruiter.addBody(jsonData);
    if (!jsonData.length) {
      PDF.addContent()
        .fontSize(18)
        .text(`No Records Found!`, 305, 100)
        .fontSize(10);
    }
    /* Recruiter End */
  
    var file = filePath + "/RecruiterPerformanceReport.pdf";
    let i;
    let end;
    const range = PDF.bufferedPageRange();
    for (
      i = range.start, end = range.start + range.count, range.start <= end;
      i < end;
      i++
    ) {
      PDF.switchToPage(i);
      PDF.text(`Page ${i + 1} of ${range.count}`, 750, PDF.page.height - 25, {
        lineBreak: false
      });
    }
    PDF.end();
    PDF.pipe(fs.createWriteStream(file)).on("finish", function (err, success) {
      if (err) {
        console.log("\n>>>>>END<<<<<");
        callback(true, err);
      } else {
        console.log("\n>>>>>END<<<<<");
        callback(false, file);
      }
    });
  }