var db = require('../db/manageDB');
var query = require('../queries/msp.query');
var moment = require('moment-timezone');

exports.getMsp = function (req, res, next) {
    var SQL = "SELECT ms.id, ms.name mspName, ms.value mspValue, ms.fee_type feeType, CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) clientCostName, ms.client_id clientId, ms.cost_center_id costCenterId, ms.is_active isActive, (SELECT u.email_id FROM prms_app_employees u WHERE u.app_employee_id = ms.created_by) createdBy, (SELECT u.email_id FROM prms_app_employees u WHERE u.app_employee_id = ms.updated_by) updatedBy, ms.created_at createdAt, ms.updated_at updatedAt FROM prms_msp_names ms LEFT JOIN prms_clients cl on cl.client_id = ms.client_id LEFT JOIN prms_cost_centers cs ON cs.cost_center_id = ms.cost_center_id WHERE ms.is_active = 1 ORDER BY ms.name ASC, cl.name ASC, cs.name ASC;";
    console.log("\n > getMsp SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getMsp SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.mspExport = function (req, res, next) {
    var SQL = "SELECT ms.id AS 'MSP ID', CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) CLIENT, ms.name AS 'MSP / PORTAL', ms.value AS 'MSP / PORTAL FEE(%)', ms.fee_type AS 'TYPE', IF(ms.is_active, 'Active', 'In Active') STATUS, (SELECT u.email_id FROM prms_app_employees u WHERE u.app_employee_id = ms.created_by) AS 'CREATED BY', (SELECT u.email_id FROM prms_app_employees u WHERE u.app_employee_id = ms.updated_by) AS 'UPDATED BY', DATE_FORMAT(ms.created_at,'%Y-%m-%d') AS 'CREATED ON', DATE_FORMAT(ms.updated_at,'%Y-%m-%d') AS 'UPDATED ON' FROM prms_msp_names ms LEFT JOIN prms_clients cl on cl.client_id = ms.client_id LEFT JOIN prms_cost_centers cs ON cs.cost_center_id = ms.cost_center_id ORDER BY ms.name ASC, cl.name ASC, cs.name ASC";
    console.log("\n > mspExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > mspExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getClientCost = function (req, res, next) {
    var SQL = "SELECT CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) clientCostName, cl.client_id clientId, cl.name clientName, cl.client_account_type clientType, cs.cost_center_id costCenterId, cs.name costCenterName FROM prms_clients cl LEFT JOIN prms_cost_centers cs ON cs.client_id = cl.client_id WHERE cs.client_id IS NOT NULL AND cl.is_active=1 AND cs.is_active=1 ORDER BY cl.name, cs.name;";
    console.log("\n > getClientCost SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClientCost SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.createMsp = function (req, res, next) {
    var params = req.body;
    if (
        params.mspName === undefined
        || params.feeType === undefined
        || params.mspValue === undefined
        || params.clientId === undefined
        || params.costCenterId === undefined
        || params.loginUserId === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var mspName = params.mspName.toString().trim();
        var feeType = params.feeType.toString().trim();
        var mspValue = params.mspValue.toString().trim();
        var clientId = params.clientId.toString().trim();
        var costCenterId = params.costCenterId.toString().trim();
        var loginUserId = params.loginUserId.toString().trim();

        if (
            mspName === ''
            || feeType === ''
            || mspValue === ''
            || clientId === ''
            || costCenterId === ''
            || loginUserId === ''
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        } else {
            var checkMSP = "SELECT COUNT(*) inValid FROM prms_msp_names WHERE fee_type = 'feeTypeRe' AND client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1';";
            // checkMSP = checkMSP.replace("mspNameRe", mspName);
            checkMSP = checkMSP.replace("feeTypeRe", feeType);
            checkMSP = checkMSP.replace("clientIdRe", clientId);
            checkMSP = checkMSP.replace("costCenterIdRe", costCenterId);

            console.log("\n > checkMSP SQL ---> ", checkMSP);
            db.query(checkMSP, function (error, results, fields) {
                if (error) {
                    console.log("\n > checkMSP SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else if (results[0].inValid) {
                    res.status(400).json({ status: 'failure', error: 'MSP / PORTAL Already Exist!' });
                } else {
                    // res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                    insertNewMSP(params, function (error, message) {
                        if (error) {
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    }
};

exports.updateMsp = function (req, res, next) {
    var params = req.body;
    var mspId = req.params.id;
    if (
        params.mspName === undefined
        || params.feeType === undefined
        || params.mspValue === undefined
        || params.clientId === undefined
        || params.costCenterId === undefined
        || params.loginUserId === undefined
    ) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var mspName = params.mspName.toString().trim();
        var feeType = params.feeType.toString().trim();
        var mspValue = params.mspValue.toString().trim();
        var clientId = params.clientId.toString().trim();
        var costCenterId = params.costCenterId.toString().trim();
        var loginUserId = params.loginUserId.toString().trim();

        if (
            mspId === ''
            || mspName === ''
            || feeType === ''
            || mspValue === ''
            || clientId === ''
            || costCenterId === ''
            || loginUserId === ''
        ) {
            res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
        } else {
            var checkMSP = "SELECT COUNT(*) inValid FROM prms_msp_names WHERE fee_type = 'feeTypeRe' AND client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1' AND id NOT IN ('mspIdRe');";
            // checkMSP = checkMSP.replace("mspNameRe", mspName);
            checkMSP = checkMSP.replace("feeTypeRe", feeType);
            checkMSP = checkMSP.replace("clientIdRe", clientId);
            checkMSP = checkMSP.replace("costCenterIdRe", costCenterId);
            checkMSP = checkMSP.replace("mspIdRe", mspId);

            console.log("\n > checkMSP SQL ---> ", checkMSP);
            db.query(checkMSP, function (error, results, fields) {
                if (error) {
                    console.log("\n > checkMSP SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else if (results[0].inValid) {
                    res.status(400).json({ status: 'failure', error: 'MSP / PORTAL Already Exist!' });
                } else {
                    // res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                    updateMSPTable(params, function (error, message) {
                        if (error) {
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    }
};

function insertNewMSP(params, callBackMSP) {
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

    var SQL = "INSERT INTO prms_msp_names (name, value, fee_type, client_id, cost_center_id, is_active, created_by, updated_by, created_at, updated_at) VALUES ('mspNameRe', 'mspValueRe', 'feeTypeRe', 'clientIdRe', 'costCenterIdRe', '1', 'loginUserIdRe', 'loginUserIdRe', 'timesNowRe', 'timesNowRe');";
    SQL = SQL.replace("mspNameRe", params.mspName);
    SQL = SQL.replace("feeTypeRe", params.feeType);
    SQL = SQL.replace("mspValueRe", params.mspValue);
    SQL = SQL.replace("clientIdRe", params.clientId);
    SQL = SQL.replace("costCenterIdRe", params.costCenterId);
    SQL = SQL.replace(/loginUserIdRe/g, params.loginUserId);
    SQL = SQL.replace(/timesNowRe/g, timesNow);

    console.log("\n > insertNewMSP SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > insertNewMSP SQL Err ---> ", error.code);
            callBackMSP(true, error.code);
        } else {
            params.mspId = results.insertId;
            updateGrossProfit(params, function (error, message) {
                if (error) {
                    console.log("\n > updateGrossProfit Err ---> ", message);
                    callBackMSP(true, message);
                } else {
                    // res.status(200).json({ status: 'success', data: 'Updated Succefully', updateGrossProfit: successProfit });
                    callBackMSP(false, message);
                }
            });
        }
    });
}

function updateMSPTable(params, callBackMSP) {
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var SQL = "UPDATE prms_msp_names SET name = 'mspNameRe', value = 'mspValueRe', fee_type = 'feeTypeRe', client_id = 'clientIdRe', cost_center_id = 'costCenterIdRe', is_active = '1', updated_by = 'loginUserIdRe', updated_at = 'timesNowRe' WHERE id = 'mspIdRe';"
    SQL = SQL.replace("mspNameRe", params.mspName);
    SQL = SQL.replace("feeTypeRe", params.feeType);
    SQL = SQL.replace("mspValueRe", params.mspValue);
    SQL = SQL.replace("clientIdRe", params.clientId);
    SQL = SQL.replace("costCenterIdRe", params.costCenterId);
    SQL = SQL.replace(/loginUserIdRe/g, params.loginUserId);
    SQL = SQL.replace(/timesNowRe/g, timesNow);
    SQL = SQL.replace("mspIdRe", params.mspId);
    console.log("\n > updateMSPTable SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > updateMSP SQL Err ---> ", error.code);
            callBackMSP(true, error.code);
        } else {
            updateGrossProfit(params, function (error, message) {
                if (error) {
                    console.log("\n > updateGrossProfit Err ---> ", message);
                    callBackMSP(true, message);
                } else {
                    // res.status(200).json({ status: 'success', data: 'Updated Succefully', updateGrossProfit: successProfit });
                    callBackMSP(false, message);
                }
            });
        }
    });
}

function updateGrossProfit(params, callBackProfit) {
    // callBackProfit(false, 'Updated Successfully');
    // callBackProfit(true, JSON.stringify(params));
    // var updateProfit = "UPDATE prms_pg_hr_master hr LEFT JOIN prms_client_cc_mapping cm on cm.client_cc_mapping_id = hr.prms_client_cc_mapping_id LEFT JOIN prms_msp_names AS msp ON msp.id = cm.prms_msp_names_id SET hr.gross_margin = IF(hr.employee_mode = 'Contract', (IFNULL(hr.billing_amt, 0) - IFNULL(hr.salary, 0) - IFNULL(hr.bgv_cost, 0) - (IFNULL(hr.billing_amt, 0) * (IFNULL(msp.value, 0)/100))), (IFNULL(hr.billing_amt, 0) - IFNULL(hr.bgv_cost, 0) - (IFNULL(hr.billing_amt, 0) * (IFNULL(msp.value, 0)/100)))) WHERE hr.prms_client_cc_mapping_id IN (SELECT cm1.client_cc_mapping_id FROM prms_client_cc_mapping cm1 WHERE cm1.prms_msp_names_id = 'mspIdRe');";
    var updateClientMap = "UPDATE prms_client_cc_mapping SET new_msp_id = (SELECT GROUP_CONCAT(id) mspId FROM prms_msp_names WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1' GROUP BY client_id, cost_center_id), new_msp_name = (SELECT IFNULL((SELECT name FROM prms_msp_names WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND fee_type = 'MSP' AND is_active = '1'),'NA') AS mspName), new_msp_type = (SELECT GROUP_CONCAT(fee_type) mspType FROM prms_msp_names WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1' GROUP BY client_id, cost_center_id), new_msp_fee = (SELECT SUM(value) mspFee FROM prms_msp_names WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1' GROUP BY client_id, cost_center_id) WHERE client_id = 'clientIdRe' AND cost_center_id = 'costCenterIdRe' AND is_active = '1';";
    updateClientMap = updateClientMap.replace(/mspIdRe/g, params.mspId);
    updateClientMap = updateClientMap.replace(/clientIdRe/g, params.clientId);
    updateClientMap = updateClientMap.replace(/costCenterIdRe/g, params.costCenterId);
    console.log('\n> updateClientMap ---> ', updateClientMap);

    db.query(updateClientMap, function (errorProfit, resultProfit, fieldProfit) {
        var updateGrossProfit = "UPDATE prms_pg_hr_master hr JOIN prms_client_cc_mapping cm on cm.client_cc_mapping_id = hr.prms_client_cc_mapping_id SET hr.gross_margin = IF(hr.employee_mode = 'Contract', (IFNULL(hr.billing_amt, 0) - IFNULL(hr.salary, 0) - IFNULL(hr.bgv_cost, 0) - (IFNULL(hr.billing_amt, 0) * (IFNULL(cm.new_msp_fee, 0)/100))), (IFNULL(hr.billing_amt, 0) - IFNULL(hr.bgv_cost, 0) - (IFNULL(hr.billing_amt, 0) * (IFNULL(cm.new_msp_fee, 0)/100)))) WHERE hr.prms_client_cc_mapping_id IN (SELECT cm1.client_cc_mapping_id FROM prms_client_cc_mapping cm1 WHERE cm1.client_id = 'clientIdRe' AND cm1.cost_center_id = 'costCenterIdRe' AND cm1.is_active = '1');";

        updateGrossProfit = updateGrossProfit.replace(/clientIdRe/g, params.clientId);
        updateGrossProfit = updateGrossProfit.replace(/costCenterIdRe/g, params.costCenterId);
        console.log('\n> updateGrossProfit ---> ', updateGrossProfit);

        db.query(updateGrossProfit, function (errorProfit, resultProfit, fieldProfit) {
            callBackProfit(false, 'Updated Successfully');
        });
    });
}