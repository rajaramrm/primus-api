var db = require('../db/manageDB');
var query = require('../queries/rmtSpoc.query');
var moment = require('moment-timezone');

exports.getRmtSpoc = function (req, res, next) {
    var SQL = query.getRmtSpoc;
    console.log("\n > getRmtSpoc SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getRmtSpoc SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};


exports.getClient = function (req, res, next) {
    var SQL = query.getClient;
    console.log("\n > getClient SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getClient SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getCostCenter = function (req, res, next) {
    var params = req.params.clientName;
    var WHERE = "";
    if (params) {
        WHERE = "AND cl.client_id = '" + params.toString().trim() + "'";
    }
    var SQL = query.getCostCenter;
    SQL = SQL.replace('CONDITIONALWHERE', WHERE);
    console.log("\n > getCostCenter SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getCostCenter SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.createRmt = function (req, res, next) {
    var params = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

    var rmtSpocName = removeString(params.rmtSpocName);
    var emailId = removeString(params.emailId);
    var clientId = removeString(params.clientId);
    var clientName = removeString(params.clientName);
    var costCenterId = removeString(params.costCenterId);
    var costCenter = removeString(params.costCenter);
    var status = removeString(params.status);
    var loginUserId = removeString(params.loginUserId);
    timesNow = removeString(timesNow);

    if (rmtSpocName && clientId && costCenterId && status && loginUserId) {
        var checkRmtSpoc = query.checkRmtSpoc;
        console.log("\n > checkRmtSpoc SQL ---> ", checkRmtSpoc);

        var condition = emailId ? " AND email_id = " + emailId : "";
        checkRmtSpoc = checkRmtSpoc.replace("OPTIONAL", condition);
        checkRmtSpoc = checkRmtSpoc.replace("rmt_spoc_name_re", rmtSpocName);
        checkRmtSpoc = checkRmtSpoc.replace("client_name_re", clientId);
        checkRmtSpoc = checkRmtSpoc.replace("cost_center_re", costCenterId);

        console.log("\n > checkRmtSpoc SQL ---> ", checkRmtSpoc);
        db.query(checkRmtSpoc, function (error, results, fields) {
            if (error) {
                console.log("\n > createRmt SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else if (results.length == 0) {
                var SQL = query.createRmt;
                SQL = SQL.replace("rmt_spoc_name_re", rmtSpocName);
                SQL = SQL.replace("email_id_re", emailId);
                SQL = SQL.replace("clientId_re", clientId);
                SQL = SQL.replace("client_name_re", clientName);
                SQL = SQL.replace("costCenterId_re", costCenterId);
                SQL = SQL.replace("cost_center_re", costCenter);
                SQL = SQL.replace("is_active_re", status);
                SQL = SQL.replace("created_by_re", loginUserId);
                SQL = SQL.replace("updated_by_re", loginUserId);
                SQL = SQL.replace("created_at_re", timesNow);
                SQL = SQL.replace("updated_at_re", timesNow);
                console.log("\n > createRmt SQL ---> ", SQL);
                db.query(SQL, function (err, results, fields) { });
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            } else {
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    }

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }
}


exports.updateRmt = function (req, res, next) {
    var params = req.body;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

    var rmtId = removeString(params.rmtId);
    var rmtSpocName = removeString(params.rmtSpocName);
    var emailId = removeString(params.emailId);
    var clientId = removeString(params.clientId);
    var clientName = removeString(params.clientName);
    var costCenterId = removeString(params.costCenterId);
    var costCenter = removeString(params.costCenter);
    var status = removeString(params.status);
    var loginUserId = removeString(params.loginUserId);
    timesNow = removeString(timesNow);

    console.log('> rmtId ', rmtId);
    console.log('> rmtSpocName ', rmtSpocName);
    console.log('> clientID ', clientId);
    console.log('> costCenterId ', costCenterId);
    console.log('> status ', status);
    console.log('> loginUserId ', loginUserId);
    if (rmtId && rmtSpocName && clientId && costCenterId && status && loginUserId) {
        var checkRmtSpoc = query.checkRmtSpoc;
        var condition = emailId ? " AND email_id = " + emailId : "";
        checkRmtSpoc = checkRmtSpoc.replace("OPTIONAL", condition);
        checkRmtSpoc = checkRmtSpoc.replace("rmt_spoc_name_re", rmtSpocName);
        checkRmtSpoc = checkRmtSpoc.replace("client_name_re", clientName);
        checkRmtSpoc = checkRmtSpoc.replace("cost_center_re", costCenter);

        console.log("\n > checkRmtSpoc SQL ---> ", checkRmtSpoc);
        db.query(checkRmtSpoc, function (error, results, fields) {
            if (error) {
                console.log("\n > updateRmt SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else if (results.length == 0) {
                var SQL = query.updateRmt;
                SQL = SQL.replace("rmt_spoc_id_re", rmtId);
                SQL = SQL.replace("rmt_spoc_name_re", rmtSpocName);
                SQL = SQL.replace("email_id_re", emailId);
                SQL = SQL.replace("clientId_re", clientId);
                SQL = SQL.replace("client_name_re", clientName);
                SQL = SQL.replace("costCenterId_re", costCenterId);
                SQL = SQL.replace("cost_center_re", costCenter);
                SQL = SQL.replace("is_active_re", status);
                SQL = SQL.replace("updated_by_re", loginUserId);
                SQL = SQL.replace("updated_at_re", timesNow);
                console.log("\n > updateRmt SQL ---> ", SQL);
                db.query(SQL, function (err, results, fields) { });
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            } else {
                res.status(200).json({ status: 'success', data: 'Created Successfully' });
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Please sent required fields!' });
    }

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else {
            return null;
        }
    }
}

exports.getVerticalForExport = function (req, res, next) {
    var params = req.body;
    var WHERE_RE = "";
    if (params.searchValue != undefined && params.searchValue != null && params.searchValue != "") {
        WHERE_RE = "WHERE v.vertical_name LIKE '%SEARCH_RE%' OR v.is_active LIKE '%SEARCH_RE%' OR v.created_by LIKE '%SEARCH_RE%' OR v.created_at LIKE '%SEARCH_RE%' OR v.updated_by LIKE '%SEARCH_RE%' OR v.updated_at LIKE '%SEARCH_RE%'";
        WHERE_RE = WHERE_RE.replace(/SEARCH_RE/g, params.searchValue.toString().trim());
    }
    var SQL = query.getVerticalForExport;
    SQL = SQL.replace("WHERE_RE", WHERE_RE);
    console.log("\n > getVerticalForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getVerticalForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};