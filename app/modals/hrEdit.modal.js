var db = require('../db/manageDB');
var query = require('../queries/hrEdit.query');
var moment = require('moment-timezone');
var xlsx = require('xlsx');
var mkdirp = require('mkdirp');
var fs = require('fs');
var config = require('../../config');
var formidable = require('formidable');

exports.getDropdowns = function (req, res, next) {
    var SQL = "";
    SQL = "SELECT DISTINCT(`emergency_contact_relation`) text FROM `prms_pg_hr_master` WHERE emergency_contact_relation  IS NOT NULL;";
    SQL += "SELECT DISTINCT(`edu_qualification`) text FROM `prms_pg_hr_master` WHERE edu_qualification IS NOT NULL;";
    SQL += "SELECT DISTINCT(`location`) text FROM `prms_pg_hr_master` WHERE location IS NOT NULL;";
    console.log("\n > getDropdowns SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getDropdowns SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let data = {
                contactRelations: results[0],
                eduQualification: results[1],
                locations: results[2],
            }
            res.status(200).json({ status: 'success', data: data });
        }
    });
};


exports.select2SkillSet = function (req, res, next) {
    var SQL = query.select2SkillSet;
    console.log("\n > select2SkillSet SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > select2SkillSet SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
}

exports.getDomainBySkill = function (req, res, next) {
    var skillSetId = req.body;
    var SQL = query.getDomainBySkill;
    SQL = SQL.replace('skillSetId', skillSetId);
    console.log("\n > getDomainBySkill SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getDomainBySkill SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
}

exports.select2EndClients = function (req, res, next) {
    var SQL = query.select2EndClients;
    console.log("\n > select2EndClients SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > select2EndClients SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
}

exports.getHrEditDropDownDetails = function (req, res, next) {
    var params = req.body;
    params.type = req.params.type;
    if (params.type == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.type.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        if (params.type.toString().trim() == "selectClientName") {

            // var SQL = query.getClientMappingDropDownDetails;
            // SQL = SQL.replace("SELECT_RE", "DISTINCT(`client_name`) AS clientName,(SELECT `client_account_type` FROM `prms_clients` WHERE `name`=clientName limit 1) AS clientAccountType");
            // SQL = SQL.replace("WHERE_RE", "WHERE `is_active`='1' AND `client_name` != ''");
            // runSqlQuery(SQL);

            var SQL = "SELECT DISTINCT(cl.name) AS clientName, cl.client_account_type clientAccountType FROM prms_client_cc_mapping cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id WHERE cm.is_active='1';";
            runSqlQuery(SQL);
        } else if (params.type.toString().trim() == "selectCostCenter") {
            if (params.clientName == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                // SQL = SQL.replace("SELECT_RE", "DISTINCT(`cost_center`) AS costCenter");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `is_active`='1'");
                // SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                // runSqlQuery(SQL);
                var SQL = "SELECT DISTINCT(cs.name) AS costCenter FROM prms_client_cc_mapping cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cm.is_active='1' AND cl.name = 'clientNameRe';";
                SQL += "SELECT csi.id csiId, csi.name csiName, cl.client_id clientId, cl.name clientName FROM prms_cost_centers_invoice csi LEFT JOIN prms_clients cl ON cl.client_id = csi.client_id WHERE cl.name = 'clientNameRe' ORDER BY csi.name;";
                SQL = SQL.replace(/clientNameRe/g, params.clientName.toString().trim());
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectMspName") {

            if (params.clientName == undefined || params.costCenter == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = "SELECT IF((cm.new_msp_name = '' OR cm.new_msp_name IS NULL),'NA',cm.new_msp_name) AS mspName, cm.new_msp_fee AS mspFee FROM prms_client_cc_mapping cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cl.name = 'client_name_re' AND cs.name = 'cost_center_re' AND cm.is_active = '1' GROUP BY cm.client_id, cm.cost_center_id;";
                var SQL = "SELECT IF(fee_type='PORTAL','NA',name) AS mspName,SUM(value) AS mspFee FROM (SELECT * FROM prms_msp_names WHERE is_active = 1 AND `client_id`=(SELECT c.client_id FROM `prms_clients` AS c LEFT JOIN `prms_cost_centers` AS cc ON c.client_id = cc.client_id WHERE c.name ='client_name_re' limit 1) AND `cost_center_id`=(SELECT cc.cost_center_id  FROM `prms_clients` AS c LEFT JOIN `prms_cost_centers` AS cc ON c.client_id = cc.client_id WHERE c.name ='client_name_re' AND cc.name='cost_center_re' limit 1) ORDER BY `fee_type` ASC) AS MSP GROUP BY `client_id`,`cost_center_id`;";
                SQL = SQL.replace(/client_name_re/g, params.clientName.toString().trim());
                SQL = SQL.replace(/cost_center_re/g, params.costCenter.toString().trim());
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectRmtSpoc") {
            if (params.clientName == undefined || params.costCenter == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                var SQL = "SELECT rmt_spoc_id rmtSpocId, rmt_spoc_name rmtSpocName, client_name clientName, cost_center costCenter FROM `prms_rmtspoc` WHERE client_name = 'client_name_re' AND cost_center = 'cost_center_re' AND `is_active`='1';";
                SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectSourcedBy") {
            // var SQL = query.getSourcedByDetails;
            var SQL = "";
            var sourcedBy = "SELECT app_employee_id AS sourcedId, `name` AS sourcedBy,`employee_code` AS sourcedByCode FROM `prms_app_employees` WHERE `user_type`='TA' AND `status`='ACTIVE';";
            var routing = "SELECT `routing_id` AS sourcedId, `routing_name` AS sourcedBy, NULL AS sourcedByCode FROM `prms_app_routing`;";
            if (params.isRouting) {
                SQL = routing;
            } else {
                SQL = sourcedBy;
            }
            runSqlQuery(SQL);
        } else if (params.type.toString().trim() == "selectHrSpoc") {

            if (params.clientName == undefined || params.costCenter == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                // SQL = SQL.replace("SELECT_RE", "DISTINCT(`hr_spoc`) AS hrSpoc");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `is_active`='1'");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `prms_rmtspoc_id`='rmt_spoc_re' AND `is_active`='1'");
                var SQL = "SELECT DISTINCT(`hr_spoc`) AS hrSpoc FROM `prms_client_cc_mapping` cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cl.name ='client_name_re' AND cs.name='cost_center_re' AND cm.is_active = '1';";
                SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
                var mspRE = "";
                if (params.mspName.toString().trim() !== 'NA') {
                    mspRE = "AND msp.name='" + params.mspName.toString().trim() + "'";
                }
                SQL = SQL.replace("MSPWHERERE", mspRE);
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectFinanceSpoc") {

            if (params.clientName == undefined || params.costCenter == undefined || params.hrSpoc == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "" || params.hrSpoc.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                // SQL = SQL.replace("SELECT_RE", "DISTINCT(`fin_spoc`) AS financeSpoc");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `hr_spoc`='hr_spoc_re' AND `is_active`='1'");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `rmt_spoc`='rmt_spoc_re' AND `hr_spoc`='hr_spoc_re' AND `is_active`='1'");
                var SQL = "SELECT DISTINCT(`fin_spoc`) AS financeSpoc FROM `prms_client_cc_mapping` cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cl.name ='client_name_re' AND cs.name = 'cost_center_re' AND cm.hr_spoc = 'hr_spoc_re' AND cm.is_active='1';";
                SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
                var mspRE = "";
                if (params.mspName.toString().trim() !== 'NA') {
                    mspRE = "AND msp.name='" + params.mspName.toString().trim() + "'";
                }
                SQL = SQL.replace("MSPWHERERE", mspRE);
                SQL = SQL.replace("hr_spoc_re", params.hrSpoc.toString().trim());
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectPlHead") {

            if (params.clientName == undefined || params.costCenter == undefined || params.hrSpoc == undefined || params.financeSpoc == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "" || params.hrSpoc.toString().trim() == "" || params.financeSpoc.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                // SQL = SQL.replace("SELECT_RE", "DISTINCT(`pl_head`) AS plHead,`pgt_code_pandl_head` AS 'plHeadCode'");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `hr_spoc`='hr_spoc_re' AND `fin_spoc`='fin_spoc_re' AND `is_active`='1'");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `rmt_spoc`='rmt_spoc_re' AND `hr_spoc`='hr_spoc_re' AND `fin_spoc`='fin_spoc_re' AND `is_active`='1'");
                var SQL = "SELECT DISTINCT(`pl_head`) AS plHead,`pgt_code_pandl_head` AS 'plHeadCode' FROM `prms_client_cc_mapping` cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cl.name ='client_name_re' AND cs.name = 'cost_center_re' AND cm.hr_spoc = 'hr_spoc_re' AND cm.fin_spoc = 'fin_spoc_re' AND cm.is_active = '1';";
                SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
                var mspRE = "";
                if (params.mspName.toString().trim() !== 'NA') {
                    mspRE = "AND msp.name='" + params.mspName.toString().trim() + "'";
                }
                SQL = SQL.replace("MSPWHERERE", mspRE);
                SQL = SQL.replace("hr_spoc_re", params.hrSpoc.toString().trim());
                SQL = SQL.replace("fin_spoc_re", params.financeSpoc.toString().trim());
                runSqlQuery(SQL);
            }

        } else if (params.type.toString().trim() == "selectBusinessManager") {

            if (params.clientName == undefined || params.costCenter == undefined || params.hrSpoc == undefined || params.financeSpoc == undefined || params.plHead == undefined) {
                res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
            } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "" || params.hrSpoc.toString().trim() == "" || params.financeSpoc.toString().trim() == "" || params.plHead.toString().trim() == "") {
                res.status(400).json({ status: 'failure', error: 'Please send required field' });
            } else {
                // var SQL = query.getClientMappingDropDownDetails;
                // SQL = SQL.replace("SELECT_RE", "DISTINCT(`bsns_mgr`) AS businessManager,`pgt_code_bsns_mgr` AS 'businessManagerCode'");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `hr_spoc`='hr_spoc_re' AND `fin_spoc`='fin_spoc_re' AND `pl_head`='pl_head_re' AND `is_active`='1' AND `bsns_mgr`<>''");
                // SQL = SQL.replace("WHERE_RE", "WHERE `client_name`='client_name_re' AND `cost_center`='cost_center_re' AND `msp_name`='msp_name_re' AND `rmt_spoc`='rmt_spoc_re' AND `hr_spoc`='hr_spoc_re' AND `fin_spoc`='fin_spoc_re' AND `pl_head`='pl_head_re' AND `is_active`='1' AND `acct_mgr`<>''");
                var SQL = "SELECT DISTINCT(`bsns_mgr`) AS businessManager,`pgt_code_bsns_mgr` AS 'businessManagerCode' FROM `prms_client_cc_mapping` cm LEFT JOIN prms_clients cl ON cl.client_id = cm.client_id LEFT JOIN prms_cost_centers AS cs ON cs.cost_center_id = cm.cost_center_id WHERE cl.name ='client_name_re' AND cs.name = 'cost_center_re' AND cm.hr_spoc = 'hr_spoc_re' AND cm.fin_spoc = 'fin_spoc_re' AND cm.pl_head = 'pl_head_re' AND cm.is_active = '1' AND cm.bsns_mgr <> '';";
                SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
                SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
                var mspRE = "";
                if (params.mspName.toString().trim() !== 'NA') {
                    mspRE = "AND msp.name='" + params.mspName.toString().trim() + "'";
                }
                SQL = SQL.replace("MSPWHERERE", mspRE);
                SQL = SQL.replace("hr_spoc_re", params.hrSpoc.toString().trim());
                SQL = SQL.replace("fin_spoc_re", params.financeSpoc.toString().trim());
                SQL = SQL.replace("pl_head_re", params.plHead.toString().trim());
                runSqlQuery(SQL);
            }

        } else {
            res.status(400).json({ status: 'failure', error: 'Invalid Type' });
        }
    }

    function runSqlQuery(SQL) {
        console.log("\n > getHrEditDropDownDetails SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getHrEditDropDownDetails SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};

exports.getClientMappingId = function (req, res, next) {
    var params = req.body;
    if (params.clientName == undefined || params.costCenter == undefined || params.mspName == undefined || params.rmtSpoc == undefined || params.hrSpoc == undefined || params.financeSpoc == undefined || params.plHeadCode == undefined || params.businessManagerCode == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.clientName.toString().trim() == "" || params.costCenter.toString().trim() == "" || params.rmtSpoc.toString().trim() == "" || params.hrSpoc.toString().trim() == "" || params.financeSpoc.toString().trim() == "" || params.plHeadCode.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.getClientMappingId;
        SQL = SQL.replace("client_name_re", params.clientName.toString().trim());
        SQL = SQL.replace("cost_center_re", params.costCenter.toString().trim());
        SQL = SQL.replace("rmt_spoc_re", params.rmtSpoc.toString().trim());
        SQL = SQL.replace("hr_spoc_re", params.hrSpoc.toString().trim());
        SQL = SQL.replace("fin_spoc_re", params.financeSpoc.toString().trim());
        SQL = SQL.replace("pgt_code_pandl_head_re", params.plHeadCode.toString().trim());
        SQL = SQL.replace("pgt_code_account_manager_re", params.businessManagerCode.toString().trim());

        console.log("\n > getClientMappingId SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > getClientMappingId SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    }
};


exports.createHrEdit = function (req, res, next) {
    var params = req.body;
    if (params.employeeId === undefined || params.employeeFirstName === undefined || params.employeeMiddleName === undefined || params.employeeLastName === undefined || params.clientEmployeeId === undefined || params.clientMappingId === undefined || params.sourcedBy === undefined || params.sourcedByCode === undefined || params.employeeMode === undefined || params.mobileNo === undefined || params.alternateNo === undefined || params.personalEmailId === undefined || params.officialEmailId === undefined || params.dateOfBirth === undefined || params.gender === undefined || params.maritalStatus === undefined || params.fathersHusbandsName === undefined || params.emergencyContactName === undefined || params.emergencyContactRelation === undefined || params.emergencyContactNumber === undefined || params.permanentAddress === undefined || params.presentAddress === undefined || params.panNumber === undefined || params.educationalQualification === undefined || params.aadharCardNo === undefined || params.insurance === undefined || params.nameAsPerAadhar === undefined || params.insuranceNumber === undefined || params.gratuity === undefined || params.totalExp === undefined || params.relevantExp === undefined || params.jobTitle === undefined || params.location === undefined || params.primarySkill === undefined || params.secondarySkill === undefined || params.offeredDate === undefined || params.dateOfJoining === undefined || params.status === undefined || params.relievedDate === undefined || params.resigneeMonth === undefined || params.reasonForRelieving === undefined || params.exitType === undefined || params.endClientId === undefined || params.endClientName === undefined || params.verticalId === undefined || params.verticalName === undefined || params.billMode === undefined || params.billRate === undefined || params.billingAmount === undefined || params.salary === undefined || params.grossMargin === undefined || params.poValue === undefined || params.woValue === undefined || params.buildingAddressDeployed === undefined || params.pf === undefined || params.comments === undefined || params.remarks === undefined || params.loggedInUserId === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.employeeId.toString().trim() == "" || params.employeeFirstName.toString().trim() == "" || params.employeeLastName.toString().trim() == "" || params.clientEmployeeId.toString().trim() == "" || params.clientMappingId.toString().trim() == "" || params.sourcedBy.toString().trim() == "" || params.employeeMode.toString().trim() == "" || params.mobileNo.toString().trim() == "" || params.personalEmailId.toString().trim() == "" || params.dateOfBirth.toString().trim() == "" || params.totalExp.toString().trim() == "" || params.relevantExp.toString().trim() == "" || params.offeredDate.toString().trim() == "" || params.dateOfJoining.toString().trim() == "" || params.status.toString().trim() == "" || params.billMode.toString().trim() == "" || params.billRate.toString().trim() == "" || params.billingAmount.toString().trim() == "" || params.salary.toString().trim() == "" || params.grossMargin.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.createHrMasterDetails;

        var isRouting = params.isRouting ? 1 : 0;
        var sourcedId = params.sourcedId ? params.sourcedId : null;

        SQL = SQL.replace("is_routing_re", isRouting);
        SQL = SQL.replace("sourced_id_re", sourcedId);

        SQL = SQL.replace("employee_id_re", params.employeeId.toString().trim());
        SQL = SQL.replace("employee_name_re", params.employeeFirstName.toString().trim() + (params.employeeMiddleName.toString().trim() ? (" " + params.employeeMiddleName.toString().trim() + " ") : " ") + params.employeeLastName.toString().trim());
        SQL = SQL.replace("employee_first_name_re", params.employeeFirstName.toString().trim());
        SQL = SQL.replace("employee_middle_name_re", params.employeeMiddleName.toString().trim());
        SQL = SQL.replace("employee_last_name_re", params.employeeLastName.toString().trim());
        SQL = SQL.replace("client_empid_re", params.clientEmployeeId.toString().trim());
        SQL = SQL.replace("mobile_no_re", params.mobileNo.toString().trim());
        SQL = SQL.replace("alternate_no_re", params.alternateNo.toString().trim());
        SQL = SQL.replace("personal_emailid_re", params.personalEmailId.toString().trim());
        SQL = SQL.replace("official_emailid_re", params.officialEmailId.toString().trim());
        SQL = SQL.replace("dob_re", params.dateOfBirth.toString().trim() + " 00:00:00");
        SQL = SQL.replace("gender_re", params.gender.toString().trim());
        SQL = SQL.replace("marital_status_re", params.maritalStatus.toString().trim());
        SQL = SQL.replace("father_husband_name_re", params.fathersHusbandsName.toString().trim());
        SQL = SQL.replace("emergency_contact_name_re", params.emergencyContactName.toString().trim());
        SQL = SQL.replace("emergency_contact_relation_re", params.emergencyContactRelation.toString().trim());
        SQL = SQL.replace("emergency_contact_number_re", params.emergencyContactNumber.toString().trim());
        SQL = SQL.replace("permanant_address_re", removeString(params.permanentAddress));
        SQL = SQL.replace("present_address_re", removeString(params.presentAddress));
        SQL = SQL.replace("pan_number_re", params.panNumber.toString().trim());
        SQL = SQL.replace("bank_ac_no_re", params.bankAc.toString().trim());
        SQL = SQL.replace("bank_ifsc_re", params.backIfsc.toString().trim());
        SQL = SQL.replace("bank_name_re", params.bankName.toString().trim());
        SQL = SQL.replace("edu_qualification_re", params.educationalQualification.toString().trim());
        SQL = SQL.replace("aadhar_number_re", params.aadharCardNo.toString().trim());
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("exit_typeid_re", params.exitType.toString().trim());
        SQL = SQL.replace("prms_client_cc_mapping_id_re", params.clientMappingId.toString().trim());
        var ccInvId = params.costCenterInv === undefined || params.costCenterInv === '' ? 'NULL' : params.costCenterInv.toString().trim();
        SQL = SQL.replace("cost_center_inv_id_re", ccInvId);
        SQL = SQL.replace("sourced_by_re", params.sourcedBy.toString().trim());
        SQL = SQL.replace("pgt_code_sourced_by_re", params.sourcedByCode.toString().trim());
        SQL = SQL.replace("total_exp_re", params.totalExp.toString().trim());
        SQL = SQL.replace("relevant_exp_re", params.relevantExp.toString().trim());
        SQL = SQL.replace("job_title_re", params.jobTitle.toString().trim());
        SQL = SQL.replace("location_re", params.location.toString().trim());
        SQL = SQL.replace("primary_skill_re", params.primarySkill.toString().trim());
        SQL = SQL.replace("prms_skill_set_id_re", params.primarySkillSetId.toString().trim());
        SQL = SQL.replace("prms_domain_id_re", params.domainId.toString().trim());
        SQL = SQL.replace("domain_name_re", params.domainName.toString().trim());
        SQL = SQL.replace("sec_skill1_re", params.secondarySkill.toString().trim());
        SQL = SQL.replace("offered_dt_re", params.offeredDate.toString().trim() + " 00:00:00");
        SQL = SQL.replace("doj_re", params.dateOfJoining.toString().trim() + " 00:00:00");
        SQL = SQL.replace("'joining_mon_re'", "MONTH('" + params.dateOfJoining.toString().trim() + "')");
        SQL = SQL.replace("'relieved_date_re'", (params.relievedDate.toString().trim() == "") ? null : "'" + params.relievedDate.toString().trim() + " 00:00:00'");
        SQL = SQL.replace("resigned_month_re", params.resigneeMonth.toString().trim());
        SQL = SQL.replace("relieving_reason_re", params.reasonForRelieving.toString().trim());
        SQL = SQL.replace("'end_client_id_re'", (params.endClientId.toString().trim() == "") ? null : "'" + params.endClientId.toString().trim() + "'");
        // SQL = SQL.replace("end_client_re", params.endClientName.toString().trim());
        // SQL = SQL.replace("'vertical_id_re'", (params.verticalId.toString().trim() == "") ? null : "'" + params.verticalId.toString().trim() + "'");
        // SQL = SQL.replace("vertical_re", params.verticalName.toString().trim());
        SQL = SQL.replace("bill_mode_re", params.billMode.toString().trim());
        SQL = SQL.replace("bill_rate_re", params.billRate.toString().trim());
        SQL = SQL.replace("billing_amt_re", params.billingAmount.toString().trim());
        SQL = SQL.replace("salary_re", params.salary.toString().trim());
        SQL = SQL.replace("bgvCost", params.bgvCost.toString().trim());
        SQL = SQL.replace("gross_margin_re", params.grossMargin.toString().trim());
        SQL = SQL.replace("po_re", params.poValue.toString().trim());
        SQL = SQL.replace("wo_re", params.woValue.toString().trim());
        SQL = SQL.replace("deployed_bldg_address_re", removeString(params.buildingAddressDeployed));
        SQL = SQL.replace("pf_re", params.pf.toString().trim());
        SQL = SQL.replace("comments_re", removeString(params.comments));
        SQL = SQL.replace("remarks_re", removeString(params.remarks));
        SQL = SQL.replace("insurance_re", params.insurance.toString().trim());
        SQL = SQL.replace("name_as_per_aadhar_re", params.nameAsPerAadhar.toString().trim());
        SQL = SQL.replace("insurance_number_re", params.insuranceNumber.toString().trim());
        SQL = SQL.replace("gratuity_re", params.gratuity.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("created_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("created_at_re", timesNow);
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("prms_rmtspoc_id_re", params.rmtSpoc ? params.rmtSpoc : '');
        SQL = SQL.replace("employee_mode_re", params.employeeMode.toString().trim());
        if (params.employeeMode.toString().trim() === 'Contract') {
            SQL = SQL.replace("approval_status_re", 'Pending');
        } else if (params.employeeMode.toString().trim() === 'Permanent') {
            SQL = SQL.replace("approval_status_re", 'Approved');
        } else {
            SQL = SQL.replace("approval_status_re", 'Approved');
        }
        var SQL2 = "";
        if (params.status.toString().trim() == 'Active') {
            SQL2 = query.updateStatusLog;
            SQL2 = SQL2.replace("previous_status_re", '');
            SQL2 = SQL2.replace("current_status_re", params.status.toString().trim());
            SQL2 = SQL2.replace("type_re", 'JOINEE');
            SQL2 = SQL2.replace("created_at_re", timesNow);
        }

        console.log("\n > createHrEdit SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'HrEdit Created Successfully' });

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > createHrEdit SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                if (SQL2 != "") {
                    SQL2 = SQL2.replace("hr_master_id_re", results.insertId);
                    db.query(SQL2, function () { });
                }
                res.status(200).json({ status: 'success', data: 'HrEdit Created Successfully' });
            }
        });

        function removeString(data, formatType = null) {
            if (data) {
                var reg = new RegExp("['\"`]", "g");
                data = data.toString();
                data = data.trim();
                data = data.replace(reg, '\\$&');
                return data;
            } else {
                return '';
            }
        }
    }
};

exports.updateClientMappingInfo = function (req, res, next) {
    var params = req.body;
    params.hrMasterId = req.params.hrMasterId;

    if (params.hrMasterId === undefined || params.isRouting === undefined || params.sourcedId === undefined || params.sourcedBy === undefined || params.sourcedByCode === undefined || params.loggedInUserId === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.hrMasterId === '' || params.isRouting === '' || params.sourcedBy === '' || params.loggedInUserId === '') {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var isRouting = params.isRouting ? 1 : 0;
        var sourcedId = removeString(params.sourcedId);
        var sourcedBy = removeString(params.sourcedBy);
        var sourcedByCode = removeString(params.sourcedByCode);
        var hrMasterId = removeString(params.hrMasterId);
        var loginUser = removeString(params.loggedInUserId);
        var costCenterInv = removeString(params.costCenterInv, 'QUOT');
        // var SQL = query.updateClientMappingInfoDetails;
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        timesNow = removeString(timesNow);

        var SQL = "UPDATE prms_pg_hr_master SET is_routing = isRouting, sourced_id = sourcedId, sourced_by = sourcedBy, pgt_code_sourced_by = sourcedByCode, updated_at = timesNow, updated_by = loginUser, cost_center_inv_id = costCenterInvRe WHERE hr_master_id = hrMasterId;";

        SQL = SQL.replace('isRouting', isRouting);
        SQL = SQL.replace('sourcedId', sourcedId);
        SQL = SQL.replace('sourcedBy', sourcedBy);
        SQL = SQL.replace('sourcedByCode', sourcedByCode);
        SQL = SQL.replace('timesNow', timesNow);
        SQL = SQL.replace('loginUser', loginUser);
        SQL = SQL.replace('hrMasterId', hrMasterId);
        SQL = SQL.replace('costCenterInvRe', costCenterInv);

        console.log("\n > updateClientMappingInfoDetails SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'ClientMappingInfo Updated Successfully' });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateClientMappingInfoDetails SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'ClientMappingInfo Updated Successfully' });
            }
        });
        function removeString(data, formatType = null) {
            if (data) {
                var reg = new RegExp("['\"`]", "g");
                data = data.toString();
                data = data.trim();
                data = data.replace(reg, '\\$&');
                return "'" + data + "'";
            } else {
                if (formatType === 'QUOT') {
                    return "NULL";
                }
                return null;
            }
        }
    }
};

exports.updatePersonalInfo = function (req, res, next) {
    var params = req.body;
    params.hrMasterId = req.params.hrMasterId;

    if (params.loggedInUserId == undefined || params.hrMasterId == undefined || params.oldemployeeId == undefined || params.employeeId == undefined || params.employeeFirstName == undefined || params.employeeMiddleName == undefined || params.employeeLastName == undefined || params.clientEmployeeId == undefined || params.employeeMode == undefined || params.mobileNo == undefined || params.alternateNo == undefined || params.personalEmailId == undefined || params.officialEmailId == undefined || params.dateOfBirth == undefined || params.gender == undefined || params.maritalStatus == undefined || params.fathersHusbandsName == undefined || params.emergencyContactName == undefined || params.emergencyContactRelation == undefined || params.emergencyContactNumber == undefined || params.permanentAddress == undefined || params.presentAddress == undefined || params.panNumber == undefined || params.educationalQualification == undefined || params.aadharCardNo == undefined || params.insurance == undefined || params.nameAsPerAadhar == undefined || params.insuranceNumber == undefined || params.gratuity == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.loggedInUserId.toString().trim() == "" || params.hrMasterId.toString().trim() == "" || params.oldemployeeId.toString().trim() == "" || params.employeeId.toString().trim() == "" || params.employeeFirstName.toString().trim() == "" || params.employeeLastName.toString().trim() == "" || params.clientEmployeeId.toString().trim() == "" || params.employeeMode.toString().trim() == "" || params.mobileNo.toString().trim() == "" || params.personalEmailId.toString().trim() == "" || params.dateOfBirth.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updatePersonalInfoDetails;

        SQL = SQL.replace("employee_id_re", params.employeeId.toString().trim());
        SQL = SQL.replace("employee_name_re", params.employeeFirstName.toString().trim() + (params.employeeMiddleName.toString().trim() ? (" " + params.employeeMiddleName.toString().trim() + " ") : " ") + params.employeeLastName.toString().trim());
        SQL = SQL.replace("employee_first_name_re", params.employeeFirstName.toString().trim());
        SQL = SQL.replace("employee_middle_name_re", params.employeeMiddleName.toString().trim());
        SQL = SQL.replace("employee_last_name_re", params.employeeLastName.toString().trim());
        SQL = SQL.replace("client_empid_re", params.clientEmployeeId.toString().trim());
        SQL = SQL.replace("mobile_no_re", params.mobileNo.toString().trim());
        SQL = SQL.replace("alternate_no_re", params.alternateNo.toString().trim());
        SQL = SQL.replace("personal_emailid_re", params.personalEmailId.toString().trim());
        SQL = SQL.replace("official_emailid_re", params.officialEmailId.toString().trim());
        SQL = SQL.replace("dob_re", params.dateOfBirth.toString().trim() + " 00:00:00");
        SQL = SQL.replace("gender_re", params.gender.toString().trim());
        SQL = SQL.replace("marital_status_re", params.maritalStatus.toString().trim());
        SQL = SQL.replace("father_husband_name_re", params.fathersHusbandsName.toString().trim());
        SQL = SQL.replace("emergency_contact_name_re", params.emergencyContactName.toString().trim());
        SQL = SQL.replace("emergency_contact_relation_re", params.emergencyContactRelation.toString().trim());
        SQL = SQL.replace("emergency_contact_number_re", params.emergencyContactNumber.toString().trim());
        SQL = SQL.replace("permanant_address_re", removeString(params.permanentAddress));
        SQL = SQL.replace("present_address_re", removeString(params.presentAddress));
        SQL = SQL.replace("pan_number_re", params.panNumber.toString().trim());
        SQL = SQL.replace("bank_ac_no_re", params.bankAc.toString().trim());
        SQL = SQL.replace("bank_ifsc_re", params.backIfsc.toString().trim());
        SQL = SQL.replace("bank_name_re", params.bankName.toString().trim());
        SQL = SQL.replace("edu_qualification_re", params.educationalQualification.toString().trim());
        SQL = SQL.replace("aadhar_number_re", params.aadharCardNo.toString().trim());
        SQL = SQL.replace("employee_mode_re", params.employeeMode.toString().trim());
        SQL = SQL.replace("insurance_re", params.insurance.toString().trim());
        SQL = SQL.replace("name_as_per_aadhar_re", params.nameAsPerAadhar.toString().trim());
        SQL = SQL.replace("insurance_number_re", params.insuranceNumber.toString().trim());
        SQL = SQL.replace("gratuity_re", params.gratuity.toString().trim());
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("hr_master_id_re", params.hrMasterId.toString().trim());
        if (params.employeeMode.toString().trim() !== params.oldemployeeMode.toString().trim()) {
            if (params.employeeMode.toString().trim() === 'Permanent') {
                let approve = ", 'approval_status' = 'Approved'";
                SQL = SQL.replace("DYN_SET", approve);
            }
        } else {
            SQL = SQL.replace("DYN_SET", '');
        }
        if (params.oldemployeeId.toString().trim() != params.employeeId.toString().trim()) {
            var SQL2 = query.updateStatusLog;
            SQL2 = SQL2.replace("hr_master_id_re", params.hrMasterId.toString().trim());
            SQL2 = SQL2.replace("previous_status_re", params.oldemployeeId.toString().trim());
            SQL2 = SQL2.replace("current_status_re", params.employeeId.toString().trim());
            SQL2 = SQL2.replace("type_re", 'PGT');
            SQL2 = SQL2.replace("created_at_re", timesNow);
            SQL += SQL2;
        }
        var checkStatus = "SELECT `hr_master_id` hrMasterId, `aadhar_number` oldAadharCardNo, `pan_number` oldPanNumber, `bank_ac_no` oldBankAc, `bank_ifsc` oldBackIfsc, `billing_amt` oldBillingAmount, `salary` oldSalary FROM `prms_pg_hr_master` WHERE `hr_master_id` = 'hrMasterIdRe';"
        checkStatus = checkStatus.replace('hrMasterIdRe', params.hrMasterId.toString().trim());

        console.log("\n > updatePersonalInfoDetails checkStatus ---> ", checkStatus);
        db.query(checkStatus, function (error, results, fields) {

            params.oldAadharCardNo = results[0].oldAadharCardNo;
            params.oldPanNumber = results[0].oldPanNumber;
            params.oldBankAc = results[0].oldBankAc;
            params.oldBackIfsc = results[0].oldBackIfsc;

            console.log("\n > updatePersonalInfoDetails SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updatePersonalInfoDetails SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    updateChangesApproval(params, function () {
                        res.status(200).json({ status: 'success', data: 'PersonalInfo Updated Successfully' });
                    });
                }
            });
        });

        function removeString(data, formatType = null) {
            if (data) {
                var reg = new RegExp("['\"`]", "g");
                data = data.toString();
                data = data.trim();
                data = data.replace(reg, '\\$&');
                return data;
            } else {
                return '';
            }
        }
    }
};

exports.updateWorkInfo = function (req, res, next) {
    var params = req.body;
    params.hrMasterId = req.params.hrMasterId;

    // params.primarySkillSetId = params.primarySkillSetId.toString().trim();
    // params.primarySkill = params.primarySkill.toString().trim();
    // params.domainId = params.domainId.toString().trim();
    // params.domainName = params.domainName.toString().trim();

    // res.status(400).json({ status: 'success', data: params });
    // return;

    if (params.loggedInUserId === undefined || params.hrMasterId === undefined || params.totalExp === undefined || params.relevantExp === undefined || params.jobTitle === undefined || params.location === undefined || params.primarySkill === undefined || params.secondarySkill === undefined || params.offeredDate === undefined || params.dateOfJoining === undefined || params.oldstatus === undefined || params.status === undefined || params.relievedDate === undefined || params.resigneeMonth === undefined || params.reasonForRelieving === undefined || params.exitType === undefined) {
        console.log("Tesrt==>", JSON.stringify(params));
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.loggedInUserId.toString().trim() == "" || params.hrMasterId.toString().trim() == "" || params.totalExp.toString().trim() == "" || params.relevantExp.toString().trim() == "" || params.offeredDate.toString().trim() == "" || params.dateOfJoining.toString().trim() == "" || params.oldstatus.toString().trim() == "" || params.status.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateWorkInfoDetails;

        SQL = SQL.replace("total_exp_re", params.totalExp.toString().trim());
        SQL = SQL.replace("relevant_exp_re", params.relevantExp.toString().trim());
        SQL = SQL.replace("job_title_re", params.jobTitle.toString().trim());
        SQL = SQL.replace("location_re", params.location.toString().trim());
        SQL = SQL.replace("primary_skill_re", params.primarySkill.toString().trim());
        SQL = SQL.replace("prms_skill_set_id_re", params.primarySkillSetId.toString().trim());
        SQL = SQL.replace("prms_domain_id_re", params.domainId.toString().trim());
        SQL = SQL.replace("domain_name_re", params.domainName.toString().trim());
        SQL = SQL.replace("sec_skill1_re", params.secondarySkill.toString().trim());
        SQL = SQL.replace("relieving_reason_re", params.reasonForRelieving.toString().trim());
        SQL = SQL.replace("status_re", params.status.toString().trim());
        SQL = SQL.replace("exit_typeid_re", params.exitType ? params.exitType.toString().trim() : '');
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("hr_master_id_re", params.hrMasterId.toString().trim());

        SQL = SQL.replace("resigned_month_re", params.resigneeMonth.toString().trim());

        // let offeredDate = params.offeredDate.toString().trim() ? moment(params.offeredDate).format("YYYY-MM-DD") : null;
        // console.log("\n > offeredDate SQL ---> ", offeredDate);

        // let dateOfJoining = params.dateOfJoining.toString().trim() ? moment(params.dateOfJoining).format("YYYY-MM-DD") : null;
        // console.log("\n > dateOfJoining SQL ---> ", dateOfJoining);

        // let relievedDate = params.relievedDate.toString().trim() ? moment(params.relievedDate).format("YYYY-MM-DD") : null;
        // console.log("\n > relievedDate SQL ---> ", relievedDate);


        SQL = SQL.replace("'offered_dt_re'", params.offeredDate ? "'" + params.offeredDate + "'" : null);
        SQL = SQL.replace("'doj_re'", params.dateOfJoining ? "'" + params.dateOfJoining + "'" : null);
        SQL = SQL.replace("'relieved_date_re'", params.relievedDate ? "'" + params.relievedDate + "'" : null);
        SQL = SQL.replace("'joining_mon_re'", params.relievedDate ? "MONTH('" + params.relievedDate + "')" : null);
        var DYNAMICSTATUS = "";
        if (params.oldstatus.toString().trim() != params.status.toString().trim()) {
            if (params.status.toString().trim() === 'Conversion') {
                DYNAMICSTATUS = ", `conversion_approval_status` = 'Pending'";
            }
        }
        SQL = SQL.replace("DYNAMICSTATUS", DYNAMICSTATUS);
        console.log("\n > updateWorkInfoDetails SQL ---> ", SQL);
        // res.status(200).json({ status: 'success', data: 'WorkInfo Updated Successfully' });
        if (params.oldstatus.toString().trim() != params.status.toString().trim()) {
            var resignStatus = ['Conversion', 'Conversion - No fee', 'End', 'Resigned', 'Absconded', 'Termination'];
            if (params.status.toString().trim() == 'Active') {
                var SQL2 = query.updateStatusLog;
                SQL2 = SQL2.replace("hr_master_id_re", params.hrMasterId.toString().trim());
                SQL2 = SQL2.replace("previous_status_re", params.oldstatus.toString().trim());
                SQL2 = SQL2.replace("current_status_re", params.status.toString().trim());
                SQL2 = SQL2.replace("type_re", 'JOINEE');
                SQL2 = SQL2.replace("created_at_re", timesNow);
                SQL += SQL2;
            } else if (resignStatus.includes(params.status.toString().trim())) {
                var SQL2 = query.updateStatusLog;
                SQL2 = SQL2.replace("hr_master_id_re", params.hrMasterId.toString().trim());
                SQL2 = SQL2.replace("previous_status_re", params.oldstatus.toString().trim());
                SQL2 = SQL2.replace("current_status_re", params.status.toString().trim());
                SQL2 = SQL2.replace("type_re", 'RESIGNEE');
                SQL2 = SQL2.replace("created_at_re", timesNow);
                SQL += SQL2;
            } else {
                var SQL2 = query.updateStatusLog;
                SQL2 = SQL2.replace("hr_master_id_re", params.hrMasterId.toString().trim());
                SQL2 = SQL2.replace("previous_status_re", params.oldstatus.toString().trim());
                SQL2 = SQL2.replace("current_status_re", params.status.toString().trim());
                SQL2 = SQL2.replace("type_re", 'OTHER');
                SQL2 = SQL2.replace("created_at_re", timesNow);
                SQL += SQL2;
            }

        }
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateWorkInfoDetails SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'WorkInfo Updated Successfully' });
            }
        });
    }
};

function updateChangesApproval(params, callBack) {
    let timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    let data = {
        hrMasterId: params.hrMasterId ? params.hrMasterId.toString().trim() : '',
        oldAadharCardNo: params.oldAadharCardNo ? params.oldAadharCardNo.toString().trim() : '',
        aadharCardNo: params.aadharCardNo ? params.aadharCardNo.toString().trim() : '',
        oldPanNumber: params.oldPanNumber ? params.oldPanNumber.toString().trim() : '',
        panNumber: params.panNumber ? params.panNumber.toString().trim() : '',
        oldBankAc: params.oldBankAc ? params.oldBankAc.toString().trim() : '',
        bankAc: params.bankAc ? params.bankAc.toString().trim() : '',
        oldBackIfsc: params.oldBackIfsc ? params.oldBackIfsc.toString().trim() : '',
        backIfsc: params.backIfsc ? params.backIfsc.toString().trim() : '',
        oldBillingAmount: params.oldBillingAmount ? params.oldBillingAmount.toString().trim() : '',
        billingAmount: params.billingAmount ? params.billingAmount.toString().trim() : '',
        oldSalary: params.oldSalary ? params.oldSalary.toString().trim() : '',
        salary: params.salary ? params.salary.toString().trim() : '',
        loginUserId: params.loggedInUserId ? params.loggedInUserId.toString().trim() : '',
    };
    console.log('\n> updateChangesApproval ---> ', JSON.stringify(data));
    var SQL = '';
    if (data.oldAadharCardNo !== data.aadharCardNo) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'AADHAR_NO');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'AADHAR_NO');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldAadharCardNo);
        tempSQL = tempSQL.replace('columnNewValueRe', data.aadharCardNo);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }
    if (data.oldPanNumber !== data.panNumber) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'PAN_NO');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'PAN_NO');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldPanNumber);
        tempSQL = tempSQL.replace('columnNewValueRe', data.panNumber);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }
    if (data.oldBankAc !== data.bankAc) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'BANK_AC');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'BANK_AC');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldBankAc);
        tempSQL = tempSQL.replace('columnNewValueRe', data.bankAc);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }
    if (data.oldBackIfsc !== data.backIfsc) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'BANK_IFSC');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'BANK_IFSC');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldBackIfsc);
        tempSQL = tempSQL.replace('columnNewValueRe', data.backIfsc);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }
    if (data.oldBillingAmount !== data.billingAmount) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'BILL_AMOUNT');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'BILL_AMOUNT');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldBillingAmount);
        tempSQL = tempSQL.replace('columnNewValueRe', data.billingAmount);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }
    if (data.oldSalary !== data.salary) {
        var tempDel = "DELETE FROM prms_pg_hr_changes_approval WHERE approval_status = '0' AND hr_master_id = 'hrMasterIdRe' AND column_name = 'columnNameRe';";
        tempDel = tempDel.replace('hrMasterIdRe', data.hrMasterId);
        tempDel = tempDel.replace('columnNameRe', 'SALARY');

        var tempSQL = query.saveHrChangesApproval;
        tempSQL = tempSQL.replace('hrMasterIdRe', data.hrMasterId);
        tempSQL = tempSQL.replace('columnNameRe', 'SALARY');
        tempSQL = tempSQL.replace('columnOldValueRe', data.oldSalary);
        tempSQL = tempSQL.replace('columnNewValueRe', data.salary);
        tempSQL = tempSQL.replace('loginUserIdRe', data.loginUserId);
        tempSQL = tempSQL.replace('timesNowRe', timesNow);
        SQL += tempDel + tempSQL;
    }

    if (SQL) {
        SQL += "UPDATE prms_pg_hr_master SET changes_approval_status = '0' WHERE hr_master_id = '" + data.hrMasterId + "';";
        console.log('\n> updateChangesApproval ---> ', SQL);
        db.query(SQL, function (error, results, fields) {
            callBack();
        });
    } else {
        callBack();
    }
}

exports.updateBillingInfo = function (req, res, next) {
    var params = req.body;
    params.hrMasterId = req.params.hrMasterId;

    if (params.hrMasterId === undefined || params.endClientId === undefined || params.endClientName === undefined || params.verticalId === undefined || params.verticalName === undefined || params.billMode === undefined || params.billRate === undefined || params.billingAmount === undefined || params.salary === undefined || params.bgvCost === undefined || params.bgvCost === undefined || params.grossMargin === undefined || params.poValue === undefined || params.woValue === undefined || params.buildingAddressDeployed === undefined || params.pf === undefined || params.comments === undefined || params.remarks === undefined || params.loggedInUserId === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    }
    else if (params.hrMasterId.toString().trim() == "" || params.billMode.toString().trim() == "" || params.billRate.toString().trim() == "" || params.billingAmount.toString().trim() == "" || params.salary.toString().trim() == "" || params.grossMargin.toString().trim() == "" || params.loggedInUserId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    }
    else {
        var SQL = query.updateBillingInfoDetails;
        SQL = SQL.replace("'end_client_id_re'", (params.endClientId.toString().trim() == "") ? null : "'" + params.endClientId.toString().trim() + "'");
        // SQL = SQL.replace("end_client_re", params.endClientName.toString().trim());
        // SQL = SQL.replace("'vertical_id_re'", (params.verticalId.toString().trim() == "") ? null : "'" + params.verticalId.toString().trim() + "'");
        // SQL = SQL.replace("vertical_re", params.verticalName.toString().trim());
        SQL = SQL.replace("bill_mode_re", params.billMode.toString().trim());
        SQL = SQL.replace("bill_rate_re", params.billRate.toString().trim());
        SQL = SQL.replace("billing_amt_re", params.billingAmount.toString().trim());
        SQL = SQL.replace("salary_re", params.salary.toString().trim());
        SQL = SQL.replace("bgvCost", params.bgvCost.toString().trim());
        SQL = SQL.replace("gross_margin_re", params.grossMargin.toString().trim());
        SQL = SQL.replace("po_re", params.poValue.toString().trim());
        SQL = SQL.replace("wo_re", params.woValue.toString().trim());
        SQL = SQL.replace("deployed_bldg_address_re", removeString(params.buildingAddressDeployed));
        SQL = SQL.replace("pf_re", params.pf.toString().trim());
        SQL = SQL.replace("comments_re", removeString(params.comments));
        SQL = SQL.replace("remarks_re", removeString(params.remarks));
        var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
        SQL = SQL.replace("updated_by_re", params.loggedInUserId.toString().trim());
        SQL = SQL.replace("updated_at_re", timesNow);
        SQL = SQL.replace("hr_master_id_re", params.hrMasterId.toString().trim());


        if (params.oldbillRate != params.billRate.toString().trim() || params.oldBillingAmount != params.billingAmount.toString().trim() || params.oldSalary != params.salary.toString().trim()) {
            var SQL2 = query.updateStatusLog;
            SQL2 = SQL2.replace("hr_master_id_re", params.hrMasterId.toString().trim());
            SQL2 = SQL2.replace("previous_status_re", '');
            SQL2 = SQL2.replace("current_status_re", '');
            SQL2 = SQL2.replace("type_re", 'SALARY');
            SQL2 = SQL2.replace("created_at_re", timesNow);
            SQL += SQL2;
        }

        var checkStatus = "SELECT `hr_master_id` hrMasterId, `billing_amt` oldBillingAmount, `salary` oldSalary FROM `prms_pg_hr_master` WHERE `hr_master_id` = 'hrMasterIdRe';"
        checkStatus = checkStatus.replace('hrMasterIdRe', params.hrMasterId.toString().trim());

        console.log("\n > updateBillingInfoDetails checkStatus ---> ", checkStatus);
        db.query(checkStatus, function (error, results, fields) {

            params.oldBillingAmount = results[0].oldBillingAmount;
            params.oldSalary = results[0].oldSalary;

            console.log("\n > updateBillingInfoDetails SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateBillingInfoDetails SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    updateChangesApproval(params, function () {
                        res.status(200).json({ status: 'success', data: 'BillingInfo Updated Successfully' });
                    });
                }
            });
        });
    }

    function removeString(data, formatType = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return data;
        } else {
            return '';
        }
    }
};

exports.getClientMappingInfo = function (req, res, next) {
    var SQL = query.getClientMappingInfo;
    var totalSQL = query.getClientMappingInfo;
    totalSQL = totalSQL.replace('LIMITRE', '');
    totalSQL = totalSQL.replace('ORDERBY', '');
    SQL += totalSQL;

    var params = req.body;
    var month = params.month ? params.month.toString().trim() : '';
    var year = params.year ? params.year.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var lazyload = params.lazyload;
    var WHERE = "";
    var FILTERWHERE = "";
    var ORDERBY = "";
    var DEFORDERBY = "ORDER BY clientCostMsp,employeeName ASC";

    if (month && year) {
        let serviceMonth = year + "-" + month;
        WHERE = "WHERE (DATE_FORMAT(hr.doj,'%Y-%m') <= 'SERVICEMONTH') AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL) AND hr.status NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed')";
        WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        if (searchBy && searchValue) {
            if (searchBy == "empName") {
                WHERE += " AND hr.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientName") {
                WHERE += " AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.employee_id IN (" + data + ")";
                // WHERE += " AND AND hr.employee_id = '" + searchValue + "'";
            } else if (searchBy == "personalEmail") {
                WHERE += " AND hr.personal_emailid LIKE '%" + searchValue + "%'";
            } else if (searchBy == "officialEmail") {
                WHERE += " AND hr.official_emailid LIKE '%" + searchValue + "%'";
            }
        }
    } else if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }

    FILTERWHERE += (lazyload.filters.approvalStatus != undefined) ? ((lazyload.filters.approvalStatus.value != null && lazyload.filters.approvalStatus.value != "") ? "AND approvalStatus LIKE'%" + lazyload.filters.approvalStatus.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedAt != undefined) ? ((lazyload.filters.approvedAt.value != null && lazyload.filters.approvedAt.value != "") ? "AND approvedAt LIKE'%" + lazyload.filters.approvedAt.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedBy != undefined) ? ((lazyload.filters.approvedBy.value != null && lazyload.filters.approvedBy.value != "") ? "AND approvedBy LIKE'%" + lazyload.filters.approvedBy.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.businessManager != undefined) ? ((lazyload.filters.businessManager.value != null && lazyload.filters.businessManager.value != "") ? "AND businessManager LIKE'%" + lazyload.filters.businessManager.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.businessManagerCode != undefined) ? ((lazyload.filters.businessManagerCode.value != null && lazyload.filters.businessManagerCode.value != "") ? "AND businessManagerCode ='" + lazyload.filters.businessManagerCode.value.toString().trim() + "'" : "") : "";
    FILTERWHERE += (lazyload.filters.clientCostMsp != undefined) ? ((lazyload.filters.clientCostMsp.value != null && lazyload.filters.clientCostMsp.value != "") ? "AND clientCostMsp LIKE'%" + lazyload.filters.clientCostMsp.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.clientEmployeeId != undefined) ? ((lazyload.filters.clientEmployeeId.value != null && lazyload.filters.clientEmployeeId.value != "") ? "AND clientEmployeeId LIKE'%" + lazyload.filters.clientEmployeeId.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdAt != undefined) ? ((lazyload.filters.createdAt.value != null && lazyload.filters.createdAt.value != "") ? "AND createdAt LIKE'%" + lazyload.filters.createdAt.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdBy != undefined) ? ((lazyload.filters.createdBy.value != null && lazyload.filters.createdBy.value != "") ? "AND createdBy LIKE'%" + lazyload.filters.createdBy.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeId != undefined) ? ((lazyload.filters.employeeId.value != null && lazyload.filters.employeeId.value != "") ? "AND employeeId ='" + lazyload.filters.employeeId.value.toString().trim() + "'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeName != undefined) ? ((lazyload.filters.employeeName.value != null && lazyload.filters.employeeName.value != "") ? "AND employeeName LIKE'%" + lazyload.filters.employeeName.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.financeSpoc != undefined) ? ((lazyload.filters.financeSpoc.value != null && lazyload.filters.financeSpoc.value != "") ? "AND financeSpoc LIKE'%" + lazyload.filters.financeSpoc.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.hrSpoc != undefined) ? ((lazyload.filters.hrSpoc.value != null && lazyload.filters.hrSpoc.value != "") ? "AND hrSpoc LIKE'%" + lazyload.filters.hrSpoc.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.personalEmailId != undefined) ? ((lazyload.filters.personalEmailId.value != null && lazyload.filters.personalEmailId.value != "") ? "AND personalEmailId LIKE'%" + lazyload.filters.personalEmailId.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.plHead != undefined) ? ((lazyload.filters.plHead.value != null && lazyload.filters.plHead.value != "") ? "AND plHead LIKE'%" + lazyload.filters.plHead.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.plHeadCode != undefined) ? ((lazyload.filters.plHeadCode.value != null && lazyload.filters.plHeadCode.value != "") ? "AND plHeadCode ='" + lazyload.filters.plHeadCode.value.toString().trim() + "'" : "") : "";
    FILTERWHERE += (lazyload.filters.rmtSpoc != undefined) ? ((lazyload.filters.rmtSpoc.value != null && lazyload.filters.rmtSpoc.value != "") ? "AND rmtSpoc LIKE'%" + lazyload.filters.rmtSpoc.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.sourcedBy != undefined) ? ((lazyload.filters.sourcedBy.value != null && lazyload.filters.sourcedBy.value != "") ? "AND sourcedBy LIKE'%" + lazyload.filters.sourcedBy.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.sourcedByCode != undefined) ? ((lazyload.filters.sourcedByCode.value != null && lazyload.filters.sourcedByCode.value != "") ? "AND sourcedByCode ='" + lazyload.filters.sourcedByCode.value.toString().trim() + "'" : "") : "";
    FILTERWHERE += (lazyload.filters.updatedAt != undefined) ? ((lazyload.filters.updatedAt.value != null && lazyload.filters.updatedAt.value != "") ? "AND updatedAt LIKE'%" + lazyload.filters.updatedAt.value.toString().trim() + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.updatedBy != undefined) ? ((lazyload.filters.updatedBy.value != null && lazyload.filters.updatedBy.value != "") ? "AND updatedBy LIKE'%" + lazyload.filters.updatedBy.value.toString().trim() + "%'" : "") : "";

    ORDERBY = (lazyload.sortField != undefined) ? ((lazyload.sortField != null && lazyload.sortField != "") ? "ORDER BY " + lazyload.sortField + " " + ((lazyload.sortOrder == 1) ? "ASC" : "DESC") : DEFORDERBY) : DEFORDERBY;

    SQL = SQL.replace(/CONDITIONALWHERE/g, WHERE);
    SQL = SQL.replace(/FILTERWHERE/g, FILTERWHERE);
    SQL = SQL.replace(/ORDERBY/g, ORDERBY);
    SQL = SQL.replace(/LIMITRE/g, 'LIMIT ' + lazyload.first + ',' + lazyload.rows);

    console.log("\n > getClientMappingInfo SQL ---> ", SQL);
    var pre_query = new Date().getTime();
    db.query(SQL, function (error, results, fields) {
    var post_query = new Date().getTime();
    // calculate the duration in seconds
    var duration = (post_query - pre_query) / 1000;
    console.log("\n > getClientMappingInfo Duration ---> ", duration);
        if (error) {
            console.log("\n > getClientMappingInfo SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: { "dataSource": results[0], "totalRecords": results[1].length } });
        }
    });
};


exports.getPersonalInfo = function (req, res, next) {
    var SQL = query.getPersonalInfo;
    var totalSQL = query.getPersonalInfo;
    totalSQL = totalSQL.replace('LIMITRE', '');
    totalSQL = totalSQL.replace('ORDERBY', '');
    SQL += totalSQL;

    var params = req.body;
    var month = params.month ? params.month.toString().trim() : '';
    var year = params.year ? params.year.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var lazyload = params.lazyload;

    var WHERE = "";
    var FILTERWHERE = "";
    var ORDERBY = "";
    var DEFORDERBY = "ORDER BY clientCostMsp,employeeName ASC";

    if (month && year) {
        let serviceMonth = year + "-" + month;
        WHERE = "WHERE (DATE_FORMAT(hr.doj,'%Y-%m') <= 'SERVICEMONTH') AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL) AND hr.status NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed')";
        WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        if (searchBy && searchValue) {
            if (searchBy == "empName") {
                WHERE += " AND hr.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientName") {
                WHERE += " AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.employee_id IN (" + data + ")";
                // WHERE += " AND AND hr.employee_id = '" + searchValue + "'";
            } else if (searchBy == "personalEmail") {
                WHERE += " AND hr.personal_emailid LIKE '%" + searchValue + "%'";
            } else if (searchBy == "officialEmail") {
                WHERE += " AND hr.official_emailid LIKE '%" + searchValue + "%'";
            }
        }
    } else if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }
    FILTERWHERE += (lazyload.filters.approvalStatus != undefined) ? ((lazyload.filters.approvalStatus.value != null && lazyload.filters.approvalStatus.value != "") ? "AND approvalStatus LIKE'%" + lazyload.filters.approvalStatus.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedAt != undefined) ? ((lazyload.filters.approvedAt.value != null && lazyload.filters.approvedAt.value != "") ? "AND approvedAt LIKE'%" + lazyload.filters.approvedAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedBy != undefined) ? ((lazyload.filters.approvedBy.value != null && lazyload.filters.approvedBy.value != "") ? "AND approvedBy LIKE'%" + lazyload.filters.approvedBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.clientCostMsp != undefined) ? ((lazyload.filters.clientCostMsp.value != null && lazyload.filters.clientCostMsp.value != "") ? "AND clientCostMsp LIKE'%" + lazyload.filters.clientCostMsp.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.clientEmployeeId != undefined) ? ((lazyload.filters.clientEmployeeId.value != null && lazyload.filters.clientEmployeeId.value != "") ? "AND clientEmployeeId LIKE'%" + lazyload.filters.clientEmployeeId.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdAt != undefined) ? ((lazyload.filters.createdAt.value != null && lazyload.filters.createdAt.value != "") ? "AND createdAt LIKE'%" + lazyload.filters.createdAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdBy != undefined) ? ((lazyload.filters.createdBy.value != null && lazyload.filters.createdBy.value != "") ? "AND createdBy LIKE'%" + lazyload.filters.createdBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeId != undefined) ? ((lazyload.filters.employeeId.value != null && lazyload.filters.employeeId.value != "") ? "AND employeeId LIKE'%" + lazyload.filters.employeeId.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.alternateNo != undefined) ? ((lazyload.filters.alternateNo.value != null && lazyload.filters.alternateNo.value != "") ? "AND alternateNo LIKE'%" + lazyload.filters.alternateNo.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.backIfsc != undefined) ? ((lazyload.filters.backIfsc.value != null && lazyload.filters.backIfsc.value != "") ? "AND backIfsc LIKE'%" + lazyload.filters.backIfsc.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.bankAc != undefined) ? ((lazyload.filters.bankAc.value != null && lazyload.filters.bankAc.value != "") ? "AND bankAc LIKE'%" + lazyload.filters.bankAc.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.dateOfBirth != undefined) ? ((lazyload.filters.dateOfBirth.value != null && lazyload.filters.dateOfBirth.value != "") ? "AND dateOfBirth LIKE'%" + lazyload.filters.dateOfBirth.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.educationalQualification != undefined) ? ((lazyload.filters.educationalQualification.value != null && lazyload.filters.educationalQualification.value != "") ? "AND educationalQualification LIKE'%" + lazyload.filters.educationalQualification.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.emergencyContactName != undefined) ? ((lazyload.filters.emergencyContactName.value != null && lazyload.filters.emergencyContactName.value != "") ? "AND emergencyContactName LIKE'%" + lazyload.filters.emergencyContactName.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.emergencyContactRelation != undefined) ? ((lazyload.filters.emergencyContactRelation.value != null && lazyload.filters.emergencyContactRelation.value != "") ? "AND emergencyContactRelation LIKE'%" + lazyload.filters.emergencyContactRelation.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeName != undefined) ? ((lazyload.filters.employeeName.value != null && lazyload.filters.employeeName.value != "") ? "AND employeeName LIKE'%" + lazyload.filters.employeeName.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.fathersHusbandsName != undefined) ? ((lazyload.filters.fathersHusbandsName.value != null && lazyload.filters.fathersHusbandsName.value != "") ? "AND fathersHusbandsName LIKE'%" + lazyload.filters.fathersHusbandsName.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.gender != undefined) ? ((lazyload.filters.gender.value != null && lazyload.filters.gender.value != "") ? "AND gender LIKE'%" + lazyload.filters.gender.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.gratuity != undefined) ? ((lazyload.filters.gratuity.value != null && lazyload.filters.gratuity.value != "") ? "AND gratuity LIKE'%" + lazyload.filters.gratuity.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.insurance != undefined) ? ((lazyload.filters.insurance.value != null && lazyload.filters.insurance.value != "") ? "AND insurance LIKE'%" + lazyload.filters.insurance.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.insuranceNumber != undefined) ? ((lazyload.filters.insuranceNumber.value != null && lazyload.filters.insuranceNumber.value != "") ? "AND insuranceNumber LIKE'%" + lazyload.filters.insuranceNumber.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.maritalStatus != undefined) ? ((lazyload.filters.maritalStatus.value != null && lazyload.filters.maritalStatus.value != "") ? "AND maritalStatus LIKE'%" + lazyload.filters.maritalStatus.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.mobileNo != undefined) ? ((lazyload.filters.mobileNo.value != null && lazyload.filters.mobileNo.value != "") ? "AND mobileNo LIKE'%" + lazyload.filters.mobileNo.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.nameAsPerAadhar != undefined) ? ((lazyload.filters.nameAsPerAadhar.value != null && lazyload.filters.nameAsPerAadhar.value != "") ? "AND nameAsPerAadhar LIKE'%" + lazyload.filters.nameAsPerAadhar.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.panNumber != undefined) ? ((lazyload.filters.panNumber.value != null && lazyload.filters.panNumber.value != "") ? "AND panNumber LIKE'%" + lazyload.filters.panNumber.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.permanentAddress != undefined) ? ((lazyload.filters.permanentAddress.value != null && lazyload.filters.permanentAddress.value != "") ? "AND permanentAddress LIKE'%" + lazyload.filters.permanentAddress.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.personalEmailId != undefined) ? ((lazyload.filters.personalEmailId.value != null && lazyload.filters.personalEmailId.value != "") ? "AND personalEmailId LIKE'%" + lazyload.filters.personalEmailId.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.presentAddress != undefined) ? ((lazyload.filters.presentAddress.value != null && lazyload.filters.presentAddress.value != "") ? "AND presentAddress LIKE'%" + lazyload.filters.presentAddress.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.officialEmailId != undefined) ? ((lazyload.filters.officialEmailId.value != null && lazyload.filters.officialEmailId.value != "") ? "AND officialEmailId LIKE'%" + lazyload.filters.officialEmailId.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.bankName != undefined) ? ((lazyload.filters.bankName.value != null && lazyload.filters.bankName.value != "") ? "AND bankName LIKE'%" + lazyload.filters.bankName.value + "%'" : "") : "";
    
    ORDERBY = (lazyload.sortField != undefined) ? ((lazyload.sortField != null && lazyload.sortField != "") ? "ORDER BY " + lazyload.sortField + " " + ((lazyload.sortOrder == 1) ? "ASC" : "DESC") : DEFORDERBY) : DEFORDERBY;

    SQL = SQL.replace(/CONDITIONALWHERE/g, WHERE);
    SQL = SQL.replace(/FILTERWHERE/g, FILTERWHERE);
    SQL = SQL.replace(/ORDERBY/g, ORDERBY);
    SQL = SQL.replace(/LIMITRE/g, 'LIMIT ' + lazyload.first + ',' + lazyload.rows);

    console.log("\n > getPersonalinfo SQL ---> ", SQL);
    var pre_query = new Date().getTime();
    db.query(SQL, function (error, results, fields) {
        var post_query = new Date().getTime();
        // calculate the duration in seconds
        var duration = (post_query - pre_query) / 1000;
        console.log("\n > getPersonalinfo Duration ---> ", duration);
        if (error) {
            console.log("\n > getPersonalinfo SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: { "dataSource": results[0], "totalRecords": results[1].length } });
        }
    });
};


exports.getWorkInfo = function (req, res, next) {
    var SQL = query.workInfo;
    var totalSQL = query.workInfo;
    totalSQL = totalSQL.replace('LIMITRE', '');
    totalSQL = totalSQL.replace('ORDERBY', '');
    SQL += totalSQL;

    var params = req.body;
    var month = params.month ? params.month.toString().trim() : '';
    var year = params.year ? params.year.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var lazyload = params.lazyload;
    var WHERE = "";
    var FILTERWHERE = "";
    var ORDERBY = "";
    var DEFORDERBY = "ORDER BY clientCostMsp,employeeName ASC";

    if (month && year) {
        let serviceMonth = year + "-" + month;
        WHERE = "WHERE (DATE_FORMAT(hr.doj,'%Y-%m') <= 'SERVICEMONTH') AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL) AND hr.status NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed')";
        WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        if (searchBy && searchValue) {
            if (searchBy == "empName") {
                WHERE += " AND hr.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientName") {
                WHERE += " AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.employee_id IN (" + data + ")";
                // WHERE += " AND AND hr.employee_id = '" + searchValue + "'";
            } else if (searchBy == "personalEmail") {
                WHERE += " AND hr.personal_emailid LIKE '%" + searchValue + "%'";
            } else if (searchBy == "officialEmail") {
                WHERE += " AND hr.official_emailid LIKE '%" + searchValue + "%'";
            }
        }
    } else if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }

    FILTERWHERE += (lazyload.filters.approvalStatus != undefined) ? ((lazyload.filters.approvalStatus.value != null && lazyload.filters.approvalStatus.value != "") ? "AND approvalStatus LIKE'%" + lazyload.filters.approvalStatus.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedAt != undefined) ? ((lazyload.filters.approvedAt.value != null && lazyload.filters.approvedAt.value != "") ? "AND approvedAt LIKE'%" + lazyload.filters.approvedAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.approvedBy != undefined) ? ((lazyload.filters.approvedBy.value != null && lazyload.filters.approvedBy.value != "") ? "AND approvedBy LIKE'%" + lazyload.filters.approvedBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.clientCostMsp != undefined) ? ((lazyload.filters.clientCostMsp.value != null && lazyload.filters.clientCostMsp.value != "") ? "AND clientCostMsp LIKE'%" + lazyload.filters.clientCostMsp.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.conversionApprovedAt != undefined) ? ((lazyload.filters.conversionApprovedAt.value != null && lazyload.filters.conversionApprovedAt.value != "") ? "AND conversionApprovedAt LIKE'%" + lazyload.filters.conversionApprovedAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.conversionApprovedBy != undefined) ? ((lazyload.filters.conversionApprovedBy.value != null && lazyload.filters.conversionApprovedBy.value != "") ? "AND conversionApprovedBy LIKE'%" + lazyload.filters.conversionApprovedBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.conversionStatus != undefined) ? ((lazyload.filters.conversionStatus.value != null && lazyload.filters.conversionStatus.value != "") ? "AND conversionStatus LIKE'%" + lazyload.filters.conversionStatus.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdAt != undefined) ? ((lazyload.filters.createdAt.value != null && lazyload.filters.createdAt.value != "") ? "AND createdAt LIKE'%" + lazyload.filters.createdAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.createdBy != undefined) ? ((lazyload.filters.createdBy.value != null && lazyload.filters.createdBy.value != "") ? "AND createdBy LIKE'%" + lazyload.filters.createdBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeId != undefined) ? ((lazyload.filters.employeeId.value != null && lazyload.filters.employeeId.value != "") ? "AND employeeId LIKE'%" + lazyload.filters.employeeId.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeMode != undefined) ? ((lazyload.filters.employeeMode.value != null && lazyload.filters.employeeMode.value != "") ? "AND employeeMode LIKE'%" + lazyload.filters.employeeMode.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.employeeName != undefined) ? ((lazyload.filters.employeeName.value != null && lazyload.filters.employeeName.value != "") ? "AND employeeName LIKE'%" + lazyload.filters.employeeName.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.exitType != undefined) ? ((lazyload.filters.exitType.value != null && lazyload.filters.exitType.value != "") ? "AND exitType LIKE'%" + lazyload.filters.exitType.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.jobTitle != undefined) ? ((lazyload.filters.jobTitle.value != null && lazyload.filters.jobTitle.value != "") ? "AND jobTitle LIKE'%" + lazyload.filters.jobTitle.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.location != undefined) ? ((lazyload.filters.location.value != null && lazyload.filters.location.value != "") ? "AND location LIKE'%" + lazyload.filters.location.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.primarySkill != undefined) ? ((lazyload.filters.primarySkill.value != null && lazyload.filters.primarySkill.value != "") ? "AND primarySkill LIKE'%" + lazyload.filters.primarySkill.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.reasonForRelieving != undefined) ? ((lazyload.filters.reasonForRelieving.value != null && lazyload.filters.reasonForRelieving.value != "") ? "AND reasonForRelieving LIKE'%" + lazyload.filters.reasonForRelieving.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.relevantExp != undefined) ? ((lazyload.filters.relevantExp.value != null && lazyload.filters.relevantExp.value != "") ? "AND relevantExp LIKE'%" + lazyload.filters.relevantExp.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.resigneeMonth != undefined) ? ((lazyload.filters.resigneeMonth.value != null && lazyload.filters.resigneeMonth.value != "") ? "AND resigneeMonth LIKE'%" + lazyload.filters.resigneeMonth.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.secondarySkill != undefined) ? ((lazyload.filters.secondarySkill.value != null && lazyload.filters.secondarySkill.value != "") ? "AND secondarySkill LIKE'%" + lazyload.filters.secondarySkill.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.status != undefined) ? ((lazyload.filters.status.value != null && lazyload.filters.status.value != "") ? "AND status LIKE'%" + lazyload.filters.status.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.totalExp != undefined) ? ((lazyload.filters.totalExp.value != null && lazyload.filters.totalExp.value != "") ? "AND totalExp LIKE'%" + lazyload.filters.totalExp.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.updatedAt != undefined) ? ((lazyload.filters.updatedAt.value != null && lazyload.filters.updatedAt.value != "") ? "AND updatedAt LIKE'%" + lazyload.filters.updatedAt.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters.updatedBy != undefined) ? ((lazyload.filters.updatedBy.value != null && lazyload.filters.updatedBy.value != "") ? "AND updatedBy LIKE'%" + lazyload.filters.updatedBy.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters._dateOfJoining != undefined) ? ((lazyload.filters._dateOfJoining.value != null && lazyload.filters._dateOfJoining.value != "") ? "AND dateOfJoining LIKE'%" + lazyload.filters._dateOfJoining.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters._offeredDate != undefined) ? ((lazyload.filters._offeredDate.value != null && lazyload.filters._offeredDate.value != "") ? "AND offeredDate LIKE'%" + lazyload.filters._offeredDate.value + "%'" : "") : "";
    FILTERWHERE += (lazyload.filters._relievedDate != undefined) ? ((lazyload.filters._relievedDate.value != null && lazyload.filters._relievedDate.value != "") ? "AND relievedDate LIKE'%" + lazyload.filters._relievedDate.value + "%'" : "") : "";

    ORDERBY = (lazyload.sortField != undefined) ? ((lazyload.sortField != null && lazyload.sortField != "") ? "ORDER BY " + lazyload.sortField.replace("_", "") + " " + ((lazyload.sortOrder == 1) ? "ASC" : "DESC") : DEFORDERBY) : DEFORDERBY;

    SQL = SQL.replace(/CONDITIONALWHERE/g, WHERE);
    SQL = SQL.replace(/FILTERWHERE/g, FILTERWHERE);
    SQL = SQL.replace(/ORDERBY/g, ORDERBY);
    SQL = SQL.replace(/LIMITRE/g, 'LIMIT ' + lazyload.first + ',' + lazyload.rows);

    console.log("\n > getworkinfo SQL ---> ", SQL);
    var pre_query = new Date().getTime();
    db.query(SQL, function (error, results, fields) {
        var post_query = new Date().getTime();
        // calculate the duration in seconds
        var duration = (post_query - pre_query) / 1000;
        console.log("\n > getworkinfo Duration ---> ", duration);
        if (error) {
            console.log("\n > getworkinfo SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: { "dataSource": results[0], "totalRecords": results[1].length } });
        }
    });
};

exports.getBillingInfo = function (req, res, next) {
    var SQL = query.billingInfo;
    var totalSQL = query.billingInfo;
    totalSQL = totalSQL.replace('LIMITRE', '');
    totalSQL = totalSQL.replace('ORDERBY', '');
    SQL += totalSQL;

    var params = req.body;
    var month = params.month ? params.month.toString().trim() : '';
    var year = params.year ? params.year.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var lazyload = params.lazyload;
    var WHERE = "";
    var FILTERWHERE = "";
    var ORDERBY = "";
    var DEFORDERBY = "ORDER BY clientCostMsp,employeeName ASC";

    if (month && year) {
        let serviceMonth = year + "-" + month;
        WHERE = "WHERE (DATE_FORMAT(hr.doj,'%Y-%m') <= 'SERVICEMONTH') AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL) AND hr.status NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed')";
        WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        if (searchBy && searchValue) {
            if (searchBy == "empName") {
                WHERE += " AND hr.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientName") {
                WHERE += " AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.employee_id IN (" + data + ")";
                // WHERE += " AND AND hr.employee_id = '" + searchValue + "'";
            } else if (searchBy == "personalEmail") {
                WHERE += " AND hr.personal_emailid LIKE '%" + searchValue + "%'";
            } else if (searchBy == "officialEmail") {
                WHERE += " AND hr.official_emailid LIKE '%" + searchValue + "%'";
            }
        }
    } else if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }

    FILTERWHERE += (lazyload.filters.approvalStatus != undefined)? ((lazyload.filters.approvalStatus.value != null && lazyload.filters.approvalStatus.value != "")?"AND approvalStatus LIKE'%"+lazyload.filters.approvalStatus.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.approvedAt != undefined)? ((lazyload.filters.approvedAt.value != null && lazyload.filters.approvedAt.value != "")?"AND approvedAt LIKE'%"+lazyload.filters.approvedAt.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.approvedBy != undefined)? ((lazyload.filters.approvedBy.value != null && lazyload.filters.approvedBy.value != "")?"AND approvedBy LIKE'%"+lazyload.filters.approvedBy.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.billMode != undefined)? ((lazyload.filters.billMode.value != null && lazyload.filters.billMode.value != "")?"AND billMode LIKE'%"+lazyload.filters.billMode.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.billRate != undefined)? ((lazyload.filters.billRate.value != null && lazyload.filters.billRate.value != "")?"AND billRate LIKE'%"+lazyload.filters.billRate.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.billingAmount != undefined)? ((lazyload.filters.billingAmount.value != null && lazyload.filters.billingAmount.value != "")?"AND billingAmount LIKE'%"+lazyload.filters.billingAmount.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.buildingAddressDeployed != undefined)? ((lazyload.filters.buildingAddressDeployed.value != null && lazyload.filters.buildingAddressDeployed.value != "")?"AND buildingAddressDeployed LIKE'%"+lazyload.filters.buildingAddressDeployed.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.clientCostMsp != undefined)? ((lazyload.filters.clientCostMsp.value != null && lazyload.filters.clientCostMsp.value != "")?"AND clientCostMsp LIKE'%"+lazyload.filters.clientCostMsp.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.comments != undefined)? ((lazyload.filters.comments.value != null && lazyload.filters.comments.value != "")?"AND comments LIKE'%"+lazyload.filters.comments.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.createdAt != undefined)? ((lazyload.filters.createdAt.value != null && lazyload.filters.createdAt.value != "")?"AND createdAt LIKE'%"+lazyload.filters.createdAt.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.createdBy != undefined)? ((lazyload.filters.createdBy.value != null && lazyload.filters.createdBy.value != "")?"AND createdBy LIKE'%"+lazyload.filters.createdBy.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.employeeId != undefined)? ((lazyload.filters.employeeId.value != null && lazyload.filters.employeeId.value != "")?"AND employeeId LIKE'%"+lazyload.filters.employeeId.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.employeeMode != undefined)? ((lazyload.filters.employeeMode.value != null && lazyload.filters.employeeMode.value != "")?"AND employeeMode LIKE'%"+lazyload.filters.employeeMode.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.employeeName != undefined)? ((lazyload.filters.employeeName.value != null && lazyload.filters.employeeName.value != "")?"AND employeeName LIKE'%"+lazyload.filters.employeeName.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.endClientName != undefined)? ((lazyload.filters.endClientName.value != null && lazyload.filters.endClientName.value != "")?"AND endClientName LIKE'%"+lazyload.filters.endClientName.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.grossMargin != undefined)? ((lazyload.filters.grossMargin.value != null && lazyload.filters.grossMargin.value != "")?"AND grossMargin LIKE'%"+lazyload.filters.grossMargin.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.personalEmailId != undefined)? ((lazyload.filters.personalEmailId.value != null && lazyload.filters.personalEmailId.value != "")?"AND personalEmailId LIKE'%"+lazyload.filters.personalEmailId.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.pf != undefined)? ((lazyload.filters.pf.value != null && lazyload.filters.pf.value != "")?"AND pf LIKE'%"+lazyload.filters.pf.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.poValue != undefined)? ((lazyload.filters.poValue.value != null && lazyload.filters.poValue.value != "")?"AND poValue LIKE'%"+lazyload.filters.poValue.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.remarks != undefined)? ((lazyload.filters.remarks.value != null && lazyload.filters.remarks.value != "")?"AND remarks LIKE'%"+lazyload.filters.remarks.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.salary != undefined)? ((lazyload.filters.salary.value != null && lazyload.filters.salary.value != "")?"AND salary LIKE'%"+lazyload.filters.salary.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.updatedAt != undefined)? ((lazyload.filters.updatedAt.value != null && lazyload.filters.updatedAt.value != "")?"AND updatedAt LIKE'%"+lazyload.filters.updatedAt.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.updatedBy != undefined)? ((lazyload.filters.updatedBy.value != null && lazyload.filters.updatedBy.value != "")?"AND updatedBy LIKE'%"+lazyload.filters.updatedBy.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.verticalName != undefined)? ((lazyload.filters.verticalName.value != null && lazyload.filters.verticalName.value != "")?"AND verticalName LIKE'%"+lazyload.filters.verticalName.value+"%'":"") : "";
    FILTERWHERE += (lazyload.filters.woValue != undefined)? ((lazyload.filters.woValue.value != null && lazyload.filters.woValue.value != "")?"AND woValue LIKE'%"+lazyload.filters.woValue.value+"%'":"") : "";

    ORDERBY = (lazyload.sortField != undefined)? ((lazyload.sortField != null && lazyload.sortField != "")?"ORDER BY "+lazyload.sortField.replace("_","")+" "+((lazyload.sortOrder ==1)?"ASC": "DESC"):DEFORDERBY) : DEFORDERBY;

    SQL = SQL.replace(/CONDITIONALWHERE/g, WHERE);
    SQL = SQL.replace(/FILTERWHERE/g, FILTERWHERE);
    SQL = SQL.replace(/ORDERBY/g, ORDERBY);
    SQL = SQL.replace(/LIMITRE/g, 'LIMIT ' + lazyload.first + ',' + lazyload.rows);

    console.log("\n > getBillingInfo SQL ---> ", SQL);
    var pre_query = new Date().getTime();
    db.query(SQL, function (error, results, fields) {
      var post_query = new Date().getTime();
        // calculate the duration in seconds
        var duration = (post_query - pre_query) / 1000;
        console.log("\n > getBillingInfo Duration ---> ", duration);
        if (error) {
            console.log("\n > getBillingInfo SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: { "dataSource": results[0], "totalRecords": results[1].length } });
        }
    });
};

exports.getHrEditForExport = function (req, res, next) {
    var SQL = query.getHrEditForExport;
    var params = req.body;
    var month = params.month ? params.month.toString().trim() : '';
    var year = params.year ? params.year.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var WHERE = "";
    if (month && year) {
        let serviceMonth = year + "-" + month;
        WHERE = "WHERE (DATE_FORMAT(hr.doj,'%Y-%m') <= 'SERVICEMONTH') AND (DATE_FORMAT(hr.relieved_date,'%Y-%m') >= 'SERVICEMONTH' OR hr.relieved_date IS NULL) AND hr.status NOT IN ('No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed')";
        WHERE = WHERE.replace(/SERVICEMONTH/g, serviceMonth);
        if (searchBy && searchValue) {
            if (searchBy == "empName") {
                WHERE += " AND hr.employee_name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientName") {
                WHERE += " AND cl.name LIKE '%" + searchValue + "%'";
            } else if (searchBy == "clientEmpId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.client_empid IN (" + data + ")";
            } else if (searchBy == "empId") {
                searchValue = searchValue.toString().replace(/,/g, ' ');
                searchValue = searchValue.split(' ');
                var data = '';
                searchValue.forEach(e => {
                    if (e) {
                        data += "'" + e + "', ";
                    }
                });
                data += "''";
                console.log('\n> search value ---> ', data);
                WHERE += " AND hr.employee_id IN (" + data + ")";
                // WHERE += " AND AND hr.employee_id = '" + searchValue + "'";
            } else if (searchBy == "personalEmail") {
                WHERE += " AND hr.personal_emailid LIKE '%" + searchValue + "%'";
            } else if (searchBy == "officialEmail") {
                WHERE += " AND hr.official_emailid LIKE '%" + searchValue + "%'";
            }
        }
    } else if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }
    SQL = SQL.replace('CONDITIONALWHERE', WHERE);
    console.log("\n > getHrEditForExport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getHrEditForExport SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            console.log('\n -----> ', JSON.stringify(results[0]));
            res.status(200).json({ status: 'success', data: results });
        }
    });
};


exports.getHrInfo = function (req, res, next) {
    var SQL = query.getHrInfo;
    var params = req.body;
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var WHERE = "";

    if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }
    SQL = SQL.replace('CONDITIONALWHERE', WHERE);
    console.log("\n > getHrInfo SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getHrInfo SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.getHrPending = function (req, res, next) {
    var SQL = query.getHrPending;
    var params = req.body;
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var WHERE = "";

    if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }
    SQL = SQL.replace('CONDITIONALWHERE', WHERE);
    console.log("\n > getHrPending SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getHrPending SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.updateHrPending = function (req, res, next) {
    var params = req.body;
    if (params.hrMasterId === undefined || params.fName === undefined || params.mName === undefined || params.lName === undefined || params.panNumber === undefined || params.bankAc === undefined || params.bankIfsc === undefined || params.aadharCardNo === undefined || params.billAmount === undefined || params.salary === undefined || params.hrStatus === undefined || params.loginUserId === undefined || params.dateOfBirth === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var hrMasterId = params.hrMasterId.toString().trim();
        var fName = params.fName ? params.fName.toString().trim() : '';
        var mName = params.mName ? (' ' + params.mName.toString().trim()) : '';
        var lName = params.lName ? (' ' + params.lName.toString().trim()) : '';
        var eName = fName + mName + lName;
        var dateOfBirth = params.dateOfBirth ? moment(params.dateOfBirth.toString().trim()).format("YYYY-MM-DD") : '';
        var panNumber = params.panNumber ? params.panNumber.toString().trim() : '';
        var bankAc = params.bankAc ? params.bankAc.toString().trim() : '';
        var bankIfsc = params.bankIfsc ? params.bankIfsc.toString().trim() : '';
        var aadharCardNo = params.aadharCardNo ? params.aadharCardNo.toString().trim() : '';
        var billAmount = params.billAmount ? params.billAmount.toString().trim() : '';
        var salary = params.salary ? params.salary.toString().trim() : '';
        var hrStatus = params.hrStatus ? params.hrStatus.toString().trim() : '';
        var loginUserId = params.loginUserId ? params.loginUserId.toString().trim() : '';
        var actionType = params.actionType ? params.actionType.toString().trim() : '';
        var WHEREUPDATE = '';
        var WHEREAPPROVED = '';
        if (hrMasterId === '' || fName === '' || lName === '' || billAmount === '' || salary === '' || loginUserId === '' || actionType === '' || dateOfBirth === '') {
            res.status(400).json({ status: 'failure', error: 'Please send required field' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            if (actionType === 'update') {
                WHEREUPDATE = ", `updated_by` = '" + loginUserId + "', `updated_at` = '" + timesNow + "'";
            } else if (actionType === 'approved') {
                WHEREAPPROVED = ", `approval_status` = 'Approved', `approved_by` = '" + loginUserId + "', `approved_at` = '" + timesNow + "'";
            }
            var SQL = query.updateHrPending;
            SQL = SQL.replace('EMPNAME', eName);
            SQL = SQL.replace('FNAME', fName);
            SQL = SQL.replace('MNAME', mName);
            SQL = SQL.replace('LNAME', lName);
            SQL = SQL.replace('BANKAC', bankAc);
            SQL = SQL.replace('BANKIFSC', bankIfsc);
            SQL = SQL.replace('PANNO', panNumber);
            SQL = SQL.replace('HRSTATUS', hrStatus);
            SQL = SQL.replace('BILLAMNT', billAmount);
            SQL = SQL.replace('HRSALARY', salary);
            SQL = SQL.replace('AADHARNO', aadharCardNo);
            SQL = SQL.replace('DOBRE', dateOfBirth);
            SQL = SQL.replace('WHEREUPDATE', WHEREUPDATE);
            SQL = SQL.replace('WHEREAPPROVED', WHEREAPPROVED);
            SQL = SQL.replace('HRID', hrMasterId);

            console.log("\n > updateHrPending SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateHrPending SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                }
            });
            // res.status(200).json({ status: 'success', error: SQL });
        }
    }
};

exports.aprrovedHr = function (req, res, next) {
    var params = req.body;
    var loginUserId = req.params.loginUserId;
    var hrMasterId = params.hrMasterId ? params.hrMasterId.toString().trim() : '';
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

    var SQL = query.aprrovedHr;

    SQL = SQL.replace('approvedBy', loginUserId);
    SQL = SQL.replace('timesNow', timesNow);
    SQL = SQL.replace('HRID', hrMasterId);

    console.log("\n > aprrovedHr SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > aprrovedHr SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: 'Approved Successfully' });
        }
    });
}

exports.getHrConversion = function (req, res, next) {
    var SQL = query.getHrConversion;
    var params = req.body;
    var searchBy = params.searchBy ? params.searchBy.toString().trim() : '';
    var searchValue = params.searchValue ? params.searchValue.toString().trim() : '';
    var WHERE = "";

    if (searchBy && searchValue) {
        if (searchBy == "empName") {
            WHERE = "WHERE hr.employee_name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientName") {
            WHERE = "WHERE cl.name LIKE '%" + searchValue + "%'";
        } else if (searchBy == "clientEmpId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.client_empid IN (" + data + ")";
        } else if (searchBy == "empId") {
            searchValue = searchValue.toString().replace(/,/g, ' ');
            searchValue = searchValue.split(' ');
            var data = '';
            searchValue.forEach(e => {
                if (e) {
                    data += "'" + e + "', ";
                }
            });
            data += "''";
            console.log('\n> search value ---> ', data);
            WHERE = "WHERE hr.employee_id IN (" + data + ")";
            // WHERE = "WHERE AND hr.employee_id = '" + searchValue + "'";
        } else if (searchBy == "personalEmail") {
            WHERE = "WHERE hr.personal_emailid LIKE '%" + searchValue + "%'";
        } else if (searchBy == "officialEmail") {
            WHERE = "WHERE hr.official_emailid LIKE '%" + searchValue + "%'";
        }
    }
    SQL = SQL.replace('CONDITIONALWHERE', WHERE);
    console.log("\n > getHrConversion SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getHrConversion SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.updateHrConversion = function (req, res, next) {
    var params = req.body;
    if (params.hrMasterId === undefined || params.fName === undefined || params.mName === undefined || params.lName === undefined || params.dateOfJoin === undefined || params.relievedDate === undefined || params.hrStatus === undefined || params.loginUserId === undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else {
        var hrMasterId = params.hrMasterId.toString().trim();
        var fName = params.fName ? params.fName.toString().trim() : '';
        var mName = params.mName ? (' ' + params.mName.toString().trim()) : '';
        var lName = params.lName ? (' ' + params.lName.toString().trim()) : '';
        var eName = fName + mName + lName;
        var dateOfJoin = params.dateOfJoin ? moment(params.dateOfJoin.toString().trim()).format("YYYY-MM-DD") : '';
        var relievedDate = params.relievedDate ? moment(params.relievedDate.toString().trim()).format("YYYY-MM-DD") : '';
        var hrStatus = params.hrStatus ? params.hrStatus.toString().trim() : '';
        var loginUserId = params.loginUserId ? params.loginUserId.toString().trim() : '';
        var actionType = params.actionType ? params.actionType.toString().trim() : '';
        var WHEREUPDATE = '';
        var WHEREAPPROVED = '';
        if (hrMasterId === '' || fName === '' || lName === '' || loginUserId === '' || actionType === '' || dateOfJoin === '' || relievedDate === '' || relievedDate === '' || hrStatus === '') {
            res.status(400).json({ status: 'failure', error: 'Please send required field' });
        } else {
            var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
            if (actionType === 'update') {
                WHEREUPDATE = ", `updated_by` = '" + loginUserId + "', `updated_at` = '" + timesNow + "'";
            } else if (actionType === 'approved') {
                WHEREAPPROVED = ", `conversion_approval_status` = 'Approved', `conversion_approved_by` = '" + loginUserId + "', `conversion_approved_at` = '" + timesNow + "'";
            }
            var SQL = query.updateHrConversion;
            SQL = SQL.replace('EMPNAME', eName);
            SQL = SQL.replace('FNAME', fName);
            SQL = SQL.replace('MNAME', mName);
            SQL = SQL.replace('LNAME', lName);
            SQL = SQL.replace('HRSTATUS', hrStatus);
            SQL = SQL.replace('DOJRE', dateOfJoin);
            SQL = SQL.replace('RELEIVEDRE', relievedDate);
            SQL = SQL.replace('WHEREUPDATE', WHEREUPDATE);
            SQL = SQL.replace('WHEREAPPROVED', WHEREAPPROVED);
            SQL = SQL.replace('HRID', hrMasterId);

            console.log("\n > updateHrConversion SQL ---> ", SQL);
            db.query(SQL, function (error, results, fields) {
                if (error) {
                    console.log("\n > updateHrConversion SQL Err ---> ", error.code);
                    res.status(400).json({ status: 'failure', error: error.code });
                } else {
                    res.status(200).json({ status: 'success', data: 'Updated Successfully' });
                }
            });
            // res.status(200).json({ status: 'success', error: SQL });
        }
    }
};

exports.aprrovedHrConversion = function (req, res, next) {
    var params = req.body;
    var loginUserId = req.params.loginUserId;
    var hrMasterId = params.hrMasterId ? params.hrMasterId.toString().trim() : '';
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");

    var SQL = query.aprrovedHrConversion;

    SQL = SQL.replace('approvedBy', loginUserId);
    SQL = SQL.replace('timesNow', timesNow);
    SQL = SQL.replace('HRID', hrMasterId);

    console.log("\n > aprrovedHrConversion SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > aprrovedHrConversion SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: 'Approved Successfully' });
        }
    });
}


exports.getOnsiteEmpEndclient = function (req, res, next) {
    var type = req.params.type;
    console.log("Type==>", type);
    var SQL = query.getOnsiteEmpEndclient;
    if (type == "NEW") {
        SQL = SQL.replace("WHERE_RE", "WHERE hr.status = 'Active' AND (hr.end_client_id IS NULL OR hr.end_client_id = '')");
    } else if (type == "EDIT") {
        SQL = SQL.replace("WHERE_RE", "WHERE hr.status = 'Active' AND hr.end_client_id IS NOT NULL AND hr.end_client_id <> ''");
    } else {
        SQL = SQL.replace("WHERE_RE", "");
    }
    console.log("\n > getOnsiteEmpEndclient SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getOnsiteEmpEndclient SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
}

exports.updateEndClient = function (req, res, next) {
    params = req.body;

    if (params.endClientId == undefined || params.endClient == undefined || params.verticalId == undefined || params.vertical == undefined || params.hrMasterId == undefined) {
        res.status(400).json({ status: 'failure', error: 'Invalid payload data' });
    } else if (params.endClientId.toString().trim() == "" || params.endClient.toString().trim() == "" || params.verticalId.toString().trim() == "" || params.vertical.toString().trim() == "" || params.hrMasterId.toString().trim() == "") {
        res.status(400).json({ status: 'failure', error: 'Please send required field' });
    } else {
        var SQL = query.updateEndClient;
        SQL = SQL.replace("end_client_id_re", params.endClientId.toString().trim());
        // SQL = SQL.replace("end_client_re", params.endClient.toString().trim());
        // SQL = SQL.replace("vertical_id_re", params.verticalId.toString().trim());
        // SQL = SQL.replace("vertical_re", params.vertical.toString().trim());       
        SQL = SQL.replace("hr_master_id_re", params.hrMasterId.toString().trim());

        console.log("\n > updateEndClient SQL ---> ", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > updateEndClient SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: 'Endclient Updated Successfully' });
            }
        });
    }
};

exports.importEndclient = function (req, res, next) {
    var filePath = config.upload_path + "/uploads/";
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        // console.log('\n> fields ----> ', fields);
        files = files.uploads;
        console.log('\n> files ----> ', files);
        var oldpath = files.path;
        var newpath = filePath + files.name;
        if (!fs.existsSync(filePath)) {
            mkdirp(filePath, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    fs.rename(oldpath, newpath, function (err) {
                        if (err) throw err;
                        else {
                            let filePath = newpath;
                            var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                            var sheets = file.SheetNames;
                            let jsonData = [];
                            for (let i = 0; i < sheets.length; i++) {
                                var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                                temp.forEach((res, index) => {
                                    jsonData.push(res);
                                });
                            }
                            fs.unlink(filePath, function (err) {
                                if (err) {
                                    throw err
                                } else {
                                    console.log("Successfully deleted the file.")
                                }
                            });
                            var data = jsonData;
                            insertRow(data, function (err, message) {
                                if (err) {
                                    console.log("\n > createTimesheet SQL Err ---> ", message);
                                    res.status(400).json({ status: 'failure', error: message });
                                } else {
                                    console.log("\n > createTimesheet SQL success ---> ", message);
                                    res.status(200).json({ status: 'success', data: message });
                                }
                            });
                        }
                    });
                }
            });
        } else {
            fs.rename(oldpath, newpath, function (err) {
                if (err) throw err;
                else {
                    //console.log(success);
                    let filePath = newpath;
                    var file = xlsx.readFile(filePath, { defval: '', type: 'binary', cellDates: true, cellNF: false, cellText: false });
                    var sheets = file.SheetNames;
                    let jsonData = [];
                    for (let i = 0; i < sheets.length; i++) {
                        var temp = xlsx.utils.sheet_to_json(file.Sheets[file.SheetNames[i]], { defval: '', dateNF: "YYYY-MM-DD HH:mm:ss" });
                        temp.forEach((res, index) => {
                            jsonData.push(res);
                        });
                    }
                    fs.unlink(filePath, function (err) {
                        if (err) {
                            throw err
                        } else {
                            console.log("Successfully deleted the file.")
                        }
                    });
                    var data = jsonData;
                    insertRow(data, function (err, message) {
                        if (err) {
                            console.log("\n > createTimesheet SQL Err ---> ", message);
                            res.status(400).json({ status: 'failure', error: message });
                        } else {
                            console.log("\n > createTimesheet SQL success ---> ", message);
                            res.status(200).json({ status: 'success', data: message });
                        }
                    });
                }
            });
        }
    });

    function insertRow(data, callback) {
        try {
            if (data.length > 0) {
                var row = 0;
                var insertRow = function (json, tableName) {
                    var data = json[row];
                    var HR_ID = removeString(data.HR_ID);
                    var END_CLIENT = removeString(data.END_CLIENT);
                    var DOMAIN_NAME = removeString(data.DOMAIN);
                    var getEndClients = "SELECT endclient_id ID, endclient_name endclientName FROM prms_endclient WHERE endclient_name = END_CLIENT;";
                    getEndClients = getEndClients.replace('END_CLIENT', END_CLIENT);
                    console.log('\n > getEndClients[' + row + '] ---> ', getEndClients);
                    // row++;
                    // if (row >= json.length) {
                    //     callback(false, 'Migrated Successfully');
                    // } else {
                    //     insertRow(json, tableName);
                    // }
                    db.query(getEndClients, function (err, results, fields) {
                        if (results.length > 0) {
                            if (row >= json.length) {
                                callback(false, 'Migrated Successfully');
                            } else {
                                var updateEndClients = "UPDATE prms_pg_hr_master SET end_client_id = end_client_id_re WHERE hr_master_id = hr_master_id_re;"
                                updateEndClients = updateEndClients.replace('end_client_id_re', removeString(results[0].ID));
                                updateEndClients = updateEndClients.replace('hr_master_id_re', HR_ID);
                                console.log('\n > updateEndClients[' + row + '] ---> ', updateEndClients);
                                // row++;
                                // if (row >= json.length) {
                                //     callback(false, 'Migrated Successfully');
                                // } else {
                                //     insertRow(json, tableName);
                                // }
                                db.query(updateEndClients, function (err, results, fields) {
                                    row++;
                                    if (row >= json.length) {
                                        callback(false, 'Migrated Successfully');
                                    } else {
                                        insertRow(json, tableName);
                                    }
                                });
                            }
                        } else if (row >= json.length) {
                            callback(false, 'Migrated Successfully');
                        } else {
                            var getDomains = "SELECT vertical_id domainID, vertical_name domainName FROM prms_vertical WHERE vertical_name = DOMAIN_NAME_RE;";
                            getDomains = getDomains.replace('DOMAIN_NAME_RE', DOMAIN_NAME);
                            console.log('\n > getDomains[' + row + '] ---> ', getDomains);
                            // row++;
                            // if (row >= json.length) {
                            //     callback(false, 'Migrated Successfully');
                            // } else {
                            //     insertRow(json, tableName);
                            // }
                            db.query(getDomains, function (err, results, fields) {
                                if (results.length > 0) {
                                    if (row >= json.length) {
                                        callback(false, 'Migrated Successfully');
                                    } else {
                                        var insertEndclient = "INSERT INTO `prms_endclient` (prms_vertical_id, endclient_name, is_active) VALUES (domain_id_re, endclient_re, '1')";
                                        insertEndclient = insertEndclient.replace('domain_id_re', results[0].domainID);
                                        insertEndclient = insertEndclient.replace('endclient_re', END_CLIENT);
                                        console.log('\n > insertEndclient[' + row + '] ---> ', insertEndclient);
                                        db.query(insertEndclient, function (err, insertResults, fields) {
                                            console.log('\n > insertRows ----> ', JSON.stringify(insertResults));
                                            var updateEndClients = "UPDATE prms_pg_hr_master SET end_client_id = end_client_id_re WHERE hr_master_id = hr_master_id_re;"
                                            updateEndClients = updateEndClients.replace('end_client_id_re', removeString(insertResults.insertId));
                                            updateEndClients = updateEndClients.replace('hr_master_id_re', HR_ID);
                                            console.log('\n > updateEndClients[' + row + '] ---> ', updateEndClients);
                                            db.query(updateEndClients, function (err, results, fields) {
                                                row++;
                                                if (row >= json.length) {
                                                    callback(false, 'Migrated Successfully');
                                                } else {
                                                    insertRow(json, tableName);
                                                }
                                            });
                                        });
                                    }
                                } else {
                                    // row++;
                                    // if (row >= json.length) {
                                    //     callback(false, 'Migrated Successfully');
                                    // } else {
                                    //     insertRow(json, tableName);
                                    // }
                                    var insertDomain = "INSERT INTO `prms_vertical` (vertical_name, is_active) VALUES (domain_name_re,'1');";
                                    insertDomain = insertDomain.replace('domain_name_re', DOMAIN_NAME);
                                    console.log('\n > insertDomain[' + row + '] ---> ', insertDomain);
                                    db.query(insertDomain, function (err, insertDom, fields) {
                                        var insertEndclient = "INSERT INTO `prms_endclient` (prms_vertical_id, endclient_name, is_active) VALUES (domain_id_re, endclient_re, '1');";
                                        insertEndclient = insertEndclient.replace('domain_id_re', insertDom.insertId);
                                        insertEndclient = insertEndclient.replace('endclient_re', END_CLIENT);
                                        console.log('\n > insertEndclient[' + row + '] ---> ', insertEndclient);
                                        db.query(insertEndclient, function (err, insertResults, fields) {
                                            console.log('\n > insertRows ----> ', JSON.stringify(insertResults));
                                            var updateEndClients = "UPDATE prms_pg_hr_master SET end_client_id = end_client_id_re WHERE hr_master_id = hr_master_id_re;"
                                            updateEndClients = updateEndClients.replace('end_client_id_re', removeString(insertResults.insertId));
                                            updateEndClients = updateEndClients.replace('hr_master_id_re', HR_ID);
                                            console.log('\n > updateEndClients[' + row + '] ---> ', updateEndClients);
                                            db.query(updateEndClients, function (err, results, fields) {
                                                row++;
                                                if (row >= json.length) {
                                                    callback(false, 'Migrated Successfully');
                                                } else {
                                                    insertRow(json, tableName);
                                                }
                                            });
                                        });
                                    });
                                }
                            });
                        }
                    });
                };
                insertRow(data, 'HrMaster');
            } else {
                callback(true, 'Empty Sets');
            }
        } catch (e) {
            console.log(e);
            callback(true, e);
        }
    }
    function removeString(data, type = null) {
        if (data) {
            var reg = new RegExp("['\"`]", "g");
            data = data.toString();
            data = data.trim();
            data = data.replace(reg, '\\$&');
            return "'" + data + "'";
        } else if (data !== '') {
            return data;
        } else {
            return null;
        }
    }
};

exports.getHrChangesApproval = function (req, res, next) {
    var SQL = query.getHrChangesApproval;
    console.log("\n > getHrChangesApproval SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > getHrChangesApproval SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: results });
        }
    });
};

exports.approvedHrChanges = function (req, res, next) {
    var params = req.body;
    var loginUserId = req.params.loginUserId;
    var hrMasterId = params.hrMasterId;
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var masterIds = '';

    if (params.aadharId || params.panId || params.bankId || params.bankIfscId || params.billingAmountId || params.salaryId) {
        if (params.aadharId) {
            masterIds += "'" + params.aadharId + "',";
        }
        if (params.panId) {
            masterIds += "'" + params.panId + "',";
        }
        if (params.bankId) {
            masterIds += "'" + params.bankId + "',";
        }
        if (params.bankIfscId) {
            masterIds += "'" + params.bankIfscId + "',";
        }
        if (params.billingAmountId) {
            masterIds += "'" + params.billingAmountId + "',";
        }
        if (params.salaryId) {
            masterIds += "'" + params.salaryId + "',";
        }
        masterIds += "''";
    }

    if (hrMasterId && masterIds) {
        var SQL = query.approvedHrChanges;
        SQL = SQL.replace(/approvedByRe/g, loginUserId);
        SQL = SQL.replace(/timesNowRe/g, timesNow);
        SQL = SQL.replace('CONDITIONAL_IDS', masterIds);
        SQL = SQL.replace('hrMasterIdRe', hrMasterId);
        console.log("\n > approvedHrChanges SQL ---> ", SQL);

        db.query(SQL, function (error, results, fields) {
            if (error) {
                console.log("\n > approvedHrChanges SQL Err ---> ", error.code);
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: results });
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Invalid Payload Data!' });
    }
};

exports.updateApprovedHrChanges = function (req, res, next) {
    var params = req.body;
    let timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    var actionType = params.actionType;
    var hrMasterId = params.hrMasterId;
    var aadharId = params.aadharId;
    var aadhaarNo = params.aadhaarNo;
    var panId = params.panId;
    var panNo = params.panNo;
    var bankId = params.bankId;
    var bankAc = params.bankAc;
    var bankIfscId = params.bankIfscId;
    var bankIfsc = params.bankIfsc;
    var billingAmountId = params.billingAmountId;
    var billingAmount = params.billingAmount;
    var salaryId = params.salaryId;
    var salary = params.salary;
    var loginUserId = params.loginUserId;

    var SQL = '';
    if (aadharId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', aadhaarNo);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', aadharId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET aadhar_number = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', aadhaarNo);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }

    if (panId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', panNo);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', panId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET pan_number = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', panNo);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }

    if (bankId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', bankAc);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', bankId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET bank_ac_no = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', bankAc);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }
    if (bankIfscId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', bankIfsc);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', bankIfscId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET bank_ifsc = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', bankIfsc);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }
    if (billingAmountId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', billingAmount);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', billingAmountId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET billing_amt = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', billingAmount);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }
    if (salaryId) {
        var tempSQL = query.updateApprovedHrChanges;
        tempSQL = tempSQL.replace('columnNewValueRe', salary);
        tempSQL = tempSQL.replace('CONDITION_ID_RE', salaryId);
        if (actionType === 'approveAndSave') {
            var data = ", approval_status = '1', approved_by = 'approvedByRe', approved_at = 'timesNowRe'";
            data = data.replace('approvedByRe', loginUserId);
            data = data.replace('timesNowRe', timesNow);
            tempSQL = tempSQL.replace('DYNAMIC_DATA', data);

            var hrUpdate = "UPDATE prms_pg_hr_master SET salary = 'columnNewValueRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('columnNewValueRe', salary);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            tempSQL += hrUpdate;
        } else {
            tempSQL = tempSQL.replace('DYNAMIC_DATA', '');
        }
        SQL += tempSQL;
    }

    if (SQL) {
        if (actionType === 'approveAndSave') {
            var hrUpdate = "UPDATE prms_pg_hr_master SET changes_approval_status = '1', changes_approval_by = 'approvedByRe', changes_approval_at = 'timesNowRe' WHERE hr_master_id = 'hrMasterIdRe';";
            hrUpdate = hrUpdate.replace('approvedByRe', loginUserId);
            hrUpdate = hrUpdate.replace('timesNowRe', timesNow);
            hrUpdate = hrUpdate.replace('hrMasterIdRe', hrMasterId);
            SQL += hrUpdate;
        }
        console.log('\n> updateChangesApproval ---> ', SQL);
        // res.status(200).json({ status: 'success', data: SQL });
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(400).json({ status: 'failure', error: error.code });
            } else {
                res.status(200).json({ status: 'success', data: SQL });
            }
        });
    } else {
        res.status(400).json({ status: 'failure', error: 'Invalid Payload Data!' });
    }
};

exports.deleteOnBoardingDetails = function (req, res, next) {
    var hrMasterId = req.params.hrMasterId;
    var SQL = query.deleteOnBoardingDetails;
    SQL = SQL.replace(/hr_master_idRe/g, hrMasterId);
    console.log("\ndeleteOnBoardingDetails delete Sql-->", SQL);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            res.status(200).json({ status: 'success', data: "Deleted Successfully" });
        }
    });

}

exports.autofillBankName = function (req, res, next) {

    var bankIfsc = req.params.bankIfsc;
    if (bankIfsc == undefined) {
        res.status(400).json({ status: 'failure', error: "Please send required field" });
    }
    else if (bankIfsc == '') {
        res.status(400).json({ status: 'failure', error: "Invalid Params" });
    }
    else {
        var SQL = query.autofillBankName;
        if (bankIfsc.includes('ARIHT'))
            SQL = SQL.replace(/IFSCRE/g, '%ARIHT%');
        else if (bankIfsc.includes('PLB'))
            SQL = SQL.replace(/IFSCRE/g, '%PLB%');
        else
            SQL = SQL.replace(/IFSCRE/g, bankIfsc.substring(0, 4) + '%');
        console.log("\nIFSC code original and substring values", bankIfsc + " ", bankIfsc.substring(0, 4));
        console.log("\nAutofillBankName Sql-->", SQL);
        db.query(SQL, function (error, results, fields) {
            if (error) {
                res.status(200).json({ status: 'failure', error: error.code });
            } else {
                if (results.length == 0)
                    res.status(200).json({ status: 'failure', error: 'Invalid IFSC Code' });
                else
                    res.status(200).json({ status: 'success', data: results[0] });
            }
        });
    }
}