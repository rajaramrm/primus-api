var db = require("../db/manageDB");
var query = require("../queries/report.query");
var moment = require("moment-timezone");
var xlsx = require("xlsx");
var fs = require("fs");
var mkdirp = require("mkdirp");
var pdf = require("html-pdf");
var config = require("../../config");
var mockData = require("../dailyReport.json");
var nodemailer = require("nodemailer");
const formatAmount = require('indian-currency-formatter');

exports.searchNotInvoicedContract = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined ||
    params.type === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var WHERE = "";

    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND C1.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND C1.pgt_code_pandl_head = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      var SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND C1.pgt_code_bsns_mgr = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      SQL +=
        params.type == "NOT_INVOICED"
          ? query.getNotInvoicedContract
          : query.getPartiallyNotInvoicedContract;


      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }
      var serviceMonth = year + "-" + month;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);
      SQL = SQL.replace(/CONDITIONWHERE/g, WHERE);
    });

    console.log("\n > getNotInvoicedContract SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getNotInvoicedContract SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        if (months.length == 1) {
          res.status(200).json({ status: "success", data: results });
        } else {
          var indx = 1;
          months.shift();
          months.forEach(month => {
            results[indx].forEach(e => {
              var fIndx = results[0].findIndex(function (item, i) {
                return (
                  item.clientName === e.clientName &&
                  item.comments === e.comments
                );
              });

              if (fIndx == -1) {
                results[0].push(e);
              } else {
                results[0][fIndx].invoicePending += e.invoicePending;
                results[0][fIndx].timesheetPending += e.timesheetPending;
              }
            });
            indx++;
          });
          res.status(200).json({ status: "success", data: results[0], searchType: params.searchType ? params.searchType.toString().trim() : '' });
        }
      }
    });
  }
};

exports.searchNotInvoicedPerm = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var WHERE = "";

    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND C1.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND C1.pgt_code_pandl_head = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      var SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND C1.pgt_code_bsns_mgr IN (" + SBM + ")";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND C1.pgt_code_bsns_mgr = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR C1.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      SQL += query.getNotInvoicedPerm;

      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }
      var serviceMonth = year + "-" + month;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);
      SQL = SQL.replace(/CONDITIONWHERE/g, WHERE);
    });

    console.log("\n > getNotInvoicedPerm SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getNotInvoicedPerm SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        if (months.length == 1) {
          res.status(200).json({ status: "success", data: results });
        } else {
          var indx = 1;
          months.shift();
          months.forEach(month => {
            results[indx].forEach(e => {
              var fIndx = results[0].findIndex(function (item, i) {
                return (
                  item.clientName === e.clientName
                );
              });

              if (fIndx == -1) {
                results[0].push(e);
              } else {
                results[0][fIndx].PermanentInvoicePending += e.PermanentInvoicePending;
                results[0][fIndx].conversionInvoicePending += e.conversionInvoicePending;
              }
            });
            indx++;
          });
          res.status(200).json({ status: "success", data: results[0] });
        }
      }
    });
  }
};

exports.downloadNotInvoicedContract = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined ||
    params.type === undefined ||
    params.downloadType === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var downloadType = params.downloadType.toString().trim();
    var WHERE = "";

    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND CCM.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND CCM.pgt_code_pandl_head = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      var SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND CCM.pgt_code_bsns_mgr = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      SQL +=params.type == "NOT_INVOICED" ? query.downloadNotInvoicedContract: query.downloadPartiallyInvoicedContract;


      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }
      var serviceMonth = year + "-" + month;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);
      SQL = SQL.replace(/CONDITIONWHERE/g, WHERE);
      SQL = (downloadType == 'comments') ? SQL.replace(/WHERECOMMENTS/g, "AND T.comments IN ('Absconded','Terminated','Not Approved','No Timesheet')") : SQL.replace(/WHERECOMMENTS/g, "");
    });


    // var SQL = query.downloadNotInvoicedContract;

    // var serviceMonth = year + "-" + month;

    // SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
    // SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
    // SQL = SQL.replace(/WHEREYEAR/g, year);
    // SQL = (type == 'comments') ? SQL.replace(/WHERECOMMENTS/g, "AND T.comments IN ('Absconded','Terminated','Not Approved','No Timesheet')") : SQL.replace(/WHERECOMMENTS/g, "");

    console.log("\n > downloadNotInvoicedContract SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log(
          "\n > downloadNotInvoicedContract SQL Err ---> ",
          error.code
        );
        res.status(400).json({ status: "failure", error: error.code });
      } else {        
        if (results.length > 0) {
          var result;
          if(months.length>1) {
            result = [];
           for(i=0;i<results.length;i++){
            result = result.concat(results[i]);            
           }            
          } else {
            result = results;
          }
          res.status(200).json({ status: "success", data: result });
        } else {
          res.status(400).json({ status: "failure", error: 'No Records Found' });
        }

      }
    });
  }
};

exports.downloadNotInvoicedPerm= function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var WHERE = "";

    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND CCM.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND CCM.pgt_code_pandl_head = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue && seniorManager) {
        var SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      var SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND CCM.pgt_code_bsns_mgr IN (" + SBM + ")";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND CCM.pgt_code_bsns_mgr = '" + pgtCode + "'";
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR CCM.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      SQL += query.downloadNotInvoicedPerm+query.downloadNotInvoicedConv;

      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }
      var serviceMonth = year + "-" + month;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);
      SQL = SQL.replace(/CONDITIONWHERE/g, WHERE);
    });

    console.log("\n > downloadNotInvoicedPermanent SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {          
      if (error) {
        console.log(
          "\n > downloadNotInvoicedPermanent SQL Err ---> ",
          error.code
        );
        res.status(400).json({ status: "failure", error: error.code });
      } else {        
        if (results.length > 0) {
          var result;
          if(months.length>2) {
            result = {
              "permanent":[],
              "conversion":[]
            };
           for(i=0;i<results.length;i=i+2){
            result.permanent = result.permanent.concat(results[i]); 
            result.conversion = result.conversion.concat(results[i+1]);                         
           }            
          } else {
            result = {
              "permanent": results[0],
              "conversion": results[1]
            };
          }
          res.status(200).json({ status: "success", data: result });
        } else {
          res.status(400).json({ status: "failure", error: 'No Records Found' });
        }

      }
    });
  }
};

exports.projectedSummaryOld = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var year = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;

    var WHERE = "";
    var SUBWHERE = "";
    var nxtMonth = "";
    var nxtYear = "";
    if (month == 12) {
      nxtMonth = "01";
      nxtYear = Number(year) + 1;
    } else {
      nxtMonth = Number(month) + 1;
      nxtYear = year;
    }

    var currentDate = year + "/" + month + "/" + "01";
    var date = new Date(currentDate),
      y = date.getFullYear(),
      m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);

    firstDay = moment(firstDay)
      .utc()
      .tz("Asia/Kolkata")
      .format("YYYY-MM-DD");
    lastDay = moment(lastDay)
      .utc()
      .tz("Asia/Kolkata")
      .format("YYYY-MM-DD");

    console.log("\n > firstDay ---> ", firstDay);
    console.log("\n > lastDay ---> ", lastDay);
    console.log("\n > month ---> ", month.toString().padStart(2, "0"));
    console.log("\n > year ---> ", year);
    console.log("\n > nxtMonth ---> ", nxtMonth.toString().padStart(2, "0"));
    console.log("\n > nxtYear ---> ", nxtYear);
    // AND cmTotal.pgt_code_bsns_mgr = 'PGT4848' GROUP BY cmTotal.client_name
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    }

    var getContractSummary = query.getContractSummary;
    var getContractInvoiced = query.getContractInvoiced;
    // var getNxtSummary = query.getNxtSummary;
    var getPermenentNew = query.getPermenentNew;
    var getConversionNew = query.getConversionNew;

    var serviceMonth = year + "-" + month.toString().padStart(2, "0");
    var nxtServiceMonth = nxtYear + "-" + nxtMonth.toString().padStart(2, "0");

    var SQL = "";
    SQL += getContractSummary;
    SQL += getContractInvoiced;
    // SQL += getNxtSummary;
    SQL += getPermenentNew;
    SQL += getConversionNew;

    SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
    SQL = SQL.replace(/nextServieMonth/g, nxtServiceMonth);

    SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
    SQL = SQL.replace(/WHEREYEAR/g, year);

    SQL = SQL.replace(/nextMonthWhere/g, nxtMonth.toString().padStart(2, "0"));
    SQL = SQL.replace(/nextYearWhere/g, nxtYear);

    SQL = SQL.replace(/SUBCONDITONWHERE/g, SUBWHERE);
    SQL = SQL.replace(/CONDITONWHERE/g, WHERE);
    console.log("\n > projectedSummary SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > projectedSummary SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var contractSummary = results[0];
        var contractInvoiced = results[1];
        var permenentNew = results[2];
        var conversionNew = results[3];
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var totalBilled = 0;
        var ctc = 0;
        var grossProfit = 0;
        var marginVendor = 0;
        var revenue = 0;
        var workingDays = 0;
        var totalDays = noOfDays(firstDay, lastDay);
        var nxtSummary = 0;
        var mspFee = 0;
        contractSummary.forEach(e => {
          if (e.isNewJoinee && e.isEndJoinee) {
            workingDays = noOfDays(e.joinDate, e.endDate);
            totalBilled += 1;

            // console.log('\n<--- Projected JoineeEnd ---> ');
            // console.log('> joinDate ---> ', e.joinDate);
            // console.log('> endDate ---> ', e.endDate);
            // console.log('> revenue ---> ', e.revenue);
            // console.log('> ctc ---> ', e.ctc);
            // console.log('> totalDays ---> ', totalDays);
            // console.log('> workingDays ---> ', workingDays);

            if (workingDays > 1) {
              revenue += (e.revenue / totalDays) * workingDays;
              ctc += (e.ctc / totalDays) * workingDays;
              grossProfit += (e.grossProfit / totalDays) * workingDays;
              marginVendor += (e.marginVendor / totalDays) * workingDays;
              mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
              // console.log('\n> finalRevenue ---> ', (e.revenue / totalDays) * workingDays);
              // console.log('> finalCtc ---> ', (e.ctc / totalDays) * workingDays);
            }
          } else if (e.isNewJoinee) {
            workingDays = noOfDays(e.joinDate, lastDay);
            totalBilled += 1;
            revenue += (e.revenue / totalDays) * workingDays;
            ctc += (e.ctc / totalDays) * workingDays;
            grossProfit += (e.grossProfit / totalDays) * workingDays;
            marginVendor += (e.marginVendor / totalDays) * workingDays;
            mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
          } else if (e.isEndJoinee) {
            workingDays = noOfDays(firstDay, e.endDate);
            totalBilled += 1;
            revenue += (e.revenue / totalDays) * workingDays;
            ctc += (e.ctc / totalDays) * workingDays;
            grossProfit += (e.grossProfit / totalDays) * workingDays;
            marginVendor += (e.marginVendor / totalDays) * workingDays;
            mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
          } else {
            totalBilled += 1;
            revenue += e.revenue;
            ctc += e.ctc;
            grossProfit += e.grossProfit;
            marginVendor += e.marginVendor;
            mspFee += e.mspFee * e.revenue;
            // console.log('\n > isEndJoinee ---> ', e.isEndJoinee);
          }
          if (e.isEndJoinee) {
            nxtSummary += 1;
          }
        });
        nxtSummary = totalBilled - nxtSummary;
        var contract = {
          totalBilled: totalBilled,
          grossMargin: Math.round((grossProfit / revenue) * 100),
          grossProfit: Math.round(grossProfit),
          // grossMargin: Math.round(((revenue-ctc-marginVendor-mspFee) / revenue) * 100),
          // grossProfit: Math.round(revenue-ctc-marginVendor-mspFee),
          revenue: Math.round(revenue),
          ctc: Math.round(ctc),
          mspFee: Math.round(mspFee),
          marginVendor: Math.round(marginVendor),
          nxtSummary: nxtSummary,
          attrition: parseFloat(
            ((totalBilled - nxtSummary) / totalBilled) * 100
          ).toFixed(2)
        };

        var totalInvoiced = 0;
        var ctcInvoiced = 0;
        var grossProfitInvoiced = 0;
        var marginVendorInvoiced = 0;
        var revenueInvoiced = 0;
        var workingDaysInvoiced = 0;
        var totalDaysInvoiced = noOfDays(firstDay, lastDay);
        var mspFeeInvoiced = 0;
        contractInvoiced.forEach(e => {
          if (e.isNewJoinee && e.isEndJoinee) {
            workingDaysInvoiced = noOfDays(e.joinDate, e.endDate);
            totalInvoiced += 1;

            // console.log('\n<--- Invoiced JoineeEnd ---> ');
            // console.log('> joinDate ---> ', e.joinDate);
            // console.log('> endDate ---> ', e.endDate);
            // console.log('> revenue ---> ', e.revenue);
            // console.log('> ctc ---> ', e.ctc);
            // console.log('> totalDays ---> ', totalDays);
            // console.log('> workingDaysInvoiced ---> ', workingDaysInvoiced);

            if (workingDaysInvoiced > 1) {
              revenueInvoiced += e.revenue;
              ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
              grossProfitInvoiced +=
                (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
              marginVendorInvoiced +=
                (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
              mspFeeInvoiced +=
                e.mspFee * ((e.revenue / totalDays) * workingDays);

              // console.log('\n> finalRevenue ---> ', revenueInvoiced);
              // console.log('> finalCtc ---> ', (e.ctc / totalDaysInvoiced) * workingDaysInvoiced);
            }
          } else if (e.isNewJoinee) {
            workingDaysInvoiced = noOfDays(e.joinDate, lastDay);
            totalInvoiced += 1;
            revenueInvoiced += e.revenue;
            ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
            grossProfitInvoiced +=
              (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
            marginVendorInvoiced +=
              (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
            mspFeeInvoiced +=
              e.mspFee * ((e.revenue / totalDays) * workingDays);
          } else if (e.isEndJoinee) {
            workingDaysInvoiced = noOfDays(firstDay, e.endDate);
            totalInvoiced += 1;
            revenueInvoiced += e.revenue;
            ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
            grossProfitInvoiced +=
              (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
            marginVendorInvoiced +=
              (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
            mspFeeInvoiced +=
              e.mspFee * ((e.revenue / totalDays) * workingDays);
          } else {
            totalInvoiced += 1;
            revenueInvoiced += e.revenue;
            ctcInvoiced += e.ctc;
            grossProfitInvoiced += e.grossProfit;
            marginVendorInvoiced += e.marginVendor;
            mspFeeInvoiced += e.mspFee * e.revenue;
          }
        });
        var invoiced = {
          totalInvoiced: totalInvoiced,
          // grossMargin: Math.round((grossProfitInvoiced / revenueInvoiced) * 100),
          // grossProfit: Math.round(grossProfitInvoiced),
          grossMargin: Math.round(
            ((revenueInvoiced -
              ctcInvoiced -
              marginVendorInvoiced -
              mspFeeInvoiced) /
              revenueInvoiced) *
            100
          ),
          grossProfit: Math.round(
            revenueInvoiced -
            ctcInvoiced -
            marginVendorInvoiced -
            mspFeeInvoiced
          ),
          revenue: Math.round(revenueInvoiced),
          ctc: Math.round(ctcInvoiced),
          mspFee: Math.round(mspFeeInvoiced),
          marginVendor: Math.round(marginVendorInvoiced),
          nxtSummary: nxtSummary,
          attrition: parseFloat(
            ((totalBilled - nxtSummary) / totalBilled) * 100
          ).toFixed(2)
        };
        // console.log('\n> revenueInvoiced ---> ', revenueInvoiced);
        // console.log('\n> ctcInvoiced ---> ', ctcInvoiced);
        // console.log('\n> marginVendor ---> ', marginVendor);
        // console.log('\n> mspFeeInvoiced ---> ', mspFeeInvoiced);
        invoiced.grossMargin =
          invoiced.grossMargin < 0 ? 0 : invoiced.grossMargin;
        invoiced.grossProfit =
          invoiced.grossProfit < 0 ? 0 : invoiced.grossProfit;
        // var cInvoiced = {
        //     totalInvoiced: 0,
        //     revenue: 0,
        //     ctc: 0,
        //     grossProfit: 0,
        //     grossMargin: 0
        // };
        // contractInvoiced.forEach(e => {
        //     cInvoiced.totalInvoiced += 1;
        //     cInvoiced.revenue += e.revenue;
        //     cInvoiced.ctc += e.ctc;
        //     cInvoiced.grossProfit += e.grossProfit;
        //     cInvoiced.grossMargin += e.grossMargin;
        // });

        // cInvoiced.grossMargin = Math.round((cInvoiced.grossProfit / cInvoiced.revenue) * 100);
        // cInvoiced.grossProfit = Math.round(cInvoiced.grossProfit);
        // cInvoiced.revenue = Math.round(cInvoiced.revenue);
        // cInvoiced.ctc = Math.round(cInvoiced.ctc);
        // cInvoiced.nxtInvoiced = nxtSummary;

        permenentNew.forEach(e => {
          e.clientName = e.client;
          e.totalRevenue = Math.round(e.totalRevenue);
        });
        conversionNew.forEach(e => {
          e.clientName = e.client;
          e.totalRevenue = Math.round(e.totalRevenue);
        });
        let data = {
          contractSummary: contract,
          contractInvoiced: invoiced,
          permenentNew: permenentNew,
          conversionNew: conversionNew
        };
        res.status(200).json({ status: "success", data: data });
      }
    });
  }
};

exports.projectedSummary = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;

    var WHERE = "";
    var SUBWHERE = "";
    var SUBWHEREE = "";

    // AND cmTotal.pgt_code_bsns_mgr = 'PGT4848' GROUP BY cmTotal.client_name
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + searchValue + "'";
        SUBWHEREE = "AND cmRevenue.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + pgtCode + "'";
      SUBWHEREE = "AND cmRevenue.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHEREE += " AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " AND cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
      SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " OR cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + pgtCode + "'";
      SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " OR cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      var nxtMonth = "";
      var nxtYear = "";

      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }

      if (month == 12) {
        nxtMonth = "01";
        nxtYear = Number(year) + 1;
      } else {
        nxtMonth = Number(month) + 1;
        nxtYear = year;
      }

      var serviceMonth = year + "-" + month.toString().padStart(2, "0");
      var nxtServiceMonth =
        nxtYear + "-" + nxtMonth.toString().padStart(2, "0");

      var getContractSummary = query.getContractSummary;
      var getContractInvoiced = query.getContractInvoiced;
      var getPermenentNew = query.getPermenentNew;
      var getConversionNew = query.getConversionNew;

      SQL += getContractSummary;
      SQL += getContractInvoiced;
      SQL += getPermenentNew;
      SQL += getConversionNew;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/nextServieMonth/g, nxtServiceMonth);

      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);

      SQL = SQL.replace(
        /nextMonthWhere/g,
        nxtMonth.toString().padStart(2, "0")
      );
      SQL = SQL.replace(/nextYearWhere/g, nxtYear);

      SQL = SQL.replace(/SUBCONDITONALWHERE/g, SUBWHEREE);
      SQL = SQL.replace(/SUBCONDITONWHERE/g, SUBWHERE);
      SQL = SQL.replace(/CONDITONWHERE/g, WHERE);
    });

    console.log("\n > projectedSummary SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > projectedSummary SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var indx = 0;
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };

        var contract = {
          totalBilled: [],
          grossMargin: 0,
          grossProfit: 0,
          revenue: 0,
          ctc: 0,
          mspFee: 0,
          marginVendor: 0,
          nxtSummary: 0,
          attrition: 0,
          avgBilling: 0
        };

        var invoiced = {
          totalInvoiced: [],
          grossMargin: 0,
          grossProfit: 0,
          revenue: 0,
          ctc: 0,
          mspFee: 0,
          marginVendor: 0,
          nxtSummary: 0,
          attrition: 0,
          avgBilling: 0
        };

        var data = {
          searchType: params.searchType ? params.searchType.toString().trim() : '',
          contractSummary: {},
          contractInvoiced: {},
          permenentNew: [],
          conversionNew: []
        };

        months.forEach(month => {
          var contractSummary = results[indx];
          var contractInvoiced = results[indx + 1];
          var permenentNew = results[indx + 2];
          var conversionNew = results[indx + 3];
          indx += 4;
          var totalBilled = 0;
          var ctc = 0;
          var grossProfit = 0;
          var marginVendor = 0;
          var revenue = 0;
          var workingDays = 0;
          var avgTotal = 0;

          var splitYear = fyear.split("-");
          var year;
          if (month >= 4) {
            year = splitYear[0];
          } else {
            year = splitYear[1];
          }

          var currentDate = year + "/" + month + "/" + "01";
          var date = new Date(currentDate),
            y = date.getFullYear(),
            m = date.getMonth();
          var firstDay = new Date(y, m, 1);
          var lastDay = new Date(y, m + 1, 0);
          firstDay = moment(firstDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          lastDay = moment(lastDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          var totalDays = noOfDays(firstDay, lastDay);

          var nxtSummary = 0;
          var mspFee = 0;
          contractSummary.forEach(e => {
            avgTotal += e.revenue;
            if (e.isNewJoinee && e.isEndJoinee) {
              workingDays = noOfDays(e.joinDate, e.endDate);
              totalBilled += 1;

              if (workingDays > 1) {
                revenue += (e.revenue / totalDays) * workingDays;
                ctc += (e.ctc / totalDays) * workingDays;
                grossProfit += (e.grossProfit / totalDays) * workingDays;
                marginVendor += (e.marginVendor / totalDays) * workingDays;
                mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
              }
            } else if (e.isNewJoinee) {
              workingDays = noOfDays(e.joinDate, lastDay);
              totalBilled += 1;
              revenue += (e.revenue / totalDays) * workingDays;
              ctc += (e.ctc / totalDays) * workingDays;
              grossProfit += (e.grossProfit / totalDays) * workingDays;
              marginVendor += (e.marginVendor / totalDays) * workingDays;
              mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else if (e.isEndJoinee) {
              workingDays = noOfDays(firstDay, e.endDate);
              totalBilled += 1;
              revenue += (e.revenue / totalDays) * workingDays;
              ctc += (e.ctc / totalDays) * workingDays;
              grossProfit += (e.grossProfit / totalDays) * workingDays;
              marginVendor += (e.marginVendor / totalDays) * workingDays;
              mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else {
              totalBilled += 1;
              revenue += e.revenue;
              ctc += e.ctc;
              grossProfit += e.grossProfit;
              marginVendor += e.marginVendor;
              mspFee += e.mspFee * e.revenue;
              // console.log('\n > isEndJoinee ---> ', e.isEndJoinee);
            }
            if (e.isEndJoinee) {
              nxtSummary += 1;
            }
            contract.totalBilled.push(e.hrId);
          });
          contract.grossProfit += grossProfit;
          contract.revenue += revenue;
          contract.ctc += ctc;
          contract.mspFee += mspFee;
          contract.marginVendor += marginVendor;
          contract.nxtSummary += nxtSummary;
          contract.avgBilling += (contractSummary.length > 0 ? avgTotal / contractSummary.length : 0);
          var totalInvoiced = 0;
          var ctcInvoiced = 0;
          var grossProfitInvoiced = 0;
          var marginVendorInvoiced = 0;
          var revenueInvoiced = 0;
          var workingDaysInvoiced = 0;
          var totalDaysInvoiced = noOfDays(firstDay, lastDay);
          var mspFeeInvoiced = 0;
          var avgTotalInv = 0;
          contractInvoiced.forEach(e => {
            avgTotalInv += e.revenue;
            if (e.isNewJoinee && e.isEndJoinee) {
              workingDaysInvoiced = noOfDays(e.joinDate, e.endDate);
              totalInvoiced += 1;
              if (workingDaysInvoiced > 1) {
                revenueInvoiced += e.revenue;
                ctcInvoiced +=
                  (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
                grossProfitInvoiced +=
                  (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
                marginVendorInvoiced +=
                  (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
                mspFeeInvoiced +=
                  e.mspFee * ((e.revenue / totalDays) * workingDays);
              }
            } else if (e.isNewJoinee) {
              workingDaysInvoiced = noOfDays(e.joinDate, lastDay);
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
              grossProfitInvoiced +=
                (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
              marginVendorInvoiced +=
                (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
              mspFeeInvoiced +=
                e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else if (e.isEndJoinee) {
              workingDaysInvoiced = noOfDays(firstDay, e.endDate);
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
              grossProfitInvoiced +=
                (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
              marginVendorInvoiced +=
                (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
              mspFeeInvoiced +=
                e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else {
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += e.ctc;
              grossProfitInvoiced += e.grossProfit;
              marginVendorInvoiced += e.marginVendor;
              mspFeeInvoiced += e.mspFee * e.revenue;
            }
            invoiced.totalInvoiced.push(e.totalInvoiced);
          });
          invoiced.revenue += revenueInvoiced;
          invoiced.ctc += ctcInvoiced;
          invoiced.mspFee += mspFeeInvoiced;
          invoiced.marginVendor += marginVendorInvoiced;
          invoiced.nxtSummary += nxtSummary;
          invoiced.avgBilling += (contractInvoiced.length > 0 ? avgTotalInv / contractInvoiced.length : 0);
          permenentNew.forEach(e => {
            e.clientName = e.client;
            e.totalRevenue = Math.round(e.totalRevenue);

            var fIndx = data.permenentNew.findIndex(function (item, i) {
              return item.client === e.client;
            });

            if (fIndx == -1) {
              data.permenentNew.push(e);
            } else {
              data.permenentNew[fIndx].totalClient += e.totalClient;
              data.permenentNew[fIndx].totalInvoiced += e.totalInvoiced;
              data.permenentNew[fIndx].totalRevenue += e.totalRevenue;
            }
          });
          conversionNew.forEach(e => {
            e.clientName = e.client;
            e.totalRevenue = Math.round(e.totalRevenue);

            var fIndx = data.conversionNew.findIndex(function (item, i) {
              return item.client === e.client;
            });

            if (fIndx == -1) {
              data.conversionNew.push(e);
            } else {
              data.conversionNew[fIndx].totalClient += e.totalClient;
              data.conversionNew[fIndx].totalInvoiced += e.totalInvoiced;
              data.conversionNew[fIndx].totalRevenue += e.totalRevenue;
            }
          });
        });

        contract.totalBilled = [...new Set(contract.totalBilled)].length;
        contract.grossMargin = Math.round(
          (contract.grossProfit / contract.revenue) * 100
        );
        contract.grossProfit = Math.round(contract.grossProfit);
        contract.revenue = Math.round(contract.revenue);
        contract.ctc = Math.round(contract.ctc);
        contract.mspFee = Math.round(contract.mspFee);
        contract.marginVendor = Math.round(contract.marginVendor);
        contract.nxtSummary = contract.totalBilled - contract.nxtSummary;
        contract.attrition = parseFloat(
          ((contract.totalBilled - contract.nxtSummary) /
            contract.totalBilled) *
          100
        ).toFixed(2);

        invoiced.totalInvoiced = [...new Set(invoiced.totalInvoiced)].length;
        invoiced.grossMargin = Math.round(
          ((invoiced.revenue -
            invoiced.ctc -
            invoiced.marginVendor -
            invoiced.mspFee) /
            invoiced.revenue) *
          100
        );
        (invoiced.grossProfit = Math.round(
          invoiced.revenue -
          invoiced.ctc -
          invoiced.marginVendor -
          invoiced.mspFee
        )),
          (invoiced.revenue = Math.round(invoiced.revenue));
        invoiced.ctc = Math.round(invoiced.ctc);
        invoiced.mspFee = Math.round(invoiced.mspFee);
        invoiced.marginVendor = Math.round(invoiced.marginVendor);
        invoiced.nxtSummary = contract.nxtSummary;
        invoiced.attrition = contract.attrition;

        invoiced.grossMargin =
          invoiced.grossMargin < 0 ? 0 : invoiced.grossMargin;
        invoiced.grossProfit =
          invoiced.grossProfit < 0 ? 0 : invoiced.grossProfit;

        data.contractSummary = contract;
        data.contractInvoiced = invoiced;

        res.status(200).json({ status: "success", data: data });
      }
    });
  }
};

exports.netAddition = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var year = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var CONDITIONWHERE = "";
    var WHERE_HR_STATUS =
      "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";

    var serviceMonth = year + "-" + month;
    var SQL = query.netAdditionProjected;
    var SBM = "";
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    }

    SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
    SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
    SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);

    console.log("\n > netAdditionProjected SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > netAdditionProjected SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var currentDate = year + "/" + month + "/" + "01";
        var date = new Date(currentDate),
          y = date.getFullYear(),
          m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);
        firstDay = moment(firstDay)
          .utc()
          .tz("Asia/Kolkata")
          .format("YYYY-MM-DD");
        lastDay = moment(lastDay)
          .utc()
          .tz("Asia/Kolkata")
          .format("YYYY-MM-DD");

        console.log("\n > firstDay ---> ", firstDay);
        console.log("\n > lastDay ---> ", lastDay);
        console.log("\n > month ---> ", month.toString().padStart(2, "0"));
        console.log("\n > year ---> ", year);

        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var totalDays = noOfDays(firstDay, lastDay);
        var myObj = {};
        var totalRevTemp = 0;
        results.forEach(item => {
          var workingDays = 0;
          var revenue = 0;
          // console.log('\n> _firstDay ---> ', firstDay);
          // console.log('> _lastDay ---> ', lastDay);
          // console.log('> totalDays ---> ', totalDays);
          if (!(item.clientName in myObj)) {
            // console.log('\n > obj not ---> ', item.clientName);
            myObj[item.clientName] = {
              new_route: 0,
              new_sourced: 0,
              end: 0,
              net: 0,
              total_route: 0,
              total_sourced: 0,
              total_rev: 0,
              avg_billing: 0,
              gross_margin: 0,
              percentage: 0
            };
            myObj[item.clientName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].end += item.isEndJoinee ? 1 : 0;
            myObj[item.clientName].total_route += item.isRouting ? 1 : 0;
            myObj[item.clientName].total_sourced += !item.isRouting ? 1 : 0;

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              // console.log('\n<--- Projected JoineeEnd ---> ');
              // console.log('> joinDate ---> ', item.joinDate);
              // console.log('> endDate ---> ', item.endDate);
              // console.log('> revenue ---> ', item.revenue);
              // console.log('> ctc ---> ', item.ctc);
              // console.log('> totalDays ---> ', totalDays);
              // console.log('> workingDays ---> ', workingDays);

              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.clientName].total_rev += revenue;
                totalRevTemp += revenue;
                myObj[item.clientName].gross_margin += grossProfit;
                // console.log('\n> finalRevenue ---> ', (item.revenue / totalDays) * workingDays);
                // console.log('> finalCtc ---> ', (item.ctc / totalDays) * workingDays);
                // console.log('> Both revenue ---> ', revenue);
              }
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.joinDate, ' -- ', workingDays);
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.joinDate, ' -- ', workingDays);
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.joinDate, ' -- ', workingDays);
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.joinDate, ' -- ', workingDays);
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.endDate, ' -- ', workingDays);
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.clientName].total_rev += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName].gross_margin += item.grossMargin;
              // console.log('> ', item.hrID, ' -- ', item.revenue);
              // console.log('> Revenue --------> ', item.revenue);
            }
          } else if (item.clientName in myObj) {
            myObj[item.clientName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].end += item.isEndJoinee ? 1 : 0;
            myObj[item.clientName].total_route += item.isRouting ? 1 : 0;
            myObj[item.clientName].total_sourced += !item.isRouting ? 1 : 0;

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              // console.log('\n<--- Projected JoineeEnd ---> ');
              // console.log('> joinDate ---> ', item.joinDate);
              // console.log('> endDate ---> ', item.endDate);
              // console.log('> revenue ---> ', item.revenue);
              // console.log('> ctc ---> ', item.ctc);
              // console.log('> totalDays ---> ', totalDays);
              // console.log('> workingDays ---> ', workingDays);

              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.clientName].total_rev += revenue;
                totalRevTemp += revenue;
                myObj[item.clientName].gross_margin += grossProfit;
                // console.log('\n> finalRevenue ---> ', (item.revenue / totalDays) * workingDays);
                // console.log('> finalCtc ---> ', (item.ctc / totalDays) * workingDays);
                // console.log('> Both revenue ---> ', revenue);
              }
              // console.log('> ', item.hrID, ' -- ', revenue, ' -- ', item.joinDate, ' -- ', workingDays);
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              totalRevTemp += revenue;
              // console.log('> ', item.hrID, ' -- ', revenue);
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> ', item.hrID, ' -- ', revenue);
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.clientName].total_rev += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.clientName].gross_margin += item.grossMargin;
              // console.log('> ', item.hrID, ' -- ', item.revenue);
              // console.log('> Revenue --------> ', item.revenue);
            }
            // console.log('> ', item.hrID, ' -- ', totalRevTemp);
          }
        });
        for (obj in myObj) {
          myObj[obj].net =
            myObj[obj].new_route + myObj[obj].new_sourced - myObj[obj].end;
          myObj[obj].avg_billing =
            myObj[obj].total_rev /
            (myObj[obj].total_route + myObj[obj].total_sourced);
          myObj[obj].percentage =
            (myObj[obj].gross_margin / myObj[obj].total_rev) * 100;
          myObj[obj].total_rev = Math.round(myObj[obj].total_rev);
          myObj[obj].avg_billing = Math.round(myObj[obj].avg_billing);
          myObj[obj].gross_margin = Math.round(myObj[obj].gross_margin);
          myObj[obj].percentage = Math.round(myObj[obj].percentage);
        }
        var data = [];
        for (i in myObj) {
          let ob = {
            clientName: i,
            new_route: myObj[i].new_route,
            new_sourced: myObj[i].new_sourced,
            end: myObj[i].end,
            net: myObj[i].net,
            total_route: myObj[i].total_route,
            total_sourced: myObj[i].total_sourced,
            total_rev: myObj[i].total_rev,
            avg_billing: myObj[i].avg_billing,
            gross_margin: myObj[i].gross_margin,
            percentage: myObj[i].percentage
          };
          data.push(ob);
        }
        res.status(200).json({ status: "success", data: data });
        // res.status(200).json({ status: 'success', myObj: myObj, data: data });
      }
    });
  }
};

exports.getPLs = function (req, res, next) {
  var SQL =
    "SELECT DISTINCT(employee_code) AS empCode, app_employee_id AS empId, name AS empName, user_type userRole, designation FROM prms_app_employees WHERE user_type IN ('BUHead') AND `status`='ACTIVE';";
  console.log("\n > getPLs SQL ---> ", SQL);
  db.query(SQL, function (error, results, fields) {
    res.status(200).json({ status: "success", data: results });
  });
};

exports.getBMs = function (req, res, next) {
  var params = req.body;

  var userRole =
    params.userRole === undefined ? "" : params.userRole.toString().trim();
  var pgtCode =
    params.pgtCode === undefined ? "" : params.pgtCode.toString().trim();
  // var conditionWhere = '';

  var SQL =
    "SELECT DISTINCT(employee_code) AS empCode, app_employee_id AS empId, name AS empName, user_type userRole, designation FROM prms_app_employees WHERE user_type IN ('BM') AND `status`='ACTIVE'";
  // if(userRole === 'BUHead' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // } else if(userRole === 'BM' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // }
  if (userRole === "BUHead" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  } else if (userRole === "BM" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  }
  // SQL = SQL.replace(/conditionWhere/g, conditionWhere);
  console.log("\n > getBMs SQL ---> ", SQL);
  db.query(SQL, function (error, results, fields) {
    res.status(200).json({ status: "success", data: results });
  });
};

exports.getBMsOnly = function (req, res, next) {
  var params = req.body;

  var userRole =
    params.userRole === undefined ? "" : params.userRole.toString().trim();
  var pgtCode =
    params.pgtCode === undefined ? "" : params.pgtCode.toString().trim();
  // var conditionWhere = '';

  var SQL =
    "SELECT DISTINCT(employee_code) AS empCode, app_employee_id AS empId, name AS empName, user_type userRole, designation FROM prms_app_employees WHERE user_type IN ('BM') AND `designation`='Business Manager' AND  `status`='ACTIVE';";
  // if(userRole === 'BUHead' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // } else if(userRole === 'BM' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // }
  if (userRole === "BUHead" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.designation = 'Business Manager' AND  em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.designation = 'Business Manager' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  } else if (userRole === "BM" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.designation = 'Business Manager' AND em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.designation = 'Business Manager' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  }
  // SQL = SQL.replace(/conditionWhere/g, conditionWhere);
  console.log("\n > getBMs SQL ---> ", SQL);
  db.query(SQL, function (error, results, fields) {
    res.status(200).json({ status: "success", data: results });
  });
};

exports.getSBMsOnly = function (req, res, next) {
  var params = req.body;

  var userRole =
    params.userRole === undefined ? "" : params.userRole.toString().trim();
  var pgtCode =
    params.pgtCode === undefined ? "" : params.pgtCode.toString().trim();
  // var conditionWhere = '';

  var SQL =
    "SELECT DISTINCT(employee_code) AS empCode, app_employee_id AS empId, name AS empName, user_type userRole, designation FROM prms_app_employees WHERE user_type IN ('BM') AND `designation`='Senior Business Manager' AND  `status`='ACTIVE';";
  // if(userRole === 'BUHead' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // } else if(userRole === 'BM' && pgtCode){
  //     conditionWhere = "AND (report_manager_id IN (SELECT rm.app_employee_id FROM prms_app_employees rm WHERE rm.employee_code = 'PGTCODE'))";
  //     conditionWhere = conditionWhere.replace(/PGTCODE/g, pgtCode);
  // }
  if (userRole === "BUHead" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.designation = 'Senior Business Manager' AND  em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.designation = 'Senior Business Manager' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  } else if (userRole === "BM" && pgtCode) {
    SQL =
      "SELECT DISTINCT(em1.employee_code) AS empCode, em1.app_employee_id AS empId, em1.name AS empName, em1.user_type userRole, em1.designation FROM prms_app_employees em1 WHERE em1.user_type = 'BM' AND em1.designation = 'Senior Business Manager' AND em1.status = 'ACTIVE' AND (em1.report_manager_id IN (SELECT rm1.app_employee_id FROM prms_app_employees rm1 WHERE rm1.employee_code = 'PGTCODE') OR em1.report_manager_id IN (SELECT em2.app_employee_id AS empId FROM prms_app_employees em2 WHERE em2.user_type = 'BM' AND em2.designation = 'Senior Business Manager' AND em2.status = 'ACTIVE' AND (em2.report_manager_id IN (SELECT rm2.app_employee_id FROM prms_app_employees rm2 WHERE rm2.employee_code = 'PGTCODE'))));";
    SQL = SQL.replace(/PGTCODE/g, pgtCode);
  }
  // SQL = SQL.replace(/conditionWhere/g, conditionWhere);
  console.log("\n > getBMs SQL ---> ", SQL);
  db.query(SQL, function (error, results, fields) {
    res.status(200).json({ status: "success", data: results });
  });
};

exports.searchHrReportData = function (req, res, next) {
  var params = req.body;

  if (params.searchDate == undefined) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else if (params.searchDate.toString().trim() == "") {
    res.status(400).json({ status: "failure", error: "Send Required Field" });
  } else {
    var SQL =
      query.getHrReportDataJoinee +
      query.getHrReportDataAmdmntJoinee +
      query.getHrReportDataResignee +
      query.getHrReportDataPermanentJoinee +
      query.getHrReportDataPermanentConversion;
    SQL = SQL.replace(/date_re/g, params.searchDate.toString().trim());

    console.log("\n > searchHrReportData SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > searchHrReportData SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        let result = {
          joinee: results[0],
          amdmntJoinee: results[1],
          resignee: results[2],
          permanentJoinee: results[3],
          conversion: results[4]
        };
        // let result = {
        //   joinee: mockData.joinee,
        //   amdmntJoinee: mockData.amdmntJoinee,
        //   resignee: mockData.resignee,
        //   permanentJoinee: mockData.permanentJoinee,
        //   conversion: mockData.conversion,
        // };
        res.status(200).json({ status: "success", data: result });
      }
    });
  }
};

exports.searchMonthlyInvoices = function (req, res, next) {
  var params = req.body;

  if (params.month == undefined || params.year == undefined) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var year = params.year.toString().trim();
    var SQL = query.getMonthlyInvoices;

    SQL = SQL.replace(/month_re/g, month.toString().padStart(2, "0"));
    SQL = SQL.replace(/year_re/g, year);

    console.log("\n > getMonthlyInvoices SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getMonthlyInvoices SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        res.status(200).json({ status: "success", data: results });
      }
    });
  }
};

exports.getVerticalsReport = function (req, res, next) {
  var params = req.body;
  var date = new Date(),
    y = date.getFullYear(),
    m = date.getMonth();
  var serviceMonth = new Date(y, m, 1);
  serviceMonth = moment(serviceMonth)
    .utc()
    .tz("Asia/Kolkata")
    .format("YYYY-MM");
  serviceMonth =
    params.serviceMonth === undefined || params.serviceMonth === ""
      ? serviceMonth
      : params.serviceMonth;
  var userRole =
    params.userRole === undefined || params.userRole === ""
      ? ""
      : params.userRole.toString().trim();
  var pgtCode =
    params.pgtCode === undefined || params.pgtCode === ""
      ? ""
      : params.pgtCode.toString().trim();
  var seniorManager =
    params.seniorManager === undefined || params.seniorManager === ""
      ? false
      : params.seniorManager;
  var CONDITIONWHERE = "";
  var WHERE_HR_STATUS =
    "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";
  var SBM = "";
  if (userRole === "BUHead") {
    CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
  } else if (userRole === "BM" && seniorManager) {
    SBM =
      "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
    SBM = SBM.replace(/PGTCODE/g, pgtCode);
    console.log("\n > SBM SQL ---> ", SBM);
    CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
  } else if (userRole === "BM") {
    CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
  }

  var SQL = query.getVerticalsReport;
  SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
  SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);
  SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
  console.log("\n > getVerticalsReport SQL ---> ", SQL);
  console.log("\n > serviceMonth ---> ", serviceMonth);

  db.query(SQL, function (error, results, fields) {
    if (error) {
      console.log("\n > getVerticalsReport SQL Err ---> ", error.code);
      res.status(400).json({ status: "failure", error: error.code });
    } else {
      var year = moment(serviceMonth)
        .utc()
        .tz("Asia/Kolkata")
        .format("YYYY");
      var month = moment(serviceMonth)
        .utc()
        .tz("Asia/Kolkata")
        .format("MM");
      var currentDate = year + "/" + month + "/" + "01";
      var date = new Date(currentDate),
        y = date.getFullYear(),
        m = date.getMonth();
      var firstDay = new Date(y, m, 1);
      var lastDay = new Date(y, m + 1, 0);
      firstDay = moment(firstDay)
        .utc()
        .tz("Asia/Kolkata")
        .format("YYYY-MM-DD");
      lastDay = moment(lastDay)
        .utc()
        .tz("Asia/Kolkata")
        .format("YYYY-MM-DD");

      console.log("\n > firstDay ---> ", firstDay);
      console.log("\n > lastDay ---> ", lastDay);
      console.log("\n > month ---> ", month.toString().padStart(2, "0"));
      console.log("\n > year ---> ", year);

      var noOfDays = function (dateFrom, dateTo) {
        var date1 = new Date(dateFrom);
        var date2 = new Date(dateTo);
        var Difference_In_Time = date2.getTime() - date1.getTime();
        var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
        return Difference_In_Days + 1;
      };

      var total = {
        vertical: 0,
        horizontal: 0,
        verticalRevenue: 0,
        horizontalRevenue: 0,
        averageBillingVertial: 0,
        averageBillingHorizontal: 0
      };

      var totalDays = noOfDays(firstDay, lastDay);
      var myObj = {};
      results[0].forEach(item => {
        var workingDays = 0;
        var revenue = 0;
        if (!(item.verticalName in myObj)) {
          myObj[item.verticalName] = {
            verticalID: item.verticalID,
            verticalName: item.verticalName,
            headCount: 0,
            revenue: 0,
            avgBilling: 0,
            grossMargin: 0,
          };
          myObj[item.verticalName].headCount += 1;
          myObj[item.verticalName].avgBilling += item.revenue;
          if (item.isNewJoinee && item.isEndJoinee) {
            workingDays = noOfDays(item.joinDate, item.endDate);
            if (workingDays > 1) {
              revenue = (item.revenue / totalDays) * workingDays;
              grossMargin = (item.grossMargin / totalDays) * workingDays;
              myObj[item.verticalName].revenue += revenue;
              myObj[item.verticalName].grossMargin += grossMargin;
            }
          } else if (item.isNewJoinee) {
            workingDays = noOfDays(item.joinDate, lastDay);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj[item.verticalName].revenue += revenue;
            myObj[item.verticalName].grossMargin += grossMargin;
          } else if (item.isEndJoinee) {
            workingDays = noOfDays(firstDay, item.endDate);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj[item.verticalName].revenue += revenue;
            myObj[item.verticalName].grossMargin += grossMargin;
          } else {
            myObj[item.verticalName].revenue += item.revenue;
            myObj[item.verticalName].grossMargin += item.grossMargin;
          }
        } else if (item.verticalName in myObj) {
          myObj[item.verticalName].headCount += 1;
          myObj[item.verticalName].avgBilling += item.revenue;
          if (item.isNewJoinee && item.isEndJoinee) {
            workingDays = noOfDays(item.joinDate, item.endDate);
            if (workingDays > 1) {
              revenue = (item.revenue / totalDays) * workingDays;
              grossMargin = (item.grossMargin / totalDays) * workingDays;
              myObj[item.verticalName].revenue += revenue;
              myObj[item.verticalName].grossMargin += grossMargin;
            }
          } else if (item.isNewJoinee) {
            workingDays = noOfDays(item.joinDate, lastDay);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj[item.verticalName].revenue += revenue;
            myObj[item.verticalName].grossMargin += grossMargin;
          } else if (item.isEndJoinee) {
            workingDays = noOfDays(firstDay, item.endDate);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj[item.verticalName].revenue += revenue;
            myObj[item.verticalName].grossMargin += grossMargin;
          } else {
            myObj[item.verticalName].revenue += item.revenue;
            myObj[item.verticalName].grossMargin += item.grossMargin;
          }
        }
      });

      var verticals = [];
      for (i in myObj) {
        let ob = {
          verticalID: myObj[i].verticalID,
          verticalName: i,
          headCount: myObj[i].headCount,
          revenue: myObj[i].revenue,
          grossMargin: myObj[i].grossMargin,
          avgBillingTotal: myObj[i].avgBilling,
          avgBilling: myObj[i].headCount > 0 ? myObj[i].avgBilling / myObj[i].headCount : 0
        };
        verticals.push(ob);
      }
      var myObj1 = {};
      results[1].forEach(item => {
        var workingDays = 0;
        var revenue = 0;
        if (!(item.horizontalID in myObj1)) {
          myObj1[item.horizontalID] = {
            horizontalID: item.horizontalID,
            horizontalName: item.horizontalName,
            headCount: 0,
            revenue: 0,
            grossMargin: 0,
            avgBilling: 0
          };
          myObj1[item.horizontalID].headCount += 1;
          myObj1[item.horizontalID].avgBilling += item.revenue;
          if (item.isNewJoinee && item.isEndJoinee) {
            workingDays = noOfDays(item.joinDate, item.endDate);
            if (workingDays > 1) {
              revenue = (item.revenue / totalDays) * workingDays;
              grossMargin = (item.grossMargin / totalDays) * workingDays;
              myObj1[item.horizontalID].revenue += revenue;
              myObj1[item.horizontalID].grossMargin += grossMargin;
            }
          } else if (item.isNewJoinee) {
            workingDays = noOfDays(item.joinDate, lastDay);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj1[item.horizontalID].revenue += revenue;
            myObj1[item.horizontalID].grossMargin += grossMargin;
          } else if (item.isEndJoinee) {
            workingDays = noOfDays(firstDay, item.endDate);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj1[item.horizontalID].revenue += revenue;
            myObj1[item.horizontalID].grossMargin += grossMargin;
          } else {
            myObj1[item.horizontalID].revenue += item.revenue;
            myObj1[item.horizontalID].grossMargin += item.grossMargin;
          }
        } else if (item.horizontalID in myObj1) {
          myObj1[item.horizontalID].headCount += 1;
          myObj1[item.horizontalID].avgBilling += item.revenue;
          if (item.isNewJoinee && item.isEndJoinee) {
            workingDays = noOfDays(item.joinDate, item.endDate);
            if (workingDays > 1) {
              revenue = (item.revenue / totalDays) * workingDays;
              grossMargin = (item.grossMargin / totalDays) * workingDays;
              myObj1[item.horizontalID].revenue += revenue;
              myObj1[item.horizontalID].grossMargin += grossMargin;
            }
          } else if (item.isNewJoinee) {
            workingDays = noOfDays(item.joinDate, lastDay);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj1[item.horizontalID].revenue += revenue;
            myObj1[item.horizontalID].grossMargin += grossMargin;
          } else if (item.isEndJoinee) {
            workingDays = noOfDays(firstDay, item.endDate);
            revenue = (item.revenue / totalDays) * workingDays;
            grossMargin = (item.grossMargin / totalDays) * workingDays;

            myObj1[item.horizontalID].revenue += revenue;
            myObj1[item.horizontalID].grossMargin += grossMargin;
          } else {
            myObj1[item.horizontalID].revenue += item.revenue;
            myObj1[item.horizontalID].grossMargin += item.grossMargin;
          }
        }
      });

      var horizontals = [];
      for (i in myObj1) {
        let ob = {
          horizontalID: i,
          horizontalName: myObj1[i].horizontalName,
          headCount: myObj1[i].headCount,
          revenue: myObj1[i].revenue,
          grossMargin: myObj1[i].grossMargin,
          avgBillingTotal: myObj1[i].avgBilling,
          avgBilling: myObj1[i].headCount > 0 ? myObj1[i].avgBilling / myObj1[i].headCount : 0
        };
        horizontals.push(ob);
      }

      horizontals.forEach(e => {
        e.percentage = Math.round((e.grossMargin / e.revenue) * 100);
        e.grossMargin = Math.round(e.grossMargin);
        e.revenue = Math.round(e.revenue);
        total.horizontal += e.headCount;
        total.horizontalRevenue += Math.round(e.revenue);
        total.averageBillingHorizontal += e.avgBillingTotal;
      });
      total.averageBillingHorizontal = total.averageBillingHorizontal / total.horizontal;

      verticals.forEach(e => {
        e.percentage = Math.round((e.grossMargin / e.revenue) * 100);
        e.grossMargin = Math.round(e.grossMargin);
        e.revenue = Math.round(e.revenue);
        total.vertical += e.headCount;
        total.verticalRevenue += Math.round(e.revenue)
        total.averageBillingVertial += e.avgBillingTotal;
      });
      total.averageBillingVertial = total.averageBillingVertial / total.vertical;

      horizontals.sort((a, b) => {
        return b.percentage - a.percentage;
      });

      verticals.sort((a, b) => {
        return b.percentage - a.percentage;
      });
      let data = {
        verticals: verticals,
        horizontals: horizontals,
        total: total
      };
      res.status(200).json({ status: "success", data: data });
    }
  });
};

exports.getVerticalsReportNew = function (req, res, next) {
  var params = req.body;
  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;

    var WHERE = "";
    var WHERE_HR_STATUS =
      "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";

    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";

      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";

      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";

      }
    } else if (userRole === "BUHead") {
      WHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";

      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";

      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";

      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";

      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";

      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";

      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }


    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }

      if (month == 12) {
        nxtMonth = "01";
        nxtYear = Number(year) + 1;
      } else {
        nxtMonth = Number(month) + 1;
        nxtYear = year;
      }

      var serviceMonth = year + "-" + month.toString().padStart(2, "0");
      var getVerticalsReport = query.getVerticalsReport;
      SQL += getVerticalsReport;
      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
      SQL = SQL.replace(/CONDITIONWHERE/g, WHERE);
    });

    console.log("getverticalreport sql-->", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getVerticalsReport SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var indx = 0;
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var myObj = {};
        var myObj1 = {};
        var total = {
          vertical: 0,
          horizontal: 0,
          verticalRevenue: 0,
          horizontalRevenue: 0,
          averageBillingVertial: 0,
          averageBillingHorizontal: 0
        };
        var vavgBill = 0;
        var vavgTotal = 0;
        var havgBill = 0;
        var havgTotal = 0;
        months.forEach(month => {
          vavgBill = 0;
          havgBill = 0;
          var vertical = results[indx];
          var horizontal = results[indx + 1];
          indx += 2;
          var splitYear = fyear.split("-");
          var year;

          if (month >= 4) {
            year = splitYear[0];
          } else {
            year = splitYear[1];
          }

          var currentDate = year + "/" + month + "/" + "01";
          var date = new Date(currentDate),
            y = date.getFullYear(),
            m = date.getMonth();
          var firstDay = new Date(y, m, 1);
          var lastDay = new Date(y, m + 1, 0);
          firstDay = moment(firstDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          lastDay = moment(lastDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          var totalDays = noOfDays(firstDay, lastDay);

          vertical.forEach(item => {
            var workingDays = 0;
            var revenue = 0;
            vavgBill += item.revenue;

            if (!(item.verticalName in myObj)) {
              myObj[item.verticalName] = {
                verticalID: item.verticalID,
                verticalName: item.verticalName,
                headCount: [],
                revenue: 0,
                avgBilling: 0,
                grossMargin: 0,
              };
              myObj[item.verticalName].headCount.push(item.masterId);
              myObj[item.verticalName].avgBilling += item.revenue;
              if (item.isNewJoinee && item.isEndJoinee) {
                workingDays = noOfDays(item.joinDate, item.endDate);
                if (workingDays > 1) {
                  revenue = (item.revenue / totalDays) * workingDays;
                  grossMargin = (item.grossMargin / totalDays) * workingDays;
                  myObj[item.verticalName].revenue += revenue;
                  myObj[item.verticalName].grossMargin += grossMargin;
                }
              } else if (item.isNewJoinee) {
                workingDays = noOfDays(item.joinDate, lastDay);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj[item.verticalName].revenue += revenue;
                myObj[item.verticalName].grossMargin += grossMargin;
              } else if (item.isEndJoinee) {
                workingDays = noOfDays(firstDay, item.endDate);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj[item.verticalName].revenue += revenue;
                myObj[item.verticalName].grossMargin += grossMargin;
              } else {
                myObj[item.verticalName].revenue += item.revenue;
                myObj[item.verticalName].grossMargin += item.grossMargin;
              }
            } else if (item.verticalName in myObj) {
              myObj[item.verticalName].headCount.push(item.masterId);
              myObj[item.verticalName].avgBilling += item.revenue;
              if (item.isNewJoinee && item.isEndJoinee) {
                workingDays = noOfDays(item.joinDate, item.endDate);
                if (workingDays > 1) {
                  revenue = (item.revenue / totalDays) * workingDays;
                  grossMargin = (item.grossMargin / totalDays) * workingDays;
                  myObj[item.verticalName].revenue += revenue;
                  myObj[item.verticalName].grossMargin += grossMargin;
                }
              } else if (item.isNewJoinee) {
                workingDays = noOfDays(item.joinDate, lastDay);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj[item.verticalName].revenue += revenue;
                myObj[item.verticalName].grossMargin += grossMargin;
              } else if (item.isEndJoinee) {
                workingDays = noOfDays(firstDay, item.endDate);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj[item.verticalName].revenue += revenue;
                myObj[item.verticalName].grossMargin += grossMargin;
              } else {
                myObj[item.verticalName].revenue += item.revenue;
                myObj[item.verticalName].grossMargin += item.grossMargin;
              }
            }
          });
          vavgTotal += vavgBill / vertical.length;

          horizontal.forEach(item => {

            var workingDays = 0;
            var revenue = 0;
            havgBill += item.revenue;
            if (!(item.horizontalID in myObj1)) {
              myObj1[item.horizontalID] = {
                horizontalID: item.horizontalID,
                horizontalName: item.horizontalName,
                headCount: [],
                revenue: 0,
                grossMargin: 0,
                avgBilling: 0
              };
              myObj1[item.horizontalID].headCount.push(item.masterId);
              myObj1[item.horizontalID].avgBilling += item.revenue;
              if (item.isNewJoinee && item.isEndJoinee) {
                workingDays = noOfDays(item.joinDate, item.endDate);
                if (workingDays > 1) {
                  revenue = (item.revenue / totalDays) * workingDays;
                  grossMargin = (item.grossMargin / totalDays) * workingDays;
                  myObj1[item.horizontalID].revenue += revenue;
                  myObj1[item.horizontalID].grossMargin += grossMargin;
                }
              } else if (item.isNewJoinee) {
                workingDays = noOfDays(item.joinDate, lastDay);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj1[item.horizontalID].revenue += revenue;
                myObj1[item.horizontalID].grossMargin += grossMargin;
              } else if (item.isEndJoinee) {
                workingDays = noOfDays(firstDay, item.endDate);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj1[item.horizontalID].revenue += revenue;
                myObj1[item.horizontalID].grossMargin += grossMargin;
              } else {
                myObj1[item.horizontalID].revenue += item.revenue;
                myObj1[item.horizontalID].grossMargin += item.grossMargin;
              }
            } else if (item.horizontalID in myObj1) {
              myObj1[item.horizontalID].headCount.push(item.masterId);
              myObj1[item.horizontalID].avgBilling += item.revenue;
              if (item.isNewJoinee && item.isEndJoinee) {
                workingDays = noOfDays(item.joinDate, item.endDate);
                if (workingDays > 1) {
                  revenue = (item.revenue / totalDays) * workingDays;
                  grossMargin = (item.grossMargin / totalDays) * workingDays;
                  myObj1[item.horizontalID].revenue += revenue;
                  myObj1[item.horizontalID].grossMargin += grossMargin;
                }
              } else if (item.isNewJoinee) {
                workingDays = noOfDays(item.joinDate, lastDay);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj1[item.horizontalID].revenue += revenue;
                myObj1[item.horizontalID].grossMargin += grossMargin;
              } else if (item.isEndJoinee) {
                workingDays = noOfDays(firstDay, item.endDate);
                revenue = (item.revenue / totalDays) * workingDays;
                grossMargin = (item.grossMargin / totalDays) * workingDays;

                myObj1[item.horizontalID].revenue += revenue;
                myObj1[item.horizontalID].grossMargin += grossMargin;
              } else {
                myObj1[item.horizontalID].revenue += item.revenue;
                myObj1[item.horizontalID].grossMargin += item.grossMargin;
              }
            }
          });

          havgTotal += havgBill / horizontal.length;
        });
        var verticals = [];
        for (i in myObj) {
          let ob = {
            verticalID: myObj[i].verticalID,
            verticalName: i,
            headCount: [...new Set(myObj[i].headCount)].length,
            revenue: myObj[i].revenue,
            grossMargin: myObj[i].grossMargin,
            avgBillingTotal: myObj[i].avgBilling,
            avgBilling: [...new Set(myObj[i].headCount)].length > 0 ? myObj[i].avgBilling / [...new Set(myObj[i].headCount)].length : 0
          };
          verticals.push(ob);
        }

        var horizontals = [];
        for (i in myObj1) {
          let ob = {
            horizontalID: i,
            horizontalName: myObj1[i].horizontalName,
            headCount: [...new Set(myObj1[i].headCount)].length,
            revenue: myObj1[i].revenue,
            grossMargin: myObj1[i].grossMargin,
            avgBillingTotal: myObj1[i].avgBilling,
            avgBilling: [...new Set(myObj1[i].headCount)].length > 0 ? myObj1[i].avgBilling / [...new Set(myObj1[i].headCount)].length : 0
          };
          horizontals.push(ob);
        }

        horizontals.forEach(e => {
          e.percentage = Math.round((e.grossMargin / e.revenue) * 100);
          e.grossMargin = Math.round(e.grossMargin);
          e.revenue = Math.round(e.revenue);
          total.horizontal += e.headCount;
          total.horizontalRevenue += Math.round(e.revenue);

        });
        total.averageBillingHorizontal = havgTotal;

        verticals.forEach(e => {
          e.percentage = Math.round((e.grossMargin / e.revenue) * 100);
          e.grossMargin = Math.round(e.grossMargin);
          e.revenue = Math.round(e.revenue);
          total.vertical += e.headCount;
          total.verticalRevenue += Math.round(e.revenue)

        });
        total.averageBillingVertial = vavgTotal;

        horizontals.sort((a, b) => {
          return b.percentage - a.percentage;
        });

        verticals.sort((a, b) => {
          return b.percentage - a.percentage;
        });
        let data = {
          verticals: verticals,
          horizontals: horizontals,
          total: total
        };
        res.status(200).json({ status: "success", data: data });
      }
    });
  }
}

exports.getAbscondTerminated = function (req, res, next) {
  params = req.body;
  if (params.hrMasterIds === undefined || params.hrMasterIds === "") {
    res
      .status(400)
      .json({ status: "failure", error: "Please send required field" });
  } else {
    var SQL = query.getAbscondTerminated;
    SQL = SQL.replace("hr_master_id_re", params.hrMasterIds.toString().trim());

    console.log("\n > getAbscondTerminated SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getAbscondTerminated SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        res.status(200).json({ status: "success", data: results });
      }
    });
  }
};

exports.updateOnboardingEmp = function (req, res, next) {
  params = req.body;
  if (params.loginUserId === undefined || params.loginUserId === "") {
    res
      .status(400)
      .json({ status: "failure", error: "Please send loginUserId " });
  } else {
    var timesNow = moment
      .utc()
      .tz("Asia/Kolkata")
      .format("YYYY-MM-DD HH:mm:ss");
    var hrStatus = params.hrStatus
      ? ", status = '" + params.hrStatus.toString().trim() + "'"
      : "";
    var SQL = query.updateOnboardingEmp;
    var rDate = "NULL";
    if (params.relievedDate) {
      rDate = "'" + params.relievedDate.toString().trim() + "'";
      hrStatus += params.hrStatus ? ", is_update_from_not_invoice = '1'" : "";
    }
    SQL = SQL.replace("relievedDateRe", rDate);
    SQL = SQL.replace("timesNowRe", timesNow);
    SQL = SQL.replace("loginUserIdRe", params.loginUserId.toString().trim());
    SQL = SQL.replace("hrMasterIdRe", params.hrMasterId.toString().trim());
    SQL = SQL.replace("WHERESETDATA", hrStatus);

    console.log("\n > updateOnboardingEmp SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > updateOnboardingEmp SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        res
          .status(200)
          .json({ status: "success", data: "Updated Successfully" });
      }
    });
  }
};

exports.getMonthlyBillingReport = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined ||
    params.clientId === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var year = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var clientId = params.clientId.toString().trim();
    var costCenterId = params.costCenterId
      ? params.costCenterId.toString().trim()
      : "";

    var CONDITIONWHERE = "";
    var WHERE_HR_STATUS =
      "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";
    var CLIENTWHERE = "";

    var serviceMonth = year + "-" + month;
    var SQL =
      query.getMonthlyBillingReportContract +
      query.getMonthlyBillingReportPermanent +
      query.getMonthlyBillingReportConversion;
    var SBM = "";
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId && costCenterId) {
        CLIENTWHERE =
          "AND cm.client_id = '" +
          clientId +
          "' AND cm.cost_center_id = '" +
          costCenterId +
          "'";
      } else if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
    } else if (userRole === "BUHead") {
      CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
    SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
    SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);
    SQL = SQL.replace(/CLIENTWHERE/g, CLIENTWHERE);

    console.log("\n > getMonthlyBillingReport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getMonthlyBillingReport SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var currentDate = year + "/" + month + "/" + "01";
        var date = new Date(currentDate),
          y = date.getFullYear(),
          m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);
        firstDay = moment(firstDay)
          .utc()
          .tz("Asia/Kolkata")
          .format("YYYY-MM-DD");
        lastDay = moment(lastDay)
          .utc()
          .tz("Asia/Kolkata")
          .format("YYYY-MM-DD");

        console.log("\n > firstDay ---> ", firstDay);
        console.log("\n > lastDay ---> ", lastDay);
        console.log("\n > month ---> ", month.toString().padStart(2, "0"));
        console.log("\n > year ---> ", year);

        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var totalDays = noOfDays(firstDay, lastDay);

        var contractTotalBillingAmount = 0;
        var contractTotalCtc = 0;

        var permTotalBillingAmount = 0;
        var permTotalCtc = 0;

        var convTotalBillingAmount = 0;
        var convTotalCtc = 0;

        results[0].forEach(item => {
          var workingDays = 0;
          if (item.isNewJoinee && item.isEndJoinee) {
            workingDays = noOfDays(item.joinDate, item.endDate);
            if (workingDays > 1) {
              item.billingAmount =
                (item.billingAmount / totalDays) * workingDays;
              item.ctc = (item.ctc / totalDays) * workingDays;
            } else {
              item.billingAmount = 0;
              item.ctc = 0;
            }
          } else if (item.isNewJoinee) {
            workingDays = noOfDays(item.joinDate, lastDay);
            item.billingAmount = (item.billingAmount / totalDays) * workingDays;
            item.ctc = (item.ctc / totalDays) * workingDays;
          } else if (item.isEndJoinee) {
            workingDays = noOfDays(firstDay, item.endDate);
            item.billingAmount = (item.billingAmount / totalDays) * workingDays;
            item.ctc = (item.ctc / totalDays) * workingDays;
          }
          contractTotalBillingAmount += item.billingAmount;
          contractTotalCtc += item.ctc;
          delete item.isNewJoinee;
          delete item.isEndJoinee;
        });

        results[1].forEach(item => {
          permTotalBillingAmount += item.billingAmount;
          permTotalCtc += item.ctc;
        });

        results[2].forEach(item => {
          convTotalBillingAmount += item.billingAmount;
          convTotalCtc += item.ctc;
        });

        var resData = {
          contract: results[0],
          permanent: results[1],
          conversion: results[2],
          contractTotal: {
            billingAmount: contractTotalBillingAmount,
            ctc: contractTotalCtc
          },
          permanentTotal: {
            billingAmount: permTotalBillingAmount,
            ctc: permTotalCtc
          },
          conversionTotal: {
            billingAmount: convTotalBillingAmount,
            ctc: convTotalCtc
          }
        };
        res.status(200).json({ status: "success", data: resData });
      }
    });
  }
};

exports.getOfferPipelineReport = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.clientId === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var clientId = params.clientId.toString().trim();

    var CONDITIONWHERE = "";
    var CLIENTWHERE = "";

    var serviceMonth = moment
      .utc()
      .tz("Asia/Kolkata")
      .format("YYYY-MM-DD");
    var SQL =
      query.getOfferPipelineReportContract +
      query.getOfferPipelineReportPermanent;
    var SBM = "";
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
    } else if (userRole === "BUHead") {
      CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      // if (clientId) {
      //     CONDITIONWHERE += "AND cm.client_id = '" + clientId + "'";
      // }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CONDITIONWHERE += "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      // if (clientId) {
      //     CONDITIONWHERE += "AND cm.client_id = '" + clientId + "'";
      // }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
    SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);
    SQL = SQL.replace(/CLIENTWHERE/g, CLIENTWHERE);

    console.log("\n > getOfferPipelineReport SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getOfferPipelineReport SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var contractTotalBillingAmount = 0;
        var contractTotalCtc = 0;

        var permTotalBillingAmount = 0;
        var permTotalCtc = 0;

        results[0].forEach(item => {
          contractTotalBillingAmount += item.billingAmount;
          contractTotalCtc += item.ctc;
        });

        results[1].forEach(item => {
          permTotalBillingAmount += item.billingAmount;
          permTotalCtc += item.ctc;
        });

        var resData = {
          contract: results[0],
          permanent: results[1],
          contractTotal: {
            billingAmount: contractTotalBillingAmount,
            ctc: contractTotalCtc
          },
          permanentTotal: {
            billingAmount: permTotalBillingAmount,
            ctc: permTotalCtc
          }
        };
        res.status(200).json({ status: "success", data: resData });
      }
    });
  }
};

exports.getSelect2ClientCost = function (req, res, next) {
  var params = req.body;
  if (params.userRole === undefined || params.pgtCode === undefined) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    // var SQL = "SELECT CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) clientCostName, cl.client_id clientId, cl.name clientName, cl.client_account_type clientType, cs.cost_center_id costCenterId, cs.name costCenterName FROM prms_clients cl LEFT JOIN prms_cost_centers cs ON cs.client_id = cl.client_id INNER JOIN prms_client_cc_mapping AS cm ON cl.client_id=cm.client_id AND cs.cost_center_id=cm.cost_center_id WHERE cs.client_id IS NOT NULL AND is_active = '1' SUBWHERE GROUP BY cl.name,cs.name ORDER BY cl.name, cs.name;";
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var loginUserType = params.loginUserType
      ? params.loginUserType.toString().trim()
      : "";
    var SQL = "";

    if (loginUserType === "") {
      var SQL =
        "SELECT cl.client_id clientCostId, cl.client_id clientId, '' costCenterId, cl.name clientName FROM prms_clients cl INNER JOIN prms_client_cc_mapping AS cm ON cm.client_id=cl.client_id WHERE cm.client_id IS NOT NULL AND cm.is_active = '1' SUBWHERE GROUP BY cl.name ORDER BY cl.name;";
    } else if (loginUserType === "BUHead" || loginUserType === "BM") {
      SQL =
        "SELECT CONCAT(cl.client_id, ':', cs.cost_center_id) clientCostId, cl.client_id clientId, cs.cost_center_id costCenterId, CONCAT(cl.name,IF(cs.name = 'NA','',CONCAT(' (',cs.name,')'))) clientName FROM prms_clients cl LEFT JOIN prms_cost_centers cs ON cs.client_id = cl.client_id INNER JOIN prms_client_cc_mapping AS cm ON cl.client_id=cm.client_id AND cs.cost_center_id=cm.cost_center_id WHERE cs.client_id IS NOT NULL AND cm.is_active = '1' SUBWHERE GROUP BY cl.name,cs.name ORDER BY cl.name, cs.name;";
    } else {
      SQL =
        "SELECT cl.client_id clientCostId, cl.client_id clientId, '' costCenterId, cl.name clientName FROM prms_clients cl INNER JOIN prms_client_cc_mapping AS cm ON cm.client_id=cl.client_id WHERE cm.client_id IS NOT NULL AND cm.is_active = '1' SUBWHERE GROUP BY cl.name ORDER BY cl.name;";
    }
    var WHERE = "";
    if (userRole === "BUHead") {
      WHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
    } else if (userRole === "BM") {
      WHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
    }
    SQL = SQL.replace("SUBWHERE", WHERE);
    console.log("\n > getSelect2ClientCost SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getSelect2ClientCost SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        res.status(200).json({ status: "success", data: results });
      }
    });
  }
};

exports.genHrReportData = function (req, res, next) {
  var params = req.body;

  if (params.searchDate == undefined) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else if (params.searchDate.toString().trim() == "") {
    res.status(400).json({ status: "failure", error: "Send Required Field" });
  } else {
    var SQL =
      query.getHrReportDataJoinee +
      query.getHrReportDataAmdmntJoinee +
      query.getHrReportDataResignee +
      query.getHrReportDataPermanentJoinee +
      query.getHrReportDataPermanentConversion;
    SQL = SQL.replace(/date_re/g, params.searchDate.toString().trim());
    console.log("\n > searchHrReportData SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > searchHrReportData SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        let result = {
          joinee: results[0],
          amdmntJoinee: results[1],
          resignee: results[2],
          permanentJoinee: results[3],
          conversion: results[4]
        };
        generatePDF(result, function (err, data) {
          if (err) {
            console.log("\n> generatePDF CallBack Err ---> ", data);
          } else {
            console.log("\n> generatePDF CallBack Success ---> ", data);
            res.download(data);
          }
        });
      }
    });
  }
};

function generatePDF(jsonData, callback) {
  var filePath = config.upload_path + "/downloads";
  console.log("\n -------------> filePath ---> ", filePath);
  if (!fs.existsSync(filePath)) {
    mkdirp(filePath, function (err) {
      generate(jsonData, filePath, function (err, data) {
        callback(err, data);
      });
    });
  } else {
    generate(jsonData, filePath, function (err, data) {
      callback(err, data);
    });
  }
}

function generate(jsonData, filePath, callback) {

  // jsonData = {
  //   joinee: mockData.joinee,
  //   amdmntJoinee: mockData.amdmntJoinee,
  //   resignee: mockData.resignee,
  //   permanentJoinee: mockData.permanentJoinee,
  //   conversion: mockData.conversion,
  // };
  const PDFDocument = require("pdfkit");
  const PDFTable = require("voilab-pdf-table");
  const PDF = new PDFDocument({
    bufferPages: true,
    autoFirstPage: false,
    size: "A4",
    layout: "landscape",
    margin: 30
  });

  /* Joinees Start */
  PDF.addPage();
  PDF.addContent()
    .fontSize(18)
    .text("Joinees").fontSize(10)
    .text("Total Joinees:" + jsonData.joinee.length, { align: "right" })
    .fontSize(5).fontSize(10);
  const tableJoinee = new PDFTable(PDF, { bottomMargin: 0 });
  tableJoinee.setColumnsDefaults({
    headerBorder: ["T", "B"],
    border: ["T", "B"],
    borderOpacity: 0.2,
    align: "center",
    valign: "center",
    lineGap: 0,
    headerPadding: [5, 0, 5, 0]
  });

  tableJoinee.addColumns([
    {
      id: "Client_Name",
      header: "CLIENT",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Name",
      header: "EMP NAME",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Id",
      header: "EMP ID",
      align: "left",
      width: 50,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Billing_Amount",
      header: "BILLING AMOUNT",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data, draw) {
        return "Rs " + formatAmount(data.Billing_Amount) + "/-";
      }
    },
    {
      id: "CTC",
      header: "CTC",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return "Rs " + formatAmount(data.CTC) + "/-";
      }
    },
    {
      id: "DOJ",
      header: "DOJ",
      align: "left",
      width: 70,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return moment(data.DOJ)
          .utc()
          .tz("Asia/Kolkata")
          .format("DD-MMM-YYYY");
      }
    },
    {
      id: "Skill",
      header: "SKILL",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Sourced_By",
      header: "SOURCED BY",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    }
  ]);

  tableJoinee.onPageAdded(function (tb) {
    tb.addHeader();
  });
  tableJoinee.addBody(jsonData.joinee);
  if (!jsonData.joinee.length) {
    PDF.addContent()
      .fontSize(18)
      .text(`No Records Found!`, 305, 100)
      .fontSize(10);
  }
  /* Joinees End */

  /* Exit Start */
  PDF.addPage();
  PDF.addContent()
    .fontSize(18).text("Exits").fontSize(10)
    .text("Total Exits:" + jsonData.resignee.length, { align: "right" })
    .fontSize(5).fontSize(10);
  const tableExit = new PDFTable(PDF, { bottomMargin: 0 });
  tableExit.setColumnsDefaults({
    headerBorder: ["T", "B"],
    border: ["T", "B"],
    borderOpacity: 0.2,
    align: "center",
    valign: "center",
    lineGap: 0,
    headerPadding: [5, 0, 5, 0]
  });

  tableExit.addColumns([
    {
      id: "Client_Name",
      header: "CLIENT",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Name",
      header: "EMP NAME",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Id",
      header: "EMP ID",
      align: "left",
      width: 70,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Billing_Amount",
      header: "BILLING AMOUNT",
      align: "left",
      width: 80,
      padding: [4, 0, 4, 0],
      valign: "center",
      renderer: function (tb, data, draw) {
        return "Rs " + formatAmount(data.Billing_Amount) + "/-";
      }
    },
    {
      id: "CTC",
      header: "CTC",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return "Rs " + formatAmount(data.CTC) + "/-";
      }
    },
    {
      id: "Status",
      header: "STATUS",
      align: "left",
      width: 70,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Relieved_Date",
      header: "RELIEVED DATE",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return moment(data.Relieved_Date)
          .utc()
          .tz("Asia/Kolkata")
          .format("DD-MMM-YYYY");
      }
    },
    {
      id: "Skill",
      header: "SKILL",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Sourced_By",
      header: "SOURCED BY",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center"
    }
  ]);

  tableExit.onPageAdded(function (tb) {
    tb.addHeader();
  });
  tableExit.addBody(jsonData.resignee);
  if (!jsonData.resignee.length) {
    PDF.addContent()
      .fontSize(18)
      .text(`No Records Found!`, 305, 100)
      .fontSize(10);
  }
  /* Exit End */

  // /* Amendment Start */
  PDF.addPage();
  PDF.addContent()
    .fontSize(18)
    .text("Amendment of Joinees")
    .fontSize(10)
    .text("Total Amendment:" + jsonData.amdmntJoinee.length, { align: "right" })
    .fontSize(5).fontSize(10);
  const tableAmendment = new PDFTable(PDF, { bottomMargin: 0 });
  tableAmendment.setColumnsDefaults({
    headerBorder: ["T", "B"],
    border: ["T", "B"],
    borderOpacity: 0.2,
    align: "center",
    valign: "center",
    lineGap: 0,
    headerPadding: [5, 0, 5, 0]
  });

  tableAmendment.addColumns([
    {
      id: "Client_Name",
      header: "CLIENT",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Name",
      header: "EMP NAME",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Id",
      header: "EMP ID",
      align: "left",
      width: 70,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Billing_Amount",
      header: "BILLING AMOUNT",
      align: "left",
      width: 80,
      padding: [4, 0, 4, 0],
      valign: "center",
      renderer: function (tb, data, draw) {
        return "Rs " + formatAmount(data.Billing_Amount) + "/-";
      }
    },
    {
      id: "CTC",
      header: "CTC",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return "Rs " + formatAmount(data.CTC) + "/-";
      }
    },
    {
      id: "DOJ",
      header: "DOJ",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return moment(data.DOJ)
          .utc()
          .tz("Asia/Kolkata")
          .format("DD-MMM-YYYY");
      }
    },
    {
      id: "Skill",
      header: "SKILL",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Sourced_By",
      header: "SOURCED BY",
      align: "left",
      width: 80,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Mode",
      header: "EMP MODE",
      align: "left",
      width: 70,
      padding: [3, 0, 3, 0],
      valign: "center"
    }
  ]);

  tableAmendment.onPageAdded(function (tb) {
    tb.addHeader();
  });
  tableAmendment.addBody(jsonData.amdmntJoinee);
  if (!jsonData.amdmntJoinee.length) {
    PDF.addContent()
      .fontSize(18)
      .text(`No Records Found!`, 305, 100)
      .fontSize(10);
  }
  // /* Amendment End */

  /* Permanent Start */
  PDF.addPage();
  PDF.addContent()
    .fontSize(18)
    .text("Permanent Joinees")
    .fontSize(10).text("Total Permanent Joinees:" + jsonData.permanentJoinee.length, { align: "right" })
    .fontSize(5).fontSize(10);
  const tablePermanent = new PDFTable(PDF, { bottomMargin: 0 });
  tablePermanent.setColumnsDefaults({
    headerBorder: ["T", "B"],
    border: ["T", "B"],
    borderOpacity: 0.2,
    align: "center",
    valign: "center",
    lineGap: 0,
    headerPadding: [5, 0, 5, 0]
  });

  tablePermanent.addColumns([
    {
      id: "Client_Name",
      header: "CLIENT",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Name",
      header: "EMP NAME",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "CTC",
      header: "CTC Offered by Client",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return "Rs " + formatAmount(data.CTC) + "/-";
      }
    },
    {
      id: "DOJ",
      header: "DOJ",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return moment(data.DOJ)
          .utc()
          .tz("Asia/Kolkata")
          .format("DD-MMM-YYYY");
      }
    },
    {
      id: "Skill",
      header: "SKILL",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Sourced_By",
      header: "SOURCED BY",
      align: "left",
      width: 140,
      padding: [3, 0, 3, 0],
      valign: "center"
    }
  ]);

  tablePermanent.onPageAdded(function (tb) {
    tb.addHeader();
  });
  tablePermanent.addBody(jsonData.permanentJoinee);
  if (!jsonData.permanentJoinee.length) {
    PDF.addContent()
      .fontSize(18)
      .text(`No Records Found!`, 305, 100)
      .fontSize(10);
  }
  /* Permanent End */

  /* Conversion Start */
  PDF.addPage();
  PDF.addContent()
    .fontSize(18)
    .text("Conversion")
    .fontSize(10).text("Total Conversion:" + jsonData.conversion.length, { align: "right", rightMargin: 40 })
    .fontSize(5).fontSize(10);
  const tableConversion = new PDFTable(PDF, { bottomMargin: 0, });
  tableConversion.setColumnsDefaults({
    headerBorder: ["T", "B"],
    border: ["T", "B"],
    borderOpacity: 0.2,
    align: "center",
    valign: "center",
    lineGap: 0,
    headerPadding: [5, 0, 5, 0]
  });

  tableConversion.addColumns([
    {
      id: "Client_Name",
      header: "CLIENT",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Employee_Name",
      header: "EMP NAME",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "CTC",
      header: "CTC Offered by Client",
      align: "left",
      width: 150,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return "Rs " + formatAmount(data.CTC) + "/-";
      }
    },
    {
      id: "DOJ",
      header: "DOJ",
      align: "left",
      width: 100,
      padding: [3, 0, 3, 0],
      valign: "center",
      renderer: function (tb, data) {
        return moment(data.DOJ)
          .utc()
          .tz("Asia/Kolkata")
          .format("DD-MMM-YYYY");
      }
    },
    {
      id: "Skill",
      header: "SKILL",
      align: "left",
      width: 120,
      padding: [3, 0, 3, 0],
      valign: "center"
    },
    {
      id: "Sourced_By",
      header: "SOURCED BY",
      align: "left",
      width: 140,
      padding: [3, 0, 3, 0],
      valign: "center"
    }
  ]);

  tableConversion.onPageAdded(function (tb) {
    tb.addHeader();
  });
  tableConversion.addBody(jsonData.conversion);
  if (!jsonData.conversion.length) {
    PDF.addContent()
      .fontSize(18)
      .text(`No Records Found!`, 305, 100)
      .fontSize(10);
  }
  /* Conversion End */

  var file = filePath + "/HrReportData2.pdf";
  let i;
  let end;
  const range = PDF.bufferedPageRange();
  for (
    i = range.start, end = range.start + range.count, range.start <= end;
    i < end;
    i++
  ) {
    PDF.switchToPage(i);
    PDF.text(`Page ${i + 1} of ${range.count}`, 750, PDF.page.height - 25, {
      lineBreak: false
    });
  }
  PDF.end();
  PDF.pipe(fs.createWriteStream(file)).on("finish", function (err, success) {
    if (err) {
      console.log("\n>>>>>END<<<<<");
      callback(true, err);
    } else {
      console.log("\n>>>>>END<<<<<");
      callback(false, file);
    }
  });
}

exports.netAdditionQ = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    console.log("\n>Month ---> ", month.split(","));
    month = month.split(",");
    var reportJson = [];
    getReports(month, params, reportJson, function (err, results) {
      if (err) {
        console.log("\n > getReports err ---> ", results);
        res.status(200).json({ status: "error", data: results });
      } else {
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var myObj = {};
        var totalRevTemp = 0;
        results.forEach(item => {
          var totalDays = noOfDays(item._firstDay, item._lastDay);
          var workingDays = 0;
          var revenue = 0;
          var ctc = 0;
          // console.log('\n> _firstDay ---> ', item._firstDay);
          // console.log('> _lastDay ---> ', item._lastDay);
          // console.log('> totalDays ---> ', totalDays);
          if (!(item.clientName in myObj)) {
            myObj[item.clientName] = {
              hr_masters: [],
              total_route: [],
              total_sourced: [],
              new_route: 0,
              new_sourced: 0,
              end: 0,
              net: 0,
              total_ctc: 0,
              total_rev: 0,
              avg_billing: 0,
              gross_margin: 0,
              percentage: 0
            };
            myObj[item.clientName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].end += item.isEndJoinee ? 1 : 0;

            myObj[item.clientName].hr_masters.push(item.hrID);
            item.isRouting
              ? myObj[item.clientName].total_route.push(item.hrID)
              : myObj[item.clientName].total_sourced.push(item.hrID);

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                ctc = (item.ctc / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.clientName].total_rev += revenue;
                myObj[item.clientName].total_ctc += ctc;
                totalRevTemp += revenue;
                myObj[item.clientName].gross_margin += grossProfit;
                // console.log('> Both revenue ---> ', revenue);
              }
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, item._lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              ctc = (item.ctc / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              myObj[item.clientName].total_ctc += ctc;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(item._firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              ctc = (item.ctc / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              myObj[item.clientName].total_ctc += ctc;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.clientName].total_rev += item.revenue;
              myObj[item.clientName].total_ctc += item.ctc;
              totalRevTemp += item.revenue;
              myObj[item.clientName].gross_margin += item.grossMargin;
              // console.log('> Revenue --------> ', item.revenue);
            }
          } else if (item.clientName in myObj) {
            myObj[item.clientName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.clientName].end += item.isEndJoinee ? 1 : 0;

            myObj[item.clientName].hr_masters.push(item.hrID);
            item.isRouting
              ? myObj[item.clientName].total_route.push(item.hrID)
              : myObj[item.clientName].total_sourced.push(item.hrID);

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                ctc = (item.ctc / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.clientName].total_rev += revenue;
                myObj[item.clientName].total_ctc += ctc;
                totalRevTemp += revenue;
                myObj[item.clientName].gross_margin += grossProfit;
                // console.log('> Both revenue ---> ', revenue);
              }
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, item._lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              ctc = (item.ctc / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              myObj[item.clientName].total_ctc += ctc;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(item._firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              ctc = (item.ctc / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.clientName].total_rev += revenue;
              myObj[item.clientName].total_ctc += ctc;
              totalRevTemp += revenue;
              myObj[item.clientName].gross_margin += grossProfit;
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.clientName].total_rev += item.revenue;
              myObj[item.clientName].total_ctc += item.ctc;
              totalRevTemp += item.revenue;
              myObj[item.clientName].gross_margin += item.grossMargin;
              // console.log('> Revenue --------> ', item.revenue);
            }
          }
        });
        for (obj in myObj) {
          myObj[obj].hr_masters = [...new Set(myObj[obj].hr_masters)].length;
          myObj[obj].total_route = [...new Set(myObj[obj].total_route)].length;
          myObj[obj].total_sourced = [...new Set(myObj[obj].total_sourced)].length;
          myObj[obj].net = myObj[obj].new_route + myObj[obj].new_sourced - myObj[obj].end;
          myObj[obj].avg_billing =
            myObj[obj].total_rev /
            (myObj[obj].total_route + myObj[obj].total_sourced);
          myObj[obj].percentage =
            (myObj[obj].gross_margin / myObj[obj].total_rev) * 100;
          // myObj[obj].total_rev = Math.round(myObj[obj].total_rev);
          // myObj[obj].avg_billing = Math.round(myObj[obj].avg_billing);
          // myObj[obj].gross_margin = Math.round(myObj[obj].gross_margin);
          // myObj[obj].percentage = Math.round(myObj[obj].percentage);
          myObj[obj].total_rev = myObj[obj].total_rev;
          myObj[obj].total_ctc = myObj[obj].total_ctc;
          myObj[obj].mark_up_percentage = ((myObj[obj].total_rev -  myObj[obj].total_ctc)/ myObj[obj].total_ctc)*100;
          myObj[obj].avg_billing = myObj[obj].avg_billing;
          myObj[obj].gross_margin = myObj[obj].gross_margin;
          myObj[obj].percentage = myObj[obj].percentage;
        }
        var data = [];
        for (i in myObj) {
          let ob = {
            clientName: i,
            hr_masters: myObj[i].hr_masters,
            total_route: myObj[i].total_route,
            total_sourced: myObj[i].total_sourced,
            new_route: myObj[i].new_route,
            new_sourced: myObj[i].new_sourced,
            end: myObj[i].end,
            net: myObj[i].net,
            total_rev: myObj[i].total_rev,
            total_ctc: myObj[i].total_ctc,
            avg_billing: myObj[i].avg_billing,
            gross_margin: myObj[i].gross_margin,
            mark_up_percentage: myObj[i].mark_up_percentage,
            percentage: myObj[i].percentage
          };
          data.push(ob);
        }
        res.status(200).json({ status: "success", data: data });
      }
    });

    function getReports(months, params, reportJson, callback) {
      try {
        var row = 0;
        var insertRow = function (months, params, reportJson) {
          var month = months[row];
          var fyear = params.year.toString().trim();
          var userRole = params.userRole.toString().trim();
          var pgtCode = params.pgtCode.toString().trim();
          var searchBy = params.searchBy.toString().trim();
          var searchValue = params.searchValue.toString().trim();
          var seniorManager = params.seniorManager ? true : false;
          var CONDITIONWHERE = "";
          var WHERE_HR_STATUS =
            "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";

          var splitYear = fyear.split("-");
          var year;
          if (month >= 4) {
            year = splitYear[0];
          } else {
            year = splitYear[1];
          }

          var serviceMonth = year + "-" + month;
          var SQL = query.netAdditionProjected;
          var SBM = "";
          if (
            userRole === "SuperAdmin" ||
            userRole === "Admin" ||
            userRole === "HRAdmin" ||
            userRole === "HRAdminReadOnly"
          ) {
            if (searchBy == "BUHead" && searchValue) {
              CONDITIONWHERE =
                "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
            } else if (searchBy == "BM" && searchValue && seniorManager) {
              SBM =
                "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
              SBM = SBM.replace(/PGTCODE/g, searchValue);
              console.log("\n > SBM SQL ---> ", SBM);
              CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
            } else if (searchBy === "BM" && searchValue) {
              CONDITIONWHERE =
                "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
            }
          } else if (userRole === "BUHead") {
            CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
            /* New Concept */
            if (searchBy == "BM" && searchValue && seniorManager) {
              SBM =
                "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
              SBM = SBM.replace(/PGTCODE/g, searchValue);
              console.log("\n > SBM SQL ---> ", SBM);
              CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
            } else if (searchBy === "BM" && searchValue) {
              CONDITIONWHERE +=
                " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
            }
            /* New Concept */
          } else if (userRole === "BM" && seniorManager) {
            SBM =
              "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
            SBM = SBM.replace(/PGTCODE/g, pgtCode);
            console.log("\n > SBM SQL ---> ", SBM);
            CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
            /* New Concept */
            if (searchBy == "BM" && searchValue) {
              CONDITIONWHERE +=
                " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
            }
            /* New Concept */
          } else if (userRole === "BM") {
            CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
            /* New Concept */
            if (searchBy === "BM" && searchValue) {
              CONDITIONWHERE +=
                " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
            }
            /* New Concept */
          } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
            res.status(400).json({ status: 'failure', error: "Invalid Role" });
            return;
          }

          SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
          SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
          SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);

          console.log("\n > netAdditionProjected SQL(" + row + ") ---> ", SQL);
          // row++;
          // if (row >= months.length) {
          //     callback(false, 'Migrated Successfully');
          // } else {
          //     setTimeout(() => { insertRow(months, params, reportJson); });
          // }
          db.query(SQL, function (error, results, fields) {
            if (error) {
              console.log(
                "\n > netAdditionProjected SQL Err ---> ",
                error.code
              );
              res.status(400).json({ status: "failure", error: error.code });
            } else {
              var currentDate = year + "/" + month + "/" + "01";
              var date = new Date(currentDate),
                y = date.getFullYear(),
                m = date.getMonth();
              var firstDay = new Date(y, m, 1);
              var lastDay = new Date(y, m + 1, 0);
              firstDay = moment(firstDay)
                .utc()
                .tz("Asia/Kolkata")
                .format("YYYY-MM-DD");
              lastDay = moment(lastDay)
                .utc()
                .tz("Asia/Kolkata")
                .format("YYYY-MM-DD");
              results.forEach(item => {
                item["_firstDay"] = firstDay;
                item["_lastDay"] = lastDay;
              });
              reportJson = reportJson.concat(results);
              row++;
              if (row >= months.length) {
                callback(false, reportJson);
              } else {
                setTimeout(() => {
                  insertRow(months, params, reportJson);
                });
              }
            }
          });
        };
        insertRow(months, params, reportJson);
      } catch (e) {
        console.log("\n > try catch error ---> ", e);
        callback(true, "try catch error");
      }
    }
  }
};


exports.getGMLessthanFifteen = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined ||
    params.clientId === undefined

  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var clientId = params.clientId.toString().trim();

    var CONDITIONWHERE = "";
    var CLIENTWHERE = "";

    var SBM = "";
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }

      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
    } else if (userRole === "BUHead") {
      CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CLIENTWHERE = "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      if (clientId) {
        CONDITIONWHERE += "AND cm.client_id = '" + clientId + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy === "BM" && searchValue) {
        CONDITIONWHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
      }

      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";
    months.forEach(month => {

      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }

      var serviceMonth = year + "-" + month;

      SQL += (SQL != "") ? " UNION ALL " : "";
      SQL += query.getGMLessthanFifteen;
      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/MONTH/g, month);
      SQL = SQL.replace(/YEAR/g, year);
      SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);
      SQL = SQL.replace(/CLIENTWHERE/g, CLIENTWHERE);

    });

    SQL1 = query.getGMLessthanFifteenRapper;
    SQL1 = SQL1.replace("SQLRE", SQL);

    console.log("\n > getGMLessthanFifteen SQL ---> ", SQL1);
    db.query(SQL1, function (error, results, fields) {
      if (error) {
        console.log("\n > getGMLessthanFifteen SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {

        res.status(200).json({ status: "success", data: results });
      }
    });
  }
};

exports.getNetAdditionByPractice = function (req, res, next) {
  var params = req.body;
  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    console.log("\n>Month ---> ", month.split(","));
    month = month.split(",");
    var reportJson = [];
    getReports(month, params, reportJson, function (err, results) {
      if (err) {
        console.log("\n > getReports err ---> ", results);
        res.status(200).json({ status: "error", data: results });
      } else {
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };
        var myObj = {};
        var totalRevTemp = 0;
        results.forEach(item => {
          var totalDays = noOfDays(item._firstDay, item._lastDay);
          var workingDays = 0;
          var revenue = 0;
          if (!(item.horizontalName in myObj)) {
            myObj[item.horizontalName] = {
              hr_masters: [],
              total_route: [],
              total_sourced: [],
              new_route: 0,
              new_sourced: 0,
              end: 0,
              net: 0,
              total_rev: 0,
              avg_billing: 0,
              gross_margin: 0,
              percentage: 0
            };
            myObj[item.horizontalName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.horizontalName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.horizontalName].end += item.isEndJoinee ? 1 : 0;

            myObj[item.horizontalName].hr_masters.push(item.hrID);
            item.isRouting
              ? myObj[item.horizontalName].total_route.push(item.hrID)
              : myObj[item.horizontalName].total_sourced.push(item.hrID);

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.horizontalName].total_rev += revenue;
                totalRevTemp += revenue;
                myObj[item.horizontalName].gross_margin += grossProfit;
                // console.log('> Both revenue ---> ', revenue);
              }
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, item._lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.horizontalName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.horizontalName].gross_margin += grossProfit;
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(item._firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.horizontalName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.horizontalName].gross_margin += grossProfit;
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.horizontalName].total_rev += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.horizontalName].gross_margin += item.grossMargin;
              // console.log('> Revenue --------> ', item.revenue);
            }
          } else if (item.horizontalName in myObj) {
            myObj[item.horizontalName].new_route +=
              item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.horizontalName].new_sourced +=
              !item.isRouting && item.isNewJoinee ? 1 : 0;
            myObj[item.horizontalName].end += item.isEndJoinee ? 1 : 0;

            myObj[item.horizontalName].hr_masters.push(item.hrID);
            item.isRouting
              ? myObj[item.horizontalName].total_route.push(item.hrID)
              : myObj[item.horizontalName].total_sourced.push(item.hrID);

            if (item.isNewJoinee && item.isEndJoinee) {
              workingDays = noOfDays(item.joinDate, item.endDate);
              if (workingDays > 1) {
                revenue = (item.revenue / totalDays) * workingDays;
                grossProfit = (item.grossMargin / totalDays) * workingDays;
                myObj[item.horizontalName].total_rev += revenue;
                totalRevTemp += revenue;
                myObj[item.horizontalName].gross_margin += grossProfit;
                // console.log('> Both revenue ---> ', revenue);
              }
            } else if (item.isNewJoinee) {
              workingDays = noOfDays(item.joinDate, item._lastDay);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.horizontalName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.horizontalName].gross_margin += grossProfit;
              // console.log('> New revenue ----> ', revenue);
            } else if (item.isEndJoinee) {
              workingDays = noOfDays(item._firstDay, item.endDate);
              revenue = (item.revenue / totalDays) * workingDays;
              grossProfit = (item.grossMargin / totalDays) * workingDays;
              myObj[item.horizontalName].total_rev += revenue;
              totalRevTemp += revenue;
              myObj[item.horizontalName].gross_margin += grossProfit;
              // console.log('> End revenue ----> ', revenue);
            } else {
              myObj[item.horizontalName].total_rev += item.revenue;
              totalRevTemp += item.revenue;
              myObj[item.horizontalName].gross_margin += item.grossMargin;
              // console.log('> Revenue --------> ', item.revenue);
            }
          }
        });
        for (obj in myObj) {
          myObj[obj].hr_masters = [...new Set(myObj[obj].hr_masters)].length;
          myObj[obj].total_route = [...new Set(myObj[obj].total_route)].length;
          myObj[obj].total_sourced = [
            ...new Set(myObj[obj].total_sourced)
          ].length;
          myObj[obj].net =
            myObj[obj].new_route + myObj[obj].new_sourced - myObj[obj].end;
          myObj[obj].avg_billing =
            myObj[obj].total_rev /
            (myObj[obj].total_route + myObj[obj].total_sourced);
          myObj[obj].percentage =
            (myObj[obj].gross_margin / myObj[obj].total_rev) * 100;

          myObj[obj].total_rev = myObj[obj].total_rev;
          myObj[obj].avg_billing = myObj[obj].avg_billing;
          myObj[obj].gross_margin = myObj[obj].gross_margin;
          myObj[obj].percentage = myObj[obj].percentage;
        }
        var data = [];
        for (i in myObj) {
          let ob = {
            horizontalName: i,
            hr_masters: myObj[i].hr_masters,
            total_route: myObj[i].total_route,
            total_sourced: myObj[i].total_sourced,
            new_route: myObj[i].new_route,
            new_sourced: myObj[i].new_sourced,
            end: myObj[i].end,
            net: myObj[i].net,
            total_rev: myObj[i].total_rev,
            avg_billing: myObj[i].avg_billing,
            gross_margin: myObj[i].gross_margin,
            percentage: myObj[i].percentage
          };
          data.push(ob);
        }
        res.status(200).json({ status: "success", data: data });
      }
    });
  }
  function getReports(months, params, reportJson, callback) {
    try {
      var row = 0;
      var insertRow = function (months, params, reportJson) {
        var month = months[row];
        var fyear = params.year.toString().trim();
        var userRole = params.userRole.toString().trim();
        var pgtCode = params.pgtCode.toString().trim();
        var searchBy = params.searchBy.toString().trim();
        var searchValue = params.searchValue.toString().trim();
        var seniorManager = params.seniorManager ? true : false;
        var CONDITIONWHERE = "";
        var WHERE_HR_STATUS =
          "'No Show', 'Offer Declined', 'Offer Released', 'Offer Revoked', 'Postponed'";

        var splitYear = fyear.split("-");
        var year;
        if (month >= 4) {
          year = splitYear[0];
        } else {
          year = splitYear[1];
        }

        var serviceMonth = year + "-" + month;
        var SQL = query.getPractice;
        var SBM = "";
        if (
          userRole === "SuperAdmin" ||
          userRole === "Admin" ||
          userRole === "HRAdmin" ||
          userRole === "HRAdminReadOnly"
        ) {
          if (searchBy == "BUHead" && searchValue) {
            CONDITIONWHERE =
              "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
          } else if (searchBy == "BM" && searchValue && seniorManager) {
            SBM =
              "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
            SBM = SBM.replace(/PGTCODE/g, searchValue);
            console.log("\n > SBM SQL ---> ", SBM);
            CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          } else if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE =
              "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          }
        } else if (userRole === "BUHead") {
          CONDITIONWHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
          /* New Concept */
          if (searchBy == "BM" && searchValue && seniorManager) {
            SBM =
              "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
            SBM = SBM.replace(/PGTCODE/g, searchValue);
            console.log("\n > SBM SQL ---> ", SBM);
            CONDITIONWHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          } else if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE +=
              " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole === "BM" && seniorManager) {
          SBM =
            "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
          SBM = SBM.replace(/PGTCODE/g, pgtCode);
          console.log("\n > SBM SQL ---> ", SBM);
          CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
          /* New Concept */
          if (searchBy == "BM" && searchValue) {
            CONDITIONWHERE +=
              " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole === "BM") {
          CONDITIONWHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
          /* New Concept */
          if (searchBy === "BM" && searchValue) {
            CONDITIONWHERE +=
              " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
          }
          /* New Concept */
        } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
          res.status(400).json({ status: 'failure', error: "Invalid Role" });
          return;
        }

        SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
        SQL = SQL.replace(/WHERE_HR_STATUS/g, WHERE_HR_STATUS);
        SQL = SQL.replace(/CONDITIONWHERE/g, CONDITIONWHERE);

        console.log("\n > getNetAdditionByPractice SQL(" + row + ") ---> ", SQL);
        // row++;
        // if (row >= months.length) {
        //     callback(false, 'Migrated Successfully');
        // } else {
        //     setTimeout(() => { insertRow(months, params, reportJson); });
        // }
        db.query(SQL, function (error, results, fields) {
          if (error) {
            console.log(
              "\n > getNetAdditionByPractice SQL Err ---> ",
              error.code
            );
            res.status(400).json({ status: "failure", error: error.code });
          } else {
            var currentDate = year + "/" + month + "/" + "01";
            var date = new Date(currentDate),
              y = date.getFullYear(),
              m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, m + 1, 0);
            firstDay = moment(firstDay)
              .utc()
              .tz("Asia/Kolkata")
              .format("YYYY-MM-DD");
            lastDay = moment(lastDay)
              .utc()
              .tz("Asia/Kolkata")
              .format("YYYY-MM-DD");
            results.forEach(item => {
              item["_firstDay"] = firstDay;
              item["_lastDay"] = lastDay;
            });
            reportJson = reportJson.concat(results);
            row++;
            if (row >= months.length) {
              callback(false, reportJson);
            } else {
              setTimeout(() => {
                insertRow(months, params, reportJson);
              });
            }
          }
        });
      };
      insertRow(months, params, reportJson);
    } catch (e) {
      console.log("\n > try catch error ---> ", e);
      callback(true, "try catch error");
    }
  }

};


exports.getdashboardCount = function (req, res, next) {
  var params = req.body;

  if (
    params.userRole === undefined ||
    params.pgtCode === undefined ||
    params.searchBy === undefined ||
    params.searchValue === undefined ||
    params.year === undefined ||
    params.month === undefined
  ) {
    res.status(400).json({ status: "failure", error: "Invalid payload data" });
  } else {
    var month = params.month.toString().trim();
    var fyear = params.year.toString().trim();
    var userRole = params.userRole.toString().trim();
    var pgtCode = params.pgtCode.toString().trim();
    var searchBy = params.searchBy.toString().trim();
    var searchValue = params.searchValue.toString().trim();
    var seniorManager = params.seniorManager ? true : false;

    var WHERE = "";
    var SUBWHERE = "";
    var SUBWHEREE = "";

    // AND cmTotal.pgt_code_bsns_mgr = 'PGT4848' GROUP BY cmTotal.client_name
    if (
      userRole === "SuperAdmin" ||
      userRole === "Admin" ||
      userRole === "HRAdmin" ||
      userRole === "HRAdminReadOnly"
    ) {
      if (searchBy == "BUHead" && searchValue) {
        WHERE = "AND cm.pgt_code_pandl_head = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + searchValue + "'";
        SUBWHEREE = "AND cmRevenue.pgt_code_pandl_head = '" + searchValue + "'";
      } else if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy == "BM" && searchValue) {
        WHERE = "AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
    } else if (userRole === "BUHead") {
      WHERE = "AND cm.pgt_code_pandl_head = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_pandl_head = '" + pgtCode + "'";
      SUBWHEREE = "AND cmRevenue.pgt_code_pandl_head = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue && seniorManager) {
        SBM =
          "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
        SBM = SBM.replace(/PGTCODE/g, searchValue);
        console.log("\n > SBM SQL ---> ", SBM);
        WHERE += " AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
        SUBWHEREE += " AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      } else if (searchBy === "BM" && searchValue) {
        WHERE += " AND cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " AND cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " AND cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM" && seniorManager) {
      SBM =
        "select employee_code from prms_app_employees where user_type = 'BM' and (report_manager_id in (select app_employee_id from prms_app_employees where employee_code = 'PGTCODE') or employee_code = 'PGTCODE')";
      SBM = SBM.replace(/PGTCODE/g, pgtCode);
      console.log("\n > SBM SQL ---> ", SBM);
      WHERE = "AND cm.pgt_code_bsns_mgr IN (" + SBM + ")";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr IN (" + SBM + ")";
      SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr IN (" + SBM + ")";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " OR cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole === "BM") {
      WHERE = "AND cm.pgt_code_bsns_mgr = '" + pgtCode + "'";
      SUBWHERE = "AND cmTotal.pgt_code_bsns_mgr = '" + pgtCode + "'";
      SUBWHEREE = "AND cmRevenue.pgt_code_bsns_mgr = '" + pgtCode + "'";
      /* New Concept */
      if (searchBy == "BM" && searchValue) {
        WHERE += " OR cm.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHERE += " OR cmTotal.pgt_code_bsns_mgr = '" + searchValue + "'";
        SUBWHEREE += " OR cmRevenue.pgt_code_bsns_mgr = '" + searchValue + "'";
      }
      /* New Concept */
    } else if (userRole !== 'HR' && userRole !== 'Finance' && userRole !== 'FinanceAdmin' && userRole !== 'FinanceReadOnly' && userRole !== 'TA' && userRole !== 'PayrollAdmin') {
      res.status(400).json({ status: 'failure', error: "Invalid Role" });
      return;
    }

    var months = month.split(",");
    var SQL = "";

    months.forEach(month => {
      var nxtMonth = "";
      var nxtYear = "";

      var splitYear = fyear.split("-");
      var year;
      if (month >= 4) {
        year = splitYear[0];
      } else {
        year = splitYear[1];
      }

      if (month == 12) {
        nxtMonth = "01";
        nxtYear = Number(year) + 1;
      } else {
        nxtMonth = Number(month) + 1;
        nxtYear = year;
      }

      var serviceMonth = year + "-" + month.toString().padStart(2, "0");
      var nxtServiceMonth =
        nxtYear + "-" + nxtMonth.toString().padStart(2, "0");

      var getContractSummary = query.getdashboardContract;
      var getContractInvoiced = query.getdashboardContractInvoiced;

      SQL += getContractSummary;
      SQL += getContractInvoiced;

      SQL = SQL.replace(/SERVICEMONTH/g, serviceMonth);
      SQL = SQL.replace(/nextServieMonth/g, nxtServiceMonth);

      SQL = SQL.replace(/WHEREMONTH/g, month.toString().padStart(2, "0"));
      SQL = SQL.replace(/WHEREYEAR/g, year);

      SQL = SQL.replace(
        /nextMonthWhere/g,
        nxtMonth.toString().padStart(2, "0")
      );
      SQL = SQL.replace(/nextYearWhere/g, nxtYear);

      SQL = SQL.replace(/SUBCONDITONALWHERE/g, SUBWHEREE);
      SQL = SQL.replace(/SUBCONDITONWHERE/g, SUBWHERE);
      SQL = SQL.replace(/CONDITONWHERE/g, WHERE);
    });

    console.log("\n > getdashboardCount SQL ---> ", SQL);
    db.query(SQL, function (error, results, fields) {
      if (error) {
        console.log("\n > getdashboardCount SQL Err ---> ", error.code);
        res.status(400).json({ status: "failure", error: error.code });
      } else {
        var indx = 0;
        var noOfDays = function (dateFrom, dateTo) {
          var date1 = new Date(dateFrom);
          var date2 = new Date(dateTo);
          var Difference_In_Time = date2.getTime() - date1.getTime();
          var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
          return Difference_In_Days + 1;
        };

        var contract = {
          totalBilled: [],
          grossMargin: 0,
          grossProfit: 0,
          revenue: 0,
          ctc: 0,
          mspFee: 0,
          marginVendor: 0,
          nxtSummary: 0,
          attrition: 0,
          avgBilling: 0
        };

        var invoiced = {
          totalInvoiced: [],
          grossMargin: 0,
          grossProfit: 0,
          revenue: 0,
          ctc: 0,
          mspFee: 0,
          marginVendor: 0,
          nxtSummary: 0,
          attrition: 0,
          avgBilling: 0
        };

        var data = {
          searchType: params.searchType ? params.searchType.toString().trim() : '',
          contractSummary: {},
          contractInvoiced: {},

        };

        months.forEach(month => {
          var contractSummary = results[indx];
          var contractInvoiced = results[indx + 1];
          indx += 2;
          var totalBilled = 0;
          var ctc = 0;
          var grossProfit = 0;
          var marginVendor = 0;
          var revenue = 0;
          var workingDays = 0;
          var avgTotal = 0;

          var splitYear = fyear.split("-");
          var year;
          if (month >= 4) {
            year = splitYear[0];
          } else {
            year = splitYear[1];
          }

          var currentDate = year + "/" + month + "/" + "01";
          var date = new Date(currentDate),
            y = date.getFullYear(),
            m = date.getMonth();
          var firstDay = new Date(y, m, 1);
          var lastDay = new Date(y, m + 1, 0);
          firstDay = moment(firstDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          lastDay = moment(lastDay)
            .utc()
            .tz("Asia/Kolkata")
            .format("YYYY-MM-DD");
          var totalDays = noOfDays(firstDay, lastDay);

          var nxtSummary = 0;
          var mspFee = 0;
          contractSummary.forEach(e => {
            avgTotal += e.revenue;
            if (e.isNewJoinee && e.isEndJoinee) {
              workingDays = noOfDays(e.joinDate, e.endDate);
              totalBilled += 1;

              if (workingDays > 1) {
                revenue += (e.revenue / totalDays) * workingDays;
                ctc += (e.ctc / totalDays) * workingDays;
                grossProfit += (e.grossProfit / totalDays) * workingDays;
                marginVendor += (e.marginVendor / totalDays) * workingDays;
                mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
              }
            } else if (e.isNewJoinee) {
              workingDays = noOfDays(e.joinDate, lastDay);
              totalBilled += 1;
              revenue += (e.revenue / totalDays) * workingDays;
              ctc += (e.ctc / totalDays) * workingDays;
              grossProfit += (e.grossProfit / totalDays) * workingDays;
              marginVendor += (e.marginVendor / totalDays) * workingDays;
              mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else if (e.isEndJoinee) {
              workingDays = noOfDays(firstDay, e.endDate);
              totalBilled += 1;
              revenue += (e.revenue / totalDays) * workingDays;
              ctc += (e.ctc / totalDays) * workingDays;
              grossProfit += (e.grossProfit / totalDays) * workingDays;
              marginVendor += (e.marginVendor / totalDays) * workingDays;
              mspFee += e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else {
              totalBilled += 1;
              revenue += e.revenue;
              ctc += e.ctc;
              grossProfit += e.grossProfit;
              marginVendor += e.marginVendor;
              mspFee += e.mspFee * e.revenue;
              // console.log('\n > isEndJoinee ---> ', e.isEndJoinee);
            }
            if (e.isEndJoinee) {
              nxtSummary += 1;
            }
            contract.totalBilled.push(e.hrId);
          });
          contract.grossProfit += grossProfit;
          contract.revenue += revenue;
          contract.ctc += ctc;
          contract.mspFee += mspFee;
          contract.marginVendor += marginVendor;
          contract.nxtSummary += nxtSummary;
          contract.avgBilling += (contractSummary.length > 0 ? avgTotal / contractSummary.length : 0);
          var totalInvoiced = 0;
          var ctcInvoiced = 0;
          var grossProfitInvoiced = 0;
          var marginVendorInvoiced = 0;
          var revenueInvoiced = 0;
          var workingDaysInvoiced = 0;
          var totalDaysInvoiced = noOfDays(firstDay, lastDay);
          var mspFeeInvoiced = 0;
          var avgTotalInv = 0;
          contractInvoiced.forEach(e => {
            avgTotalInv += e.revenue;
            if (e.isNewJoinee && e.isEndJoinee) {
              workingDaysInvoiced = noOfDays(e.joinDate, e.endDate);
              totalInvoiced += 1;
              if (workingDaysInvoiced > 1) {
                revenueInvoiced += e.revenue;
                ctcInvoiced +=
                  (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
                grossProfitInvoiced +=
                  (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
                marginVendorInvoiced +=
                  (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
                mspFeeInvoiced +=
                  e.mspFee * ((e.revenue / totalDays) * workingDays);
              }
            } else if (e.isNewJoinee) {
              workingDaysInvoiced = noOfDays(e.joinDate, lastDay);
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
              grossProfitInvoiced +=
                (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
              marginVendorInvoiced +=
                (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
              mspFeeInvoiced +=
                e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else if (e.isEndJoinee) {
              workingDaysInvoiced = noOfDays(firstDay, e.endDate);
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += (e.ctc / totalDaysInvoiced) * workingDaysInvoiced;
              grossProfitInvoiced +=
                (e.grossProfit / totalDaysInvoiced) * workingDaysInvoiced;
              marginVendorInvoiced +=
                (e.marginVendor / totalDaysInvoiced) * workingDaysInvoiced;
              mspFeeInvoiced +=
                e.mspFee * ((e.revenue / totalDays) * workingDays);
            } else {
              totalInvoiced += 1;
              revenueInvoiced += e.revenue;
              ctcInvoiced += e.ctc;
              grossProfitInvoiced += e.grossProfit;
              marginVendorInvoiced += e.marginVendor;
              mspFeeInvoiced += e.mspFee * e.revenue;
            }
            invoiced.totalInvoiced.push(e.totalInvoiced);
          });
          invoiced.revenue += revenueInvoiced;
          invoiced.ctc += ctcInvoiced;
          invoiced.mspFee += mspFeeInvoiced;
          invoiced.marginVendor += marginVendorInvoiced;
          invoiced.nxtSummary += nxtSummary;
          invoiced.avgBilling += (contractInvoiced.length > 0 ? avgTotalInv / contractInvoiced.length : 0);

        });

        contract.totalBilled = [...new Set(contract.totalBilled)].length;
        contract.grossMargin = Math.round(
          (contract.grossProfit / contract.revenue) * 100
        );
        contract.grossProfit = Math.round(contract.grossProfit);
        contract.revenue = Math.round(contract.revenue);
        contract.ctc = Math.round(contract.ctc);
        contract.mspFee = Math.round(contract.mspFee);
        contract.marginVendor = Math.round(contract.marginVendor);
        contract.nxtSummary = contract.totalBilled - contract.nxtSummary;
        contract.attrition = parseFloat(
          ((contract.totalBilled - contract.nxtSummary) /
            contract.totalBilled) *
          100
        ).toFixed(2);

        invoiced.totalInvoiced = [...new Set(invoiced.totalInvoiced)].length;
        invoiced.grossMargin = Math.round(
          ((invoiced.revenue -
            invoiced.ctc -
            invoiced.marginVendor -
            invoiced.mspFee) /
            invoiced.revenue) *
          100
        );
        (invoiced.grossProfit = Math.round(
          invoiced.revenue -
          invoiced.ctc -
          invoiced.marginVendor -
          invoiced.mspFee
        )),
          (invoiced.revenue = Math.round(invoiced.revenue));
        invoiced.ctc = Math.round(invoiced.ctc);
        invoiced.mspFee = Math.round(invoiced.mspFee);
        invoiced.marginVendor = Math.round(invoiced.marginVendor);
        invoiced.nxtSummary = contract.nxtSummary;
        invoiced.attrition = contract.attrition;

        invoiced.grossMargin =
          invoiced.grossMargin < 0 ? 0 : invoiced.grossMargin;
        invoiced.grossProfit =
          invoiced.grossProfit < 0 ? 0 : invoiced.grossProfit;

        data.contractSummary = contract;
        data.contractInvoiced = invoiced;

        res.status(200).json({ status: "success", data: data });
      }
    });
  }
};