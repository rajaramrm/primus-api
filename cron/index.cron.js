var CronJob = require('cron').CronJob;
var db = require('../app/db/manageDB');
var config = require('../config');
var query = require('../app/queries/report.query');
var nodemailer = require('nodemailer');
var moment = require('moment-timezone');
var fs = require('fs');
var mkdirp = require('mkdirp');
var mockData = require("../app/dailyReport.json");
const formatAmount = require('indian-currency-formatter');

// '59 18 * * 1-5' '00 19 * * 1-5'
exports.dailyReport = new CronJob('00 19 * * 1-5', function () {
    var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
    console.log('\n> dailyReport ---> ', timesNow);
    sentDailyReport(timesNow);
    // sentDailyReport('2018-06-15');
});

// var timesNow = moment.utc().tz("Asia/Kolkata").format("YYYY-MM-DD HH:mm:ss");
// var timesNow = '2021-12-15';
// console.log('\n> dailyReport ---> ', timesNow);
// sentDailyReport(timesNow);

function sentDailyReport(date) {
    date = moment(date).utc().tz("Asia/Kolkata").format("YYYY-MM-DD");
    var SQL = query.getHrReportDataJoinee + query.getHrReportDataAmdmntJoinee + query.getHrReportDataResignee + query.getHrReportDataPermanentJoinee + query.getHrReportDataPermanentConversion;
    SQL = SQL.replace(/date_re/g, date);
    db.query(SQL, function (error, results, fields) {
        if (error) {
            console.log("\n > searchHrReportData SQL Err ---> ", error.code);
            res.status(400).json({ status: 'failure', error: error.code });
        } else {
            let result = {
                joinee: results[0],
                amdmntJoinee: results[1],
                resignee: results[2],
                permanentJoinee: results[3],
                conversion: results[4]
            };
            // let result = {
            //     joinee: mockData.joinee,
            //     amdmntJoinee: mockData.amdmntJoinee,
            //     resignee: mockData.resignee,
            //     permanentJoinee: mockData.permanentJoinee,
            //     conversion: mockData.conversion,
            // };
            generatePDF(result, function (err, data) {
                if (err) {
                    console.log('\n> generatePDF CallBack Err ---> ', data);
                } else {
                    console.log('\n> generatePDF CallBack Success ---> ', data);
                    sentMail(data);
                }
            });
        }
    });
}

function generatePDF(jsonData, callback) {
    var filePath = config.upload_path + '/crons';
    console.log('\n -------------> filePath ---> ', filePath);
    if (!fs.existsSync(filePath)) {
        mkdirp(filePath, function (err) {
            generate(jsonData, filePath, function (err, data) {
                callback(err, data);
            });
        });
    } else {
        generate(jsonData, filePath, function (err, data) {
            callback(err, data);
        });
    }
}

function generate(jsonData, filePath, callback) {
    const PDFDocument = require('pdfkit');
    const PDFTable = require('voilab-pdf-table');
    const PDF = new PDFDocument({ bufferPages: true, autoFirstPage: false, size: 'A4', layout: 'landscape', margin: 30 });

    /* Joinees Start */
    if (jsonData.joinee.length) {
        // PDF.addContent().fontSize(18).text(`No Records Found!`, 305, 100).fontSize(10);
        PDF.addPage();
        PDF.addContent().fontSize(18).text('Joinees').fontSize(10)
        .text("Total Joinees:" + jsonData.joinee.length, { align: "right" })
        .fontSize(5).fontSize(10);
        const tableJoinee = new PDFTable(PDF, { bottomMargin: 0 });
        tableJoinee.setColumnsDefaults({
            headerBorder: ['T', 'B'],
            border: ['T', 'B'],
            borderOpacity: 0.2,
            align: 'center',
            valign: 'center',
            lineGap: 0,
            headerPadding: [5, 0, 5, 0]
        });

        tableJoinee.addColumns([
            {
                id: 'Client_Name',
                header: 'CLIENT',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Name',
                header: 'EMP NAME',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Id',
                header: 'EMP ID',
                align: 'left',
                width: 50,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Billing_Amount',
                header: 'BILLING AMOUNT',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data, draw) {
                    return "Rs " + formatAmount(data.Billing_Amount) + "/-";
                }
            },
            {
                id: 'CTC',
                header: 'CTC',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return "Rs " + formatAmount(data.CTC) + "/-";
                }
            },
            {
                id: 'DOJ',
                header: 'DOJ',
                align: 'left',
                width: 70,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return moment(data.DOJ).utc().tz("Asia/Kolkata").format("DD-MMM-YYYY");
                }
            },
            {
                id: 'Skill',
                header: 'SKILL',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Sourced_By',
                header: 'SOURCED BY',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            }
        ]);

        tableJoinee.onPageAdded(function (tb) {
            tb.addHeader();
        });
        tableJoinee.addBody(jsonData.joinee);
    }
    /* Joinees End */

    /* Exit Start */
    if (jsonData.resignee.length) {
        // PDF.addContent().fontSize(18).text(`No Records Found!`, 305, 100).fontSize(10);
        PDF.addPage();
        PDF.addContent().fontSize(18).text('Exits').fontSize(10)
        .text("Total Exits:" + jsonData.resignee.length, { align: "right" })
        .fontSize(5).fontSize(10);
        const tableExit = new PDFTable(PDF, { bottomMargin: 0 });
        tableExit.setColumnsDefaults({
            headerBorder: ['T', 'B'],
            border: ['T', 'B'],
            borderOpacity: 0.2,
            align: 'center',
            valign: 'center',
            lineGap: 0,
            headerPadding: [5, 0, 5, 0]
        });

        tableExit.addColumns([
            {
                id: 'Client_Name',
                header: 'CLIENT',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Name',
                header: 'EMP NAME',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Id',
                header: 'EMP ID',
                align: 'left',
                width: 70,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Billing_Amount',
                header: 'BILLING AMOUNT',
                align: 'left',
                width: 80,
                padding: [4, 0, 4, 0],
                valign: 'center',
                renderer: function (tb, data, draw) {
                    return "Rs " + formatAmount(data.Billing_Amount) + "/-";
                }
            },
            {
                id: 'CTC',
                header: 'CTC',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return "Rs " + formatAmount(data.CTC) + "/-";
                }
            },
            {
                id: 'Status',
                header: 'STATUS',
                align: 'left',
                width: 70,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Relieved_Date',
                header: 'RELIEVED DATE',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return moment(data.Relieved_Date).utc().tz("Asia/Kolkata").format("DD-MMM-YYYY");
                }
            },
            {
                id: 'Skill',
                header: 'SKILL',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Sourced_By',
                header: 'SOURCED BY',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center'
            }
        ]);

        tableExit.onPageAdded(function (tb) {
            tb.addHeader();
        });
        tableExit.addBody(jsonData.resignee);
    }
    /* Exit End */

    /* Amendment Start */
    if (jsonData.amdmntJoinee.length) {
        // PDF.addContent().fontSize(18).text(`No Records Found!`, 305, 100).fontSize(10);
        PDF.addPage();
        PDF.addContent().fontSize(18).text('Amendment of Joinees').fontSize(10)
        .text("Total Amendment:" + jsonData.amdmntJoinee.length, { align: "right" })
        .fontSize(5).fontSize(10);
        const tableAmendment = new PDFTable(PDF, { bottomMargin: 0 });
        tableAmendment.setColumnsDefaults({
            headerBorder: ['T', 'B'],
            border: ['T', 'B'],
            borderOpacity: 0.2,
            align: 'center',
            valign: 'center',
            lineGap: 0,
            headerPadding: [5, 0, 5, 0]
        });

        tableAmendment.addColumns([
            {
                id: 'Client_Name',
                header: 'CLIENT',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Name',
                header: 'EMP NAME',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Id',
                header: 'EMP ID',
                align: 'left',
                width: 70,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Billing_Amount',
                header: 'BILLING AMOUNT',
                align: 'left',
                width: 80,
                padding: [4, 0, 4, 0],
                valign: 'center',
                renderer: function (tb, data, draw) {
                    return "Rs " + formatAmount(data.Billing_Amount) + "/-";
                }
            },
            {
                id: 'CTC',
                header: 'CTC',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return "Rs " + formatAmount(data.CTC) + "/-";
                }
            },
            {
                id: 'DOJ',
                header: 'DOJ',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return moment(data.DOJ).utc().tz("Asia/Kolkata").format("DD-MMM-YYYY");
                }
            },
            {
                id: 'Skill',
                header: 'SKILL',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Sourced_By',
                header: 'SOURCED BY',
                align: 'left',
                width: 80,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Mode',
                header: 'EMP MODE',
                align: 'left',
                width: 70,
                padding: [3, 0, 3, 0],
                valign: 'center'
            }
        ]);

        tableAmendment.onPageAdded(function (tb) {
            tb.addHeader();
        });
        tableAmendment.addBody(jsonData.amdmntJoinee);
    }
    /* Amendment End */

    /* Permanent Start */
    if (jsonData.permanentJoinee.length) {
        // PDF.addContent().fontSize(18).text(`No Records Found!`, 305, 100).fontSize(10);
        PDF.addPage();
        PDF.addContent().fontSize(18).text('Permanent Joinees').fontSize(10)
        .text("Total Permanent Joinees:" + jsonData.permanentJoinee.length, { align: "right" })
        .fontSize(5).fontSize(10);
        const tablePermanent = new PDFTable(PDF, { bottomMargin: 0 });
        tablePermanent.setColumnsDefaults({
            headerBorder: ['T', 'B'],
            border: ['T', 'B'],
            borderOpacity: 0.2,
            align: 'center',
            valign: 'center',
            lineGap: 0,
            headerPadding: [5, 0, 5, 0]
        });

        tablePermanent.addColumns([
            {
                id: 'Client_Name',
                header: 'CLIENT',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Name',
                header: 'EMP NAME',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'CTC',
                header: 'CTC Offered by Client',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return "Rs " + formatAmount(data.CTC) + "/-";
                }
            },
            {
                id: 'DOJ',
                header: 'DOJ',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return moment(data.DOJ).utc().tz("Asia/Kolkata").format("DD-MMM-YYYY");
                }
            },
            {
                id: 'Skill',
                header: 'SKILL',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Sourced_By',
                header: 'SOURCED BY',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            }
        ]);

        tablePermanent.onPageAdded(function (tb) {
            tb.addHeader();
        });
        tablePermanent.addBody(jsonData.permanentJoinee);
    }
    /* Permanent End */

    /* Conversion Start */
    if (jsonData.conversion.length) {
        // PDF.addContent().fontSize(18).text(`No Records Found!`, 305, 100).fontSize(10);
        PDF.addPage();
        PDF.addContent().fontSize(18).text('Conversion').fontSize(10)
        .text("Total Conversion:" + jsonData.conversion.length, { align: "right", rightMargin: 40 })
        .fontSize(5).fontSize(10);
        const tableConversion = new PDFTable(PDF, { bottomMargin: 0 });
        tableConversion.setColumnsDefaults({
            headerBorder: ['T', 'B'],
            border: ['T', 'B'],
            borderOpacity: 0.2,
            align: 'center',
            valign: 'center',
            lineGap: 0,
            headerPadding: [5, 0, 5, 0]
        });

        tableConversion.addColumns([
            {
                id: 'Client_Name',
                header: 'CLIENT',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Employee_Name',
                header: 'EMP NAME',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'CTC',
                header: 'CTC Offered by Client',
                align: 'left',
                width: 150,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return "Rs "+formatAmount(data.CTC)+"/-";
                }
            },
            {
                id: 'DOJ',
                header: 'DOJ',
                align: 'left',
                width: 100,
                padding: [3, 0, 3, 0],
                valign: 'center',
                renderer: function (tb, data) {
                    return moment(data.DOJ).utc().tz("Asia/Kolkata").format("DD-MMM-YYYY");
                }
            },
            {
                id: 'Skill',
                header: 'SKILL',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            },
            {
                id: 'Sourced_By',
                header: 'SOURCED BY',
                align: 'left',
                width: 120,
                padding: [3, 0, 3, 0],
                valign: 'center'
            }
        ]);

        tableConversion.onPageAdded(function (tb) {
            tb.addHeader();
        });
        tableConversion.addBody(jsonData.conversion);
    }
    /* Conversion End */

    if (jsonData.joinee.length || jsonData.resignee.length || jsonData.amdmntJoinee.length || jsonData.permanentJoinee.length || jsonData.conversion.length) {
        var file = filePath + '/HrReportData2.pdf';
        let i;
        let end;
        const range = PDF.bufferedPageRange();
        for (i = range.start, end = range.start + range.count, range.start <= end; i < end; i++) {
            PDF.switchToPage(i);
            PDF.text(`Page ${i + 1} of ${range.count}`, 750, PDF.page.height - 25, {
                lineBreak: false
            });
        }
        PDF.end();
        PDF.pipe(fs.createWriteStream(file)).on('finish', function (err, success) {
            if (err) {
                console.log('\n>>>>>END<<<<<');
                callback(true, err);
            } else {
                console.log('\n>>>>>END<<<<<');
                callback(false, file);
            }
        });
    } else {
        console.log('\n>>>>>END<<<<<');
        callback(false, 'NOPDF');
    }
}

function sentMail(data) {
    // var transporter = nodemailer.createTransport({
    //     host: "smtp.gmail.com",
    //     port: 465,
    //     secure: true,
    //     service: 'gmail',
    //     auth: config.mail.auth
    // });

    var transporter = nodemailer.createTransport({
        host: "smtp.office365.com",
        port: 587,
        secure: false,
        service: "office365",
        auth: config.mail.auth
    });

    var mailOptions = {
        from: config.mail.from,
        to: config.mail.to,
        // cc: config.mail.cc,
        subject: 'Daily Onsite Emp Status',
    };
    if (data === 'NOPDF') {
        mailOptions.html = 'There is no activities today.';
    } else {
        mailOptions.html = 'PFA';
        mailOptions.attachments = [
            {
                filename: 'HrReportData' + moment.utc().tz("Asia/Kolkata").format("YYYYMMDD") + '.pdf',
                content: fs.createReadStream(data)
            }
        ];
    }

    console.log('\n> mailOptions ---> ', JSON.stringify(mailOptions));

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}